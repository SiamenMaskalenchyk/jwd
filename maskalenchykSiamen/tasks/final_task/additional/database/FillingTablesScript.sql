INSERT INTO role (role_name) VALUES ('admin');
INSERT INTO role (role_name) VALUES ('client');
INSERT INTO role (role_name) VALUES ('author');

INSERT INTO request_status (request_status) VALUES ('considering');
INSERT INTO request_status (request_status) VALUES ('accepted');
INSERT INTO request_status (request_status) VALUES ('denied');

INSERT INTO disciple (disciple_name) VALUES('economics');
INSERT INTO disciple (disciple_name) VALUES('programming');
INSERT INTO disciple (disciple_name) VALUES('social_sciences');

INSERT INTO user_order_status (user_order_status_name) VALUES('order_registered');
INSERT INTO user_order_status (user_order_status_name) VALUES('author_assigned');
INSERT INTO user_order_status (user_order_status_name) VALUES('waiting_for_prepay');
INSERT INTO user_order_status (user_order_status_name) VALUES('order_in_process');
INSERT INTO user_order_status (user_order_status_name) VALUES('waiting_for_payment');
INSERT INTO user_order_status (user_order_status_name) VALUES('order_is_ready');
INSERT INTO user_order_status (user_order_status_name) VALUES('correction_order');

INSERT INTO request_type (request_type) VALUES('purchase');
INSERT INTO request_type (request_type) VALUES('execution');

INSERT INTO type_of_paper (type_of_paper) VALUES('coursework');
INSERT INTO type_of_paper (type_of_paper) VALUES('essay');
INSERT INTO type_of_paper (type_of_paper) VALUES('lab_report');
INSERT INTO type_of_paper (type_of_paper) VALUES('presentation');
INSERT INTO type_of_paper (type_of_paper) VALUES('drawing');
INSERT INTO type_of_paper (type_of_paper) VALUES('control_work');
INSERT INTO type_of_paper (type_of_paper) VALUES('graduate_work');

INSERT INTO wallet(wallet_amount) VALUES(0);
INSERT INTO user_account (user_account_name,user_account_email,user_account_phone,user_account_password,user_account_registration_date,wallet_id,role_id) VALUES('admin','siamenmaskalenchyk@gmail.com','+375257167203','708a9c84b47404c5524405e5cbd910b8','2019-07-19',1,1);

INSERT INTO wallet(wallet_amount) VALUES(0);
INSERT INTO user_account (user_account_name,user_account_email,user_account_phone,user_account_password,user_account_registration_date,wallet_id,role_id) VALUES('author','author@gmail.com','+375257167208','708a9c84b47404c5524405e5cbd910b8','2019-07-19',2,3);

INSERT INTO wallet(wallet_amount) VALUES(0);
INSERT INTO user_account (user_account_name,user_account_email,user_account_phone,user_account_password,user_account_registration_date,wallet_id,role_id) VALUES('client','client@gmail.com','+375257167228','708a9c84b47404c5524405e5cbd910b8','2019-07-19',3,2);

INSERT INTO request_for_coursework (request_theme,request_deadline,request_description,request_registration_date,disciple_id,type_of_paper_id,request_type_id,user_client_id,user_author_id,request_status_id) VALUES('some theme','2020-09-17','test request2','2020-07-17',1,2,1,2,3,1);
INSERT INTO request_for_coursework (request_theme,request_deadline,request_description,request_registration_date,disciple_id,type_of_paper_id,request_type_id,user_client_id,user_author_id,request_status_id) VALUES('some theme','2020-09-17','test request2','2020-07-17',1,2,1,2,3,1);

INSERT INTO user_order_feedback (order_feedback_grade,order_feedback_comment) VALUES(5,'comment');
INSERT INTO user_order_feedback (order_feedback_grade,order_feedback_comment) VALUES(7,'comment');

INSERT INTO user_order(user_order_price,user_order_prepayment,user_order_description,request_for_coursework_id,order_feedback_id,user_order_status_id) VALUES(500,100,'test',1,1,1);
INSERT INTO user_order(user_order_price,user_order_prepayment,user_order_description,request_for_coursework_id,order_feedback_id,user_order_status_id) VALUES(700,100,'test',1,1,1);

INSERT INTO result_file(result_file,result_file_registration_date,result_file_grade,result_file_price,result_file_count_of_downloads,user_order_id) VALUES('','2020-07-17',5,70,0,1);
INSERT INTO result_file(result_file,result_file_registration_date,result_file_grade,result_file_price,result_file_count_of_downloads,user_order_id) VALUES('','2020-07-17',5,80,0,1);

INSERT INTO task_file (task_file, request_id) VALUES('',1);
INSERT INTO task_file (task_file, request_id) VALUES('',2);

