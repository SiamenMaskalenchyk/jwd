-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema education-helper_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema education-helper_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `education-helper_db` DEFAULT CHARACTER SET utf8 ;
USE `education-helper_db` ;

-- -----------------------------------------------------
-- Table `education-helper_db`.`wallet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `education-helper_db`.`wallet` (
  `id_wallet` INT NOT NULL AUTO_INCREMENT,
  `wallet_amount` DECIMAL(10,2) NULL DEFAULT 0.0,
  PRIMARY KEY (`id_wallet`),
  UNIQUE INDEX `id_wallet_UNIQUE` (`id_wallet` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `education-helper_db`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `education-helper_db`.`role` (
  `id_role` INT NOT NULL AUTO_INCREMENT,
  `role_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_role`),
  UNIQUE INDEX `id_role_UNIQUE` (`id_role` ASC) VISIBLE,
  UNIQUE INDEX `role_name_UNIQUE` (`role_name` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `education-helper_db`.`user_account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `education-helper_db`.`user_account` (
  `id_user_account` INT NOT NULL AUTO_INCREMENT,
  `user_account_name` VARCHAR(100) NOT NULL,
  `user_account_email` VARCHAR(100) NOT NULL,
  `user_account_phone` VARCHAR(20) NOT NULL,
  `user_account_password` VARCHAR(100) NOT NULL,
  `user_account_registration_date` TIMESTAMP(6) NOT NULL,
  `wallet_id` INT NOT NULL,
  `role_id` INT NOT NULL,
  PRIMARY KEY (`id_user_account`, `wallet_id`, `role_id`),
  UNIQUE INDEX `id_UNIQUE` (`id_user_account` ASC) VISIBLE,
  UNIQUE INDEX `user_accoun_email_UNIQUE` (`user_account_email` ASC) VISIBLE,
  UNIQUE INDEX `user_account_phone_UNIQUE` (`user_account_phone` ASC) VISIBLE,
  INDEX `fk_user_account_wallet_idx` (`wallet_id` ASC) VISIBLE,
  UNIQUE INDEX `user_accoun_name_UNIQUE` (`user_account_name` ASC) VISIBLE,
  INDEX `fk_user_account_role1_idx` (`role_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_account_wallet`
    FOREIGN KEY (`wallet_id`)
    REFERENCES `education-helper_db`.`wallet` (`id_wallet`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_account_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `education-helper_db`.`role` (`id_role`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `education-helper_db`.`disciple`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `education-helper_db`.`disciple` (
  `id_disciple` INT NOT NULL AUTO_INCREMENT,
  `disciple_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_disciple`),
  UNIQUE INDEX `id_disciple_UNIQUE` (`id_disciple` ASC) VISIBLE,
  UNIQUE INDEX `disciple_name_UNIQUE` (`disciple_name` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `education-helper_db`.`type_of_paper`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `education-helper_db`.`type_of_paper` (
  `id_type_of_paper` INT NOT NULL AUTO_INCREMENT,
  `type_of_paper` VARCHAR(90) NOT NULL,
  PRIMARY KEY (`id_type_of_paper`),
  UNIQUE INDEX `id_type_of_paper_UNIQUE` (`id_type_of_paper` ASC) VISIBLE,
  UNIQUE INDEX `type_of_paper_UNIQUE` (`type_of_paper` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `education-helper_db`.`request_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `education-helper_db`.`request_type` (
  `id_request_type` INT NOT NULL AUTO_INCREMENT,
  `request_type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_request_type`),
  UNIQUE INDEX `id_request_type_UNIQUE` (`id_request_type` ASC) VISIBLE,
  UNIQUE INDEX `request_type_UNIQUE` (`request_type` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `education-helper_db`.`request_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `education-helper_db`.`request_status` (
  `id_request_status` INT NOT NULL AUTO_INCREMENT,
  `request_status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_request_status`),
  UNIQUE INDEX `id_request_status_UNIQUE` (`id_request_status` ASC) VISIBLE,
  UNIQUE INDEX `request_status_UNIQUE` (`request_status` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `education-helper_db`.`request_for_coursework`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `education-helper_db`.`request_for_coursework` (
  `id_request` INT NOT NULL AUTO_INCREMENT,
  `request_theme` VARCHAR(45) NULL,
  `request_deadline` TIMESTAMP(6) NULL,
  `request_description` VARCHAR(500) NULL,
  `request_registration_date` TIMESTAMP(6) NOT NULL,
  `disciple_id` INT NOT NULL,
  `type_of_paper_id` INT NOT NULL,
  `request_type_id` INT NOT NULL,
  `user_client_id` INT NOT NULL,
  `user_author_id` INT NULL,
  `request_status_id` INT NOT NULL,
  UNIQUE INDEX `id_request_UNIQUE` (`id_request` ASC) VISIBLE,
  PRIMARY KEY (`id_request`, `disciple_id`, `type_of_paper_id`, `request_type_id`, `user_client_id`, `request_status_id`),
  INDEX `fk_request_disciple1_idx` (`disciple_id` ASC) VISIBLE,
  INDEX `fk_request_type_of_paper1_idx` (`type_of_paper_id` ASC) VISIBLE,
  INDEX `fk_request_request_type1_idx` (`request_type_id` ASC) VISIBLE,
  INDEX `fk_request_user_account1_idx` (`user_author_id` ASC) VISIBLE,
  INDEX `fk_request_for_coursework_request_status1_idx` (`request_status_id` ASC) VISIBLE,
  CONSTRAINT `fk_request_disciple1`
    FOREIGN KEY (`disciple_id`)
    REFERENCES `education-helper_db`.`disciple` (`id_disciple`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_request_type_of_paper1`
    FOREIGN KEY (`type_of_paper_id`)
    REFERENCES `education-helper_db`.`type_of_paper` (`id_type_of_paper`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_request_request_type1`
    FOREIGN KEY (`request_type_id`)
    REFERENCES `education-helper_db`.`request_type` (`id_request_type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_request_user_account1`
    FOREIGN KEY (`user_author_id`)
    REFERENCES `education-helper_db`.`user_account` (`id_user_account`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_request_for_coursework_request_status1`
    FOREIGN KEY (`request_status_id`)
    REFERENCES `education-helper_db`.`request_status` (`id_request_status`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `education-helper_db`.`task_file`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `education-helper_db`.`task_file` (
  `id_task_file` INT NOT NULL AUTO_INCREMENT,
  `task_file` MEDIUMBLOB NULL,
  `request_id` INT NOT NULL,
  PRIMARY KEY (`id_task_file`, `request_id`),
  INDEX `fk_task_file_request1_idx` (`request_id` ASC) VISIBLE,
  UNIQUE INDEX `id_task_file_UNIQUE` (`id_task_file` ASC) VISIBLE,
  CONSTRAINT `fk_task_file_request1`
    FOREIGN KEY (`request_id`)
    REFERENCES `education-helper_db`.`request_for_coursework` (`id_request`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `education-helper_db`.`user_order_feedback`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `education-helper_db`.`user_order_feedback` (
  `id_order_feedback` INT NOT NULL AUTO_INCREMENT,
  `order_feedback_grade` TINYINT(2) NULL,
  `order_feedback_comment` VARCHAR(500) NULL,
  PRIMARY KEY (`id_order_feedback`),
  UNIQUE INDEX `id_order_feedback_UNIQUE` (`id_order_feedback` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `education-helper_db`.`user_order_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `education-helper_db`.`user_order_status` (
  `id_user_order_status` INT NOT NULL AUTO_INCREMENT,
  `user_order_status_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_user_order_status`),
  UNIQUE INDEX `id_user_order_status_UNIQUE` (`id_user_order_status` ASC) VISIBLE,
  UNIQUE INDEX `user_order_status_name_UNIQUE` (`user_order_status_name` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `education-helper_db`.`user_order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `education-helper_db`.`user_order` (
  `id_user_order` INT NOT NULL AUTO_INCREMENT,
  `user_order_price` DECIMAL(10,2) NOT NULL,
  `user_order_prepayment` DECIMAL(10,2) NOT NULL,
  `user_order_description` VARCHAR(200) NULL,
  `request_for_coursework_id` INT NOT NULL,
  `order_feedback_id` INT NOT NULL,
  `user_order_status_id` INT NOT NULL,
  UNIQUE INDEX `id_user_order_UNIQUE` (`id_user_order` ASC) VISIBLE,
  PRIMARY KEY (`id_user_order`, `request_for_coursework_id`, `order_feedback_id`, `user_order_status_id`),
  INDEX `fk_user_order_request1_idx` (`request_for_coursework_id` ASC) VISIBLE,
  INDEX `fk_user_order_order_feedback1_idx` (`order_feedback_id` ASC) VISIBLE,
  INDEX `fk_user_order_user_order_status1_idx` (`user_order_status_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_order_request1`
    FOREIGN KEY (`request_for_coursework_id`)
    REFERENCES `education-helper_db`.`request_for_coursework` (`id_request`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_order_order_feedback1`
    FOREIGN KEY (`order_feedback_id`)
    REFERENCES `education-helper_db`.`user_order_feedback` (`id_order_feedback`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_order_user_order_status1`
    FOREIGN KEY (`user_order_status_id`)
    REFERENCES `education-helper_db`.`user_order_status` (`id_user_order_status`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `education-helper_db`.`result_file`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `education-helper_db`.`result_file` (
  `id_result_file` INT NOT NULL AUTO_INCREMENT,
  `result_file` MEDIUMBLOB NOT NULL,
  `result_file_registration_date` TIMESTAMP(6) NOT NULL,
  `result_file_grade` TINYINT(2) NULL,
  `result_file_price` DECIMAL(10,2) NULL,
  `result_file_count_of_downloads` INT NULL DEFAULT 0,
  `user_order_id` INT NOT NULL,
  PRIMARY KEY (`id_result_file`, `user_order_id`),
  UNIQUE INDEX `id_result_file_UNIQUE` (`id_result_file` ASC) VISIBLE,
  INDEX `fk_result_file_user_order1_idx` (`user_order_id` ASC) VISIBLE,
  CONSTRAINT `fk_result_file_user_order1`
    FOREIGN KEY (`user_order_id`)
    REFERENCES `education-helper_db`.`user_order` (`id_user_order`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
