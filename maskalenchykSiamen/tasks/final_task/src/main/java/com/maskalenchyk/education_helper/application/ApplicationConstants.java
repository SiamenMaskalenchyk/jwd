package com.maskalenchyk.education_helper.application;

public class ApplicationConstants {
    public static final String DB_PROPERTY_RESOURCE_FILE = "/mysql_connection.properties";
    public static final String EMAIL_SERVICE_RESOURCE_FILE = "/mailService.properties";

    public static final String COMMAND_NAME_PARAMETER = "cmd";
    public static final String USER_ATTRIBUTE = "user";
    public static final String USER_ROLE_ATTRIBUTE = "userRole";

    public static final String LOGIN_PARAMETER = "username";
    public static final String ERROR_LOGIN = "errorLogin";

    public static final String REQUEST_PHONE_NUMBER_PARAMETER = "phoneNumber";
    public static final String REQUEST_EMAIL_PARAMETER = "email";
    public static final String REQUEST_THEME_PARAMETER = "theme";
    public static final String REQUEST_DISCIPLE_PARAMETER = "disciple";
    public static final String REQUEST_TYPE_OF_PAPER_PARAMETER = "typeOfPaper";
    public static final String REQUEST_DEADLINE_PARAMETER = "deadline";
    public static final String REQUEST_DESCRIPTION_PARAMETER = "description";
    public static final String REQUEST_TASK_FILES = "files";
//    public static final String REQUEST_

    public static final String MODAL_TITLE_PARAMETER = "modalTitle";
    public static final String MODAL_TEXT_PARAMETER = "modalText";

    public static final String LOCALE_PARAMETER = "locale";

    public static final String ERROR_TITLE = "errorTitle";
    public static final String ERROR_MESSAGE = "errorMessage";

    private ApplicationConstants() {
    }
}
