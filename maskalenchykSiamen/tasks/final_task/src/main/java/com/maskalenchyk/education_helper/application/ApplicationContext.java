package com.maskalenchyk.education_helper.application;

import com.maskalenchyk.education_helper.command.CreateRequestForCourseworkCommand;
import com.maskalenchyk.education_helper.command.LoginCommand;
import com.maskalenchyk.education_helper.command.LogoutCommand;
import com.maskalenchyk.education_helper.command.view.ContactsCommand;
import com.maskalenchyk.education_helper.command.view.CompleteWorkPageCommand;
import com.maskalenchyk.education_helper.command.view.HomePageCommand;
import com.maskalenchyk.education_helper.command.view.ShowCabinetCommand;
import com.maskalenchyk.education_helper.conrtoller.ControllerServlet;
import com.maskalenchyk.education_helper.core.BeanRegistry;
import com.maskalenchyk.education_helper.core.BeanRegistryImpl;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManagerImpl;
import com.maskalenchyk.education_helper.dal.connection_pool.ConnectionPool;
import com.maskalenchyk.education_helper.dal.connection_pool.ConnectionPoolImpl;
import com.maskalenchyk.education_helper.dal.dao.RequestForCourseworkDaoImpl;
import com.maskalenchyk.education_helper.dal.dao.TaskFileDaoImpl;
import com.maskalenchyk.education_helper.dal.dao.UserAccountDaoImpl;
import com.maskalenchyk.education_helper.dal.dao.WalletDaoImpl;
import com.maskalenchyk.education_helper.dal.transaction_manager.TransactionInterceptor;
import com.maskalenchyk.education_helper.dal.transaction_manager.TransactionManagerImpl;
import com.maskalenchyk.education_helper.service.*;
import org.apache.log4j.Logger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ApplicationContext implements BeanRegistry {

    private static final Logger LOGGER = Logger.getLogger(ApplicationContext.class);
    private static final Lock LOCK = new ReentrantLock();
    private static ApplicationContext instance;

    private BeanRegistry beanRegistry = new BeanRegistryImpl();

    private ApplicationContext() {
    }

    public static void initialize() {
        LOCK.lock();
        try {
            if (instance == null) {
                ApplicationContext applicationContext = new ApplicationContext();
                applicationContext.init();
                instance = applicationContext;
                LOGGER.info("Context initialized successful");
            } else {
                LOGGER.error("Context init error. Context already initialized");
                throw new ApplicationContextException("Context already initialized");
            }
        } finally {
            LOCK.unlock();
        }
    }

    public static ApplicationContext getInstance() {
        if (instance == null) {
            throw new ApplicationContextException("Context wasn't initialized");
        } else {
            return instance;
        }
    }

    @Override
    public <T> void registerBean(T bean) {
        this.beanRegistry.registerBean(bean);
    }

    @Override
    public <T> void registerBean(Class<T> beanClass) {
        this.beanRegistry.registerBean(beanClass);
    }

    @Override
    public <T> T getBean(String name) {
        return this.beanRegistry.getBean(name);
    }

    @Override
    public <T> T getBean(Class<T> beanClass) {
        return this.beanRegistry.getBean(beanClass);
    }

    @Override
    public <T> boolean removeBean(T bean) {
        return this.beanRegistry.removeBean(bean);
    }

    @Override
    public void destroy() {
        ApplicationContext context = getInstance();
        ConnectionPool dataSource = context.getBean(ConnectionPool.class);
        dataSource.close();
        beanRegistry.destroy();
        LOGGER.info("Context destroyed");
    }

    private void init() {
        registerDataSource();
        registerClasses();
    }

    private void registerClasses() {
        registerBean(UserAccountDaoImpl.class);
        registerBean(WalletDaoImpl.class);
        registerBean(TaskFileDaoImpl.class);
        registerBean(RequestForCourseworkDaoImpl.class);

        registerBean(PasswordServiceImpl.class);
        registerBean(UserServiceImpl.class);
        registerBean(CourseworkRequestServiceImpl.class);
        registerBean(TaskFileServiceImpl.class);
        registerBean(EmailServiceImpl.class);

        registerBean(ControllerServlet.class);
        registerBean(HomePageCommand.class);
        registerBean(CompleteWorkPageCommand.class);
        registerBean(ContactsCommand.class);
        registerBean(ShowCabinetCommand.class);
        registerBean(CreateRequestForCourseworkCommand.class);
        registerBean(LogoutCommand.class);
        registerBean(LoginCommand.class);
    }

    private void registerDataSource() {
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        registerBean(connectionPool);
        registerBean(TransactionManagerImpl.class);
        registerBean(ConnectionManagerImpl.class);
        registerBean(TransactionInterceptor.class);
    }

}
