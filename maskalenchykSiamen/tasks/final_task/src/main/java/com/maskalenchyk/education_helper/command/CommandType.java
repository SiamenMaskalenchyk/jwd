package com.maskalenchyk.education_helper.command;

import java.util.Optional;
import java.util.stream.Stream;

public enum CommandType {

    VIEW_HOME_PAGE,
    COMPLETE_WORKS,
    SHOW_CABINET,
    CONTACTS,
    SIGN_IN,
    LOGOUT,
    CREATE_NEW_ACCOUNT,
    CREATE_NEW_COURSEWORK_REQUEST,
    DELETE_COURSEWORK_REQUEST,
    DELETE_USER,
    TOP_UP_BALANCE;

    public static Optional<CommandType> of(String name) {
        return Stream.of(CommandType.values()).filter(type -> type.name().equalsIgnoreCase(name)).findFirst();
    }
}
