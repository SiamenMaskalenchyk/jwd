package com.maskalenchyk.education_helper.command;

import com.maskalenchyk.education_helper.command.security.AccessLevel;
import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.entity.UserAccount;
import com.maskalenchyk.education_helper.entity.UserRole;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;
import com.maskalenchyk.education_helper.service.UserService;
import com.maskalenchyk.education_helper.service.exceptions.UserServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@Bean(name = "CREATE_NEW_ACCOUNT")
@AccessLevel(roles = UserRole.ADMIN)
public class CreateNewUserAccountCommand extends AbstractCommand {

    private static final Logger LOGGER = Logger.getLogger(CreateNewUserAccountCommand.class);
    private UserService userService;

    public CreateNewUserAccountCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    protected void executeWrapper(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String email = request.getParameter("email");
        String name = request.getParameter("userName");
        String phoneNumber = request.getParameter("phoneNumber");
        UserRole userRole = UserRole.valueOf(request.getParameter("userRole"));

        try {
            HttpSession session = request.getSession();
            UserAccount userAccount = userService.createUserAccount(email, phoneNumber, userRole);
            session.setAttribute("user", userAccount);

        } catch (UserServiceException e) {
            LOGGER.error("Registration failed for " + email + ", " + phoneNumber);
            String errorMessage;
            switch (e.getErrorCode()) {
                case UserServiceException.EMAIL_EXISTS:
                    errorMessage = "User with email " + email + " already exists";
                    break;
                case UserServiceException.PHONE_NUMBER_EXISTS:
                    errorMessage = "User with phone number " + phoneNumber + " already exists";
                    break;
                default:
                    errorMessage = "Registration failed for" + email + ", " + phoneNumber + ". Incorrect input. Please check input date";
                    break;
            }
            LOGGER.error(errorMessage, e);
            //message about fail

        } catch (ServiceException e) {
            LOGGER.error(e);
            throw new CommandException(e);
        }

    }

}
