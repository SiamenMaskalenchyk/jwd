package com.maskalenchyk.education_helper.command;

import com.maskalenchyk.education_helper.application.ApplicationConstants;
import com.maskalenchyk.education_helper.command.security.AccessLevel;
import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.entity.*;
import com.maskalenchyk.education_helper.service.CourseworkRequestService;
import com.maskalenchyk.education_helper.service.UserService;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Bean(name = "CREATE_NEW_COURSEWORK_REQUEST")
@AccessLevel(roles = {UserRole.ADMIN, UserRole.AUTHOR, UserRole.CLIENT})
public class CreateRequestForCourseworkCommand extends AbstractCommand {

    private CourseworkRequestService courseworkRequestService;
    private UserService userService;

    public CreateRequestForCourseworkCommand(CourseworkRequestService courseworkRequestService, UserService userService) {
        this.courseworkRequestService = courseworkRequestService;
        this.userService = userService;
    }

    @Override
    protected void executeWrapper(HttpServletRequest request, HttpServletResponse response) throws CommandException {
//        try {
//            List<Part> filesPart = request.getParts().stream()
//                    .filter(p -> p.getName().equals(ApplicationConstants.REQUEST_TASK_FILES))
//                    .collect(Collectors.toList());
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (ServletException e) {
//            e.printStackTrace();
//        }
        HttpSession session = request.getSession();
        UserAccount userAccount = (UserAccount) session.getAttribute(ApplicationConstants.USER_ATTRIBUTE);
        RequestForCoursework requestForCoursework = userAccount == null ? buildRequestForNewUser(request)
            : buildRequestForExistUser(request, userAccount);
        //сервисы в сервисы загнать

//        try {
//            RequestForCoursework requestForCoursework = requestForCourseworkBuilderFromRequest(request);
//            courseworkRequestService.addCourseworkRequest(requestForCoursework);
//            //response
//        } catch (ServiceException e) {
//            throw new CommandException("Adding coursework request exception", e);
//        }
    }

    private RequestForCoursework buildRequestForExistUser(HttpServletRequest request, UserAccount userAccount)
        throws CommandException {
        try {
            RequestForCoursework requestForCoursework = requestForCourseworkBuilderFromRequest(request);
            requestForCoursework.setClient(userAccount);
            return courseworkRequestService.addCourseworkRequest(requestForCoursework);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
    }

    private RequestForCoursework buildRequestForNewUser(HttpServletRequest request) throws CommandException {
        String email = request.getParameter(ApplicationConstants.REQUEST_EMAIL_PARAMETER);
        String phoneNumber = request.getParameter(ApplicationConstants.REQUEST_PHONE_NUMBER_PARAMETER);
        try {
            UserAccount client = userService.createUserAccount(email, phoneNumber, UserRole.CLIENT);
            RequestForCoursework requestForCoursework = requestForCourseworkBuilderFromRequest(request);
            requestForCoursework.setClient(client);
            return courseworkRequestService.addCourseworkRequestWithNewUser(requestForCoursework, email, phoneNumber);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
    }


    private RequestForCoursework requestForCourseworkBuilderFromRequest(HttpServletRequest request) throws CommandException {
        String theme = request.getParameter(ApplicationConstants.REQUEST_THEME_PARAMETER);
        Disciple disciple = Disciple.valueOf(request.getParameter(ApplicationConstants.REQUEST_DISCIPLE_PARAMETER));
        PaperType paperType = PaperType.valueOf(request.getParameter(ApplicationConstants.REQUEST_TYPE_OF_PAPER_PARAMETER));
        String description = request.getParameter(ApplicationConstants.REQUEST_DESCRIPTION_PARAMETER);
        Long deadLine = getDeadLineFromRequest(request);
        Long registrationDate = System.currentTimeMillis();

        List<TaskFile> taskFileList = taskFileListBuilderFromRequest(request);

        RequestForCoursework requestForCoursework = new RequestForCoursework();
        requestForCoursework.setTheme(theme);
        requestForCoursework.setRequestType(RequestType.EXECUTION);
        requestForCoursework.setPaperType(paperType);
        requestForCoursework.setRequestStatus(RequestStatus.CONSIDERING);
        requestForCoursework.setDeadline(deadLine);
        requestForCoursework.setRegistrationDate(registrationDate);
        requestForCoursework.setDisciple(disciple);
        if (description != null) {
            requestForCoursework.setDescription(description);
        }
        return requestForCoursework;
    }

    private List<TaskFile> taskFileListBuilderFromRequest(HttpServletRequest request) throws CommandException {
        try {
            List<Part> filesPart = request.getParts().stream()
                .filter(p -> p.getName().equals(ApplicationConstants.REQUEST_TASK_FILES))
                .collect(Collectors.toList());
            return null;

        } catch (IOException | ServletException e) {
            throw new CommandException("Upload file exception ", e);
        }
    }

    private Long getDeadLineFromRequest(HttpServletRequest request) throws CommandException {
        try {
            String dateString = request.getParameter(ApplicationConstants.REQUEST_DEADLINE_PARAMETER);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date deadLineDate = dateFormat.parse(dateString);
            return deadLineDate.getTime();
        } catch (ParseException e) {
            throw new CommandException("Incorrect date", e);
        }
    }
}
