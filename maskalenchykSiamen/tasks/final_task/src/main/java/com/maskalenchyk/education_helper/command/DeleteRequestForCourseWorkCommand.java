package com.maskalenchyk.education_helper.command;

import com.maskalenchyk.education_helper.command.security.AccessLevel;
import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.entity.UserRole;
import com.maskalenchyk.education_helper.service.CourseworkRequestService;
import com.maskalenchyk.education_helper.service.exceptions.CourseworkRequestException;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Bean(name = "DELETE_COURSEWORK_REQUEST")
@AccessLevel(roles = UserRole.ADMIN)
public class DeleteRequestForCourseWorkCommand extends AbstractCommand {

    private CourseworkRequestService courseworkRequestService;

    public DeleteRequestForCourseWorkCommand(CourseworkRequestService courseworkRequestService) {
        this.courseworkRequestService = courseworkRequestService;
    }

    @Override
    protected void executeWrapper(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            Integer courseworkRequestId = Integer.parseInt(request.getParameter("courseworkRequestId"));
            courseworkRequestService.deleteCourseworkRequest(courseworkRequestId);
//        response
        } catch (CourseworkRequestException e) {
// error response
        } catch (ServiceException e) {
            throw new CommandException("Coursework request deleting exception", e);
        }
    }
}
