package com.maskalenchyk.education_helper.command;

import com.maskalenchyk.education_helper.command.security.AccessLevel;
import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.entity.UserRole;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;
import com.maskalenchyk.education_helper.service.UserService;
import com.maskalenchyk.education_helper.service.exceptions.UserServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Bean(name = "DELETE_USER")
@AccessLevel(roles = UserRole.ADMIN)
public class DeleteUserCommand extends AbstractCommand {

    private static final Logger LOGGER = Logger.getLogger(DeleteUserCommand.class);
    private UserService userService;

    public DeleteUserCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    protected void executeWrapper(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            String id = request.getParameter("userId");
            userService.deleteById(Integer.parseInt(id));
            //getMessage
        } catch (UserServiceException e) {

        } catch (ServiceException e) {

            e.printStackTrace();
        }

    }
}
