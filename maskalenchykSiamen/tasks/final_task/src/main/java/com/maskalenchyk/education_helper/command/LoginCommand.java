package com.maskalenchyk.education_helper.command;

import com.maskalenchyk.education_helper.application.ApplicationConstants;
import com.maskalenchyk.education_helper.application.ApplicationUtils;
import com.maskalenchyk.education_helper.command.security.AccessLevel;
import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.entity.UserAccount;
import com.maskalenchyk.education_helper.entity.UserRole;
import com.maskalenchyk.education_helper.service.UserService;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;
import com.maskalenchyk.education_helper.service.exceptions.UserServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Bean(name = "SIGN_IN")
@AccessLevel(roles = {UserRole.ADMIN, UserRole.AUTHOR, UserRole.CLIENT})
public class LoginCommand extends AbstractCommand {

    private static final Logger LOGGER = Logger.getLogger(LoginCommand.class);
    private static final String PASSWORD_PARAMETER = "password";

    private UserService userService;

    public LoginCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    protected void executeWrapper(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String login = request.getParameter(ApplicationConstants.LOGIN_PARAMETER);
        String password = request.getParameter(PASSWORD_PARAMETER);
        if (ApplicationUtils.isMobilePhoneNumber(login) || ApplicationUtils.isValidEmailAddress(login)) {
            try {
                HttpSession session = request.getSession();
                UserAccount user = (UserAccount) session.getAttribute(ApplicationConstants.USER_ATTRIBUTE);
                if (user == null) {
                    user = userService.signIn(login, password);
                    session.setAttribute(ApplicationConstants.USER_ATTRIBUTE, user);
                    request.setAttribute(ApplicationConstants.MODAL_TITLE_PARAMETER, "Signed in successfully");
                    request.setAttribute(ApplicationConstants.MODAL_TEXT_PARAMETER, "Welcome to education-helper service");
                    request.setAttribute(ApplicationConstants.ERROR_LOGIN, null);
                } else {
                    String errorMessage = "User already signed in";
                    LOGGER.debug(errorMessage);
                    request.setAttribute(ApplicationConstants.ERROR_LOGIN, errorMessage);
                }
            } catch (UserServiceException e) {
                String errorMessage = "Authorization failed for " + login + ". " + e.getMessage();
                LOGGER.debug(errorMessage);
                request.setAttribute(ApplicationConstants.ERROR_LOGIN, errorMessage);
            } catch (ServiceException e) {
                LOGGER.error(e);
                throw new CommandException(e);
            }
        } else {
            String errorMessage = "Incorrect login, please check data and try again";
            LOGGER.debug("Sign in failed - incorrect login: " + login);
            request.setAttribute(ApplicationConstants.ERROR_LOGIN, errorMessage);
        }
        request.setAttribute(ApplicationConstants.LOGIN_PARAMETER, login);
        forward(request, response, "/homepage");
    }
}
