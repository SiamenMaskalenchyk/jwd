package com.maskalenchyk.education_helper.command;

import com.maskalenchyk.education_helper.application.ApplicationConstants;
import com.maskalenchyk.education_helper.command.security.AccessLevel;
import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.entity.UserRole;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Bean(name = "LOGOUT")
@AccessLevel(roles = {UserRole.ADMIN, UserRole.AUTHOR, UserRole.CLIENT})
public class LogoutCommand extends AbstractCommand {

    private static final Logger LOGGER = Logger.getLogger(LogoutCommand.class);

    @Override
    protected void executeWrapper(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        request.getSession().removeAttribute(ApplicationConstants.USER_ATTRIBUTE);
        LOGGER.info("User logged out");
        request.getSession().invalidate();
        redirect(response, "/homepage");
    }
}
