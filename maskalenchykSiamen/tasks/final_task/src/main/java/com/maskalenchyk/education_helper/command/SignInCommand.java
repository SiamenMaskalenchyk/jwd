package com.maskalenchyk.education_helper.command;

import com.maskalenchyk.education_helper.application.ApplicationConstants;
import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.entity.UserAccount;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;
import com.maskalenchyk.education_helper.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Bean(name = "SIGN_IN")
public class SignInCommand extends AbstractCommand {

    private static final Logger LOGGER = Logger.getLogger(SignInCommand.class);
    private UserService userService;

    public SignInCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    protected void executeWrapper(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        try {
            String enteredLogin = request.getParameter("login");
            String enteredPassword = request.getParameter("pass");
            UserAccount userAccount = userService.getByLogin(enteredLogin);
            if (userAccount != null && enteredPassword.equals(userAccount.getPassword())) {
                HttpSession session = request.getSession();
                session.setAttribute(ApplicationConstants.USER_ATTRIBUTE, userAccount);
                session.setAttribute(ApplicationConstants.USER_ROLE_ATTRIBUTE,userAccount.getUserRole());
                forward(request, response, "main-page.jsp");
            } else {
                request.setAttribute("ERROR_MESSAGE", "Entered login or password is incorrect");
                forward(request, response, "error.jsp");
            }
        } catch (ServiceException e) {
            LOGGER.error(e);
            throw new CommandException(e);
        }

    }
}
