package com.maskalenchyk.education_helper.command.security;

import com.maskalenchyk.education_helper.entity.UserRole;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AccessLevel {
public UserRole[] roles();
}
