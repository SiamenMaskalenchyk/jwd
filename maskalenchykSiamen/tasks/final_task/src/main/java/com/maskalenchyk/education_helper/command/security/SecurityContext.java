package com.maskalenchyk.education_helper.command.security;

import com.maskalenchyk.education_helper.entity.UserAccount;

public interface SecurityContext {

    boolean canExecute(String commandName);

    boolean canExecute(UserAccount userAccount, String commandName);

    void setCurrentSessionId(String sessionId);

    void login(UserAccount userAccount, String sessionId);

   }
