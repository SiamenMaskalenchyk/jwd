package com.maskalenchyk.education_helper.command.security;

import com.maskalenchyk.education_helper.application.ApplicationContext;
import com.maskalenchyk.education_helper.entity.UserAccount;
import com.maskalenchyk.education_helper.entity.UserRole;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

public class SecurityContextImpl implements SecurityContext {

    private static final SecurityContext SECURITY_CONTEXT = new SecurityContextImpl();
    private final Map<String, UserAccount> userAccountMap = new ConcurrentHashMap<>(500);
    private final ThreadLocal<String> currentSessionId = new ThreadLocal<>();

    private SecurityContextImpl() {
    }

    public static SecurityContext getInstance() {
        return SECURITY_CONTEXT;
    }

    @Override
    public boolean canExecute(String commandName) {
        UserAccount currentUserAccount = getCurrentUser();
        return canExecute(currentUserAccount, commandName);

    }

    @Override
    public boolean canExecute(UserAccount userAccount, String commandName) {
        Class<?> commandClass = ApplicationContext.getInstance().getBean(commandName);
        AccessLevel accessLevel = commandClass.getAnnotation(AccessLevel.class);
        if (accessLevel != null) {
            UserRole userAccountRole = userAccount.getUserRole();
            Optional<UserRole> userRole = Stream.of(accessLevel.roles()).filter(role -> role.equals(userAccountRole)).findFirst();
            return userRole.isPresent();
        }
        return true;
    }

    @Override
    public void setCurrentSessionId(String sessionId) {
        currentSessionId.set(sessionId);
    }

    @Override
    public void login(UserAccount userAccount, String sessionId) {
        userAccountMap.put(sessionId, userAccount);
    }

    public String getCurrentSessionId() {
        return currentSessionId.get();
    }

    public UserAccount getCurrentUser() {
        String currentSessionID = getCurrentSessionId();
        return currentSessionID != null ? userAccountMap.get(currentSessionID) : null;
    }
}
