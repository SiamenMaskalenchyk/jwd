package com.maskalenchyk.education_helper.command.user;

import com.maskalenchyk.education_helper.application.ApplicationConstants;
import com.maskalenchyk.education_helper.command.AbstractCommand;
import com.maskalenchyk.education_helper.command.CommandException;
import com.maskalenchyk.education_helper.command.security.AccessLevel;
import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.entity.UserAccount;
import com.maskalenchyk.education_helper.entity.UserRole;
import com.maskalenchyk.education_helper.entity.Wallet;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Bean(name = "TOP_UP_BALANCE")
@AccessLevel(roles = {UserRole.AUTHOR, UserRole.CLIENT})
public class TopUpBalanceCommand extends AbstractCommand {

    private static final Logger LOGGER = Logger.getLogger(TopUpBalanceCommand.class);

    @Override
    protected void executeWrapper(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        UserAccount userAccount = (UserAccount) request.getSession().getAttribute(ApplicationConstants.USER_ATTRIBUTE);
        if(userAccount!=null){
            Wallet wallet = userAccount.getWallet();

        } else {
            request.setAttribute(ApplicationConstants.ERROR_MESSAGE,"Balance top up unavailable. User isn't sign in");
            redirect(response,"error-page.jsp");
            String errorMessage = "Balance top p unavailable. User isn't sign in";
//            LOGGER.error();
        }
    }
}
