package com.maskalenchyk.education_helper.command.view;

import com.maskalenchyk.education_helper.command.AbstractCommand;
import com.maskalenchyk.education_helper.command.security.AccessLevel;
import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.entity.UserRole;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Returns to client page with complete works
 */

@Bean(name = "COMPLETE_WORKS")
@AccessLevel(roles = {UserRole.ADMIN, UserRole.AUTHOR, UserRole.CLIENT})
public class CompleteWorkPageCommand extends AbstractCommand {

    @Override
    protected void executeWrapper(HttpServletRequest request, HttpServletResponse response) {
        forward(request, response, "/complete");
    }
}
