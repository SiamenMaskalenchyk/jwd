package com.maskalenchyk.education_helper.command.view;

import com.maskalenchyk.education_helper.command.AbstractCommand;
import com.maskalenchyk.education_helper.command.security.AccessLevel;
import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.entity.UserRole;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Returns to client home page
 */
@Bean(name = "VIEW_HOME_PAGE")
@AccessLevel(roles = {UserRole.AUTHOR, UserRole.CLIENT, UserRole.ADMIN})
public class HomePageCommand extends AbstractCommand {

    @Override
    public void executeWrapper(HttpServletRequest request, HttpServletResponse response) {
        forward(request, response, "/homepage");
    }


}

