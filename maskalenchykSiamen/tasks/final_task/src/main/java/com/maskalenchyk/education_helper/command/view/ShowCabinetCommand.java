package com.maskalenchyk.education_helper.command.view;

import com.maskalenchyk.education_helper.application.ApplicationConstants;
import com.maskalenchyk.education_helper.command.AbstractCommand;
import com.maskalenchyk.education_helper.command.CommandException;
import com.maskalenchyk.education_helper.command.security.AccessLevel;
import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.entity.UserAccount;
import com.maskalenchyk.education_helper.entity.UserRole;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Shows user cabinet
 * If session user is null show error page
 */
@Bean(name = "SHOW_CABINET")
@AccessLevel(roles = {UserRole.AUTHOR, UserRole.CLIENT, UserRole.ADMIN})
public class ShowCabinetCommand extends AbstractCommand {
    @Override
    protected void executeWrapper(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        UserAccount currentUser = (UserAccount) session.getAttribute(ApplicationConstants.USER_ATTRIBUTE);
        if (currentUser != null) {
            switch (currentUser.getUserRole()) {
                case ADMIN:
                    forward(request, response, "/cabinet-admin");
                    break;
                case AUTHOR:
                    forward(request, response, "/cabinet-author");
                    break;
                case CLIENT:
                    forward(request, response, "/cabinet-client");
                    break;
                default:
                    throw new CommandException("Unknown user role");
            }
        }else{
            request.setAttribute(ApplicationConstants.ERROR_MESSAGE, "Unauthorized user trying get access to cabinet");
            redirect(response, "/error.jsp");
        }
    }
}
