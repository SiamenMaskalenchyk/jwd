package com.maskalenchyk.education_helper.dal.connection_manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@FunctionalInterface
public interface ConnectionConsumer {

    void apply(PreparedStatement statement) throws ConnectionManagerException, SQLException;
}
