package com.maskalenchyk.education_helper.dal.connection_manager;

import com.maskalenchyk.education_helper.dal.dao.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@FunctionalInterface
public interface ConnectionFunction<R> {

    R apply(PreparedStatement statement) throws ConnectionManagerException, SQLException, DaoException;
}
