package com.maskalenchyk.education_helper.dal.connection_manager;

import com.maskalenchyk.education_helper.dal.dao.DaoException;

import java.sql.SQLException;

public interface ConnectionManager {

    <R> R doWithConnection(String query, ConnectionFunction<R> connectionFunction) throws ConnectionManagerException, SQLException, DaoException;

    void doWithConnection(String query, ConnectionConsumer connectionConsumer) throws ConnectionManagerException, SQLException;
}
