package com.maskalenchyk.education_helper.dal.connection_manager;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.connection_pool.ConnectionPool;
import com.maskalenchyk.education_helper.dal.connection_pool.ConnectionPoolException;
import com.maskalenchyk.education_helper.dal.dao.DaoException;
import com.maskalenchyk.education_helper.dal.transaction_manager.TransactionManager;
import com.maskalenchyk.education_helper.dal.transaction_manager.TransactionManagerException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Bean
public class ConnectionManagerImpl implements ConnectionManager {

    private static final Logger LOGGER = Logger.getLogger(ConnectionManagerImpl.class);
    private final Lock lock = new ReentrantLock();
    private ConnectionPool connectionPool;
    private TransactionManager transactionManager;


    public ConnectionManagerImpl(ConnectionPool connectionPool, TransactionManager transactionManager) {
        this.connectionPool = connectionPool;
        this.transactionManager = transactionManager;
    }

    @Override
    public <R> R doWithConnection(String query, ConnectionFunction<R> connectionFunction) throws ConnectionManagerException, DaoException {
        lock.lock();
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            return connectionFunction.apply(statement);
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ConnectionManagerException(e);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void doWithConnection(String query, ConnectionConsumer connectionConsumer) throws ConnectionManagerException {
        lock.lock();
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            connectionConsumer.apply(statement);
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ConnectionManagerException(e);
        } finally {
            lock.unlock();
        }
    }

    private Connection getConnection() throws ConnectionManagerException {
        try {
            Connection connection = transactionManager.getConnection();
            return connection != null ? connection : connectionPool.getConnection();
        } catch (ConnectionPoolException | TransactionManagerException e) {
            String errorMessage = "Getting connection exception";
            LOGGER.error(errorMessage, e);
            throw new ConnectionManagerException(errorMessage, e);
        }
    }
}
