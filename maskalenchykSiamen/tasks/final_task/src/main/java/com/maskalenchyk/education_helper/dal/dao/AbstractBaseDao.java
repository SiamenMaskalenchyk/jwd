package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.core.AbstractSuperclass;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManagerException;
import com.maskalenchyk.education_helper.entity.Entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@AbstractSuperclass
abstract class AbstractBaseDao<T extends Entity> {

    ConnectionManager connectionManager;

    AbstractBaseDao(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    protected abstract T parseResultSet(ResultSet resultSet) throws SQLException, DaoException;

    List<T> executeStatementAndParseResultSetToList(PreparedStatement statement) throws SQLException, DaoException {
        List<T> entityList = new ArrayList<>();
        try (ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                entityList.add(parseResultSet(resultSet));
            }

        }
        return entityList;
    }

    T executeStatementAndParseResultSet(PreparedStatement statement) throws SQLException, DaoException {
        try (ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                return parseResultSet(resultSet);
            } else {
                return null;
            }
        }
    }

    void executeDelete(String query, Integer id) throws DaoException {
        try {
            connectionManager.doWithConnection(query, (statement) -> {
                try {
                    statement.setInt(1, id);
                    statement.executeUpdate();
                } catch (SQLException e) {
                    throw new ConnectionManagerException(e);
                }
            });
        } catch (ConnectionManagerException | SQLException e) {
            throw new DaoException(e);
        }
    }

    T executeSelectById(String query, Integer id) throws DaoException {
        try {
            return connectionManager.doWithConnection(query, (statement) -> {
                statement.setInt(1, id);
                return executeStatementAndParseResultSet(statement);
            });
        } catch (ConnectionManagerException | SQLException e) {
            throw new DaoException(e);
        }
    }

    List<T> executeAndReturnEntityList(String query) throws DaoException {
        try {
            return connectionManager.doWithConnection(query, this::executeStatementAndParseResultSetToList);
        } catch (ConnectionManagerException | SQLException e) {
            throw new DaoException(e);
        }
    }

    Integer getInsertedId(PreparedStatement statement) throws DaoException {
        try (ResultSet keys = statement.getGeneratedKeys()) {
            if (keys.next()) {
                return keys.getInt(1);
            } else {
                throw new DaoException("Generated keys are empty");
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

}
