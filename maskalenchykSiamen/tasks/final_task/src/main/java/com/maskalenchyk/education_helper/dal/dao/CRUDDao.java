package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.entity.Entity;

import java.util.List;

public interface CRUDDao<T extends Entity> {

    T save(T entity) throws DaoException;

    void update(T entity) throws DaoException;

    void delete(T entity) throws DaoException;

    T findById(Integer id) throws DaoException;

    List<T> findAll() throws DaoException;
}
