package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.entity.Disciple;

public interface DiscipleDao extends CRUDDao<Disciple> {
}
