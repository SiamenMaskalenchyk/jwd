package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.entity.Disciple;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Bean
public class DiscipleDaoImpl extends AbstractBaseDao<Disciple> implements DiscipleDao {

    private static final String QUERY_SELECT_BY_ID = "SELECT id_disciple, disciple_name FROM disciple WHERE id_disciple=?;";
    private static final String QUERY_SELECT_ALL = "SELECT id_disciple, disciple_name FROM disciple;";

    public DiscipleDaoImpl(ConnectionManager connectionManager) {
        super(connectionManager);
    }

    @Override
    public Disciple save(Disciple entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Disciple entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Disciple entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Disciple findById(Integer id) throws DaoException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public List<Disciple> findAll() throws DaoException {
        return executeAndReturnEntityList(QUERY_SELECT_ALL);
    }

    @Override
    protected Disciple parseResultSet(ResultSet resultSet) throws SQLException {
        String disciple = resultSet.getString("disciple_name").toUpperCase();
        return Disciple.valueOf(disciple);
    }
}
