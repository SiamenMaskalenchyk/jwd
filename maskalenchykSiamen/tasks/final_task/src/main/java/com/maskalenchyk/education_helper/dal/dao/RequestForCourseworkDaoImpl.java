package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManagerException;
import com.maskalenchyk.education_helper.entity.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

@Bean
public class RequestForCourseworkDaoImpl extends AbstractBaseDao<RequestForCoursework> implements RequestForCourseworkDao {

    private static final String QUERY_SELECT_ALL = "SELECT req.id_request,req.request_theme,req.request_deadline,req.request_description,req.request_registration_date,req.user_client_id,req.user_author_id, " +
            "disciple.disciple_name, " +
            "type_of_paper.type_of_paper, " +
            "request_type.request_type, " +
            "request_status.request_status " +
            "FROM request_for_coursework AS req " +
            "JOIN disciple ON disciple.id_disciple=req.disciple_id  " +
            "JOIN type_of_paper ON type_of_paper.id_type_of_paper=req.type_of_paper_id " +
            "JOIN request_type ON request_type.id_request_type=req.request_type_id " +
            "JOIN request_status ON request_status.id_request_status=req.request_status_id ";
    private static final String QUERY_SELECT_BY_ID = QUERY_SELECT_ALL + "WHERE req.id_request=1;";
    private static final String QUERY_INSERT = "INSERT INTO request_for_coursework (request_theme,request_deadline,request_description,request_registration_date,disciple_id,type_of_paper_id,request_type_id,user_client_id,request_status_id) " +
            "VALUES(?,?,?,?,?,?,?,?,?);";
    private static final String QUERY_DELETE = "DELETE FROM request_for_coursework WHERE id_request=?;";
    private static final String QUERY_UPDATE = "UPDATE request_for_coursework SET request_theme=?,request_deadline=?,request_description=?,request_registration_date=?,disciple_id=?,type_of_paper_id=?,request_type_id=?,user_client_id=?,user_author_id=?,request_status_id=? WHERE id_request=?;";

    public RequestForCourseworkDaoImpl(ConnectionManager connectionManager) {
        super(connectionManager);
    }

    @Override
    public RequestForCoursework save(RequestForCoursework entity) throws DaoException {
        try {
            return connectionManager.doWithConnection(QUERY_INSERT, statement -> {
                int i = 0;
                Timestamp deadline = new Timestamp(entity.getDeadline());
                Timestamp registrationDate = new Timestamp(entity.getRegistrationDate());
                statement.setString(++i, entity.getTheme());
                statement.setTimestamp(++i, deadline);
                statement.setString(++i, entity.getDescription());
                statement.setTimestamp(++i, registrationDate);
                statement.setInt(++i, entity.getDisciple().getId());
                statement.setInt(++i, entity.getPaperType().getId());
                statement.setInt(++i, entity.getRequestType().getId());
                statement.setInt(++i, entity.getClient().getId());
                statement.setInt(++i, entity.getRequestStatus().getId());
                statement.executeUpdate();
                entity.setId(getInsertedId(statement));
                return entity;
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void update(RequestForCoursework entity) throws DaoException {
        try {
            connectionManager.doWithConnection(QUERY_UPDATE, statement -> {
                int i = 0;
                Timestamp deadline = new Timestamp(entity.getDeadline());
                Timestamp registrationDate = new Timestamp(entity.getRegistrationDate());
                statement.setString(++i, entity.getTheme());
                statement.setTimestamp(++i, deadline);
                statement.setTimestamp(++i, registrationDate);
                statement.setInt(++i, entity.getDisciple().getId());
                statement.setInt(++i, entity.getPaperType().getId());
                statement.setInt(++i, entity.getRequestType().getId());
                statement.setInt(++i, entity.getClient().getId());
                statement.setInt(++i, entity.getAuthor().getId());
                statement.setInt(++i, entity.getRequestStatus().getId());
                statement.setInt(++i, entity.getId());
                statement.executeUpdate();
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void delete(RequestForCoursework entity) throws DaoException {
        executeDelete(QUERY_DELETE, entity.getId());
    }

    @Override
    public RequestForCoursework findById(Integer id) throws DaoException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public List<RequestForCoursework> findAll() throws DaoException {
        return executeAndReturnEntityList(QUERY_SELECT_ALL);
    }

    @Override
    protected RequestForCoursework parseResultSet(ResultSet resultSet) throws SQLException {
        Integer requestId = resultSet.getInt("id_request");
        String theme = resultSet.getString("requestTheme");
        Timestamp deadline = resultSet.getTimestamp("request_deadline");
        String description = resultSet.getString("request_description");
        Timestamp registrationDate = resultSet.getTimestamp("request_registration_date");
        Integer clientId = resultSet.getInt("user_client_id");
        Integer authorId = resultSet.getInt("user_author_id");
        String discipleName = resultSet.getString("disciple_name");
        String paperTypeName = resultSet.getString("type_of_paper");
        String requestTypeName = resultSet.getString("request_type");
        String requestStatusName = resultSet.getString("request_status");

        Disciple disciple = Disciple.valueOf(discipleName.toUpperCase());
        PaperType paperType = PaperType.valueOf(paperTypeName.toUpperCase());
        RequestType requestType = RequestType.valueOf(requestTypeName.toUpperCase());
        RequestStatus requestStatus = RequestStatus.valueOf(requestStatusName.toUpperCase());

        UserAccount client = new UserAccount();
        client.setId(clientId);
        UserAccount author = new UserAccount();
        author.setId(authorId);

        RequestForCoursework requestForCoursework = new RequestForCoursework();
        requestForCoursework.setId(requestId);
        requestForCoursework.setTheme(theme);
        requestForCoursework.setDeadline(deadline.getTime());
        requestForCoursework.setDescription(description);
        requestForCoursework.setRegistrationDate(registrationDate.getTime());
        requestForCoursework.setDisciple(disciple);
        requestForCoursework.setPaperType(paperType);
        requestForCoursework.setRequestType(requestType);
        requestForCoursework.setRequestStatus(requestStatus);
        requestForCoursework.setClient(client);
        requestForCoursework.setAuthor(author);
        return requestForCoursework;
    }
}
