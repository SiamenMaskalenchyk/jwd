package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.entity.RequestStatus;

public interface RequestStatusDao extends CRUDDao<RequestStatus> {
}
