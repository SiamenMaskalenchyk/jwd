package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.entity.RequestStatus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Bean
public class RequestStatusDaoImpl extends AbstractBaseDao<RequestStatus> implements RequestStatusDao {

    private static final String QUERY_SELECT_BY_ID = "SELECT id_request_status, request_status FROM request_status WHERE id_request_status=?;";
    private static final String QUERY_SELECT_ALL = "SELECT id_request_status, request_status FROM request_status;";

    public RequestStatusDaoImpl(ConnectionManager connectionManager) {
        super(connectionManager);
    }

    @Override
    public RequestStatus save(RequestStatus entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(RequestStatus entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(RequestStatus entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public RequestStatus findById(Integer id) throws DaoException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public List<RequestStatus> findAll() throws DaoException {
        return executeAndReturnEntityList(QUERY_SELECT_ALL);
    }

    @Override
    protected RequestStatus parseResultSet(ResultSet resultSet) throws SQLException {
        String requestStatus = resultSet.getString("request_status").toUpperCase();
        return RequestStatus.valueOf(requestStatus);
    }
}
