package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.entity.RequestType;

public interface RequestTypeDao extends CRUDDao<RequestType> {
}
