package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.entity.RequestType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class RequestTypeDaoImpl extends AbstractBaseDao<RequestType> implements RequestTypeDao {

    private static final String QUERY_SELECT_BY_ID = "SELECT id_request_type, request_type FROM request_type WHERE id_request_type=?;";
    private static final String QUERY_SELECT_ALL = "SELECT id_request_type, request_type FROM request_type;";

    public RequestTypeDaoImpl(ConnectionManager connectionManager) {
        super(connectionManager);
    }

    @Override
    public RequestType save(RequestType entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(RequestType entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(RequestType entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public RequestType findById(Integer id) throws DaoException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public List<RequestType> findAll() throws DaoException {
        return executeAndReturnEntityList(QUERY_SELECT_ALL);
    }

    @Override
    protected RequestType parseResultSet(ResultSet resultSet) throws SQLException {
        String requestType = resultSet.getString("request_type").toUpperCase();
        return RequestType.valueOf(requestType);
    }
}
