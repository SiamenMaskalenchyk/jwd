package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.entity.ResultFile;

import java.math.BigDecimal;
import java.util.List;

public interface ResultFileDao extends CRUDDao<ResultFile> {

}
