package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManagerException;
import com.maskalenchyk.education_helper.entity.ResultFile;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class ResultFileDaoImpl extends AbstractBaseDao<ResultFile> implements ResultFileDao {

    private static final String QUERY_SELECT_BY_ID = "SELECT id_result_file,result_file,result_file_registration_date,result_file_grade,result_file_price,result_file_count_of_downloads FROM result_file WHERE id_result_file=?;";
    private static final String QUERY_SELECT_ALL = "SELECT id_result_file,result_file,result_file_registration_date,result_file_grade,result_file_price,result_file_count_of_downloads FROM result_file;";
    private static final String QUERY_INSERT = "INSERT INTO result_file(result_file,result_file_registration_date,result_file_grade,result_file_price,result_file_count_of_downloads) VALUES(?,?,?,?,?);";
    private static final String QUERY_DELETE = "DELETE FROM result_file WHERE id_result_file=? ;";
    private static final String QUERY_UPDATE = "UPDATE result_file SET result_file=?,result_file_registration_date=?,result_file_grade=?,result_file_price=?,result_file_count_of_downloads=? WHERE id_result_file=?;";

    public ResultFileDaoImpl(ConnectionManager connectionManager) {
        super(connectionManager);
    }

    @Override
    public ResultFile save(ResultFile entity) throws DaoException {
        try {
            return connectionManager.doWithConnection(QUERY_INSERT, (statement) -> {
                int i = 1;
                Timestamp registrationDate = new Timestamp(entity.getRegistrationDate());
                statement.setBlob(++i, entity.getFile());
                statement.setTimestamp(++i, registrationDate);
                statement.setByte(++i, entity.getGrade());
                statement.setBigDecimal(++i, entity.getPrice());
                statement.setInt(++i, entity.getCountOfDownloads());
                statement.executeUpdate();
                return findById(getInsertedId(statement));
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void update(ResultFile entity) throws DaoException {
        try {
            connectionManager.doWithConnection(QUERY_UPDATE, (statement) -> {
                int i = 0;
                Timestamp registrationDate = new Timestamp(entity.getRegistrationDate());
                statement.setInt(++i, entity.getId());
                statement.setBlob(++i, entity.getFile());
                statement.setLong(++i, registrationDate.getTime());
                statement.setByte(++i, entity.getGrade());
                statement.setBigDecimal(++i, entity.getPrice());
                statement.setInt(++i, entity.getCountOfDownloads());
                statement.executeUpdate();
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void delete(ResultFile entity) throws DaoException {
        executeDelete(QUERY_DELETE, entity.getId());
    }

    @Override
    public ResultFile findById(Integer id) throws DaoException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public List<ResultFile> findAll() throws DaoException {
        return executeAndReturnEntityList(QUERY_SELECT_ALL);
    }

    @Override
    protected ResultFile parseResultSet(ResultSet resultSet) throws SQLException {
        Integer resultFileId = resultSet.getInt("id_result_file");
        InputStream resultFileStream = resultSet.getBinaryStream("result_file");
        Timestamp registrationDate = resultSet.getTimestamp("result_file_registration_date");
        Byte resultFileGrade = resultSet.getByte("result_file_grade");
        BigDecimal resultFilePrice = resultSet.getBigDecimal("result_file_price");
        Integer downloadCounts = resultSet.getInt("result_file_count_of_downloads");

        ResultFile resultFile = new ResultFile();
        resultFile.setId(resultFileId);
        resultFile.setFile(resultFileStream);
        resultFile.setRegistrationDate(registrationDate.getTime());
        resultFile.setGrade(resultFileGrade);
        resultFile.setPrice(resultFilePrice);
        resultFile.setCountOfDownloads(downloadCounts);
        return resultFile;
    }
}
