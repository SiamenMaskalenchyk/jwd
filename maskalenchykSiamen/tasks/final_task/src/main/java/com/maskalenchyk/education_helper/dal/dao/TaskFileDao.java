package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.entity.TaskFile;

import java.util.List;

public interface TaskFileDao extends CRUDDao<TaskFile> {

    List<TaskFile> getByRequestForCourseworkId(Integer requestId) throws DaoException;
}
