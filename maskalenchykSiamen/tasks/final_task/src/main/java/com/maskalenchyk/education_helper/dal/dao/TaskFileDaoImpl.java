package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManagerException;
import com.maskalenchyk.education_helper.entity.TaskFile;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Bean
public class TaskFileDaoImpl extends AbstractBaseDao<TaskFile> implements TaskFileDao {

    private static final String QUERY_SELECT_BY_ID = "SELECT id_task_file, task_file, FROM task_file WHERE id_task_file?;";
    private static final String QUERY_SELECT_BY_REQUEST_ID = "SELECT id_task_file, task_file, FROM task_file WHERE request_id=?;";
    private static final String QUERY_SELECT_ALL = "SELECT id_result_file, task_file, FROM task_file;";
    private static final String QUERY_INSERT = "INSERT INTO task_file (task_file, request_id) VALUES(?,?);";
    private static final String QUERY_DELETE = "DELETE FROM task_file WHERE id_task_file=? ;";
    private static final String QUERY_UPDATE = "UPDATE task_file SET task_file=? WHERE id_task_file=?;";

    public TaskFileDaoImpl(ConnectionManager connectionManager) {
        super(connectionManager);
    }

    @Override
    public TaskFile save(TaskFile entity) throws DaoException {
        try {
            return connectionManager.doWithConnection(QUERY_INSERT, statement -> {
                int i = 0;
                statement.setBlob(++i, entity.getTaskFileStream());
                statement.setInt(++i, entity.getIdRequestForCoursework());
                statement.executeUpdate();
                return findById(getInsertedId(statement));
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void update(TaskFile entity) throws DaoException {
        try {
            connectionManager.doWithConnection(QUERY_UPDATE, statement -> {
                int i = 0;
                statement.setInt(++i, entity.getId());
                statement.setBlob(++i, entity.getTaskFileStream());
                statement.executeUpdate();
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void delete(TaskFile entity) throws DaoException {
        executeDelete(QUERY_DELETE, entity.getId());
    }

    @Override
    public TaskFile findById(Integer id) throws DaoException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public List<TaskFile> findAll() throws DaoException {
        return executeAndReturnEntityList(QUERY_SELECT_ALL);
    }

    @Override
    public List<TaskFile> getByRequestForCourseworkId(Integer requestId) throws DaoException {
        return executeAndReturnEntityList(QUERY_SELECT_BY_REQUEST_ID);
    }

    @Override
    protected TaskFile parseResultSet(ResultSet resultSet) throws SQLException {
        Integer taskFileId = resultSet.getInt("id_task_file");
        InputStream taskFileStream = resultSet.getBinaryStream("task_file");
        Integer courseworkRequestId = resultSet.getInt("request_id");

        TaskFile taskFile = new TaskFile();
        taskFile.setId(taskFileId);
        taskFile.setTaskFileStream(taskFileStream);
        taskFile.setIdRequestForCoursework(courseworkRequestId);
        return taskFile;
    }
}
