package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.entity.PaperType;

public interface TypeOfPaperDao extends CRUDDao<PaperType> {
}
