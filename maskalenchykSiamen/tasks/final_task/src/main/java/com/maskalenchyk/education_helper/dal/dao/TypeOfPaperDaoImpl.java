package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.entity.PaperType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Bean
public class TypeOfPaperDaoImpl extends AbstractBaseDao<PaperType> implements TypeOfPaperDao {

    private static final String QUERY_SELECT_BY_ID = "SELECT id_type_of_paper, type_of_paper FROM type_of_paper WHERE id_type_of_paper=?;";
    private static final String QUERY_SELECT_ALL = "SELECT id_type_of_paper, type_of_paper FROM type_of_paper;";

    public TypeOfPaperDaoImpl(ConnectionManager connectionManager) {
        super(connectionManager);
    }

    @Override
    public PaperType save(PaperType entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(PaperType entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(PaperType entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public PaperType findById(Integer id) throws DaoException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public List<PaperType> findAll() throws DaoException {
        return executeAndReturnEntityList(QUERY_SELECT_ALL);
    }

    @Override
    protected PaperType parseResultSet(ResultSet resultSet) throws SQLException {
        String paperType = resultSet.getString("type_of_paper").toUpperCase();
        return PaperType.valueOf(paperType);
    }
}
