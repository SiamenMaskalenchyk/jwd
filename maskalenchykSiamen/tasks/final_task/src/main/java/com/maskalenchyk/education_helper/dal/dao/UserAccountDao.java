package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManagerException;
import com.maskalenchyk.education_helper.entity.UserAccount;

public interface UserAccountDao extends CRUDDao<UserAccount> {

    UserAccount findByPhoneNumber(String phoneNumber) throws DaoException;

    UserAccount findByEmail(String email) throws DaoException;

    UserAccount findByName(String name) throws ConnectionManagerException, DaoException;

    Integer getUserCount() throws ConnectionManagerException, DaoException;

}
