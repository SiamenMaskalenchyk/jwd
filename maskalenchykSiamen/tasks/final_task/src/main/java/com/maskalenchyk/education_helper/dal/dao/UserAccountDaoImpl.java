package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManagerException;
import com.maskalenchyk.education_helper.entity.UserAccount;
import com.maskalenchyk.education_helper.entity.UserRole;
import com.maskalenchyk.education_helper.entity.Wallet;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

@Bean
public class UserAccountDaoImpl extends AbstractBaseDao<UserAccount> implements UserAccountDao {

    private static final String QUERY_SELECT_ALL = "SELECT u.id_user_account,u.user_account_name,u.user_account_email,u.user_account_phone,u.user_account_password,u.user_account_registration_date,u.wallet_id,w.wallet_amount,role.role_name " +
            "FROM user_account AS u " +
            "JOIN wallet AS w ON w.id_wallet = u.wallet_id " +
            "JOIN role ON role. id_role=u.role_id ";
    private static final String QUERY_SELECT_BY_ID = QUERY_SELECT_ALL + "WHERE u.id_user_account=?;";
    private static final String QUERY_SELECT_BY_NAME = QUERY_SELECT_ALL + "WHERE u.user_account_name=?;";
    private static final String QUERY_SELECT_BY_PHONE = QUERY_SELECT_ALL + "WHERE u.user_account_phone=?;";
    private static final String QUERY_SELECT_BY_EMAIL = QUERY_SELECT_ALL + "WHERE u.user_account_email=?;";
    private static final String QUERY_INSERT = "INSERT INTO user_account (user_account_name,user_account_email,user_account_phone,user_account_password,user_account_registration_date,wallet_id,role_id) VALUES(?,?,?,?,?,?,?);";
    private static final String QUERY_DELETE = "DELETE FROM user_account WHERE id_user_account=? ;";
    private static final String QUERY_UPDATE = "UPDATE user_account SET user_account_name=?,user_account_email=?,user_account_phone=?,user_account_password=?,user_account_registration_date=?,wallet_id=?,role_id=? WHERE id_user_account=?;";
    private static final String QUERY_COUNT_ALL = "SELECT COUNT (id_user_account) FROM user_account;";

    public UserAccountDaoImpl(ConnectionManager connectionManager) {
        super(connectionManager);
    }

    @Override
    public UserAccount findByPhoneNumber(String phoneNumber) throws DaoException {
        return getUserAccountFromDB(phoneNumber, QUERY_SELECT_BY_PHONE);
    }

    @Override
    public UserAccount findByEmail(String email) throws DaoException {
        return getUserAccountFromDB(email, QUERY_SELECT_BY_EMAIL);
    }

    @Override
    public UserAccount findByName(String name) throws DaoException {
        return getUserAccountFromDB(name, QUERY_SELECT_BY_NAME);
    }

    @Override
    public Integer getUserCount() throws DaoException {
        try {
            return connectionManager.doWithConnection(QUERY_COUNT_ALL, (statement) -> {
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        return resultSet.getInt(1);
                    } else {
                        throw new ConnectionManagerException("Cannot select user count, empty result set");
                    }
                }
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public UserAccount save(UserAccount entity) throws DaoException {
        try {
            return connectionManager.doWithConnection(QUERY_INSERT, (statement) -> {
                int i = 0;
                Timestamp registrationDate = new Timestamp(entity.getRegistrationDate());
                statement.setString(++i, entity.getName());
                statement.setString(++i, entity.getEmail());
                statement.setString(++i, entity.getPhoneNumber());
                statement.setString(++i, entity.getPassword());
                statement.setTimestamp(++i, registrationDate);
                statement.setInt(++i, entity.getWallet().getId());
                statement.setInt(++i, entity.getUserRole().getId());
                statement.executeUpdate();
                return findById(getInsertedId(statement));
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void update(UserAccount entity) throws DaoException {
        try {
            connectionManager.doWithConnection(QUERY_UPDATE, (statement) -> {
                int i = 0;
                Timestamp registrationDate = new Timestamp(entity.getRegistrationDate());
                statement.setString(++i, entity.getName());
                statement.setString(++i, entity.getEmail());
                statement.setString(++i, entity.getPhoneNumber());
                statement.setString(++i, entity.getPassword());
                statement.setTimestamp(++i, registrationDate);
                statement.setInt(++i, entity.getWallet().getId());
                statement.setInt(++i, entity.getUserRole().getId());
                statement.setInt(++i, entity.getId());
                statement.executeUpdate();
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void delete(UserAccount entity) throws DaoException {
        executeDelete(QUERY_DELETE, entity.getId());
    }

    @Override
    public UserAccount findById(Integer id) throws DaoException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public List<UserAccount> findAll() throws DaoException {
        return executeAndReturnEntityList(QUERY_SELECT_ALL);
    }

    @Override
    protected UserAccount parseResultSet(ResultSet resultSet) throws SQLException {
        Integer userAccountId = resultSet.getInt("id_user_account");
        String userAccountName = resultSet.getString("user_account_name");
        String userAccountEmail = resultSet.getString("user_account_email");
        String userAccountPhone = resultSet.getString("user_account_phone");
        String userAccountPassword = resultSet.getString("user_account_password");
        Timestamp registrationDate = resultSet.getTimestamp("user_account_registration_date");

        Integer walletId = resultSet.getInt("wallet_id");
        BigDecimal amount = resultSet.getBigDecimal("wallet_amount");
        Wallet wallet = new Wallet();
        wallet.setId(walletId);
        wallet.setAmount(amount);

        String roleName = resultSet.getString("role_name");
        UserRole userRole = UserRole.valueOf(roleName.toUpperCase());

        UserAccount userAccount = new UserAccount();
        userAccount.setId(userAccountId);
        userAccount.setName(userAccountName);
        userAccount.setEmail(userAccountEmail);
        userAccount.setPhoneNumber(userAccountPhone);
        userAccount.setPassword(userAccountPassword);
        userAccount.setRegistrationDate(registrationDate.getTime());
        userAccount.setWallet(wallet);
        userAccount.setUserRole(userRole);
        return userAccount;
    }

    private UserAccount getUserAccountFromDB(String parameter, String query) throws DaoException {
        try {
            return connectionManager.doWithConnection(query, ((statement) -> {
                statement.setString(1, parameter);
                return executeStatementAndParseResultSet(statement);
            }));
        } catch (ConnectionManagerException | SQLException e) {
            throw new DaoException(e);
        }
    }
}
