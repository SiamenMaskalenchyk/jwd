package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.entity.UserOrder;

public interface UserOrderDao extends CRUDDao<UserOrder> {

}
