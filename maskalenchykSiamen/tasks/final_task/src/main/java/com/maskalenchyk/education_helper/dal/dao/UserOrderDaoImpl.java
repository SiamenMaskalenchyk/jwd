package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManagerException;
import com.maskalenchyk.education_helper.entity.RequestForCoursework;
import com.maskalenchyk.education_helper.entity.UserOrder;
import com.maskalenchyk.education_helper.entity.UserOrderStatus;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Bean
public class UserOrderDaoImpl extends AbstractBaseDao<UserOrder> implements UserOrderDao {

    private static final String QUERY_INSERT = "INSERT INTO user_order(user_order_price,user_order_prepayment,user_order_description,request_for_coursework_id,order_feedback_id,user_order_status_id) VALUES(?,?,?,?,?,?);";
    private static final String QUERY_SELECT_BY_ID = "SELECT id_user_order,user_order_price,user_order_prepayment,user_order_description,request_for_coursework_id,order_feedback_id,user_order_status_id FROM user_order WHERE id_user_order = ?;";
    private static final String QUERY_SELECT_ALL = "SELECT id_user_order,user_order_price,user_order_prepayment,user_order_description,request_for_coursework_id,order_feedback_id,user_order_status_id FROM user_order;";
    private static final String QUERY_UPDATE = "UPDATE user_order SET user_order_price=?,user_order_prepayment=?,user_order_description=?,request_for_coursework_id=?,order_feedback_id=?,user_order_status_id=? WHERE id_user_order = ?;";
    private static final String QUERY_DELETE = "DELETE FROM user_order WHERE id_user_order = ?;";

    private RequestForCourseworkDao requestForCourseworkDao;

    public UserOrderDaoImpl(ConnectionManager connectionManager, RequestForCourseworkDao requestForCourseworkDao) {
        super(connectionManager);
        this.requestForCourseworkDao = requestForCourseworkDao;
    }

    @Override
    public UserOrder save(UserOrder entity) throws DaoException {
        try {
            return connectionManager.doWithConnection(QUERY_INSERT, (statement) -> {
                int i = 0;
                statement.setBigDecimal(++i, entity.getPrice());
                statement.setBigDecimal(++i, entity.getPrepayment());
                statement.setString(++i, entity.getDescription());
                statement.setInt(++i, entity.getResultFileId());
                statement.setInt(++i, entity.getFeedbackId());
                statement.setInt(++i, entity.getUserOrderStatus().ordinal() + 1);
                statement.executeUpdate();
                entity.setId(getInsertedId(statement));
                return entity;
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void update(UserOrder entity) throws DaoException {
        try {
            connectionManager.doWithConnection(QUERY_UPDATE, (statement) -> {
                int i = 0;
                statement.setBigDecimal(++i, entity.getPrice());
                statement.setBigDecimal(++i, entity.getPrepayment());
                statement.setString(++i, entity.getDescription());
                statement.setInt(++i, entity.getResultFileId());
                statement.setInt(++i, entity.getRequestForCoursework().getId());
                statement.setInt(++i, entity.getFeedbackId());
                statement.setInt(++i, entity.getUserOrderStatus().ordinal() - 1);
                statement.executeUpdate();
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void delete(UserOrder entity) throws DaoException {
        executeDelete(QUERY_DELETE, entity.getId());
    }

    @Override
    public UserOrder findById(Integer id) throws DaoException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public List<UserOrder> findAll() throws DaoException {
        return executeAndReturnEntityList(QUERY_SELECT_ALL);
    }

    @Override
    protected UserOrder parseResultSet(ResultSet resultSet) throws SQLException, DaoException {
        Integer userOrderId = resultSet.getInt("id_user_order");
        BigDecimal price = resultSet.getBigDecimal("user_order_price");
        BigDecimal prepayment = resultSet.getBigDecimal("user_order_prepayment");
        String description = resultSet.getString("user_order_description");
        Integer resultFileId = resultSet.getInt("result_file_id");
        Integer requestId = resultSet.getInt("request_for_coursework_id");
        Integer feedbackId = resultSet.getInt("order_feedback_id");
        Integer userOrderStatusId = resultSet.getInt("user_order_status_id");

        RequestForCoursework requestForCoursework = requestForCourseworkDao.findById(requestId);
        UserOrderStatus userOrderStatus = UserOrderStatus.values()[userOrderStatusId - 1];

        UserOrder userOrder = new UserOrder();
        userOrder.setId(userOrderId);
        userOrder.setPrice(price);
        userOrder.setPrepayment(prepayment);
        userOrder.setDescription(description);
        userOrder.setResultFileId(resultFileId);
        userOrder.setFeedbackId(feedbackId);
        userOrder.setRequestForCoursework(requestForCoursework);
        userOrder.setUserOrderStatus(userOrderStatus);
        return userOrder;
    }
}
