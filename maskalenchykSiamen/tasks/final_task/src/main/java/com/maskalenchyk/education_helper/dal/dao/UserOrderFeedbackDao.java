package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.entity.UserOrderFeedback;

import java.util.List;

public interface UserOrderFeedbackDao extends CRUDDao<UserOrderFeedback> {

}
