package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManagerException;
import com.maskalenchyk.education_helper.entity.UserOrderFeedback;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Bean
public class UserOrderFeedbackDaoImpl extends AbstractBaseDao<UserOrderFeedback> implements UserOrderFeedbackDao {

    private static final String QUERY_SELECT_BY_ID = "SELECT id_order_feedback, order_feedback_grade, order_feedback_comment FROM user_order_feedback WHERE id_order_feedback=?;";
    private static final String QUERY_INSERT = "INSERT INTO user_order_feedback (order_feedback_grade,order_feedback_comment) VALUES(?,?);";
    private static final String QUERY_DELETE = "DELETE FROM user_order_feedback WHERE id_order_feedback=?;";
    private static final String QUERY_UPDATE = "UPDATE user_order_feedback SET order_feedback_grade=?,order_feedback_comment=? WHERE id_order_feedback=?;";
    private static final String QUERY_SELECT_ALL = "SELECT id_order_feedback,order_feedback_grade,order_feedback_comment FROM user_order_feedback;";

    public UserOrderFeedbackDaoImpl(ConnectionManager connectionManager) {
        super(connectionManager);
    }

    @Override
    public UserOrderFeedback save(UserOrderFeedback entity) throws DaoException {
        try {
            return connectionManager.doWithConnection(QUERY_INSERT, (statement) -> {
                int i = 0;
                statement.setByte(++i, entity.getGrade());
                statement.setString(++i, entity.getComment());
                statement.executeUpdate();
                return findById(getInsertedId(statement));
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void update(UserOrderFeedback entity) throws DaoException {
        try {
            connectionManager.doWithConnection(QUERY_UPDATE, (statement) -> {
                int i = 0;
                statement.setInt(++i, entity.getId());
                statement.setByte(++i, entity.getGrade());
                statement.setString(++i, entity.getComment());
                statement.executeUpdate();
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void delete(UserOrderFeedback entity) throws DaoException {
        executeDelete(QUERY_DELETE, entity.getId());
    }

    @Override
    public UserOrderFeedback findById(Integer id) throws DaoException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public List<UserOrderFeedback> findAll() throws DaoException {
        return executeAndReturnEntityList(QUERY_SELECT_ALL);
    }

    @Override
    protected UserOrderFeedback parseResultSet(ResultSet resultSet) throws SQLException {
        Integer feedbackId = resultSet.getInt("id_order_feedback");
        Byte feedbackGrade = resultSet.getByte("order_feedback_grade");
        String feedbackComment = resultSet.getString("order_feedback_comment");
        UserOrderFeedback feedback = new UserOrderFeedback();
        feedback.setId(feedbackId);
        feedback.setGrade(feedbackGrade);
        feedback.setComment(feedbackComment);
        return feedback;
    }
}
