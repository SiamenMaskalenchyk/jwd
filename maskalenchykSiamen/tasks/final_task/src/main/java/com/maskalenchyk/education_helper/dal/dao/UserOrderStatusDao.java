package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.entity.UserOrderStatus;

public interface UserOrderStatusDao extends CRUDDao<UserOrderStatus> {
}
