package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.entity.UserOrderStatus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserOrderStatusDaoImpl extends AbstractBaseDao<UserOrderStatus> implements UserOrderStatusDao {

    private static final String QUERY_SELECT_BY_ID = "SELECT id_user_order_status, user_order_status_name FROM user_order_status WHERE id_user_order_status=?;";
    private static final String QUERY_SELECT_ALL = "SELECT id_user_order_status, user_order_status_name FROM user_order_status;";

    public UserOrderStatusDaoImpl(ConnectionManager connectionManager) {
        super(connectionManager);
    }


    @Override
    public UserOrderStatus save(UserOrderStatus entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(UserOrderStatus entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(UserOrderStatus entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public UserOrderStatus findById(Integer id) throws DaoException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public List<UserOrderStatus> findAll() throws DaoException {
        return executeAndReturnEntityList(QUERY_SELECT_ALL);
    }

    @Override
    protected UserOrderStatus parseResultSet(ResultSet resultSet) throws SQLException {
        String userOrderStatus = resultSet.getString("user_order_status_name").toUpperCase();
        return UserOrderStatus.valueOf(userOrderStatus);
    }
}
