package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.entity.UserRole;

public interface UserRoleDao extends CRUDDao<UserRole> {

    Integer getUserRoleId(UserRole userRole) throws DaoException;

}
