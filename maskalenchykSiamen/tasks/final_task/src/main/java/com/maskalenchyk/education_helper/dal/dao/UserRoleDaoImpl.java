package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.entity.UserRole;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Bean
public class UserRoleDaoImpl extends AbstractBaseDao<UserRole> implements UserRoleDao {

    private static final String QUERY_SELECT_BY_ID = "SELECT id_role,role_name FROM role WHERE id_role=?;";
    private static final String QUERY_SELECT_ALL = "SELECT id_role,role_name FROM role ;";
    private static final String QUERY_SELECT_ID_BY_NAME = "SELECT id_role FROM role WHERE role_name=?;";

    public UserRoleDaoImpl(ConnectionManager connectionManager) {
        super(connectionManager);
    }

    @Override
    public UserRole save(UserRole entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(UserRole entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(UserRole entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public UserRole findById(Integer id) throws DaoException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public List<UserRole> findAll() throws DaoException {
        return executeAndReturnEntityList(QUERY_SELECT_ALL);
    }


    @Override
    protected UserRole parseResultSet(ResultSet resultSet) throws SQLException {
        String userRole = resultSet.getString("role_name").toUpperCase();
        resultSet.close();
        return UserRole.valueOf(userRole);
    }

    @Override
    public Integer getUserRoleId(UserRole userRole) throws DaoException {
        return null;
    }
}
