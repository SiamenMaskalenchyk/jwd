package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManagerException;
import com.maskalenchyk.education_helper.entity.Wallet;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Bean
public class WalletDaoImpl extends AbstractBaseDao<Wallet> implements WalletDao {

    private static final String QUERY_INSERT = "INSERT INTO wallet(wallet_amount) VALUES(?);";
    private static final String QUERY_SELECT_BY_ID = "SELECT id_wallet, wallet_amount FROM wallet WHERE id_wallet = ?;";
    private static final String QUERY_UPDATE = "UPDATE wallet SET wallet_amount=? WHERE id_wallet = ?;";
    private static final String QUERY_DELETE = "DELETE FROM wallet WHERE id_wallet = ?;";
    private static final String QUERY_SELECT_ALL = "SELECT id_wallet, wallet_amount FROM wallet;";

    public WalletDaoImpl(ConnectionManager connectionManager) {
        super(connectionManager);
    }

    @Override
    public Wallet save(Wallet entity) throws DaoException {
        try {
            return connectionManager.doWithConnection(QUERY_INSERT, ( statement) -> {
                statement.setBigDecimal(1, entity.getAmount());
                statement.executeUpdate();
                return findById(getInsertedId(statement));
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void update(Wallet entity) throws DaoException {
        try {
            connectionManager.doWithConnection(QUERY_UPDATE, ( statement) -> {
                int i = 0;
                statement.setBigDecimal(++i, entity.getAmount());
                statement.setInt(++i, entity.getId());
                statement.executeUpdate();
            });
        } catch (SQLException | ConnectionManagerException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void delete(Wallet entity) throws DaoException {
        executeDelete(QUERY_DELETE, entity.getId());
    }

    @Override
    public Wallet findById(Integer id) throws DaoException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public List<Wallet> findAll() throws DaoException {
        return executeAndReturnEntityList(QUERY_SELECT_ALL);
    }

    @Override
    protected Wallet parseResultSet(ResultSet resultSet) throws SQLException {
        Integer walletId = resultSet.getInt("id_wallet");
        BigDecimal walletAmount = resultSet.getBigDecimal("wallet_amount");
        Wallet wallet = new Wallet();
        wallet.setId(walletId);
        wallet.setAmount(walletAmount);
        return wallet;
    }

}
