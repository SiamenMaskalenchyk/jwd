package com.maskalenchyk.education_helper.dal.transaction_manager;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.connection_pool.ConnectionPool;
import com.maskalenchyk.education_helper.dal.connection_pool.ConnectionPoolException;
import org.apache.log4j.Logger;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;

@Bean
public class TransactionManagerImpl implements TransactionManager {

    private static final Logger LOGGER = Logger.getLogger(TransactionManagerImpl.class);
    private ConnectionPool connectionPool;
    private ThreadLocal<Connection> localConnection = new ThreadLocal<>();

    public TransactionManagerImpl(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    @Override
    public void beginTransaction() throws TransactionManagerException {
        try {
            Connection connection = connectionPool.getConnection();
            connection.setAutoCommit(false);
            localConnection.set(connection);
        } catch (SQLException | ConnectionPoolException e) {
            String errorMessage = "Getting connection from data source exception";
            LOGGER.error(errorMessage, e);
            throw new TransactionManagerException(errorMessage, e);
        }
    }

    @Override
    public void commitTransaction() throws TransactionManagerException {
        Connection connection = localConnection.get();
        if (connection != null) {
            try {
                connection.commit();
                connection.setAutoCommit(true);
                connection.close();
                localConnection.remove();
            } catch (SQLException e) {
                String errorMessage = "Connection commit exception";
                LOGGER.error(errorMessage, e);
                throw new TransactionManagerException(errorMessage, e);
            }
        } else {
            String errorMessage = "Local thread connection is empty";
            LOGGER.error(errorMessage);
            throw new TransactionManagerException(errorMessage);
        }
    }

    @Override
    public void rollbackTransaction() throws TransactionManagerException {
        Connection connection = localConnection.get();
        if (connection != null) {
            try {
                connection.rollback();
                connection.setAutoCommit(true);
                connection.close();
                localConnection.remove();
            } catch (SQLException e) {
                String errorMessage = "Connection rollback exception";
                LOGGER.error(errorMessage, e);
                throw new TransactionManagerException(errorMessage, e);
            }
        } else {
            String errorMessage = "Local thread connection is empty";
            LOGGER.error(errorMessage);
            throw new TransactionManagerException(errorMessage);
        }
    }

    @Override
    public Connection getConnection() throws TransactionManagerException {
        Connection connection = localConnection.get();
        if (connection != null) {
            return (Connection) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{Connection.class},
                    (proxy, method, args) -> {
                        if (method.getName().equals("close")) {
                            return null;
                        } else {
                            Connection realConnection = localConnection.get();
                            return method.invoke(realConnection, args);
                        }
                    });
        } else {
            try {
                return connectionPool.getConnection();
            } catch (ConnectionPoolException e) {
                String errorMessage = "Getting connection from data source exception";
                LOGGER.error(errorMessage, e);
                throw new TransactionManagerException(errorMessage, e);
            }
        }
    }
}
