package com.maskalenchyk.education_helper.entity;

import java.util.stream.Stream;

public enum Disciple implements Entity {
    UNDEFINED(0),
    ECONOMICS(1),
    PROGRAMMING(2),
    SOCIAL_SCIENCES(3);

    Integer id;

    Disciple(java.lang.Integer id) {
        this.id = id;
    }

    public static Disciple getDiscipleById(Integer idDisciple) {
        return Stream.of(Disciple.values())
                .filter(d -> d.getId().equals(idDisciple))
                .findFirst()
                .orElse(UNDEFINED);
    }

    public Integer getId() {
        return id;
    }
}