package com.maskalenchyk.education_helper.entity;

import java.util.stream.Stream;

public enum PaperType implements Entity {
    UNDEFINED(0, "неопределено"),
    COURSEWORK(1, "курсовая работа"),
    ESSAY(2, "ЭССЕ"),
    LAB_REPORT(3, "лабораторная"),
    PRESENTATION(4, "презентация"),
    DRAWING(5, "чертёж"),
    PROGRAM(6, "программа"),
    CONTROL_WORK(7, "контрольная работа"),
    GRADUATE_WORK(8, "дипломная работа");

    Integer id;
    String name;

    PaperType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public static PaperType getPaperTypeById(Integer idPaperType) {
        return Stream.of(PaperType.values())
                .filter(g -> g.getId().equals(idPaperType))
                .findFirst().orElse(UNDEFINED);
    }

    public Integer getId() {
        return id;
    }
}