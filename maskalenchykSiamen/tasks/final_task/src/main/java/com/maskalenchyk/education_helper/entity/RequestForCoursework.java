package com.maskalenchyk.education_helper.entity;

import java.util.List;
import java.util.Objects;

public class RequestForCoursework implements Entity {
    private Integer id;
    private String theme;
    private RequestStatus requestStatus;
    private Long deadline;
    private String description;
    private Long registrationDate;
    private Disciple disciple;
    private PaperType paperType;
    private RequestType requestType;
    private UserAccount client;
    private UserAccount author;
    private List<TaskFile> taskFiles;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public Long getDeadline() {
        return deadline;
    }

    public void setDeadline(Long deadline) {
        this.deadline = deadline;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Long registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Disciple getDisciple() {
        return disciple;
    }

    public void setDisciple(Disciple disciple) {
        this.disciple = disciple;
    }

    public PaperType getPaperType() {
        return paperType;
    }

    public void setPaperType(PaperType paperType) {
        this.paperType = paperType;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public UserAccount getClient() {
        return client;
    }

    public void setClient(UserAccount client) {
        this.client = client;
    }

    public UserAccount getAuthor() {
        return author;
    }

    public void setAuthor(UserAccount author) {
        this.author = author;
    }

    public List<TaskFile> getTaskFiles() {
        return taskFiles;
    }

    public void setTaskFiles(List<TaskFile> taskFiles) {
        this.taskFiles = taskFiles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestForCoursework that = (RequestForCoursework) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(theme, that.theme) &&
                requestStatus == that.requestStatus &&
                Objects.equals(deadline, that.deadline) &&
                Objects.equals(description, that.description) &&
                Objects.equals(registrationDate, that.registrationDate) &&
                disciple == that.disciple &&
                paperType == that.paperType &&
                requestType == that.requestType &&
                Objects.equals(client, that.client) &&
                Objects.equals(author, that.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, theme, requestStatus, deadline, description, registrationDate, disciple, paperType, requestType, client, author);
    }

    @Override
    public String toString() {
        return "RequestForCoursework{" +
                "id=" + id +
                ", theme='" + theme + '\'' +
                ", requestStatus=" + requestStatus +
                ", deadline=" + deadline +
                ", description='" + description + '\'' +
                ", registrationDate=" + registrationDate +
                ", disciple=" + disciple +
                ", paperType=" + paperType +
                ", requestType=" + requestType +
                ", client=" + client +
                ", author=" + author +
                '}';
    }
}
