package com.maskalenchyk.education_helper.entity;

import java.util.stream.Stream;

public enum RequestStatus implements Entity {
    UNDEFINED(0),
    CONSIDERING(1),
    ACCEPTED(2),
    DENIED(3);

    Integer id;

    RequestStatus(Integer id) {
        this.id = id;
    }

    public static RequestStatus getRequestStatusById(Integer idRequestStatus) {
        return Stream.of(RequestStatus.values())
                .filter(r -> r.getId().equals(idRequestStatus))
                .findFirst().orElse(UNDEFINED);
    }

    public Integer getId() {
        return id;
    }
}