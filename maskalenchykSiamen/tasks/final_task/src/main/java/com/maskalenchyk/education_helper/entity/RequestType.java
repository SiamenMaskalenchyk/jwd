package com.maskalenchyk.education_helper.entity;

import java.util.stream.Stream;

public enum RequestType implements Entity{
    PURCHASE(1),
    EXECUTION(2);

    Integer id;

    RequestType(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public static RequestType getRequestTypeById(Integer idRequestType) {
        return Stream.of(RequestType.values())
                .filter(r -> r.getId().equals(idRequestType))
                .findFirst().orElse(EXECUTION);
    }
}