package com.maskalenchyk.education_helper.entity;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

public class ResultFile implements Entity {

    private Integer id;
    private InputStream file;
    private Long registrationDate;
    private Byte grade;
    private BigDecimal price;
    private Integer countOfDownloads;

    public ResultFile() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public InputStream getFile() {
        return file;
    }

    public void setFile(InputStream file) {
        this.file = file;
    }

    public Long getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Long registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Byte getGrade() {
        return grade;
    }

    public void setGrade(Byte grade) {
        this.grade = grade;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getCountOfDownloads() {
        return countOfDownloads;
    }

    public void setCountOfDownloads(Integer countOfDownloads) {
        this.countOfDownloads = countOfDownloads;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResultFile that = (ResultFile) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(registrationDate, that.registrationDate) &&
                Objects.equals(grade, that.grade) &&
                Objects.equals(price, that.price) &&
                Objects.equals(countOfDownloads, that.countOfDownloads);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, registrationDate, grade, price, countOfDownloads);
    }

    @Override
    public String toString() {
        return "ResultFile{" +
                "id=" + id +
                ", file=" + file +
                ", registrationDate=" + registrationDate +
                ", grade=" + grade +
                ", price=" + price +
                ", countOfDownloads=" + countOfDownloads +
                '}';
    }
}
