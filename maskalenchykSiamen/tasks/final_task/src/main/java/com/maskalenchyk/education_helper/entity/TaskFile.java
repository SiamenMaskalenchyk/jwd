package com.maskalenchyk.education_helper.entity;

import java.io.InputStream;
import java.util.Objects;

public class TaskFile implements Entity {

    private Integer id;
    private InputStream taskFileStream;
    private Integer idRequestForCoursework;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public InputStream getTaskFileStream() {
        return taskFileStream;
    }

    public void setTaskFileStream(InputStream taskFileStream) {
        this.taskFileStream = taskFileStream;
    }

    public Integer getIdRequestForCoursework() {
        return idRequestForCoursework;
    }

    public void setIdRequestForCoursework(Integer idRequestForCoursework) {
        this.idRequestForCoursework = idRequestForCoursework;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskFile taskFile1 = (TaskFile) o;
        return Objects.equals(id, taskFile1.id) &&
                Objects.equals(taskFileStream, taskFile1.taskFileStream);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, taskFileStream);
    }

}
