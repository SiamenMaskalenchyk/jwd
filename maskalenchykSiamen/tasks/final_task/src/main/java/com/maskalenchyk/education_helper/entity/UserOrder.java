package com.maskalenchyk.education_helper.entity;

import java.math.BigDecimal;
import java.util.Objects;

public class UserOrder implements Entity {
    private Integer id;
    private UserOrderStatus userOrderStatus;
    private BigDecimal price;
    private BigDecimal prepayment;
    private String description;
    private Integer resultFileId;
    private RequestForCoursework requestForCoursework;
    private Integer feedbackId;

    public UserOrder() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserOrderStatus getUserOrderStatus() {
        return userOrderStatus;
    }

    public void setUserOrderStatus(UserOrderStatus userOrderStatus) {
        this.userOrderStatus = userOrderStatus;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPrepayment() {
        return prepayment;
    }

    public void setPrepayment(BigDecimal prepayment) {
        this.prepayment = prepayment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getResultFileId() {
        return resultFileId;
    }

    public void setResultFileId(Integer resultFileId) {
        this.resultFileId = resultFileId;
    }

    public RequestForCoursework getRequestForCoursework() {
        return requestForCoursework;
    }

    public void setRequestForCoursework(RequestForCoursework requestForCoursework) {
        this.requestForCoursework = requestForCoursework;
    }

    public Integer getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(Integer feedbackId) {
        this.feedbackId = feedbackId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserOrder userOrder = (UserOrder) o;
        return Objects.equals(id, userOrder.id) &&
                userOrderStatus == userOrder.userOrderStatus &&
                Objects.equals(price, userOrder.price) &&
                Objects.equals(prepayment, userOrder.prepayment) &&
                Objects.equals(description, userOrder.description) &&
                Objects.equals(requestForCoursework, userOrder.requestForCoursework);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userOrderStatus, price, prepayment, description, requestForCoursework);
    }

    @Override
    public String toString() {
        return "UserOrder{" +
                "id=" + id +
                ", userOrderStatus=" + userOrderStatus +
                ", price=" + price +
                ", prepayment=" + prepayment +
                ", description='" + description + '\'' +
                '}';
    }
}
