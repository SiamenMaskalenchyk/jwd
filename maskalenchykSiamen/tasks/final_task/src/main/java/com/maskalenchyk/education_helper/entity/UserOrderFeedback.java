package com.maskalenchyk.education_helper.entity;

import java.util.Objects;

public class UserOrderFeedback implements Entity {

    private Integer id;
    private Byte grade;
    private String comment;

    public UserOrderFeedback() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Byte getGrade() {
        return grade;
    }

    public void setGrade(Byte grade) {
        this.grade = grade;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserOrderFeedback that = (UserOrderFeedback) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(grade, that.grade) &&
                Objects.equals(comment, that.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, grade, comment);
    }

    @Override
    public String toString() {
        return "UserOrderFeedback{" +
                "grade=" + grade +
                ", comment='" + comment + '\'' +
                '}';
    }
}
