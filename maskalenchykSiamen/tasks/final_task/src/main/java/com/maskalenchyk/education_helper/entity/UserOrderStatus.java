package com.maskalenchyk.education_helper.entity;

import java.util.stream.Stream;

public enum UserOrderStatus implements Entity {
    ORDER_REGISTERED(1),
    AUTHOR_ASSIGNED(2),
    WAITING_FOR_PREPAY(3),
    ORDER_IN_PROCESS(4),
    WAITING_FOR_PAYMENT(5),
    ORDER_IS_READY(6),
    CORRECTION_ORDER(7);

    Integer id;

    UserOrderStatus(Integer id) {
        this.id = id;
    }

    public static UserOrderStatus getUserOrderStatusById(Integer idUserOrderStatus) {
        return Stream.of(UserOrderStatus.values())
                .filter(d -> d.getId().equals(idUserOrderStatus))
                .findFirst()
                .orElse(ORDER_REGISTERED);
    }

    public Integer getId() {
        return id;
    }
}