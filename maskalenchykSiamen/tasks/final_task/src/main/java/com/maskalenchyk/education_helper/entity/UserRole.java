package com.maskalenchyk.education_helper.entity;

public enum UserRole implements Entity {
    ADMIN(1),
    CLIENT(2),
    AUTHOR(3);

    Integer id;

    UserRole(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
