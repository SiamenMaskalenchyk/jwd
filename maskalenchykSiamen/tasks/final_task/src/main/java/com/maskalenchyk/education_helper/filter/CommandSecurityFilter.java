package com.maskalenchyk.education_helper.filter;

import com.maskalenchyk.education_helper.command.CommandType;
import com.maskalenchyk.education_helper.command.security.SecurityContext;
import com.maskalenchyk.education_helper.command.security.SecurityContextImpl;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

//@WebFilter(filterName = "CommandSecurityFilter")
public class CommandSecurityFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        SecurityContext securityContext = SecurityContextImpl.getInstance();
        securityContext.setCurrentSessionId(servletRequest.getSession().getId());
        String command = servletRequest.getParameter("_command");

        Optional<CommandType> commandType = CommandType.of(command);

//        if (commandType.isPresent() && securityContext.canExecute(commandType.get().name())) {
//            chain.doFilter(request, response);
//        } else {
//
//        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
