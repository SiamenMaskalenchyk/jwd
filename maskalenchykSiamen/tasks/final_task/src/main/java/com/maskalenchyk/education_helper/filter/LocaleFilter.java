package com.maskalenchyk.education_helper.filter;


import com.maskalenchyk.education_helper.application.ApplicationConstants;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Filter used to initialize default locale
 */
@WebFilter(urlPatterns = {"/*"}, initParams = {
    @WebInitParam(name = "defaultLocale", value = "en"),
    @WebInitParam(name = "supportedLocales", value = "en,ru")
})
public class LocaleFilter implements Filter {

    private String defaultLocale;
    private List<String> supportedLocales;

    @Override
    public void init(FilterConfig filterConfig) {
        defaultLocale = filterConfig.getInitParameter("defaultLocale");
        supportedLocales = Arrays.asList(filterConfig.getInitParameter("supportedLocales").split(","));
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) request).getSession();
            String locale = (String) session.getAttribute(ApplicationConstants.LOCALE_PARAMETER);
            if (locale == null) {
                String browserLocale = request.getLocale().getLanguage();
                if (supportedLocales.contains(browserLocale)) {
                    session.setAttribute(ApplicationConstants.LOCALE_PARAMETER, browserLocale);
                } else {
                    session.setAttribute(ApplicationConstants.LOCALE_PARAMETER, defaultLocale);
                }
            }


        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        defaultLocale = null;
        supportedLocales = null;
    }
}
