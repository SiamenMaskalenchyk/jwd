package com.maskalenchyk.education_helper.service;

import com.maskalenchyk.education_helper.entity.PaperType;
import com.maskalenchyk.education_helper.entity.RequestForCoursework;
import com.maskalenchyk.education_helper.entity.RequestStatus;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;

import java.util.List;

public interface CourseworkRequestService {

    RequestForCoursework addCourseworkRequest(RequestForCoursework requestForCoursework) throws ServiceException;

    RequestForCoursework addCourseworkRequestWithNewUser(RequestForCoursework requestForCoursework, String email, String phoneNumber) throws ServiceException;

    RequestForCoursework getCourseworkRequest(Integer id) throws ServiceException;

    void updateCourseworkRequest(RequestForCoursework updatedRequest) throws ServiceException;

    void deleteCourseworkRequest(Integer id) throws ServiceException;

    List<RequestForCoursework> getAllCourseworkRequestByStatus(RequestStatus requestStatus) throws ServiceException;

    List<RequestForCoursework> getAllCourseworkRequestByPaperType(PaperType paperType) throws ServiceException;

    List<RequestForCoursework> getAllCourseworkRequest() throws ServiceException;
}
