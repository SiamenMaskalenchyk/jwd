package com.maskalenchyk.education_helper.service;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.dao.DaoException;
import com.maskalenchyk.education_helper.dal.dao.RequestForCourseworkDao;
import com.maskalenchyk.education_helper.dal.dao.TaskFileDao;
import com.maskalenchyk.education_helper.dal.transaction_manager.TransactionSupport;
import com.maskalenchyk.education_helper.dal.transaction_manager.Transactional;
import com.maskalenchyk.education_helper.entity.*;
import com.maskalenchyk.education_helper.service.exceptions.CourseworkRequestException;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;

@Bean
@TransactionSupport
public class CourseworkRequestServiceImpl implements CourseworkRequestService {

    private static final Logger LOGGER = Logger.getLogger(CourseworkRequestServiceImpl.class);
    private RequestForCourseworkDao requestForCourseworkDao;
    private TaskFileDao taskFileDao;
    private UserService userService;

    public CourseworkRequestServiceImpl(RequestForCourseworkDao requestForCourseworkDao, TaskFileDao taskFileDao, UserService userService) {
        this.requestForCourseworkDao = requestForCourseworkDao;
        this.taskFileDao = taskFileDao;
        this.userService = userService;
    }

    @Override
    @Transactional
    public RequestForCoursework addCourseworkRequest(RequestForCoursework requestForCoursework) throws ServiceException {
        try {
            RequestForCoursework addedRequest = requestForCourseworkDao.save(requestForCoursework);
            List<TaskFile> taskFilesFromRequest = requestForCoursework.getTaskFiles();
            if (taskFilesFromRequest != null) {
                List<TaskFile> addedTaskFiles = requestForCoursework.getTaskFiles();
                for (TaskFile taskFile : requestForCoursework.getTaskFiles()) {
                    taskFile.setIdRequestForCoursework(addedRequest.getId());
                    TaskFile addedTaskFile = taskFileDao.save(taskFile);
                    addedTaskFiles.add(addedTaskFile);
                }
                addedRequest.setTaskFiles(addedTaskFiles);
            }
            return addedRequest;
        } catch (DaoException e) {
            String errorMessage = "Request for coursework creating exception";
            LOGGER.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }

    @Override
    public RequestForCoursework addCourseworkRequestWithNewUser(RequestForCoursework requestForCoursework, String email, String phoneNumber) throws ServiceException {
        UserAccount userAccount = userService.createUserAccount(email, phoneNumber, UserRole.CLIENT);
        requestForCoursework.setClient(userAccount);
        return addCourseworkRequest(requestForCoursework);
    }

    @Override
    public RequestForCoursework getCourseworkRequest(Integer id) throws ServiceException {
        try {
            RequestForCoursework requestForCourseworkFromDb = requestForCourseworkDao.findById(id);
            UserAccount client = userService.getById(requestForCourseworkFromDb.getClient().getId());
            UserAccount author = userService.getById(requestForCourseworkFromDb.getAuthor().getId());
            requestForCourseworkFromDb.setClient(client);
            requestForCourseworkFromDb.setAuthor(author);
            return requestForCourseworkFromDb;
        } catch (DaoException e) {
            String errorMessage = "Coursework request searching exception";
            LOGGER.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }

    @Override
    @Transactional
    public void updateCourseworkRequest(RequestForCoursework updatedRequest) throws ServiceException {
        try {
            if (isCourseworkRequestExist(updatedRequest.getId())) {
                requestForCourseworkDao.update(updatedRequest);
            } else {
                String errorMessage = "Coursework request doesn't exist";
                LOGGER.error(errorMessage);
                throw new CourseworkRequestException(errorMessage, CourseworkRequestException.COURSEWORK_NOT_EXIST);
            }
        } catch (DaoException e) {
            String errorMessage = "Coursework request updating exception";
            LOGGER.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }

    @Override
    @Transactional
    public void deleteCourseworkRequest(Integer id) throws ServiceException {
        try {
            if (isCourseworkRequestExist(id)) {
                RequestForCoursework requestForCoursework = requestForCourseworkDao.findById(id);
                for (TaskFile taskFile : requestForCoursework.getTaskFiles()) {
                    taskFileDao.delete(taskFile);
                }
                requestForCourseworkDao.delete(requestForCoursework);
            } else {
                String errorMessage = "Coursework request doesn't exist";
                LOGGER.error(errorMessage);
                throw new CourseworkRequestException(errorMessage, CourseworkRequestException.COURSEWORK_NOT_EXIST);
            }
        } catch (DaoException e) {
            String errorMessage = "Coursework request deleting exception";
            LOGGER.error(errorMessage);
            throw new ServiceException(errorMessage, e);
        }
    }

    @Override
    public List<RequestForCoursework> getAllCourseworkRequestByStatus(RequestStatus requestStatus) {
        return null;
    }

    @Override
    public List<RequestForCoursework> getAllCourseworkRequestByPaperType(PaperType paperType) {
        return null;
    }

    @Override
    public List<RequestForCoursework> getAllCourseworkRequest() throws ServiceException {
        try {
            return requestForCourseworkDao.findAll();
        } catch (DaoException e) {
            String errorMessage = "Coursework request searching exception";
            LOGGER.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }

    private boolean isCourseworkRequestExist(Integer id) throws DaoException {
        return requestForCourseworkDao.findById(id) != null;
    }
}
