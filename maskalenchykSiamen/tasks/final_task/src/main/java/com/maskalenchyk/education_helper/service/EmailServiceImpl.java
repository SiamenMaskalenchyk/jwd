package com.maskalenchyk.education_helper.service;

import com.maskalenchyk.education_helper.application.ApplicationConstants;
import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;
import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Properties;

@Bean
public class EmailServiceImpl implements EmailService {

    private static final Logger LOGGER = Logger.getLogger(EmailServiceImpl.class);
    private static final String REGISTRATION_SUBJECT = "Hello new user";
    private static final String REGISTRATON_MESSAGE = "Welcome to education-helper service\n" +
        "Your login = ? \n" +
        "Your password: ? \n" +
        "Detail about your request you can see in your private cabinet.";
    private boolean isInitialized = false;
    private Session session;
    private String hostMail;

    @Override
    public void sendRegistrationEmail(String recipient, String recipientPassword) throws ServiceException {
        if (!isInitialized) {
            initialize();
        }
        Message message = prepareRegistrationMessage(session, recipient, recipientPassword);
        try {
            Transport.send(message);
        } catch (MessagingException e) {
            String errorMessage = "Sending email error";
            LOGGER.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }

    private Message prepareRegistrationMessage(Session session, String recipient, String recipientPassword) throws ServiceException {
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(this.hostMail));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
            message.setSubject(REGISTRATION_SUBJECT);
            message.setText(MessageFormat.format(REGISTRATON_MESSAGE, recipient, recipientPassword));
            return message;
        } catch (MessagingException e) {
            throw new ServiceException(e);
        }


    }

    //    @Override
//    public void sendEmail(String emailTarget, String userPassword) throws ServiceException {
//        if (!isInitialized) {
//            try {
//                initialize();
//            } catch (GeneralSecurityException e) {
//                e.printStackTrace();
//            }
//        }
//        try {
//            MimeMessage message = new MimeMessage(session);
//            message.setFrom(new InternetAddress(emailHostAddress));
//            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailTarget));
//            message.setSubject("Registration user");
//            String sendingMessage = "Welcome to education-helper service /n" +
//                    "Your login = " + emailTarget + "/n" +
//                    "Your password: " + userPassword + "/n" +
//                    "Detail about your request you can see in your private cabinet";
//            message.setText(sendingMessage);
//
////            MimeBodyPart mimeBodyPart = new MimeBodyPart();
////            mimeBodyPart.setContent(sendingMessage,"text/html");
////            Multipart multipart = new MimeMultipart();
////            multipart.addBodyPart(mimeBodyPart);
////            message.setContent(multipart);
//            Transport.send(message);
//            System.out.println("done");
//        } catch (MessagingException e) {
//            String errorMessage = "Sending mail error. ";
//            LOGGER.error(errorMessage, e);
//            throw new ServiceException(errorMessage, e);
//        }
//    }
//
    private void initialize() throws ServiceException {
        Properties properties = new Properties();
        try {
            properties.load(getClass().getResourceAsStream(ApplicationConstants.EMAIL_SERVICE_RESOURCE_FILE));
        } catch (IOException e) {
            String errorMessage = "Loading mail service properties error";
            LOGGER.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
        this.hostMail = properties.getProperty("mail.address");
        String password = properties.getProperty("mail.password");

        this.session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(hostMail, password);
            }
        });
        isInitialized = true;
    }
}
//    Properties props = new Properties();
//        props.put("mail.smtp.auth", "true");
//            props.put("mail.smtp.starttls.enable", "true");
//            props.put("mail.smtp.host", "smtp.gmail.com");
//            props.put("mail.smtp.port", "587");
//
//            String myAccount = "educationhelperproject@gmail.com";
//            String password = "1q2w3e4reduhlp";
//
//            Session session = Session.getInstance(props, new Authenticator() {
//@Override
//protected PasswordAuthentication getPasswordAuthentication() {
//    return new PasswordAuthentication(myAccount, password);
//    }
//    });