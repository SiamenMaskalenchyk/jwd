package com.maskalenchyk.education_helper.service;

import com.maskalenchyk.education_helper.entity.TaskFile;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;

import java.util.List;

public interface TaskFileService {

    List<TaskFile> getTaskFilesByRequestId(Integer idRequestForCoursework) throws ServiceException;

    TaskFile addTaskFile(TaskFile taskFile) throws ServiceException;
}
