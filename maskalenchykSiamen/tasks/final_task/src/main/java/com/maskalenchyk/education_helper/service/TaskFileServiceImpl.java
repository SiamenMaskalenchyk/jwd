package com.maskalenchyk.education_helper.service;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.dao.DaoException;
import com.maskalenchyk.education_helper.dal.dao.TaskFileDao;
import com.maskalenchyk.education_helper.entity.TaskFile;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;

@Bean
public class TaskFileServiceImpl implements TaskFileService {

    private static final Logger LOGGER = Logger.getLogger(TaskFileServiceImpl.class);
    private TaskFileDao taskFileDao;

    public TaskFileServiceImpl(TaskFileDao taskFileDao) {
        this.taskFileDao = taskFileDao;
    }

    @Override
    public List<TaskFile> getTaskFilesByRequestId(Integer idRequestForCoursework)throws ServiceException {
        try {
            return taskFileDao.getByRequestForCourseworkId(idRequestForCoursework);
        } catch (DaoException e) {
            String errorMessage = e.getMessage();
            LOGGER.error(errorMessage,e);
            throw new ServiceException(errorMessage,e);
        }
    }

    @Override
    public TaskFile addTaskFile(TaskFile taskFile) throws ServiceException {
        try {
            return taskFileDao.save(taskFile);
        } catch (DaoException e) {
            String errorMessage = e.getMessage();
            LOGGER.error(errorMessage,e);
            throw new ServiceException(errorMessage,e);
        }
    }
}
