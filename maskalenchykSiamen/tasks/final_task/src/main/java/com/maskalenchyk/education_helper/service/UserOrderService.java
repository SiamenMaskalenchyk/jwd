package com.maskalenchyk.education_helper.service;

import com.maskalenchyk.education_helper.entity.UserOrder;

public interface UserOrderService {

    UserOrder getUserOrderFromDB(Integer id);
}
