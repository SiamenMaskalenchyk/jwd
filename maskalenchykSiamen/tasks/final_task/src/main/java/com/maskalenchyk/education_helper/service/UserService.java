package com.maskalenchyk.education_helper.service;

import com.maskalenchyk.education_helper.entity.UserAccount;
import com.maskalenchyk.education_helper.entity.UserRole;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;

import java.util.List;

/**
 * Service which provides all needed user methods.
 */
public interface UserService {

    UserAccount getById(Integer id) throws ServiceException;

    UserAccount getByLogin(String login) throws ServiceException;

    UserAccount signIn(String login, String password) throws ServiceException;

    List<UserAccount> findAll() throws ServiceException;

    void deleteById(Integer id) throws ServiceException;

    void updateUserAccount(UserAccount userAccount) throws ServiceException;

    UserAccount createUserAccount(String email, String phoneNumber, UserRole userRole) throws ServiceException;
}
