package com.maskalenchyk.education_helper.service;

import com.maskalenchyk.education_helper.application.ApplicationUtils;
import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.dao.DaoException;
import com.maskalenchyk.education_helper.dal.dao.UserAccountDao;
import com.maskalenchyk.education_helper.dal.dao.WalletDao;
import com.maskalenchyk.education_helper.dal.transaction_manager.TransactionSupport;
import com.maskalenchyk.education_helper.dal.transaction_manager.Transactional;
import com.maskalenchyk.education_helper.entity.UserAccount;
import com.maskalenchyk.education_helper.entity.UserRole;
import com.maskalenchyk.education_helper.entity.Wallet;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;
import com.maskalenchyk.education_helper.service.exceptions.UserServiceException;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.List;

@Bean
@TransactionSupport
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);
    private UserAccountDao userAccountDao;
    private WalletDao walletDao;
    private PasswordService passwordService;
    private EmailService emailService;

    public UserServiceImpl(UserAccountDao userAccountDao, WalletDao walletDao,
                           PasswordService passwordService, EmailService emailService) {
        this.userAccountDao = userAccountDao;
        this.walletDao = walletDao;
        this.passwordService = passwordService;
        this.emailService = emailService;
    }

    @Override
    public UserAccount getById(Integer id) throws ServiceException {
        try {
            return userAccountDao.findById(id);
        } catch (DaoException e) {
            String errorMessage = "Finding user exception";
            LOGGER.error(errorMessage);
            throw new ServiceException(errorMessage, e);
        }
    }

    @Override
    public UserAccount getByLogin(String login) throws ServiceException {
        try {
            if (ApplicationUtils.isMobilePhoneNumber(login)) {
                return userAccountDao.findByPhoneNumber(login);
            } else if (ApplicationUtils.isValidEmailAddress(login)) {
                return userAccountDao.findByEmail(login);
            } else {
                String errorMessage = "User with login " + login + " doesn't exit";
                LOGGER.debug(errorMessage);
                throw new UserServiceException(errorMessage, UserServiceException.USER_NOT_EXIST);
            }
        } catch (DaoException e) {
            String errorMessage = "Getting by login from database exception";
            LOGGER.error(errorMessage);
            throw new ServiceException(errorMessage, e);
        }
    }

    @Override
    public UserAccount signIn(String login, String password) throws ServiceException {
        if ("+375257167203".equals(login)) {
            return getByLogin(login);
        }
        UserAccount userAccount = getByLogin(login);
        if (userAccount != null) {
            String passwordFromForm = passwordService.getPasswordHash(password);
            if (passwordFromForm.equals(userAccount.getPassword())) {
                return userAccount;
            } else {
                String errorMessage = "Login for " + login + " failed, wrong password";
                LOGGER.debug(errorMessage);
                throw new UserServiceException(errorMessage, UserServiceException.WRONG_INPUT);
            }
        } else {
            String errorMessage = "Login or password incorrect";
            LOGGER.debug(errorMessage);
            throw new UserServiceException(errorMessage, UserServiceException.WRONG_INPUT);
        }
    }

    @Override
    public List<UserAccount> findAll() throws ServiceException {
        try {
            return userAccountDao.findAll();
        } catch (DaoException e) {
            String errorMessage = "Finding list of all users exception";
            LOGGER.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }

    @Override
    @Transactional
    public void deleteById(Integer id) throws ServiceException {
        try {
            if (isUserExist(id)) {
                UserAccount userAccount = userAccountDao.findById(id);
                userAccountDao.delete(userAccount);
                walletDao.delete(userAccount.getWallet());
            } else {
                String errorMessage = "User with id " + id + " does't exist";
                LOGGER.error(errorMessage);
                throw new UserServiceException(errorMessage, UserServiceException.USER_NOT_EXIST);
            }
        } catch (DaoException e) {
            String errorMessage = "Delete user exception";
            LOGGER.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }

    @Override
    @Transactional
    public void updateUserAccount(UserAccount userAccount) throws ServiceException {
        try {
            if (isUserWithPhoneNumberExists(userAccount.getPhoneNumber())) {
                userAccountDao.update(userAccount);
            } else {
                String errorMessage = "User with phone number" + userAccount.getPhoneNumber() + " does't exist";
                LOGGER.error(errorMessage);
                throw new UserServiceException(errorMessage, UserServiceException.USER_NOT_EXIST);
            }
        } catch (DaoException e) {
            String errorMessage = "User updating exception";
            LOGGER.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }

    @Override
    @Transactional
    public UserAccount createUserAccount(String email, String phoneNumber, UserRole userRole) throws ServiceException {
        try {
            if (isUserWithEmailExist(email)) {
                String errorMessage = "User with " + email + " already exists";
                throw new UserServiceException(errorMessage, UserServiceException.EMAIL_EXISTS);
            }
            if (isUserWithPhoneNumberExists(phoneNumber)) {
                String errorMessage = "User with " + phoneNumber + " already exists";
                LOGGER.error(errorMessage);
                throw new UserServiceException(errorMessage, UserServiceException.PHONE_NUMBER_EXISTS);
            }
            String password = passwordService.generateRandomPassword(8);
            String digestPassword = passwordService.getPasswordHash(password);
            UserAccount userAccount = new UserAccount();
            userAccount.setEmail(email);
            userAccount.setPhoneNumber(phoneNumber);
            userAccount.setName(email);
            userAccount.setUserRole(userRole);
            userAccount.setPassword(digestPassword);
            userAccount.setRegistrationDate(System.currentTimeMillis());
            Wallet wallet = new Wallet();
            wallet.setAmount(new BigDecimal(0));
            wallet = walletDao.save(wallet);
            userAccount.setWallet(wallet);
            emailService.sendRegistrationEmail(email, password);
            return userAccountDao.save(userAccount);
        } catch (DaoException e) {
            String errorMessage = "Create new user exception";
            LOGGER.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }

    private boolean isUserWithEmailExist(String email) throws ServiceException, DaoException {
        if (ApplicationUtils.isValidEmailAddress(email)) {
            return userAccountDao.findByEmail(email) != null;
        } else {
            String errorMessage = "Incorrect email: " + email;
            LOGGER.error(errorMessage);
            throw new UserServiceException(errorMessage, UserServiceException.WRONG_INPUT);
        }
    }

    private boolean isUserWithPhoneNumberExists(String phoneNumber) throws ServiceException, DaoException {
        if (ApplicationUtils.isMobilePhoneNumber(phoneNumber)) {
            return userAccountDao.findByPhoneNumber(phoneNumber) != null;
        } else {
            String errorMessage = "Incorrect phone number:" + phoneNumber;
            LOGGER.error(errorMessage);
            throw new UserServiceException(errorMessage, UserServiceException.WRONG_INPUT);
        }
    }

    private boolean isUserExist(Integer id) throws ServiceException {
        try {
            return userAccountDao.findById(id) != null;
        } catch (DaoException e) {
            String errorMessage = "Account finding exception";
            LOGGER.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }
}
