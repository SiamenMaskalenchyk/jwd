package com.maskalenchyk.education_helper.service;

import com.maskalenchyk.education_helper.core.Bean;
import com.maskalenchyk.education_helper.dal.dao.DaoException;
import com.maskalenchyk.education_helper.dal.dao.WalletDao;
import com.maskalenchyk.education_helper.dal.transaction_manager.TransactionSupport;
import com.maskalenchyk.education_helper.dal.transaction_manager.Transactional;
import com.maskalenchyk.education_helper.entity.Wallet;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;
import com.maskalenchyk.education_helper.service.exceptions.WalletException;
import org.apache.log4j.Logger;

import java.math.BigDecimal;

@Bean
@TransactionSupport
public class WalletServiceImpl implements WalletService {

    private static final Logger LOGGER = Logger.getLogger(WalletServiceImpl.class);
    private WalletDao walletDao;

    public WalletServiceImpl(WalletDao walletDao) {
        this.walletDao = walletDao;
    }

    @Override
    @Transactional
    public Wallet modifyWalletAmount(Wallet wallet, BigDecimal value, boolean isIncrease) throws ServiceException {
        Wallet uptatedWallet = isIncrease ? increaseAmount(wallet, value) : decreaseAmount(wallet, value);
        try {
            walletDao.update(uptatedWallet);
            return uptatedWallet;
        } catch (DaoException e) {
            String errorMessage = "Update amount exception database exception";
            LOGGER.error(errorMessage);
            throw new ServiceException(errorMessage, e);
        }
    }

    private Wallet increaseAmount(Wallet wallet, BigDecimal value) {
        BigDecimal walletAmount = wallet.getAmount();
        BigDecimal updatedWalletAmount = walletAmount.add(value);
        wallet.setAmount(updatedWalletAmount);
        return wallet;
    }

    private Wallet decreaseAmount(Wallet wallet, BigDecimal value) throws WalletException {
        BigDecimal walletAmount = wallet.getAmount();
        if (walletAmount.compareTo(value) >= 0) {
            BigDecimal updatedWalletAmount = walletAmount.subtract(value);
            wallet.setAmount(updatedWalletAmount);
            return wallet;
        } else {
            throw new WalletException("Not enough money");
        }
    }
}
