package com.maskalenchyk.education_helper.service.exceptions;

public class CourseworkRequestException extends ServiceException {

    public static final int COURSEWORK_NOT_EXIST = 1;

    private int errorCode;

    public CourseworkRequestException(int errorCode) {
        this.errorCode = errorCode;
    }

    public CourseworkRequestException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public CourseworkRequestException(String message, Throwable cause, int errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public CourseworkRequestException(Throwable cause, int errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public CourseworkRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, int errorCode) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
