package com.maskalenchyk.education_helper.service.exceptions;

import com.maskalenchyk.education_helper.service.exceptions.ServiceException;

public class UserServiceException extends ServiceException {

    public static final int INCORRECT_PASSWORD = 1;
    public static final int USER_NOT_EXIST = 2;
    public static final int PHONE_NUMBER_EXISTS = 3;
    public static final int EMAIL_EXISTS = 4;
    public static final int WRONG_INPUT = 5;

    private int ErrorCode;

    public UserServiceException(int errorCode) {
        ErrorCode = errorCode;
    }

    public UserServiceException(String message, int errorCode) {
        super(message);
        ErrorCode = errorCode;
    }

    public UserServiceException(String message, Throwable cause, int errorCode) {
        super(message, cause);
        ErrorCode = errorCode;
    }

    public UserServiceException(Throwable cause, int errorCode) {
        super(cause);
        ErrorCode = errorCode;
    }

    public UserServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, int errorCode) {
        super(message, cause, enableSuppression, writableStackTrace);
        ErrorCode = errorCode;
    }

    public int getErrorCode() {
        return ErrorCode;
    }
}
