<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.maskalenchyk.education_helper.application.ApplicationConstants" %>
<%@ page import="com.maskalenchyk.education_helper.command.CommandType" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/fragment/links.jsp"/>
    <link href="../static/css/style-home.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/fragment/navbar.jsp"/>
cabinet Admin

<jsp:include page="/WEB-INF/jsp/fragment/footer.jsp"/>
</body>