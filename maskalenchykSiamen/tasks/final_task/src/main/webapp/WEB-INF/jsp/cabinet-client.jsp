<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="com.maskalenchyk.education_helper.application.ApplicationConstants" %>
<%@ page import="com.maskalenchyk.education_helper.command.CommandType" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="page_content"/>
<head>
    <jsp:include page="/WEB-INF/jsp/fragment/links.jsp"/>
    <link href="../static/css/style-home.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<header>
    <jsp:include page="/WEB-INF/jsp/fragment/navbar.jsp"/>
</header>

<main>
    <div class="container-fluid bg-light">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-information-tab" data-toggle="pill" href="#pills-information"
                   role="tab" aria-controls="pills-information" aria-selected="true">Информация</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-sent-tab" data-toggle="pill" href="#pills-sent" role="tab"
                   aria-controls="pills-sent" aria-selected="false">
                    Заказы <span class="badge badge-light">4</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-bought-tab" data-toggle="pill" href="#pills-bought" role="tab"
                   aria-controls="pills-bought" aria-selected="false">
                    Купленные работы <span class="badge badge-light">4</span>
                </a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-information" role="tabpanel"
                 aria-labelledby="pills-information-tab">
                <div class="container-fluid bg-light d-flex align-items-center justify-content-center">

                </div>
                <div class="container-fluid">
                    <div class="row d-flex justify-content-center text-center">
                        <h2>Добро пожаловать в личный кабинет пользователя!</h2>
                    </div>
                    <div class="row justify-content-center text-center">
                        <p>Для проверки состояния выполнения работы перейдите на вкладку заказы</p>
                    </div>
                    <div class="row justify-content-center text-center">
                        <p>Вы также можете просмотреть и скачать ранее заказанные или купленные работы, для этого
                            перейдите
                            на вкладку купленные работы</p>
                    </div>
                    <div class="row justify-content-md">
                        <div class="col-md-4 align-items-center">
                            <p>Для оформления заяки воспользуйтесь кнопкой сделать заявку</p>
                            <p>
                                <a href="#buyCoursework" class="btn btn-primary">Сделать заявку</a>
                            </p>
                        </div>
                        <div class="col-md-4 align-items-center">
                            <p>Для покупки готовых работ воспользуйтесь кнопкой готовые работы</p>
                            <p>
                                <a href="?${ApplicationConstants.COMMAND_NAME_PARAMETER}=${CommandType.SHOW_CABINET}" class="btn btn-primary">Готовые работы</a>
                            </p>
                        </div>
                        <div class="col-md-4 align-items-center">
                            <p>Деньги в кошельке: 1500</p>
                            <p>
                                <a href="#buyCoursework" class="btn btn-primary">Пополнить счёт</a>
                            </p>
                        </div>
                        <div class="col-md-4 align-items-center">
                            <p>Изменить настройки</p>
                            <p>
                                <a href="#properties" class="btn btn-primary" data-toggle="modal"
                                   data-target="#propertiesModal">Настройки</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-sent" role="tabpanel" aria-labelledby="pills-sent-tab">
                <div class="row d-flex justify-content-center text-center">
                    <p class="text">На данной вкладке Вы можете увидеть информацию по заказам </p>
                </div>

                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Имя</th>
                        <th scope="col">Фамилия</th>
                        <th scope="col">Username</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                    </tr>
                    </tbody>
                </table>


            </div>
            <div class="tab-pane fade" id="pills-bought" role="tabpanel" aria-labelledby="pills-bought-tab">
                <div class="row d-flex justify-content-center text-center">
                    <p class="text">На данной вкладке Вы можете просмотреть список купленных работ, а также их
                        скачать</p>
                </div>

                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Имя</th>
                        <th scope="col">Фамилия</th>
                        <th scope="col">Username</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                    </tr>
                    </tbody>
                </table>

            </div>

        </div>
    </div>
</main>
<jsp:include page="/WEB-INF/jsp/fragment/footer.jsp"/>
<!-- Modal Properties -->
<div class="modal fade" id="propertiesModal" tabindex="-1" role="dialog" aria-labelledby="propertiesModal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Настройки</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="container-fluid d-flex justify-content-center">

                    <div class="form-inline">
                        <label for="userName">Имя пользователя </label>
                        <input type="text" class="form-control" id="userName" aria-describedby="userName"
                               placeholder="Имя пользователя">
                        <button type="submit" class="btn btn-default">Изменить имя</button>
                    </div>
                    https://bootstrap-4.ru/docs/4.0/components/forms/


                    <div class="row">
                        <a href="#buyCoursework" class="btn btn-default">Изменить пароль</a>
                    </div>

                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-default">Login</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Properties -->
</body>