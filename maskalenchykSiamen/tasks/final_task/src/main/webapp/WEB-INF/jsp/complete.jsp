<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="page_content"/>
<head>
    <jsp:include page="/WEB-INF/jsp/fragment/links.jsp"/>
    <link href="../static/css/style-complete.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/fragment/header-complete.jsp"/>
<main>
    <div class="container-fluid bg-light">
        <div>
            Готовые работы
        </div>
    </div>
</main>
<jsp:include page="/WEB-INF/jsp/fragment/footer.jsp"/>
</body>

