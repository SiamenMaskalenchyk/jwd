<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="page_content"/>
    <jsp:include page="/WEB-INF/jsp/fragment/links.jsp"/>
    <link href="../static/css/style.css" rel="stylesheet" type="text/css"/>
    <head>
        <jsp:include page="/WEB-INF/jsp/fragment/navbar.jsp"/>
        <link href="../static/css/style-about.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <main>
        <div id="intro" class="view">
            <div class="mask rgba-grey-strong">
                <div class="container-fluid d-flex align-items-center justify-content-center h-100">
                    <div class="row d-flex justify-content-center text-center">
                        <div class="col-md-10">
                            <h2 class="display-4 font-weight-bold text-white bg-gradient-light pt-2 mb-2">
                                <fmt:message key="contacts.text.header"/>
                            </h2>
                            <h4 class="white-text my-4">
                                <fmt:message key="contacts.text.description"/>
                            </h4>
                            <hr class="hr-light"/>
                        </div>
                        <div class="col-md-10 text-white">
                            <ul class="list-group bg-transparent">
                                <li class="list-group-item bg-transparent">
                                    <em class="fa fa-phone mr-3"></em>
                                    +375257167203
                                </li>
                                <li class="list-group-item bg-transparent">
                                    <em class="fa fa-envelope mr-3"></em>
                                    educationhelper@gmail.com
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </main>
    </body>
    <jsp:include page="/WEB-INF/jsp/fragment/footer.jsp"/>
