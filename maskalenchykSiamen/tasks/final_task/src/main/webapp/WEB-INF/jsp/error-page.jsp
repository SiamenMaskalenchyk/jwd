<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.maskalenchyk.education_helper.application.ApplicationConstants" %>
<%@ page import="com.maskalenchyk.education_helper.command.CommandType" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="page_content"/>
<div class="container my-5 py-5 z-depth-1">

    <c:choose>
        <c:when test="${requestScope.get(ApplicationConstants.ERROR_TITLE)!=null}">
            <c:out value="${requestScope.get(ApplicationConstants.ERROR_TITLE)}"/>
        </c:when>
        <c:otherwise>
            Something wrong
        </c:otherwise>
    </c:choose>

    <a href="?${ApplicationConstants.COMMAND_NAME_PARAMETER}=${CommandType.VIEW_HOME_PAGE}"
       class="btn btn-primary btn-rounded mb-4">Home page</a>
</div>