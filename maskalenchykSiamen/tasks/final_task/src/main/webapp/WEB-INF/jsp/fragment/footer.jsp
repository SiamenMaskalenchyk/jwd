<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.maskalenchyk.education_helper.application.ApplicationConstants" %>
<%@ page import="com.maskalenchyk.education_helper.command.CommandType" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="page_content"/>
    <footer class="page-footer font-small unique-color-dark">
        <div class="container">
            <div class="row py-4 mt-3">
                <div class="col-md-3 col-lg-4 col-xl-1 mb-4">
                    <img src="../static/img/logo.png" width="50" height="50" alt="logo">
                </div>
                <div class="col-md-3 col-lg-4 col-xl-3 mb-4">
                    <h6 class="text font-weight-bold"><strong><fmt:message key="footer.label.company"/> </strong></h6>
                    <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                    <p><fmt:message key="footer.label.company_name"/></p>
                    <p><fmt:message key="footer.label.description"/><p>
                </div>
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                    <h6 class="text font-weight-bold"><strong><fmt:message key="footer.label.services"/> </strong></h6>
                    <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                    <p><a
                        href="?${ApplicationConstants.COMMAND_NAME_PARAMETER}=${CommandType.VIEW_HOME_PAGE}">
                        <fmt:message key="footer.button.request"/></a></p>
                    <p><a
                        href="?${ApplicationConstants.COMMAND_NAME_PARAMETER}=${CommandType.COMPLETE_WORKS}">
                        <fmt:message key="footer.button.buy"/></a></p>
                </div>
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                    <h6 class="text font-weight-bold"><strong><fmt:message key="footer.label.languages"/> </strong></h6>
                    <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                    <p><a href="?${ApplicationConstants.LOCALE_PARAMETER}=en">
                        <span><fmt:message key="footer.button.en"/></span></a>
                    </p>
                    <p><a href="?${ApplicationConstants.LOCALE_PARAMETER}=ru">
                         <span><fmt:message key="footer.button.ru"/>
                    </a></p>
                </div>
                <div class="col-md-4 col-lg-3 col-xl-3">
                    <h6 class="text font-weight-bold"><strong><fmt:message key="footer.label.contacts"/></strong></h6>
                    <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                    <p><em class="fa fa-home mr-3"></em><fmt:message key="footer.label.city"/></p>
                    <p><em class="fa fa-envelope mr-3"></em>siamenmaskalenchyk@gmail.com</p>
                    <p><em class="fa fa-phone mr-3"></em>+375-25-716-72-03</p>
                </div>
            </div>
        </div>
    </footer>