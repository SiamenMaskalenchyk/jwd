<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="page_content"/>
<header>
    <jsp:include page="navbar.jsp"/>
    <div id="intro" class="view">
        <div class="mask rgba-grey-strong">
            <div class="container-fluid d-flex align-items-center justify-content-center h-100">
                <div class="row d-flex justify-content-center text-center">
                    <div class="col-md-10">
                        <h2 class="display-4 font-weight-bold text-white bg-gradient-light pt-2 mb-2">
                            <fmt:message key="header_complete.text.page_name"/>
                        </h2>
                        <hr class="hr-light">
                        <h4 class="white-text my-4">
                            <fmt:message key="header_complete.text.description"/>
                        </h4>
                    </div>
                    <form>
                        <div class="form-row align-items-center justify-content-center mb-2">
                            <div class="col-auto">
                                <label class="sr-only" for="inlineFormInput1">Name</label>
                                <input type="text" class="form-control mb-2" id="inlineFormInput1"
                                       placeholder="Jane Doe">
                            </div>
                            <div class="col-auto">
                                <label class="sr-only" for="inlineFormInput2">Name</label>
                                <input type="text" class="form-control mb-2" id="inlineFormInput2"
                                       placeholder="Jane Doe">
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-primary mb-2">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>