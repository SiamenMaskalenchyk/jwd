<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="page_content"/>
    <header>
        <jsp:include page="navbar.jsp"/>
        <div id="intro" class="view">
            <div class="mask rgba-grey-strong">
                <div class="container-fluid d-flex align-items-center justify-content-center h-100">
                    <div class="row d-flex justify-content-center text-center">
                        <div class="col-md-10">
                            <h2 class="display-4 font-weight-bold text-white bg-gradient-light pt-5 mb-2">
                                <fmt:message key="header_home.text.name"/>
                            </h2>
                            <hr class="hr-light">
                            <h4 class="white-text my-4">
                                <fmt:message key="header_home.text.description"/>
                            </h4>
                            <h4 class="display-4 font-weight-bold white-text pt-4 mb-2">
                                <fmt:message key="header_home.text.message"/>
                            </h4>
                            <a href="" class="btn btn-primary btn-rounded mb-4" data-toggle="modal"
                               data-target="#modalOrder"><fmt:message key="header_home.button.order"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>