<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.maskalenchyk.education_helper.application.ApplicationConstants" %>
<%@ page import="com.maskalenchyk.education_helper.command.CommandType" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="page_content"/>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top scrolling-navbar">
        <a href="#" class="navbar-brand">
            <img src="../static/img/logo.png" width="40" height="40" alt="logo">
        </a>
        <a href="?${ApplicationConstants.COMMAND_NAME_PARAMETER}=${CommandType.VIEW_HOME_PAGE}"
           class="navbar-brand active">
            <fmt:message key="navbar.name"/> </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicNav"
                aria-controls="basicNav" aria-expanded="false" aria-label="Toggle Navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="basicNav">
            <ul class="navbar-nav mr-auto smooth-scroll">
                <li class="nav-item">
                    <a href="?${ApplicationConstants.COMMAND_NAME_PARAMETER}=${CommandType.COMPLETE_WORKS}"
                       class="nav-link waves-effect waves-light"><fmt:message key="navbar.button.buy"/> </a>
                </li>
                <li class="nav-item">
                    <a href="?${ApplicationConstants.COMMAND_NAME_PARAMETER}=${CommandType.CONTACTS}"
                       class="nav-link waves-effect waves-light"><fmt:message key="navbar.button.contacts"/> </a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto smooth-scroll">
                <c:choose>
                    <c:when test="${sessionScope.user!=null}">
                        <li class="nav-item">
                            <a href="?${ApplicationConstants.COMMAND_NAME_PARAMETER}=${CommandType.SHOW_CABINET}"
                               class="nav-link waves-effect waves-light"><fmt:message key="navbar.button.cabinet"/></a>
                        </li>
                        <li class="nav-item">
                            <a href="?${ApplicationConstants.COMMAND_NAME_PARAMETER}=${CommandType.LOGOUT}"
                               class="nav-link waves-effect waves-light"><fmt:message key="navbar.button.logout"/></a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="nav-item">
                            <a class="nav-link waves-effect waves-light" data-toggle="modal"
                               data-target="#loginModal"><fmt:message key="navbar.button.login"/></a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </ul>
        </div>
    </nav>