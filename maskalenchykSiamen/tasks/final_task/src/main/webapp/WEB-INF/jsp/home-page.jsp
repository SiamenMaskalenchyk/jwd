<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.maskalenchyk.education_helper.application.ApplicationConstants" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:choose>
    <c:when test="${not empty param.get(ApplicationConstants.LOCALE_PARAMETER)}">
        <c:set var="locale" scope="session" value="${param.get(ApplicationConstants.LOCALE_PARAMETER)}"/>
    </c:when>
</c:choose>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="page_content"/>
    <!DOCTYPE html>
    <html lang="${sessionScope.locale}">
    <head>
        <jsp:include page="/WEB-INF/jsp/fragment/links.jsp"/>
        <link href="../static/css/style-home.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>

    <jsp:include page="/WEB-INF/jsp/fragment/header-home.jsp"/>

    <main>
        <div class="container-fluid bg-light">
            <section id="feature1" class="text-center">
                <div class="row d-flex justify-content-center mb-4">
                    <div class="col-md-8">
                        <p class="text"><fmt:message key="main_page.text.description"/></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 mb-5">
                        <em class="fa fa-book-open fa-4x"></em>
                        <h3 class="my-4 font-weight-bold"><fmt:message key="main_page.label.possibility_order"/></h3>
                        <p class="text"><fmt:message key="main_page.text.possibility_order"/></p>
                    </div>
                    <div class="col-md-4 mb-5">
                        <em class="fa fa-archive fa-4x"></em>
                        <h3 class="my-4 font-weight-bold"><fmt:message key="main_page.label.possibility_base"/></h3>
                        <p class="text"><fmt:message key="main_page.text.possibility_base"/></p>
                    </div>
                    <div class="col-md-4 mb-5">
                        <em class="fa fa-handshake fa-4x"></em>
                        <h3 class="my-4 font-weight-bold"><fmt:message key="main_page.label.possibility_cooperation"/></h3>
                        <p class="text"><fmt:message key="main_page.text.possibility_base"/></p>
                    </div>
                </div>
            </section>

            <hr class="my-5">
            <section id="operatingProcedure" class="text-center">
                <h4 class="mb-5 font-weight-bold"><fmt:message key="main_page.label.steps_header"/> </h4>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col">
                            <em class="fa fa-list-alt fa-3x"></em>
                            <h6 class="my-3 font-weight-bold"><fmt:message key="main_page.label.step1"/></h6>
                            <p class="text"><fmt:message key="main_page.text.step1"/></p>
                        </div>
                        <div class="col">
                            <em class="fa fa-user-circle fa-3x"></em>
                            <h6 class="my-3 font-weight-bold"><fmt:message key="main_page.label.step2"/></h6>
                            <p class="text"><fmt:message key="main_page.text.step2"/></p>
                        </div>
                        <div class="col">
                            <em class="fa fa-credit-card fa-3x"></em>
                            <h6 class="my-3 font-weight-bold"><fmt:message key="main_page.label.step3"/></h6>
                            <p class="text"><fmt:message key="main_page.text.step3"/></p>
                        </div>
                        <div class="col">
                            <em class="fa fa-pen fa-3x"></em>
                            <h6 class="my-3 font-weight-bold"><fmt:message key="main_page.label.step4"/></h6>
                            <p class="text"><fmt:message key="main_page.text.step4"/></p>
                        </div>
                        <div class="col">
                            <em class="fa fa-comment  fa-3x"></em>
                            <h6 class="my-3 font-weight-bold"><fmt:message key="main_page.label.step5"/></h6>
                            <p class="text"><fmt:message key="main_page.text.step5"/></p>
                        </div>
                        <div class="col">
                            <em class="fa fa-credit-card fa-3x"></em>
                            <h6 class="my-3 font-weight-bold"><fmt:message key="main_page.label.step6"/></h6>
                            <p class="text"><fmt:message key="main_page.text.step6"/></p>
                        </div>
                        <div class="col">
                            <em class="fa fa-check fa-3x"></em>
                            <h6 class="my-3 font-weight-bold"><fmt:message key="main_page.label.step7"/></h6>
                            <p class="text"><fmt:message key="main_page.text.step7"/></p>
                        </div>
                    </div>
                </div>
                <a href="" class="btn btn-primary btn-rounded mb-4" data-toggle="modal"
                   data-target="#modalOrder"><fmt:message key="main_page.button.order"/> <em class="fa fa-book"> </em></a>
            </section>

            <hr class="my-5">
            <div class="container-fluid">
                <div class="row text-center">
                    <div class="col-xs-12 col-sm-3"><img src="../static/img/responsive_features_clock.png" alt=""
                                                         class="w-100">
                        <h4 class="my-4 font-weight"><fmt:message key="main_page.label.advantage_fast"/></h4>
                        <p class="purple-text"><fmt:message key="main_page.text.advantage_fast"/></p>
                    </div>
                    <div class="col-xs-12 col-sm-3"><img src="../static/img/responsive_features_correction.png" alt=""
                                                         class="w-100">
                        <h4 class="my-4 font-weight"><fmt:message key="main_page.label.advantage_support"/></h4>
                        <p class="purple-text"><fmt:message key="main_page.text.advantage_support"/></p>
                    </div>
                    <div class="col-xs-12 col-sm-3"><img src="../static/img/responsive_features_specialist.png" alt=""
                                                         class="w-100">
                        <h4 class="my-4 font-weight"><fmt:message key="main_page.label.advantage_best"/></h4>
                        <p class="purple-text"><fmt:message key="main_page.text.advantage_base"/></p>
                    </div>
                    <div class="col-xs-12 col-sm-3"><img src="../static/img/responsive_features_archive.png" alt=""
                                                         class="w-100">
                        <h4 class="my-4 font-weight"><fmt:message key="main_page.label.advantage_base"/></h4>
                        <p class="purple-text"><fmt:message key="main_page.text.advantage_best"/></p>
                    </div>
                </div>
            </div>

            https://bootstrap-4.ru/docs/4.1/layout/grid/
            https://courseworkz.com/services/coursework-writing-service/

        </div>
    </main>
    <jsp:include page="/WEB-INF/jsp/fragment/footer.jsp"/>

    <jsp:include page="/WEB-INF/jsp/modal/login.jsp"/>
    <jsp:include page="/WEB-INF/jsp/modal/order-form.jsp"/>
    <c:if test="${requestScope.get(ApplicationConstants.MODAL_TITLE_PARAMETER)!=null}">
        <jsp:include page="/WEB-INF/jsp/modal/message.jsp"/>
    </c:if>

    </body>
    </html>