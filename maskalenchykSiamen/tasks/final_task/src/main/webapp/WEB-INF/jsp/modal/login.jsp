<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.maskalenchyk.education_helper.application.ApplicationConstants" %>
<%@ page import="com.maskalenchyk.education_helper.command.CommandType" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:bundle basename="page_content" prefix="login.">
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="${pageContext.request.contextPath}/" method="post">
                    <input type="hidden" name="${ApplicationConstants.COMMAND_NAME_PARAMETER}"
                           value="${CommandType.SIGN_IN}">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold"><fmt:message key="label.sign_in"/></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <c:if test="${requestScope.containsKey(ApplicationConstants.ERROR_LOGIN)}">
                            <div class="d-flex flex-row bd-highlight mb-3">
                                <em class="fa fa-exclamation text-danger">
                                    <c:out value="${requestScope.get(ApplicationConstants.ERROR_LOGIN)}"/>
                                </em>

                            </div>
                        </c:if>
                        <div class="md-form mb-5">
                            <em class="fas fa-user prefix grey-text"></em>
                            <input type="text" id="loginForm-login" name="${ApplicationConstants.LOGIN_PARAMETER}"
                                <c:if test="${requestScope.containsKey(ApplicationConstants.ERROR_LOGIN)}">
                                    value=<c:out value="${requestScope.get(ApplicationConstants.LOGIN_PARAMETER)}"/>
                                </c:if>
                                class="form-control validate" required/>
                            <label data-error="wrong" data-success="right" for="loginForm-login">
                                <fmt:message key="text.login"/></label>
                        </div>

                        <div class="md-form mb-4">
                            <em class="fas fa-lock prefix grey-text"></em>
                            <input type="password" id="loginForm-pass" name="password" minlength="6"
                                   class="form-control validate" required>
                            <label data-error="wrong" data-success="right" for="loginForm-pass"><fmt:message
                                key="text.password"/></label>
                        </div>

                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-default"><fmt:message key="button.login"/></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <c:if test="${requestScope.get(ApplicationConstants.ERROR_LOGIN)!=null}">
        <script>
            $(document).ready(function () {
                $("#loginModal").modal('show');
            });
        </script>
    </c:if>
</fmt:bundle>