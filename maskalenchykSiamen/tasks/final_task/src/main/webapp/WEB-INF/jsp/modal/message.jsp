<%@ page contentType="text/html;charset=UTF-8" %>
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="messageModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Signed in successfully</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <h4 class="modal-title">Welcome to education-helper service</h4>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#messageModal").modal('show');
    });
</script>