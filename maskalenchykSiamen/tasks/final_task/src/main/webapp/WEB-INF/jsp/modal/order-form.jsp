<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.maskalenchyk.education_helper.application.ApplicationConstants" %>
<%@ page import="com.maskalenchyk.education_helper.command.CommandType" %>
<%@ page import="com.maskalenchyk.education_helper.entity.Disciple" %>
<%@ page import="com.maskalenchyk.education_helper.entity.PaperType" %>

<div class="modal fade" id="modalOrder" tabindex="-1" role="dialog" aria-labelledby="modalOrder" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="${pageContext.request.contextPath}/" method="post" enctype="multipart/form-data">
                <input type="hidden" name="${ApplicationConstants.COMMAND_NAME_PARAMETER}"
                       value="${CommandType.CREATE_NEW_COURSEWORK_REQUEST}">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">Sign in</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="form-group">
                    <div class="modal-body mx-3">
                        <div class="form-group">
                            <label for="requestForm-phone">Your phone number</label>
                            <input type="text" id="requestForm-phone"
                                   name="${ApplicationConstants.REQUEST_PHONE_NUMBER_PARAMETER}"
                                   class="form-control validate" required/>
                            <small class="text-muted">
                                * required
                            </small>
                        </div>
                        <div class="form-group">
                            <label for="requestForm-email">Your email</label>
                            <input type="email" id="requestForm-email"
                                   name="${ApplicationConstants.REQUEST_EMAIL_PARAMETER}"
                                   class="form-control validate"
                                   required/>
                            <small class="text-muted">
                                * required
                            </small>
                        </div>
                        <div class="form-group">
                            <label for="requestForm-theme">Theme</label>
                            <input type="text" id="requestForm-theme"
                                   name="${ApplicationConstants.REQUEST_THEME_PARAMETER}"
                                   class="form-control validate"
                                   required/>
                            <small class="text-muted">
                                * required
                            </small>
                        </div>
                        <div class="form-group">
                            <label for="requestForm-deadline">Deadline</label>
                            <div class="col-xs-10">
                                <input type="date" id="requestForm-deadline"
                                       name="${ApplicationConstants.REQUEST_DEADLINE_PARAMETER}" class="form-control"
                                       required/>
                            </div>
                            <small class="text-muted">
                                * required
                            </small>
                        </div>
                        <div class="form-group">
                            <label for="requestForm-disciple">Disciple</label>
                            <select class="custom-select" id="requestForm-disciple"
                                    name="${ApplicationConstants.REQUEST_DISCIPLE_PARAMETER}">
                                <c:forEach var="entry" items="${Disciple.values()}">
                                    <c:out value="<option>${entry.name()}</option>" escapeXml="false"/>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="requestForm-type">Type of paper</label>
                            <select class="custom-select" id="requestForm-type"
                                    name="${ApplicationConstants.REQUEST_TYPE_OF_PAPER_PARAMETER}">
                                <c:forEach var="entry" items="${PaperType.values()}">
                                    <c:out value="<option>${entry.name()}</option>" escapeXml="false"/>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="requestForm-description">Description</label>
                            <textarea class="form-control" id="requestForm-description"
                                      name="${ApplicationConstants.REQUEST_DESCRIPTION_PARAMETER}"
                                      rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="file" name="${ApplicationConstants.REQUEST_TASK_FILES}"/>
                                как назначить кнопку для файлов
                        </div>
                    </div>
                </div>


                <div class="modal-footer d-flex justify-content-center">
                    <%--<input type="submit" value="Submit">--%>
                    <button class="btn btn-default">Send request</button>
                </div>
            </form>
        </div>
    </div>
</div>


<%--<div class="modal fade" id="modalOrder" tabindex="-1" role="dialog" aria-labelledby="modalOrder" aria-hidden="true">--%>
<%--<div class="modal-dialog" role="document">--%>
<%--<div class="modal-content">--%>
<%--<form action="${pageContext.request.contextPath}/" method="post" enctype="multipart/form-data">--%>
<%--<form action="${pageContext.request.contextPath}/" method="post" enctype="multipart/form-data">--%>

<%--<input type="hidden" name="${ApplicationConstants.COMMAND_NAME_PARAMETER}"--%>
<%--value="${CommandType.CREATE_NEW_COURSEWORK_REQUEST}">--%>
<%--<div class="modal-header text-center">--%>
<%--<h4 class="modal-title w-100 font-weight-bold">New Order</h4>--%>
<%--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--%>
<%--<span aria-hidden="true">&times;</span>--%>
<%--</button>--%>
<%--</div>--%>

<%--<div class="modal-body mx-3">--%>
<%--<div class="form-group">--%>
<%--<label for="requestForm-phone">Your phone number</label>--%>
<%--<input type="text" id="requestForm-phone"--%>
<%--name="${ApplicationConstants.REQUEST_PHONE_NUMBER_PARAMETER}"--%>
<%--class="form-control validate" required/>--%>
<%--<small class="text-muted">--%>
<%--* required--%>
<%--</small>--%>
<%--</div>--%>


<%--<div class="form-group">--%>
<%--<label for="requestForm-email">Your email</label>--%>
<%--<input type="email" id="requestForm-email"--%>
<%--name="${ApplicationConstants.REQUEST_EMAIL_PARAMETER}"--%>
<%--class="form-control validate"--%>
<%--required/>--%>
<%--<small class="text-muted">--%>
<%--* required--%>
<%--</small>--%>
<%--</div>--%>


<%--<div class="form-group">--%>
<%--<label for="requestForm-theme">Theme</label>--%>
<%--<input type="text" id="requestForm-theme" name="${ApplicationConstants.REQUEST_THEME_PARAMETER}"--%>
<%--class="form-control validate" --%>
<%--required/>--%>
<%--<small class="text-muted">--%>
<%--* required--%>
<%--</small>--%>
<%--</div>--%>
<%--<div class="form-group">--%>
<%--<label for="requestForm-deadline">Deadline</label>--%>
<%--<div class="col-xs-10">--%>
<%--<input type="date" id="requestForm-deadline"--%>
<%--name="${ApplicationConstants.REQUEST_DEADLINE_PARAMETER}" class="form-control"--%>
<%--required/>--%>
<%--</div>--%>
<%--<small class="text-muted">--%>
<%--* required--%>
<%--</small>--%>
<%--</div>--%>
<%--<div class="form-group">--%>
<%--<label for="requestForm-disciple">Disciple</label>--%>
<%--<select class="custom-select" id="requestForm-disciple"--%>
<%--name="${ApplicationConstants.REQUEST_DISCIPLE_PARAMETER}">--%>
<%--<c:forEach var="entry" items="${Disciple.values()}">--%>
<%--<c:out value="<option>${entry.name()}</option>" escapeXml="false"/>--%>
<%--</c:forEach>--%>
<%--</select>--%>
<%--&lt;%&ndash;</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;<div class="form-group">&ndash;%&gt;--%>
<%--<label for="requestForm-type">Type of paper</label>--%>
<%--<select class="custom-select" id="requestForm-type"--%>
<%--name="${ApplicationConstants.REQUEST_TYPE_OF_PAPER_PARAMETER}">--%>
<%--<c:forEach var="entry" items="${PaperType.values()}">--%>
<%--<c:out value="<option>${entry.name()}</option>" escapeXml="false"/>--%>
<%--</c:forEach>--%>
<%--</select>--%>
<%--</div>--%>
<%--<form action = "${pageContext.request.contextPath}/" method = "post"--%>
<%--enctype = "multipart/form-data">--%>
<%--<input type = "file" name = "file" size = "50" />--%>
<%--<br />--%>
<%--<input type = "submit" value = "Upload File" />--%>
<%--</form>--%>


<%--<form method="post" enctype="multipart/form-data">--%>
<%--<input type="file" name="${ApplicationConstants.REQUEST_TASK_FILES}" />--%>
<%--</form>--%>

<%--&lt;%&ndash;<div class="form-group">&ndash;%&gt;--%>
<%--<label for="requestForm-description">Description</label>--%>
<%--<textarea class="form-control" id="requestForm-description"--%>
<%--name="${ApplicationConstants.REQUEST_DESCRIPTION_PARAMETER}"--%>
<%--rows="3"></textarea>--%>
<%--&lt;%&ndash;</div>&ndash;%&gt;--%>
<%--</div>--%>
<%--<div class="modal-footer d-flex justify-content-center">--%>
<%--<input type="submit" value="Send"/>--%>
<%--<button class="btn btn-default">Send request</button>--%>
<%--</div>--%>

<%--</form>--%>
<%--</div>--%>
<%--</div>--%>
<%--</div>--%>
