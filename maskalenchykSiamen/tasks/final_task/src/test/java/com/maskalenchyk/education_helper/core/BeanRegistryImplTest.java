package com.maskalenchyk.education_helper.core;

import com.maskalenchyk.education_helper.command.view.HomePageCommand;
import com.maskalenchyk.education_helper.command.ServletCommand;
import com.maskalenchyk.education_helper.command.CommandType;
import org.junit.Assert;
import org.junit.Test;

public class BeanRegistryImplTest {

    @Test
    public void shouldRegisterAndReturnByInterface() {
        BeanRegistry beanRegistry = new BeanRegistryImpl();
        ServletCommand servletCommand = new HomePageCommand();
        beanRegistry.registerBean(servletCommand);
        ServletCommand foundCommand = beanRegistry.getBean(ServletCommand.class);
        Assert.assertEquals(servletCommand, foundCommand);
    }

    @Test
    public void shouldRegisterAndReturnByClass() {
        BeanRegistry beanRegistry = new BeanRegistryImpl();
        ServletCommand servletCommand = new HomePageCommand();
        beanRegistry.registerBean(servletCommand);
        ServletCommand foundCommand = beanRegistry.getBean(HomePageCommand.class);
        Assert.assertEquals(servletCommand, foundCommand);
    }

    @Test
    public void shouldRegisterAndReturnByName() {
        BeanRegistry beanRegistry = new BeanRegistryImpl();
        ServletCommand servletCommand = new HomePageCommand();
        beanRegistry.registerBean(servletCommand);
        ServletCommand foundCommand = beanRegistry.getBean(CommandType.VIEW_HOME_PAGE.name());
        Assert.assertEquals(servletCommand, foundCommand);
    }

    @Test(expected = ApplicationCoreException.class)
    public void shouldBeApplicationCoreException() {
        BeanRegistry beanRegistry = new BeanRegistryImpl();
        ServletCommand servletCommand1 = new HomePageCommand();
        ServletCommand servletCommand2 = new HomePageCommand();
        beanRegistry.registerBean(servletCommand1);
        beanRegistry.registerBean(servletCommand2);
    }
}