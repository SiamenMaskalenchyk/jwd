package com.maskalenchyk.education_helper.dal.dao;

import com.maskalenchyk.education_helper.application.ApplicationContext;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManager;
import com.maskalenchyk.education_helper.dal.connection_manager.ConnectionManagerException;
import com.maskalenchyk.education_helper.entity.Wallet;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.SQLException;

public class WalletDaoImplTest {

    @Before
    public void createTable() throws SQLException, ConnectionManagerException {
        ApplicationContext.initialize();
        String createTableSQLCommand = "CREATE TABLE wallet (" +
                " id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY," +
                " wallet_amount DECIMAL(10,2)" +
                ")";
        executeSql(createTableSQLCommand);
    }

    @After
    public void dropTable() throws SQLException, ConnectionManagerException {
        String dropTableSQLCommand = "DROP TABLE wallet";
        executeSql(dropTableSQLCommand);
        ApplicationContext.getInstance().destroy();
    }

    @Test
    public void shouldCreateNewWallet() throws DaoException {
        WalletDao walletDao = ApplicationContext.getInstance().getBean(WalletDao.class);
        Assert.assertNotNull(walletDao);
        Wallet wallet = new Wallet();
        wallet.setAmount(new BigDecimal(1500));
        Wallet wallet1 = walletDao.save(wallet);
        Assert.assertNotNull(wallet1);

    }

    private void executeSql(String sql) throws SQLException, ConnectionManagerException {
        ConnectionManager connectionManager = ApplicationContext.getInstance().getBean(ConnectionManager.class);
        connectionManager.doWithConnection(sql, ( statement) -> {
            try {
                statement.executeUpdate();
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });


    }
}