package com.maskalenchyk.education_helper.service;

import com.maskalenchyk.education_helper.service.exceptions.ServiceException;
import org.junit.Assert;
import org.junit.Test;

import javax.mail.MessagingException;

import static junit.framework.TestCase.fail;

public class EmailServiceImplTest {

    private EmailService emailService = new EmailServiceImpl();

    @Test
    public void shouldSendMessage() {

        try {
            emailService.sendRegistrationEmail("cex4agat@mail.ru", "123456");
        } catch (ServiceException e) {
            e.printStackTrace();
            fail();
        }
        Assert.assertEquals(1, 1);
    }
}