package com.maskalenchyk.education_helper.service;

import com.maskalenchyk.education_helper.service.exceptions.ServiceException;
import org.junit.Assert;
import org.junit.Test;

public class PasswordServiceImplTest {


    @Test
    public void generatedPasswordShouldBeCertainLength() throws ServiceException {
        PasswordService passwordService = new PasswordServiceImpl();
        int length1 = 5;
        int length2 = 10;
        int length3 = 3;
        String password1 = passwordService.generateRandomPassword(length1);
        String password2 = passwordService.generateRandomPassword(length2);
        String password3 = passwordService.generateRandomPassword(length3);
        Assert.assertEquals(length1, password1.length());
        Assert.assertEquals(length2, password2.length());
        Assert.assertEquals(length3, password3.length());
    }

    @Test
    public void checkPasswordHashes() throws ServiceException {
        PasswordService passwordService = new PasswordServiceImpl();
        String password = "1qaz2wsx";
        String rightPassword = "1qaz2wsx";
        String wrongPassword = "password";
        String wrongPasswordUpperCase = "1QAZ2WSX";
        String passwordHashString = passwordService.getPasswordHash(password);
        Assert.assertEquals(passwordHashString, passwordService.getPasswordHash(rightPassword));
        Assert.assertNotEquals(passwordHashString, passwordService.getPasswordHash(wrongPassword));
        Assert.assertNotEquals(passwordHashString, passwordService.getPasswordHash(wrongPasswordUpperCase));
    }
}