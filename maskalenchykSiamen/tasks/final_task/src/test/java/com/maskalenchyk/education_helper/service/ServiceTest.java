package com.maskalenchyk.education_helper.service;

import com.maskalenchyk.education_helper.application.ApplicationContext;
import com.maskalenchyk.education_helper.entity.*;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ServiceTest {

    private UserService userService;
    private CourseworkRequestService courseworkRequestService;

    @Before
    public void init() {
        ApplicationContext.initialize();
        userService = ApplicationContext.getInstance().getBean(UserService.class);
        courseworkRequestService = ApplicationContext.getInstance().getBean(CourseworkRequestServiceImpl.class);
    }

    @Test
    public void userServiceTest() throws ServiceException {
        Assert.assertNotNull(userService);
        UserAccount userAccount = userService.createUserAccount("java@gmail.ru", "+375336755643", UserRole.CLIENT);
        Assert.assertNotNull(userAccount);
        Integer userAccountId = userAccount.getId();
        userService.deleteById(userAccountId);
        UserAccount foundUserAccount = userService.getById(userAccountId);
        Assert.assertNull(foundUserAccount);
        foundUserAccount = userService.getById(3);
        Assert.assertNotNull(foundUserAccount);
        foundUserAccount.setName("Hello form java test");
        userService.updateUserAccount(foundUserAccount);
        Assert.assertEquals("Hello form java test",userService.getById(3).getName());
        List<UserAccount> userAccountList = userService.findAll();
        Assert.assertNotNull(userAccountList);
    }

    @Test
    public void courseworkRequestTest(){
    Assert.assertNotNull(courseworkRequestService);
        RequestForCoursework requestForCoursework = initRequest();
    }

    @Test
    public void addNewCourseworkWithNewClientRequestTest() throws ServiceException {
        Assert.assertNotNull(courseworkRequestService);
        RequestForCoursework requestForCoursework = initRequest();
        RequestForCoursework addedRequest = courseworkRequestService.addCourseworkRequestWithNewUser(requestForCoursework,"test@mail.com", "+375447658888");
        requestForCoursework.setId(addedRequest.getId());
        requestForCoursework.setClient(addedRequest.getClient());
        Assert.assertEquals(requestForCoursework,addedRequest);
    }
    @After
    public void destroy() {
        ApplicationContext.getInstance().destroy();
    }

    private RequestForCoursework initRequest(){
//        TaskFile taskFile = new TaskFile();
//        taskFile.setTaskFileStream(df);
        RequestForCoursework requestForCoursework = new RequestForCoursework();
        requestForCoursework.setTheme("test theme");
        requestForCoursework.setDeadline(System.currentTimeMillis());
        requestForCoursework.setDescription("test request");
        requestForCoursework.setRegistrationDate(System.currentTimeMillis());
        requestForCoursework.setDisciple(Disciple.ECONOMICS);
        requestForCoursework.setPaperType(PaperType.COURSEWORK);
        requestForCoursework.setRequestType(RequestType.EXECUTION);
        requestForCoursework.setRequestStatus(RequestStatus.CONSIDERING);
        return requestForCoursework;

    }
}