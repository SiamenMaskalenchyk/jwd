package com.maskalenchyk.education_helper.service;

import com.maskalenchyk.education_helper.application.ApplicationContext;
import com.maskalenchyk.education_helper.entity.UserAccount;
import com.maskalenchyk.education_helper.entity.UserRole;
import com.maskalenchyk.education_helper.service.exceptions.ServiceException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class UserServiceImplTest {

    private UserService userService;
    private CourseworkRequestService courseworkRequestService;

    @Before
    public void init() {
        ApplicationContext.initialize();
        userService = ApplicationContext.getInstance().getBean(UserService.class);
        courseworkRequestService=ApplicationContext.getInstance().getBean(CourseworkRequestService.class);

    }

    @Test
    public void userServiceTest() throws ServiceException {
        Assert.assertNotNull(userService);
        UserAccount userAccount = userService.createUserAccount("java@gmail.ru", "+375336755643", UserRole.CLIENT);
        Assert.assertNotNull(userAccount);
        Integer userAccountId = userAccount.getId();
        userService.deleteById(userAccountId);
        UserAccount foundUserAccount = userService.getById(userAccountId);
        Assert.assertNull(foundUserAccount);
        foundUserAccount = userService.getById(3);
        Assert.assertNotNull(foundUserAccount);
        foundUserAccount.setName("Hello form java test");
        userService.updateUserAccount(foundUserAccount);
        Assert.assertEquals("Hello form java test",userService.getById(3).getName());
        List<UserAccount> userAccountList = userService.findAll();
        Assert.assertNotNull(userAccountList);
    }

    @Test
    public void courseworkRequestServiceTest(){
        Assert.assertNotNull(courseworkRequestService);
    }

    @After
    public void destroy() {
        ApplicationContext.getInstance().destroy();
    }
}