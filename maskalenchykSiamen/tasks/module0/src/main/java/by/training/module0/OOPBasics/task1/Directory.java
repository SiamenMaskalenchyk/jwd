package by.training.module0.OOPBasics.task1;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Directory {

    private String name;
    private String path;
    private List<File> fileList;

    public Directory(String name, String path, List<File> fileList) {
        this.name = name;
        this.path = path;
        this.fileList = fileList;
    }

    public Directory(String name, String path, File... fileList) {
        this.name = name;
        this.path = path;
        this.fileList = new LinkedList<>(Arrays.asList(fileList));
    }

    public Directory(String name, String path) {
        this.name = name;
        this.path = path;
        this.fileList = new LinkedList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<File> getFileList() {
        return fileList;
    }

    public void setFileList(List<File> fileList) {
        this.fileList = fileList;
    }
}
