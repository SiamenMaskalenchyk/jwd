package by.training.module0.OOPBasics.task1;

import java.util.Objects;

public class File {

    private String fileName;
    private Directory directory;
    private String content;

    public File(String fileName, Directory directory, String content) {
        this.fileName = fileName;
        this.directory = directory;
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Directory getDirectory() {
        return directory;
    }

    public void setDirectory(Directory directory) {
        this.directory = directory;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        File file = (File) o;
        return Objects.equals(fileName, file.fileName) &&
                Objects.equals(directory, file.directory) &&
                Objects.equals(content, file.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileName, directory, content);
    }
}
