package by.training.module0.OOPBasics.task1;

import by.training.module0.OOPBasics.task1.service.*;

public class Task1Main {

    public static void main(String[] args) {
        FileCreateService fileCreateService = new FileCreator();
        EditingFileService fileEditService = new SimpleFileEditor();
        FileDeleteService fileDeleteService = new FileDestructor();
        FileReanameService fileReanameService = new StandartFileRename();
        FileOutputService fileOutputService = new FileDataGetter();
        Directory directory = new Directory("directory", "C:\\");
        File file = fileCreateService.create("file1",directory);
        file = fileEditService.additionInformation(file,"dgagdsgag");
        file = fileReanameService.rename("newFileName", file);
        fileOutputService.printContent(file);
        System.out.println(fileDeleteService.delete(file));

    }
}
