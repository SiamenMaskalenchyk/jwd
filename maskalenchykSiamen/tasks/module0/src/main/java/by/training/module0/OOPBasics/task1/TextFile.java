package by.training.module0.OOPBasics.task1;

public class TextFile extends File {

    public TextFile(String fileName, Directory directory, String content) {
        super(fileName + ".txt", directory, content);
    }

    @Override
    public void setFileName(String fileName) {
        super.setFileName(fileName + ".txt");
    }
}
