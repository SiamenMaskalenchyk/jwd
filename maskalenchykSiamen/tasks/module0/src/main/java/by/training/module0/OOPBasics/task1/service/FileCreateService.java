package by.training.module0.OOPBasics.task1.service;

import by.training.module0.OOPBasics.task1.Directory;
import by.training.module0.OOPBasics.task1.File;

public interface FileCreateService {

    File create(String name, Directory directory);
}
