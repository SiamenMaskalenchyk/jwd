package by.training.module0.OOPBasics.task1.service;

import by.training.module0.OOPBasics.task1.Directory;
import by.training.module0.OOPBasics.task1.File;

import java.util.List;

public class FileCreator implements FileCreateService {

    @Override
    public File create(String name, Directory directory) {
        File file = new File(name, directory, "");
        List<File> fileList = directory.getFileList();
        fileList.add(file);
        directory.setFileList(fileList);
        return file;
    }
}
