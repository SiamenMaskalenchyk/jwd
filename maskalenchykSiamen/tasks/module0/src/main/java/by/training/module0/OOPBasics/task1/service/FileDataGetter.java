package by.training.module0.OOPBasics.task1.service;

import by.training.module0.OOPBasics.task1.File;

public class FileDataGetter implements FileOutputService {

    @Override
    public void printContent(File file) {
        System.out.println(file.getContent());
    }
}
