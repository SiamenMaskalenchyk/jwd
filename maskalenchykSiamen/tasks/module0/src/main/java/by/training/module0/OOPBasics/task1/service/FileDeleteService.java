package by.training.module0.OOPBasics.task1.service;

import by.training.module0.OOPBasics.task1.File;

public interface FileDeleteService {

    boolean delete(File file);
}

