package by.training.module0.OOPBasics.task1.service;

import by.training.module0.OOPBasics.task1.Directory;
import by.training.module0.OOPBasics.task1.File;

import java.util.List;

public class FileDestructor implements FileDeleteService {

    @Override
    public boolean delete(File file) {
        Directory directory = file.getDirectory();
        List<File> fileList = directory.getFileList();
        boolean isSuccessful = fileList.remove(file);
        if (isSuccessful) {
            directory.setFileList(fileList);
            return true;
        } else {
            return false;
        }
    }
}
