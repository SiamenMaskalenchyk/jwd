package by.training.module0.OOPBasics.task1.service;

import by.training.module0.OOPBasics.task1.File;

public interface FileReanameService {

    File rename(String newName, File file);
}
