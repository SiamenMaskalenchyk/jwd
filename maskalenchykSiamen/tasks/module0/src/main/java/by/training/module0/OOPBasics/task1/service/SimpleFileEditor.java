package by.training.module0.OOPBasics.task1.service;

import by.training.module0.OOPBasics.task1.File;

public class SimpleFileEditor implements EditingFileService {
    @Override
    public File additionInformation(File file, String add) {
        file.setContent(add);
        return file;
    }
}
