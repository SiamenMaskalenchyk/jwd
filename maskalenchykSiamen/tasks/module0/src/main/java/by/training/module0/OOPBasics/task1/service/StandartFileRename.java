package by.training.module0.OOPBasics.task1.service;

import by.training.module0.OOPBasics.task1.File;

public class StandartFileRename implements FileReanameService {

    @Override
    public File rename(String newName, File file) {
        file.setFileName(newName);
        return file;
    }
}
