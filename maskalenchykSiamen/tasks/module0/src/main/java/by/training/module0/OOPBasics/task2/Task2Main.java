package by.training.module0.OOPBasics.task2;

import by.training.module0.OOPBasics.task2.payment.Items;
import by.training.module0.OOPBasics.task2.payment.Payment;
import by.training.module0.OOPBasics.task2.service.ChangingPayment;
import by.training.module0.OOPBasics.task2.service.DecrementPayment;
import by.training.module0.OOPBasics.task2.service.IncrementerPayment;

public class Task2Main {

    public static void main(String[] args) {

        Items items = initItems();
        Payment payment = new Payment();
        ChangingPayment incrementerPayment = new IncrementerPayment(payment, items);
        ChangingPayment decrementerPayment = new DecrementPayment(payment);
        incrementerPayment.changePayment(new Payment.Item("Bread", 100));
        incrementerPayment.changePayment(new Payment.Item("Bread", 100));
        incrementerPayment.changePayment(new Payment.Item("agaga", 1030));
        System.out.println(payment);
    }

    private static Items initItems() {
        Payment.Item bread = new Payment.Item("Bread", 100);
        Payment.Item meat = new Payment.Item("Meat", 150);
        Payment.Item vegetables = new Payment.Item("Vegetables", 30);
        Payment.Item milk = new Payment.Item("Milk", 20);
        Items items = new Items(bread, meat, vegetables, milk);
        return items;
    }
}
