package by.training.module0.OOPBasics.task2.payment;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Items {

    List<Payment.Item> items;

    public Items(List<Payment.Item> items) {
        this.items = items;
    }

    public Items(Payment.Item... items) {
        this.items = new LinkedList<>(Arrays.asList(items));
    }

    public List<Payment.Item> getItems() {
        return items;
    }
}
