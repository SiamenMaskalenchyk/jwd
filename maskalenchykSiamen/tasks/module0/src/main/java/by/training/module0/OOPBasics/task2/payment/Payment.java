package by.training.module0.OOPBasics.task2.payment;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Payment {

    List<Item> items;

    public Payment() {
        this.items = new LinkedList<>();
    }

    public List<Payment.Item> getItems() {
        return items;
    }

    public void setItems(List<Payment.Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "items=" + items +
                '}';
    }

    public static class Item {

        private String name;
        private int price;

        public Item(String name, int price) {
            this.name = name;
            this.price = price;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Item item = (Item) o;
            return price == item.price &&
                    Objects.equals(name, item.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, price);
        }

        @Override
        public String toString() {
            return "Item{" +
                    "name='" + name + '\'' +
                    ", price=" + price +
                    '}';
        }
    }
}
