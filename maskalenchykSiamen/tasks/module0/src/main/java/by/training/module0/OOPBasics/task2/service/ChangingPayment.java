package by.training.module0.OOPBasics.task2.service;

import by.training.module0.OOPBasics.task2.payment.Payment;

public interface ChangingPayment {

    void changePayment(Payment.Item item);
}
