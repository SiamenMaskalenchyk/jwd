package by.training.module0.OOPBasics.task2.service;

import by.training.module0.OOPBasics.task2.payment.Payment;

public class DecrementPayment implements ChangingPayment {

    private Payment payment;

    public DecrementPayment(Payment payment) {
        this.payment = payment;
    }

    @Override
    public void changePayment(Payment.Item item) {
        for (Payment.Item paymentItem : payment.getItems()) {
            if (paymentItem.equals(item)) {
                payment.getItems().remove(item);
            }
        }
    }
}
