package by.training.module0.OOPBasics.task2.service;

import by.training.module0.OOPBasics.task2.payment.Items;
import by.training.module0.OOPBasics.task2.payment.Payment;

import java.util.List;

public class IncrementerPayment implements ChangingPayment {

    private Payment payment;
    private Items items;

    public IncrementerPayment(Payment payment, Items items) {
        this.payment = payment;
        this.items = items;
    }

    @Override
    public void changePayment(Payment.Item item) {
        if(items.getItems().contains(item)){
            List<Payment.Item> items = payment.getItems();
            items.add(item);
            payment.setItems(items);
            System.out.println("Item putted in the payment");
        } else {
            System.out.println("Incorrect item");
        }

    }
}
