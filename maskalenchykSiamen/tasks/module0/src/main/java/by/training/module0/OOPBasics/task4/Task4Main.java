package by.training.module0.OOPBasics.task4;

import by.training.module0.OOPBasics.task4.dragonCave.DragonCave;
import by.training.module0.OOPBasics.task4.service.*;

public class Task4Main {

    public static void main(String[] args) {
        DragonCave dragonCave = new DragonCave();
        TreasureGeneratorService treasureGenerator = new TreasureGenerator();
        treasureGenerator.generateTreasure(100, dragonCave);
        SingleTreasureService maxTreasureFinder = new MaxTreasureFinder();
        DefiniteTreasureService treasureFinder = new DefiniteTreasureFinder();
        TreasureCollectorService treasureCollectorBySum = new TreasureCollectorBySum();
        System.out.println("Max treasure:" + maxTreasureFinder.findTreasure(dragonCave));
        System.out.println("Treasure 31: " + treasureFinder.findTreasure("Treasure31", dragonCave));
        System.out.println("Treasures for " + 10000 + " rub: " + treasureCollectorBySum.collectTreasure(10000, dragonCave));
    }
}
