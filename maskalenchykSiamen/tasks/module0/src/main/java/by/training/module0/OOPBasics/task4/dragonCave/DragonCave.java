package by.training.module0.OOPBasics.task4.dragonCave;

import by.training.module0.OOPBasics.task4.treasure.Treasure;

import java.util.LinkedList;
import java.util.List;

public class DragonCave {

    List<Treasure> treasures;

    public DragonCave(List<Treasure> treasures) {
        this.treasures = treasures;
    }

    public DragonCave() {
        this.treasures = new LinkedList<>();
    }

    public List<Treasure> getTreasures() {
        return treasures;
    }

    public void setTreasures(List<Treasure> treasures) {
        this.treasures = treasures;
    }
}
