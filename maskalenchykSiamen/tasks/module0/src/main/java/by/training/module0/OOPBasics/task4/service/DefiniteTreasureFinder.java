package by.training.module0.OOPBasics.task4.service;

import by.training.module0.OOPBasics.task4.dragonCave.DragonCave;
import by.training.module0.OOPBasics.task4.treasure.Treasure;

public class DefiniteTreasureFinder implements DefiniteTreasureService {

    @Override
    public Treasure findTreasure(String name, DragonCave dragonCave) {
        for (Treasure treasure : dragonCave.getTreasures()) {
            if (treasure.getName().equals(name)) {
                return treasure;
            }
        }
        return null;
    }
}
