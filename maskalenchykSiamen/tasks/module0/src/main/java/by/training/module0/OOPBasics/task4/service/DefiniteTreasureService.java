package by.training.module0.OOPBasics.task4.service;

import by.training.module0.OOPBasics.task4.dragonCave.DragonCave;
import by.training.module0.OOPBasics.task4.treasure.Treasure;

public interface DefiniteTreasureService {

    Treasure findTreasure(String name, DragonCave dragonCave);
}
