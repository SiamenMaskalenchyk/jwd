package by.training.module0.OOPBasics.task4.service;

import by.training.module0.OOPBasics.task4.dragonCave.DragonCave;
import by.training.module0.OOPBasics.task4.treasure.Treasure;

public class MaxTreasureFinder implements SingleTreasureService {


    @Override
    public Treasure findTreasure(DragonCave dragonCave) {
        int maxPrice = 0;
        Treasure biggestTreasure = dragonCave.getTreasures().get(0);
        for (Treasure treasure : dragonCave.getTreasures()) {
            if (maxPrice < treasure.getPrice()) {
                maxPrice = treasure.getPrice();
                biggestTreasure = treasure;
            }
        }
        return biggestTreasure;
    }
}
