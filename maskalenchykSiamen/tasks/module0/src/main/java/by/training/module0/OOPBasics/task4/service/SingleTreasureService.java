package by.training.module0.OOPBasics.task4.service;

import by.training.module0.OOPBasics.task4.dragonCave.DragonCave;
import by.training.module0.OOPBasics.task4.treasure.Treasure;

public interface SingleTreasureService {

    Treasure findTreasure(DragonCave dragonCave);

}
