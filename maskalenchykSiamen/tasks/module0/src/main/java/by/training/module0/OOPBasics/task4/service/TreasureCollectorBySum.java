package by.training.module0.OOPBasics.task4.service;

import by.training.module0.OOPBasics.task4.dragonCave.DragonCave;
import by.training.module0.OOPBasics.task4.treasure.Treasure;

import java.util.LinkedList;
import java.util.List;

public class TreasureCollectorBySum implements TreasureCollectorService {

    @Override
    public List<Treasure> collectTreasure(int sum, DragonCave dragonCave) {
        List<Treasure> result = new LinkedList<>();
        List<Treasure> treasures = dragonCave.getTreasures();
        int i = 0;
        while (sum > 0) {
            if (sum - treasures.get(i).getPrice() >= 0) {
                result.add(treasures.get(i));
            }
            sum -= treasures.get(i).getPrice();
            i += 1;
        }
        return result;
    }
}
