package by.training.module0.OOPBasics.task4.service;

import by.training.module0.OOPBasics.task4.dragonCave.DragonCave;
import by.training.module0.OOPBasics.task4.treasure.Treasure;

import java.util.List;

public interface TreasureCollectorService {

    List<Treasure> collectTreasure(int num, DragonCave dragonCave);
}
