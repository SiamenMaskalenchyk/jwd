package by.training.module0.OOPBasics.task4.service;

import by.training.module0.OOPBasics.task4.dragonCave.DragonCave;
import by.training.module0.OOPBasics.task4.treasure.Treasure;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class TreasureGenerator implements TreasureGeneratorService {

    @Override
    public void generateTreasure(int num, DragonCave dragonCave) {
        List<Treasure> treasures = new LinkedList<>();
        Random random = new Random();
        for (int i = 0; i < num; i++) {
            String name = "Treasure" + i;
            treasures.add(new Treasure(name, 1 + random.nextInt(3000)));
        }
        dragonCave.setTreasures(treasures);
    }
}
