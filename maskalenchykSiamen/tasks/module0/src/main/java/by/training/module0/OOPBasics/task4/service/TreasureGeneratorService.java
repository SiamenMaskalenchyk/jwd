package by.training.module0.OOPBasics.task4.service;

import by.training.module0.OOPBasics.task4.dragonCave.DragonCave;

public interface TreasureGeneratorService {

    void generateTreasure(int num, DragonCave dragonCave);
}
