package by.training.module0.OOPBasics.task5;

import by.training.module0.OOPBasics.task5.gift.*;
import by.training.module0.OOPBasics.task5.service.CandiesCompositionService;
import by.training.module0.OOPBasics.task5.service.CandyCollector;
import by.training.module0.OOPBasics.task5.service.CompositionService;
import by.training.module0.OOPBasics.task5.service.GiftCreator;

public class Task5Main {

    public static void main(String[] args) {
        CompositionService giftCreator = new GiftCreator();
        Candies candies = new Candies();
        CandiesCompositionService candyCollector = new CandyCollector(candies);
        Candy marshmallow = new Candy(CandyType.MARSHMALLOW, 100);
        Candy candy = new Candy(CandyType.CANDY, 250);
        Candy chocolate = new Candy(CandyType.CHOCOLATE, 200);
        candyCollector.addCandy(marshmallow);
        candyCollector.addCandy(candy);
        candyCollector.addCandy(chocolate);
        Wrapping wrapping = new Wrapping(Color.GREEN, WrappingType.BOX);
        Gift gift = giftCreator.create(wrapping, candies);
        System.out.println(gift);
    }
}
