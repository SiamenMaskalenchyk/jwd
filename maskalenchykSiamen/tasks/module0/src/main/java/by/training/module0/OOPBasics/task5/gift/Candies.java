package by.training.module0.OOPBasics.task5.gift;

import java.util.LinkedList;
import java.util.List;

public class Candies {

    private List<Candy> candies;

    public Candies() {
        this.candies = new LinkedList<>();
    }

    public Candies(List<Candy> candies) {
        this.candies = candies;
    }

    public List<Candy> getCandies() {
        return candies;
    }

    public void setCandies(List<Candy> candies) {
        this.candies = candies;
    }

    public double getWeightCndiesInGift(){
        double weight = 0;
        for (Candy candy : candies) {
            weight += candy.getWeight();
        }
        return weight;
    }

    @Override
    public String toString() {
        return "Candies{" +
                "candies=" + candies +
                '}';
    }
}
