package by.training.module0.OOPBasics.task5.gift;

import java.util.Objects;

public class Candy {

    private CandyType name;
    private double weight;

    public Candy(CandyType name, double weight) {
        this.name = name;
        this.weight = weight;
    }

    public CandyType getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Candy candy = (Candy) o;
        return Double.compare(candy.weight, weight) == 0 &&
                Objects.equals(name, candy.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, weight);
    }

    @Override
    public String toString() {
        return "Candy{" +
                "name='" + name.getCandyType()+ '\'' +
                ", weight=" + weight +
                '}';
    }
}
