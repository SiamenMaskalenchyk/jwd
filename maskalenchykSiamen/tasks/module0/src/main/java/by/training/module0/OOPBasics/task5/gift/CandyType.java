package by.training.module0.OOPBasics.task5.gift;

public enum CandyType {

    CHOCOLATE("chocolate"),
    CANDY("candy"),
    MARSHMALLOW("marshmallow"),
    CHOCOLATE_BAR("chocolate bar");

    private String candyType;

    CandyType(String candyType) {
        this.candyType = candyType;
    }

    public String getCandyType() {
        return candyType;
    }
}
