package by.training.module0.OOPBasics.task5.gift;

public enum Color {
    RED("red"),
    BLUE("blue"),
    GREEN("green"),
    YELLOW("yellow");

    private String colour;

    Color(String colour) {
        this.colour = colour;
    }

    public String getColour() {
        return colour;
    }
}
