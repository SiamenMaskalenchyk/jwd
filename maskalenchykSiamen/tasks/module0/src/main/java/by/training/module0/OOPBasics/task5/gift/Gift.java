package by.training.module0.OOPBasics.task5.gift;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Gift {

    private Wrapping wrapping;
    private List<Candy> candies;

    public Gift(Wrapping wrapping, List<Candy> candies) {
        this.wrapping = wrapping;
        this.candies = candies;
    }

    public Gift(Wrapping wrapping, Candy... candiesArr) {
        this.wrapping = wrapping;
        this.candies = new LinkedList<>(Arrays.asList(candiesArr));
    }

    public Gift(Wrapping wrapping, Candies candies) {
        this.wrapping = wrapping;
        this.candies = candies.getCandies();
    }

    @Override
    public String toString() {
        String res = "wrapping=" + wrapping;
        for (Candy candy : this.candies) {
            res += candy.toString();
        }
        return res;
    }
}
