package by.training.module0.OOPBasics.task5.gift;

import java.util.Objects;

public class Wrapping {

    private Color color;
    private WrappingType wrappingType;

    public Wrapping(Color color, WrappingType wrappingType) {
        this.color = color;
        this.wrappingType = wrappingType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wrapping wrapping = (Wrapping) o;
        return color == wrapping.color &&
                wrappingType == wrapping.wrappingType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, wrappingType);
    }

    @Override
    public String toString() {
        return "Wrapping{" +
                "color=" + color +
                ", wrappingType=" + wrappingType +
                '}';
    }
}
