package by.training.module0.OOPBasics.task5.gift;

public enum WrappingType {

    BOX("box"),
    PACKET("packet"),
    PAPER("paper");

    private String wrappingType;

    WrappingType(String wrappingType) {
        this.wrappingType = wrappingType;
    }

    public String getWrappingType() {
        return wrappingType;
    }
}
