package by.training.module0.OOPBasics.task5.service;

import by.training.module0.OOPBasics.task5.gift.Candies;
import by.training.module0.OOPBasics.task5.gift.Candy;

public interface CandiesCompositionService {

    Candies addCandy(Candy candy);
}
