package by.training.module0.OOPBasics.task5.service;

import by.training.module0.OOPBasics.task5.gift.Candies;
import by.training.module0.OOPBasics.task5.gift.Candy;

import java.util.List;

public class CandyCollector implements CandiesCompositionService {

    private Candies candies;

    public CandyCollector(Candies candies) {
        this.candies = candies;
    }

    @Override
    public Candies addCandy(Candy candy) {
        List<Candy> candyList = candies.getCandies();
        candyList.add(candy);
        candies.setCandies(candyList);
        return candies;
    }


}
