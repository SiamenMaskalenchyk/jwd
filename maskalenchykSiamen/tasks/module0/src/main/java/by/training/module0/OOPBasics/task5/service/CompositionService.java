package by.training.module0.OOPBasics.task5.service;

import by.training.module0.OOPBasics.task5.gift.Candies;
import by.training.module0.OOPBasics.task5.gift.Gift;
import by.training.module0.OOPBasics.task5.gift.Wrapping;

public interface CompositionService {

    Gift create(Wrapping wrapping, Candies candies);
}
