package by.training.module0.OOPBasics.task5.service;

import by.training.module0.OOPBasics.task5.gift.Candies;
import by.training.module0.OOPBasics.task5.gift.Gift;
import by.training.module0.OOPBasics.task5.gift.Wrapping;

public class GiftCreator implements CompositionService {

    @Override
    public Gift create(Wrapping wrapping, Candies candies) {
        return new Gift(wrapping, candies);
    }
}
