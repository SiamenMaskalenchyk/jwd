package by.training.module0.aggregationComposition.task1.Task1;

import by.training.module0.aggregationComposition.task1.text.Sentence;
import by.training.module0.aggregationComposition.task1.text.Text;
import by.training.module0.aggregationComposition.task1.text.Word;

public class Task1Main {

    public static void main(String[] args) {
        Text text = generateText();
    }

    private static Text generateText() {
        Word word1 = new Word("Привет");
        Word word2 = new Word("Этот");
        Word word3 = new Word("текст");
        Word word4 = new Word("из");
        Word word5 = new Word("java");
        Sentence firstSentence = new Sentence(word1);

        Text text = new Text("Test", firstSentence);

        return text;
    }
}
