package by.training.module0.aggregationComposition.task1.service;

import by.training.module0.aggregationComposition.task1.text.Sentence;

public interface ChangingText {

    void changeText(Sentence sentence);
}
