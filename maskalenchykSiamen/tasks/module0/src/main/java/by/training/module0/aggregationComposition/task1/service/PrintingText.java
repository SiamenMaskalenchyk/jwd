package by.training.module0.aggregationComposition.task1.service;

import by.training.module0.aggregationComposition.task1.text.Text;

public interface PrintingText {

    void printText(Text text);
}
