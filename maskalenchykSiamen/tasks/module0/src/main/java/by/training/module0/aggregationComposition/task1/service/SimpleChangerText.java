package by.training.module0.aggregationComposition.task1.service;

import by.training.module0.aggregationComposition.task1.text.Sentence;
import by.training.module0.aggregationComposition.task1.text.Text;

import java.util.List;

public class SimpleChangerText implements ChangingText {

    private Text text;

    public SimpleChangerText(Text text) {
        this.text = text;
    }

    @Override
    public void changeText(Sentence sentence) {
        List<Sentence> textSentences = text.getSentenceList();
        textSentences.add(sentence);
        text.setSentenceList(textSentences);
    }
}
