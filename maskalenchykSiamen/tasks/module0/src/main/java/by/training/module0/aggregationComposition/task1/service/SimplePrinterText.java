package by.training.module0.aggregationComposition.task1.service;

import by.training.module0.aggregationComposition.task1.text.Sentence;
import by.training.module0.aggregationComposition.task1.text.Text;

public class SimplePrinterText implements PrintingText {

    @Override
    public void printText(Text text) {
        System.out.println(text.getHeading());
        for (Sentence sentence : text.getSentenceList()) {
            System.out.print(sentence + " ");
        }
    }
}
