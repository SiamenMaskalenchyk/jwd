package by.training.module0.aggregationComposition.task1.text;

import java.util.LinkedList;
import java.util.List;

public class Sentence {

    private List<Word> wordList;
    private Word firstWord;

    public Sentence(List<Word> wordList) {
        this.wordList = wordList;
        this.firstWord = wordList.get(0);
    }

    public Sentence(Word word) {
        this.wordList = new LinkedList<>();
        this.wordList.add(word);
        this.firstWord = word;
    }


}
