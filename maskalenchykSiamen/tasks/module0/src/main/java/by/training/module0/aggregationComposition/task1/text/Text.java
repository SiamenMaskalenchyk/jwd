package by.training.module0.aggregationComposition.task1.text;

import java.util.LinkedList;
import java.util.List;

public class Text {

    private String heading;
    private List<Sentence> sentenceList;

    public Text(String heading, List<Sentence> sentenceList) {
        this.heading = heading;
        this.sentenceList = sentenceList;
    }

    public Text(String heading, Sentence sentence) {
        this.heading = heading;
        this.sentenceList = new LinkedList<>();
        this.sentenceList.add(sentence);
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public List<Sentence> getSentenceList() {
        return sentenceList;
    }

    public void setSentenceList(List<Sentence> sentenceList) {
        this.sentenceList = sentenceList;
    }


}
