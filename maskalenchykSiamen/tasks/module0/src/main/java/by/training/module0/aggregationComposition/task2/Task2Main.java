package by.training.module0.aggregationComposition.task2;

import by.training.module0.aggregationComposition.task2.car.Car;
import by.training.module0.aggregationComposition.task2.car.components.engine.Engine;
import by.training.module0.aggregationComposition.task2.car.components.engine.GasEngine;
import by.training.module0.aggregationComposition.task2.car.components.Wheel;
import by.training.module0.aggregationComposition.task2.car.service.RepairingCar;
import by.training.module0.aggregationComposition.task2.car.service.WheelReplacer;

public class Task2Main {

    public static void main(String[] args) {
        Engine engine = new GasEngine();
        Wheel wheel1 = new Wheel(17);
        Wheel wheel2 = new Wheel(17);
        Wheel wheel3 = new Wheel(17);
        Wheel wheel4 = new Wheel(17);
        Car car = new Car("Audi", engine, wheel1, wheel2, wheel3, wheel4);
        RepairingCar repairer = new WheelReplacer(car, wheel1, wheel2);
        System.out.println("Car brand:" + car.getCarModel());
        car.setMove(engine.start());
        while (car.isMove() && car.getFillUp() > 0) {
            int fuel = car.getFillUp();
            fuel -= 1;
            car.setFillUp(fuel);
            wheel1.turn(engine.move());
            wheel2.turn(engine.move());
            wheel3.turn(engine.move());
            wheel4.turn(engine.move());
        }
        car.setMove(engine.stop());
        car.fuelUp(120);
        repairer.repair();
    }
}
