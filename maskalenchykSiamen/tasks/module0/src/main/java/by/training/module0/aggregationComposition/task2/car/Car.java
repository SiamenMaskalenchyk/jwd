package by.training.module0.aggregationComposition.task2.car;

import by.training.module0.aggregationComposition.task2.car.components.engine.Engine;
import by.training.module0.aggregationComposition.task2.car.components.Wheel;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Car {

    private String carModel;
    private Engine engine;
    private int fillUp;
    private List<Wheel> wheels;
    private boolean isMove = false;

    public Car(String carModel, Engine engine, Wheel... wheels) {
        this.carModel = carModel;
        this.engine = engine;
        this.wheels = new LinkedList<>(Arrays.asList(wheels));
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public int getFillUp() {
        return fillUp;
    }

    public void setFillUp(int fillUp) {
        this.fillUp = fillUp;
    }

    public List<Wheel> getWheels() {
        return wheels;
    }

    public void setWheels(List<Wheel> wheels) {
        this.wheels = wheels;
    }

    public boolean isMove() {
        return isMove;
    }

    public void setMove(boolean move) {
        isMove = move;
    }

    public void fuelUp(int liters) {
        if (isMove = false) {
            this.fillUp = this.fillUp + liters;
            System.out.println("Car fuelled up on " + liters + " litres.");
        } else {
            System.out.println("Car didn't fuel up. You have to stop car");
        }
    }

    @Override
    public String toString() {
        return carModel;
    }
}
