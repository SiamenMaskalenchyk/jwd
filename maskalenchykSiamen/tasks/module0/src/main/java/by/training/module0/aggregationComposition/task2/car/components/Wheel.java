package by.training.module0.aggregationComposition.task2.car.components;

public class Wheel {

    private int radius;

    public Wheel(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public boolean turn(int power) {
        if (power > 0) {
            return true;
        } else {
            return false;
        }
    }
}
