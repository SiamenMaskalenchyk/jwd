package by.training.module0.aggregationComposition.task2.car.components.engine;

public interface Engine {

    boolean start();

    int move();

    boolean stop();
}
