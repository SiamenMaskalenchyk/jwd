package by.training.module0.aggregationComposition.task2.car.components.engine;

public class GasEngine implements Engine {

    private int maxPower;


    @Override
    public boolean start() {
        System.out.println("Gas engine started");
        return true;
    }

    @Override
    public int move() {
        return 15;
    }

    @Override
    public boolean stop() {
        System.out.println("Gas engine stopped");
        return false;
    }
}
