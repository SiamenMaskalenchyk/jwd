package by.training.module0.aggregationComposition.task2.car.service;

import by.training.module0.aggregationComposition.task2.car.Car;
import by.training.module0.aggregationComposition.task2.car.components.Wheel;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class WheelReplacer implements RepairingCar {

    private Car car;
    private List<Wheel> wheels;

    public WheelReplacer(Car car, Wheel... wheels) {
        this.car = car;
        this.wheels = new LinkedList<>(Arrays.asList(wheels));
    }

    @Override
    public void repair() {
        car.setWheels(wheels);
        System.out.println(wheels.size() + " wheel(s) have changed in" + car);
    }
}
