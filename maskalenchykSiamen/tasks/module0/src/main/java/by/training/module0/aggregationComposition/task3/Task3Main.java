package by.training.module0.aggregationComposition.task3;

import by.training.module0.aggregationComposition.task3.country.City;
import by.training.module0.aggregationComposition.task3.country.Country;
import by.training.module0.aggregationComposition.task3.country.Region;
import by.training.module0.aggregationComposition.task3.service.RegionCentreFinder;
import by.training.module0.aggregationComposition.task3.service.SquareCalculator;
import by.training.module0.aggregationComposition.task3.service.Statistics;

import java.util.LinkedList;
import java.util.List;

public class Task3Main {

    public static void main(String[] args) {
        City minsk = new City("Minsk", 1000000);
        City baranovichi = new City("Baranovichi", 134000);
        City dzerginsk = new City("Dzerginsk", 13400);
        City bragin = new City("Bragin", 90000);
        City gomel = new City("Gomel", 1000000);
        City brest = new City("Brest", 100334);
        List<City> cityList = new LinkedList<>();
        cityList.add(baranovichi);
        cityList.add(brest);
        List<City> cityList2 = new LinkedList<>();
        cityList2.add(gomel);
        cityList2.add(bragin);
        List<City> cityList1 = new LinkedList<>();
        cityList1.add(minsk);
        cityList1.add(dzerginsk);
        Region gomelRegion = new Region("Gomel region", 23000, cityList2, gomel);
        Region brestRegion = new Region("Brest region", 31000, cityList, brest);
        Region minskRegion = new Region("Minsk region", 30100, cityList1, minsk);
        List<Region> regions = new LinkedList<>();
        regions.add(gomelRegion);
        regions.add(brestRegion);
        regions.add(minskRegion);
        Country country = new Country("Some country", regions, minsk);
        Statistics squareCalculator = new SquareCalculator(country);
        Statistics regionCentreFinder = new RegionCentreFinder(country);
        System.out.println("Capital: " + country.getCapital().toString());
        System.out.println("Region numbers " + country.getRegions().size());
        squareCalculator.getStatistic();
        regionCentreFinder.getStatistic();

    }
}
