package by.training.module0.aggregationComposition.task3.country;

import java.util.LinkedList;
import java.util.List;

public class Country {

    private String name;
    private List<Region> regions;
    private City Capital;

    public Country(String name, List<Region> regions, City capital) {
        this.name = name;
        this.regions = regions;
        Capital = capital;
    }

    public City getCapital() {
        return Capital;
    }

    public List<City> getRegionCentres() {
        List<City> centres = new LinkedList<>();
        for (Region region : regions) {
            centres.add(region.getRegionalCentre());
        }
        return centres;
    }

    public double countrySquare() {
        double square = 0;
        for (Region region : regions) {
            square += region.getSquare();
        }
        return square;
    }

    public List<Region> getRegions() {
        return regions;
    }
}
