package by.training.module0.aggregationComposition.task3.country;

import java.util.List;

public class Region {

    private final String name;
    private final double square;
    private List<City> cities;
    private City regionalCentre;

    public Region(String name, double square, List<City> cities, City regionalCentre) {
        this.name = name;
        this.square = square;
        this.cities = cities;
        this.regionalCentre = regionalCentre;
    }

    public String getName() {
        return name;
    }

    public double getSquare() {
        return square;
    }

    public List<City> getCities() {
        return cities;
    }

    public City getRegionalCentre() {
        return regionalCentre;
    }
}
