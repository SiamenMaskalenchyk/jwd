package by.training.module0.aggregationComposition.task3.service;

import by.training.module0.aggregationComposition.task3.country.Country;
import by.training.module0.aggregationComposition.task3.country.Region;

public class RegionCentreFinder implements Statistics {

    private Country country;

    public RegionCentreFinder(Country country) {
        this.country = country;
    }

    @Override
    public void getStatistic() {
        System.out.println("Region centres: ");
        for (Region region : country.getRegions()) {
            System.out.println(region.getRegionalCentre());
        }
    }
}
