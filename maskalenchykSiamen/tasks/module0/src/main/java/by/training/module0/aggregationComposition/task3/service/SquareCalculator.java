package by.training.module0.aggregationComposition.task3.service;

import by.training.module0.aggregationComposition.task3.country.Country;
import by.training.module0.aggregationComposition.task3.country.Region;

public class SquareCalculator implements Statistics {

    private Country country;

    public SquareCalculator(Country country) {
        this.country = country;
    }

    @Override
    public void getStatistic() {
        double square = 0;
        for (Region region : country.getRegions()) {
            square += region.getSquare();
        }
        System.out.println("Regions square: " + square);
    }
}
