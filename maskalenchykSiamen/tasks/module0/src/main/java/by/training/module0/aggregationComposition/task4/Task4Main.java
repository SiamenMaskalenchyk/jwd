package by.training.module0.aggregationComposition.task4;

import by.training.module0.aggregationComposition.task4.account.Account;
import by.training.module0.aggregationComposition.task4.service.*;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Task4Main {

    public static void main(String[] args) {
        List<Account> accounts = init();
        Account account = new Account("First account", 1000, false);
        AmountCalculating generalCalculator = new GeneralAmountCalculator(accounts);
        AmountCalculating negativeCalculator = new NegativeAmountCalculator(accounts);
        AmountCalculating positiveCalculator = new PositiveAmountCalculator(accounts);
        Searching simpleSearcher = new SimpleSearcher(accounts);
        System.out.println(simpleSearcher.find(account));
        System.out.println(accounts);
        Collections.sort(accounts,Account.accountComparator);
        System.out.println(accounts);
        System.out.println("General amount: " + generalCalculator.calculateAmount());
        System.out.println("Positive amount: " + positiveCalculator.calculateAmount());
        System.out.println("Negative amount: " + negativeCalculator.calculateAmount());

    }

    private static List<Account> init() {
        List<Account> accounts = new LinkedList<>();
        Account account1 = new Account("First account", 1000, false);
        Account account2 = new Account("Second account", -10, false);
        Account account3 = new Account("Third account", 30, false);
        Account account4 = new Account("Fourth account", 200, false);
        Account account5 = new Account("Fifth account", -100, false);
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        accounts.add(account4);
        accounts.add(account5);
        return accounts;
    }

}
