package by.training.module0.aggregationComposition.task4.account;

import java.util.Comparator;
import java.util.Objects;

public class Account {

    private String name;
    private double amount;
    private boolean isBlocked;

    public Account(String name, double amount, boolean isBlocked) {
        this.name = name;
        this.amount = amount;
        this.isBlocked = isBlocked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Double.compare(account.amount, amount) == 0 &&
                isBlocked == account.isBlocked &&
                Objects.equals(name, account.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, amount, isBlocked);
    }

    @Override
    public String toString() {
        return "Account{" +
                "name='" + name + '\'' +
                ", amount=" + amount +
                ", isBlocked=" + isBlocked +
                '}';
    }

    public static Comparator<Account> accountComparator = (account1, account2) -> {
        int account1Amount = (int) Math.ceil(account1.getAmount() * 100);
        int account2Amount = (int) Math.ceil(account2.getAmount() * 100);
        return account1Amount - account2Amount;
    };
}
