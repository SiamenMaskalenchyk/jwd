package by.training.module0.aggregationComposition.task4.service;

public interface AmountCalculating {

    double calculateAmount();
}
