package by.training.module0.aggregationComposition.task4.service;

import by.training.module0.aggregationComposition.task4.account.Account;

import java.util.List;

public class PositiveAmountCalculator implements AmountCalculating {

    private List<Account> accounts;

    public PositiveAmountCalculator(List<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public double calculateAmount() {
        double amountSum = 0;
        for (Account account : accounts) {
            if (account.getAmount() > 0) {
                amountSum += account.getAmount();
            }
        }
        return amountSum;
    }
}
