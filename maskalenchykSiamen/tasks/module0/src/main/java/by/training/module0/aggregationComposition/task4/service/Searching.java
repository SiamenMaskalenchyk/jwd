package by.training.module0.aggregationComposition.task4.service;

import by.training.module0.aggregationComposition.task4.account.Account;

public interface Searching {

    boolean find(Account account);
}
