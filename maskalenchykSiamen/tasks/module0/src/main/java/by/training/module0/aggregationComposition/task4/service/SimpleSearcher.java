package by.training.module0.aggregationComposition.task4.service;

import by.training.module0.aggregationComposition.task4.account.Account;

import java.util.List;

public class SimpleSearcher implements Searching {

    private List<Account> accounts;

    public SimpleSearcher(List<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public boolean find(Account account) {
        for (Account account1 : accounts) {
            if (account.equals(account1)) {
                return true;
            }
        }
        return false;
    }
}
