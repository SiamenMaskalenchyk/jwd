package by.training.module0.aggregationComposition.task5;

import by.training.module0.aggregationComposition.task5.voucher.FoodType;
import by.training.module0.aggregationComposition.task5.voucher.TouristVoucher;
import by.training.module0.aggregationComposition.task5.voucher.TouristVoucherBase;
import by.training.module0.aggregationComposition.task5.voucher.Transport;
import by.training.module0.aggregationComposition.task5.voucher.VoucherType;
import by.training.module0.aggregationComposition.task5.service.SearchEngine;
import by.training.module0.aggregationComposition.task5.service.SimpleSearchService;
import by.training.module0.aggregationComposition.task5.service.SimpleVoucherSorting;

public class Task5Main {

    public static void main(String[] args) {
        TouristVoucherBase touristVoucherBase = new TouristVoucherBase();
        touristVoucherBase.addVoucher(new TouristVoucher("Good voucher", VoucherType.TREATMENT, FoodType.ALLINCLUSIVE, Transport.PLANE, 25));
        touristVoucherBase.addVoucher(new TouristVoucher("No bad voucher", VoucherType.CRUISE, FoodType.ALLINCLUSIVE, Transport.SHIP, 5));
        touristVoucherBase.addVoucher(new TouristVoucher("Shop voucher", VoucherType.SHOPPING, FoodType.WITHOUTFOOD, Transport.BUS, 2));
        touristVoucherBase.addVoucher(new TouristVoucher("Threat voucher", VoucherType.TREATMENT, FoodType.DINNERS, Transport.BUS, 10));
        touristVoucherBase.addVoucher(new TouristVoucher("Cruise voucher", VoucherType.CRUISE, FoodType.ALLINCLUSIVE, Transport.SHIP, 25));
        SimpleVoucherSorting simpleVoucherSorting = new SimpleVoucherSorting();
        SearchEngine simpleSearchService = new SimpleSearchService(touristVoucherBase);
        touristVoucherBase.showVouchers();
        System.out.println("============================");
        simpleVoucherSorting.sort(touristVoucherBase);
        touristVoucherBase.showVouchers();
        System.out.println("====================");
        simpleSearchService.findVoucher(new TouristVoucher("Good voucher", VoucherType.TREATMENT, FoodType.ALLINCLUSIVE, Transport.PLANE, 25));
        simpleSearchService.findVoucher(new TouristVoucher("Good voucher", VoucherType.SHOPPING, FoodType.WITHOUTFOOD, Transport.PLANE, 25));
    }
}
