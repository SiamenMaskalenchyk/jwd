package by.training.module0.aggregationComposition.task5.service;

import by.training.module0.aggregationComposition.task5.voucher.TouristVoucher;

public interface SearchEngine {

    void findVoucher(TouristVoucher touristVoucher);
}
