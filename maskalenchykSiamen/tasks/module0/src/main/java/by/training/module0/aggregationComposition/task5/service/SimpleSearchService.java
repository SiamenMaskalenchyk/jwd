package by.training.module0.aggregationComposition.task5.service;

import by.training.module0.aggregationComposition.task5.voucher.TouristVoucher;
import by.training.module0.aggregationComposition.task5.voucher.TouristVoucherBase;

public class SimpleSearchService implements SearchEngine {

    private TouristVoucherBase touristVoucherBase;

    public SimpleSearchService(TouristVoucherBase touristVoucherBase) {
        this.touristVoucherBase = touristVoucherBase;
    }

    @Override
    public void findVoucher(TouristVoucher touristVoucher) {
        for (TouristVoucher voucher : touristVoucherBase.getTouristVouchers()) {
            if (voucher.equals(touristVoucher)) {
                System.out.println("Required voucher founded.");
                return;
            }
        }
        System.out.println("Required voucher didn't find.");
    }
}
