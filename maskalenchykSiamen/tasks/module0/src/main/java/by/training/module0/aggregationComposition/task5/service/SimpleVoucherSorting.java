package by.training.module0.aggregationComposition.task5.service;

import by.training.module0.aggregationComposition.task5.voucher.TouristVoucher;
import by.training.module0.aggregationComposition.task5.voucher.TouristVoucherBase;

import java.util.Collections;
import java.util.Comparator;

public class SimpleVoucherSorting implements Sorting {

    @Override
    public void sort(TouristVoucherBase touristVoucherBase) {
        Collections.sort(touristVoucherBase.getTouristVouchers(), SimpleVoucherSorting.touristVoucherComparator);
    }

    private static Comparator<TouristVoucher> touristVoucherComparator = (touristVoucher1, touristVoucher2) -> touristVoucher1.getName().compareTo(touristVoucher2.getName());
}
