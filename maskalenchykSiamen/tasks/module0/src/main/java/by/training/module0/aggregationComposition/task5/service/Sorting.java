package by.training.module0.aggregationComposition.task5.service;

import by.training.module0.aggregationComposition.task5.voucher.TouristVoucherBase;

public interface Sorting {

    void sort(TouristVoucherBase touristVoucherBase);

}
