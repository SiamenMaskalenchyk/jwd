package by.training.module0.aggregationComposition.task5.voucher;

public enum FoodType {

    WITHOUTFOOD("Without food"),
    ALLINCLUSIVE("All inclusive"),
    BREAKFASTS("Breakfasts"),
    DINNERS("Dinners");

    private String foodType;

    FoodType(String foodType) {
        this.foodType = foodType;
    }

    public String getFoodType() {
        return foodType;
    }
}
