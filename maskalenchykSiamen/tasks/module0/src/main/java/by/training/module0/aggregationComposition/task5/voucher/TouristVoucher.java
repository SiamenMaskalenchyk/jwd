package by.training.module0.aggregationComposition.task5.voucher;

import java.util.Objects;

public class TouristVoucher {

    private String name;
    private VoucherType voucherType;
    private FoodType foodType;
    private Transport transport;
    private int NumberOfDays;

    public TouristVoucher(String name, VoucherType voucherType, FoodType foodType, Transport transport, int numberOfDays) {
        this.name = name;
        this.voucherType = voucherType;
        this.foodType = foodType;
        this.transport = transport;
        NumberOfDays = numberOfDays;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VoucherType getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(VoucherType voucherType) {
        this.voucherType = voucherType;
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public int getNumberOfDays() {
        return NumberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        NumberOfDays = numberOfDays;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TouristVoucher that = (TouristVoucher) o;
        return NumberOfDays == that.NumberOfDays &&
                Objects.equals(name, that.name) &&
                voucherType == that.voucherType &&
                foodType == that.foodType &&
                transport == that.transport;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, voucherType, foodType, transport, NumberOfDays);
    }

    @Override
    public String toString() {
        return name + " " + NumberOfDays + " days " + voucherType.getType() + " " + foodType.getFoodType() + " " + transport.getTransportType();
    }

}
