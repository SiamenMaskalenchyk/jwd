package by.training.module0.aggregationComposition.task5.voucher;

import java.util.LinkedList;
import java.util.List;

public class TouristVoucherBase {

    private List<TouristVoucher> touristVouchers;

    public TouristVoucherBase() {
        this.touristVouchers = new LinkedList<>();
    }

    public TouristVoucherBase(List<TouristVoucher> touristVouchers) {
        this.touristVouchers = touristVouchers;
    }

    public List<TouristVoucher> getTouristVouchers() {
        return touristVouchers;
    }

    public void setTouristVouchers(List<TouristVoucher> touristVouchers) {
        this.touristVouchers = touristVouchers;
    }

    public void addVoucher(TouristVoucher touristVoucher) {
        touristVouchers.add(touristVoucher);
    }

    public void deleteVoucher(TouristVoucher touristVoucher) {
        touristVouchers.remove(touristVoucher);
    }

    public void showVouchers() {
        for (TouristVoucher touristVoucher : touristVouchers) {
            System.out.println(touristVoucher);
        }
    }

}
