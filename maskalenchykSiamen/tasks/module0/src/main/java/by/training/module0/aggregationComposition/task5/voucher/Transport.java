package by.training.module0.aggregationComposition.task5.voucher;

public enum Transport {
    BUS("bus"),
    PLANE("plane"),
    SHIP("ship");

    private String transportType;

    Transport(String transportType) {
        this.transportType = transportType;
    }

    public String getTransportType() {
        return transportType;
    }
}
