package by.training.module0.aggregationComposition.task5.voucher;

public enum VoucherType {

    EXCURSION("Excursion"),
    TREATMENT("Treatment"),
    SHOPPING("Shopping"),
    CRUISE("Cruise");

    private String type;

    VoucherType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
