package by.training.module0.arrays;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class arraysMain {

    public static void main(String[] args) {
        task1(5, 5, 25, 6, 4, 6, 78, 8, 0, 78, 90);
        task2(0, 4, 0, 5, 6, 0, 5, 6, 0, 0);
        task3(-1, -2, 5, 4);
        task4(-1, -2, 5, 4);
        task5(0, 4, 0, 5, 6, 0, 5, 6, 0, 0);
        task6(-3, 6, 4, -5, 4);
        task7(7, 7, 7, 3, 4, 56, 2, 7, 2, 6);
        task8(56, 6, 7, 0, 0, 0, -54, -5, -4, -4, -8, 1240);
        task9(56, 6, 7, 0, 0, 0, -54, -5, -4, -4, -8, 1240);
        task10(5, 3, 4, 5, 44, 1, 345, 67, 8);
        task11(6, 5.5, 4, 3.3, 80, 45);
        task12(5.7, 5.34, 34, 355, 1, 53);
        task13(10, 5, 30, 6, 30, 50, 5465, 234, 45, 789, 3, 34, 9, 12, 568, 21, 892);
        task14(11, 234, 456, 56, 4, 856, 234, 79, 34, 568, 1);
        task15(5, 10, 54, 547, -23, 1, 54, 2, 3, 216, 56, 2, 6);
    }

    private static void task15(int c, int d, int... arr) {
        System.out.println("Task 15");
        System.out.print("Sequence between " + c + " and " + d + " is ");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= c && arr[i] <= d) {
                System.out.print(arr[i] + "; ");
            }
        }
    }

    private static void task14(int... arr) {
        System.out.println("Task 14");
        int maxEvenI = arr[0];
        int minOddI = arr[1];
        for (int i = 2; i < arr.length; i++) {
            if (i % 2 == 0 && arr[i] > maxEvenI) {
                maxEvenI = arr[i];
            }
            if (i % 2 != 0 && arr[i] < minOddI) {
                minOddI = arr[i];
            }
        }
        System.out.println("Max + min = " + (maxEvenI + minOddI));
    }

    private static void task13(int m, int l, int n, int... arr) {
        System.out.println("Task 13");
        boolean numberNotFound = true;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % m == 0 && arr[i] > l && arr[i] < n) {
                numberNotFound = false;
                System.out.print(arr[i] + " ");
            }
        }
        if(numberNotFound){
            System.out.println("Numbers satisfying the condition are not found");
        } else{
            System.out.println();
        }
    }

    private static void task12(double... n) {
        System.out.println("Task 12");
        double sumPrimeSerialNumber = 0;
        for (int i = 0; i < n.length; i++) {
            if (isPrime(i)) {
                sumPrimeSerialNumber += n[i];
            }
        }
        System.out.println("Sum of the prime serial number is " + sumPrimeSerialNumber);
    }

    private static boolean isPrime(int num) {
        if (num == 1 || num == 0) {
            return true;
        }
        return IntStream.rangeClosed(num, num / 2).anyMatch(i -> num % i == 0);
    }

    private static void task11(int m, double... a) {
        System.out.println("Task 11");
        String res = "";
        for (int i = 0; i < a.length; i++) {
            double temp = a[i] % m;
            if (temp > 0 && temp < m - 1) {
                res += a[i] + "; ";
            }
        }
        System.out.println("Result of the task 11 is " + res);
    }

    private static void task10(int i, int... a) {
        System.out.println("Task 10");
        String res = "";
        for (int j = 0; j < a.length; j++) {
            if (a[j] > i) {
                res += a[j];
            }
        }
        System.out.println("Result of the task 10 is " + res);
    }

    private static void task9(int... arr) {
        System.out.println("Task 9");
        int max = arr[0];
        int min = arr[0];
        int maxPosition = 0;
        int minPosition = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
                maxPosition = i;
            }
            if (arr[i] < max) {
                min = arr[i];
                minPosition = i;
            }
        }
        arr[maxPosition] = min;
        arr[minPosition] = max;
        System.out.println("Array after replacing");
        System.out.println(Arrays.stream(arr).mapToObj(String::valueOf).collect(Collectors.joining("; ")));
    }

    private static void task8(int... arr) {
        System.out.println("Task 8");
        int countZero = 0;
        int countPositive = 0;
        int countNegative = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 0) {
                countPositive += 1;
            } else if (arr[i] < 0) {
                countNegative += 1;
            } else {
                countZero += 1;
            }
        }
        System.out.println("Array contains i == 0 - " + countZero + "; i > 0 - " + countPositive + "; i < 0 - " + countNegative + ";");
    }

    private static void task7(int z, int... arr) {
        System.out.println("Task 7");
        int countZ = 0;
        System.out.println("Array after changing numbers on " + z + ":");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > z) {
                countZ += 1;
                arr[i] = z;
            }
            System.out.print(arr[i] + " ");
        }
        System.out.println("Count " + z + " - " + countZ);
    }

    private static void task6(int... arr) {
        System.out.println("Task 6");
        Arrays.sort(arr);
        int minLengthAxis = Math.abs(arr[1]) - Math.abs(arr[0]);
        System.out.println("Min axis length is " + minLengthAxis);
    }

    private static void task5(int... arr) {
        System.out.println("Task 5");
        int countEven = 0;
        String evenNumbers = "";
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0 && arr[i] != 0) {
                countEven += 1;
                evenNumbers += arr[i] + " ";
            }
        }
        if (countEven == 0) {
            System.out.println("Sequence don't have even numbers");
            return;
        }
        String[] evenNumbersArray = evenNumbers.trim().split(" ");
        System.out.println("New array ");
        for (int i = 0; i < evenNumbersArray.length; i++) {
            System.out.print(evenNumbersArray[i] + " ");
        }
        System.out.println();
    }

    private static void task4(int... arr) {
        System.out.println("Task 4");
        boolean isIncrease = true;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < arr[i + 1]) {
                isIncrease = false;
                break;
            }
        }
        String res = isIncrease ? "Sequence is increasing " : "Sequence isn't increasing ";
        System.out.println(res);
    }

    private static void task3(int... arr) {
        System.out.println("Task 3");
        System.out.print("Array starts with");
        if (arr[0] > 0) {
            System.out.println(" positive number");
        } else if (arr[0] < 0) {
            System.out.println(" negative number");
        } else {
            System.out.println(" zero");
        }
    }

    private static void task2(int... arr) {
        System.out.println("Task 2");
        int countZero = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                countZero += 1;
            }
        }
        int[] res = new int[countZero];
        int j = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                res[j] = i + 1;
                System.out.print(res[j] + " ");
            }
        }
        System.out.println();
    }

    private static void task1(int k, int... arr) {
        System.out.println("Task 1");
        System.out.println("These numbers are multiplies of " + k + ":");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % k == 0) {
                System.out.print(arr[i]);
            }
        }
        System.out.println();
    }
}
