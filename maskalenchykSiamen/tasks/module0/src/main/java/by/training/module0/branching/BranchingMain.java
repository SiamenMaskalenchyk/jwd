package tasks.branching;

import java.util.*;

public class BranchingMain {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        task1(1, 2);
        task2(1, 2);
        task3(3);
        task4(5, 5);
        task5(7, 8);
        task6(7, 8);
        task7(5, 7, 8, -2);
        task8(9, 15);
        task9(6, 7, 8);
        task10(5, 8);
        task11(4, 3);
        task12(4.5, -9.6, 1.2);
        task13(5.1, 6.5, 3.2, 1.2);
        task14(45, 33);
        task15(5.5, 6.7);
        task16(3, 5);
        task17(5, 5);
        task18(-5, 7 - 1);
        task19(7, -663, -5);
        task20(5, 25, 5, 3);
        task21();
        task22(4, 3321);
        task23("29.02.2019");
        task24(8);
        task25(78);
        task26(54, 65, 2);
        task27(5, 8, 7, 24);
        task28(22, 6, 4, 22);
        task29(2, 4, 5, 7, 4, 5);
        task30(5.6, 54.6, 44);
        task31(54, 20, 90, 40, 50);
        task32(23, 4, -54);
        task33();
        task36(2);
        task37(4);
        task38(2);
        task39(10);
        task40(11);
    }

    private static void task40(double x) {
        double res;
        if (x <= 13) {
            res = Math.pow(-x, 3) + 9;
        } else {
            res = -(3 / (x + 1));
        }
        System.out.println("Result task 40 is " + res);
    }

    private static void task39(double x) {
        double res;
        if (x >= 8) {
            res = Math.pow(-x, 2) + x - 9;
        } else {
            res = 1 / (Math.pow(x, 4) + 6);
        }
        System.out.println("Result task 39 is " + res);
    }

    private static void task38(double x) {
        double res;
        if (x >= 0 && x <= 3) {
            res = x * x;
        } else {
            res = 4;
        }
        System.out.println("Result task 38 is " + res);
    }

    private static void task37(double x) {
        double res;
        if (x >= 3) {
            res = Math.pow(-x, 2) + 3 * x + 9;
        } else {
            res = 1 / (Math.pow(x, 3) + 6);
        }
        System.out.println("Result task 37 is " + res);
    }

    private static void task36(double x) {
        double res;
        if (x <= 3) {
            res = x * x - 3 * x + 9;
        } else {
            res = 1 / (Math.pow(x, 3) + 6);
        }
        System.out.println("Result task36 is " + res);
    }

    private static void task33() {
        Set<String> abc = new HashSet<>();
        abc.add("9583");
        abc.add("1747");
        Set<String> bc = new HashSet<>();
        bc.add("3331");
        bc.add("7922");
        Set<String> c = new HashSet<>();
        c.add("9455");
        c.add("8997");
        System.out.println("Please enter password");
        String enteredPassword = scanner.nextLine();
        if (abc.contains(enteredPassword)) {
            System.out.println("Access to base ABC");
        } else if (bc.contains(enteredPassword)) {
            System.out.println("Access to base BC");
        } else if (c.contains(enteredPassword)) {
            System.out.println("Access to base C");
        } else {
            System.out.println("Wrong password");
        }
    }

    private static void task32(int num1, int num2, int num3) {
        if (num1 + num2 > 0 || num1 + num3 > 0 || num2 + num3 > 0) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }
    }

    private static void task31(int a, int b, int x, int y, int z) {
        if ((a >= x && (b >= y || b >= z)) || (a >= y && (b >= x || b >= z)) || (a >= z && (b >= x || b >= y))) {
            System.out.println("Brick can go through whole");
        } else {
            System.out.println("Brick can't go through whole");
        }
    }

    private static void task30(double a, double b, double c) {
        if (a > b && b > c) {
            a *= 2;
            b *= 2;
            c *= 2;
        } else {
            a = Double.MAX_VALUE;
            b = Double.MAX_VALUE;
            c = Double.MAX_VALUE;
        }
        System.out.println("a = " + a + "; " + "b = " + b + "; " + " c = " + c + ";");
    }

    private static void task29(int x1, int y1, int x2, int y2, int x3, int y3) {
        if ((x1 - x2) / (x3 - x2) == (y1 - y2) / (y3 - y2)) {
            System.out.println("Points lie on the one line");
        } else {
            System.out.println("Points don't lie on the one line");
        }
    }

    private static void task28(int d, int... nums) {
        String numsEqualD = "";
        for (int num : nums) {
            if (num == d) {
                numsEqualD += num + "; ";
            }
        }
        if (!numsEqualD.isEmpty()) {
            System.out.println("This equal d " + numsEqualD);
        } else {
            int max = d - nums[0];
            for (int i = 1; i < nums.length; i++) {
                if (max < (d - nums[i])) {
                    max = d - nums[i];
                }
            }
        }
    }

    private static void task27(int a, int b, int c, int d) {
        System.out.println(Math.max(Math.min(a, b), Math.min(c, d)));
    }

    private static void task26(int num1, int num2, int num3) {
        int max = num1;
        int min = num1;
        if (max < num2) {
            max = num2;
        }
        if (max < num3) {
            max = num3;
        }
        if (min > num2) {
            min = num2;
        }
        if (min > num3) {
            min = num3;
        }
        System.out.println(max + min);
    }

    private static void task25(double temperature) {
        if (temperature > 60) {
            System.out.println("Пожароопасная ситуация");
        }
    }

    private static void task24(int n) {
        if (n % 2 != 0) {
            System.out.println("Loves");
        } else {
            System.out.println("Doesn't love");
        }
    }

    private static void task23(String stringDate) {
        Calendar calendar = new GregorianCalendar();
        calendar.setLenient(false);
        String[] splitData = stringDate.split("\\.");
        try {
            int day = Integer.parseInt(splitData[0]);
            int month = Integer.parseInt(splitData[1]);
            int year = Integer.parseInt(splitData[2]);
            month -= 1;
            calendar.set(year, month, day);
            Date date = calendar.getTime();
            System.out.println("This date is correct");
        } catch (Exception e) {
            System.out.println("This date is incorrect");
        }
    }

    private static void task22(int x, int y) {
        int temp = Math.max(x, y);
        if (x != temp) {
            y = x;
            x = temp;
        }
        System.out.println("x = " + x + "; y = " + y);
    }

    private static void task21() {
        System.out.println("Кто ты: мальчик или девочка? Введи Д или М");
        scanner.nextLine();
        String answer = scanner.nextLine();
        if (answer.equals("Д")) {
            System.out.println("Мне нравятся девочки");
        } else if (answer.equals("М")) {
            System.out.println("Мне нравятся мальчики");
        } else {
            System.out.println("Пол не определён");
        }
    }

    private static void task20(int k, int... nums) {
        String res = "";
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] % k == 0) {
                res += nums[i] + "; ";
            }
        }
        System.out.println(res);
    }

    private static void task19(int... nums) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > 0) {
                count += 1;
            }
        }
        System.out.println("Number of positive numbers is " + count);
    }

    private static void task18(int... nums) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] < 0) {
                count += 1;
            }
        }
        System.out.println("Number of negative numbers is " + count);
    }

    private static void task17(int m, int n) {
        if (m != n) {
            int max = Math.max(m, n);
            m = max;
            n = max;
        } else {
            m = 0;
            n = 0;
        }
        System.out.println("m = " + m + "; n = " + n);
    }

    private static void task16(int x, int y) {
        if (x > 0 && y > 0) {
            System.out.println("Point is in I");
        } else if (x < 0 && y > 0) {
            System.out.println("Point is in II");
        } else if (x < 0 && y < 0) {
            System.out.println("Point is in III");
        } else {
            System.out.println("Point is in IV");
        }
    }

    private static void task15(double x, double y) {
        double halfSum = (x + y) / 2;
        double duplicityProduct = 2 * x * y;
        if (x < y) {
            x = halfSum;
            y = duplicityProduct;
        } else {
            x = duplicityProduct;
            y = halfSum;
        }
        System.out.println("x = " + x + "; y = " + y);
    }

    private static void task14(int firstAngle, int secondAngle) {
        int thirdAngle = 180 - firstAngle - secondAngle;
        if (thirdAngle > 0) {
            System.out.println("This triangle exists");
            if (thirdAngle == 90) {
                System.out.println("This triangle is rectangular");
            }
        } else {
            System.out.println("This triangle doesn't exist");
        }
    }

    private static void task13(double x1, double y1, double x2, double y2) {
        double o1 = Math.sqrt(x1 * x1 + y1 * y1);
        double o2 = Math.sqrt(x2 * x2 + y2 * y2);
        if (o1 > o2) {
            System.out.println("The first point is closer to the origin");
        } else if (o1 < o2) {
            System.out.println("The second point is closer to the origin");
        } else {
            System.out.println("The distance to the origin of both points is equal");
        }
    }

    private static void task12(double... nums) {
        for (double num : nums) {
            if (num > 0) {
                num = Math.pow(num, 2);
            } else {
                num = Math.pow(num, 4);
            }
            System.out.print(num + "; ");
        }
        System.out.println();
    }

    private static void task11(int radius1, int radius2) {
        double squareFirstCircle = Math.PI * radius1 * radius1;
        double squareSecondCircle = Math.PI * radius2 * radius2;
        if (squareFirstCircle > squareSecondCircle) {
            System.out.println("The square of the first circle is bigger");
        } else {
            System.out.println("The square of the second circle is bigger");
        }
    }

    private static void task10(int radius1, int radius2) {
        double squareFirstCircle = Math.PI * radius1 * radius1;
        double squareSecondCircle = Math.PI * radius2 * radius2;
        if (squareFirstCircle < squareSecondCircle) {
            System.out.println("The square of the first circle is smaller");
        } else {
            System.out.println("The square of the second circle is smaller");
        }
    }

    private static void task9(int a, int b, int c) {
        if (a == b && a == c) {
            System.out.println("The triangle is equilateral");
        } else {
            System.out.println("The triangle isn't equilateral");
        }
    }

    private static void task8(int num1, int num2) {
        int minSquare = num1 * num1 < num2 * num2 ? num1 * num1 : num2 * num2;
        System.out.println("Min square is " + minSquare);
    }

    private static void task7(int a, int b, int c, int x) {
        int res = 0;
        if (x > 0) {
            res = a * x * x + b * x + c;
        } else {
            res = a * x * x - b * x + c;
        }
        System.out.println("Result task 7 is " + res);
    }

    private static void task6(int num1, int num2) {
        int max = num1 > num2 ? num1 : num2;
        System.out.println("Max number is " + max);
    }

    private static void task5(int num1, int num2) {
        int min = num1 < num2 ? num1 : num2;
        System.out.println("Min number is " + min);
    }

    private static void task4(int num1, int num2) {
        if (num1 == num2) {
            System.out.println("Numbers are equal");
        } else {
            System.out.println("Numbers aren't equal");
        }
    }

    private static void task3(int num) {
        System.out.println("Please enter the number to compare with " + num);
        int enteredNumber = scanner.nextInt();
        String res = enteredNumber < num ? "yes" : "no";
        System.out.println(res);
    }

    private static void task2(int num1, int num2) {
        String res = num1 < num2 ? "yes" : "no";
        System.out.println(res);

    }

    private static void task1(int num1, int num2) {
        int res = num1 < num2 ? 7 : 8;
        System.out.println("Result of the task1 is " + res);
    }
}
