package by.training.module0.calculator.oop;

interface Calculator {

    void calculate(String[] lexemes) throws Exception;
}
