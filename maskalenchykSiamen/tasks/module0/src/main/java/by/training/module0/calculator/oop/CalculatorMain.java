package by.training.module0.calculator.oop;

import java.util.Scanner;

public class CalculatorMain {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Parser parser = new SimpleParser();
        Calculator calculator = new SimpleCalculator();
        boolean running = true;
        while (running) {
            System.out.println("Please enter command or expression");
            System.out.println("For help enter -help");
            System.out.println("For exit enter -exit");
            String input = scanner.nextLine().trim().replace(",", ".");
            switch (input) {
                case "help":
                    printHelp();
                    break;
                case "exit":
                    running = false;
                    break;
                default:
                    try {
                        String[] lexemes = parser.parse(input);
                        calculator.calculate(lexemes);
                    } catch (Exception e) {
                        System.out.println("Wrong command, please try again");
                    }
            }
        }
    }

    private static void printHelp() {
        System.out.println("For help, enter -help");
        System.out.println("For exit, enter -exit");
        System.out.println("Available operations:" + "\n" +
                "1.Addition - +" + "\n" +
                "2.Subtraction - -" + "\n" +
                "3.Composition - *" + "\n" +
                "4.Division - /");
    }

}
