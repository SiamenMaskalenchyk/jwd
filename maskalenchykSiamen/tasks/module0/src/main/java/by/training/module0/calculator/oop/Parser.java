package by.training.module0.calculator.oop;

public interface Parser {

    String[] parse(String expression);
}
