package by.training.module0.calculator.oop;

import java.text.ParseException;

public class SimpleCalculator implements Calculator {


    @Override
    public void calculate(String[] lexemes) throws Exception {
        double number1;
        double number2;
        try {
        number1 = Double.parseDouble(lexemes[0]);
        number2 = Double.parseDouble(lexemes[2]);
        } catch (Exception e){
            throw new Exception("Wrong number");
        }
        double res;
        switch (lexemes[1]) {
            case "*":
                res = number1 * number2;
                break;
            case "/":
                res = number1 / number2;
                break;
            case "+":
                res = number1 + number2;
                break;
            case "-":
                res = number1 - number2;
                break;
            default:
                throw new Exception("Wrong operation");
        }
        System.out.println("Result: " + res);
    }
}
