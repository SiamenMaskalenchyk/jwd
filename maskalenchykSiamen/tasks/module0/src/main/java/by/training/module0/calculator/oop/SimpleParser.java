package by.training.module0.calculator.oop;

public class SimpleParser implements Parser {


    @Override
    public String[] parse(String expression) {
        String[] lexemes = {"","",""};
        int j = 0;
        for (int i = 0; i < expression.length(); i++) {
            char temp = expression.charAt(i);
            if (temp != '/' && temp != '*' && temp != '+' && temp != '-') {
                lexemes[j] += temp;
            } else {
                j += 1;
                lexemes[j] += temp;
                j += 1;
            }
        }
        return lexemes;
    }
}
