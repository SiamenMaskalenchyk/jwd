package by.training.module0.calculator.procedure;

import java.util.Scanner;

public class Calculator {

    private static Scanner scanner = new Scanner(System.in);
    private static char[] operations = {'+', '-', '*', '/'};


    public static void main(String[] args) {
        boolean running = true;
        System.out.println("Welcome to the procedure calculator");
        while (running) {
            showStartMessage();
            String input = scanner.next().trim();
            if ("calc".equals(input)) {
                initCalcExpression();
            } else if ("help".equals(input)) {
                printHelp();
            } else if ("exit".equals(input)) {
                running = false;
            } else {
                System.out.println("Incorrect input, please enter command again");
            }

        }

    }

    private static void initCalcExpression() {
        double number1 = getNumber();
        char operation = getOperation();
        double number2 = getNumber();
        double res = calc(number1, number2, operation);
        System.out.println("Result: " + res);
    }

    private static double calc(double number1, double number2, char operation) {
        switch (operation) {
            case '+':
                return number1 + number2;
            case '-':
                return number1 - number2;
            case '*':
                return number1 * number2;
            case '/':
                if (number2 != 0) {
                    return number1 / number2;
                } else {
                    System.out.println("Division on zero. Please enter another number");
                    number2 = getNumber();
                    calc(number1, number2, operation);
                }

            default:
                System.out.println("Wrong operation, please enter correct operation.");
                operation = getOperation();
                return calc(number1, number2, operation);
        }
    }

    private static char getOperation() {
        System.out.println("Enter the operation");
        char operation;
        String stringOperation = scanner.next();
        if (stringOperation.length()>0) {
            operation = stringOperation.charAt(0);
        } else {
            System.out.println("Operation didn't found, please enter operation");
            operation = getOperation();
        }
        return operation;
    }

    private static double getNumber() {
        System.out.println("Enter the number");
        double number;
        if (scanner.hasNextDouble()) {
            number = scanner.nextDouble();

            return number;
        } else {
            System.out.println("Entered wrong number");
            scanner.next();
            return getNumber();
        }
    }

    private static void printHelp() {
        System.out.println("Available operations:" + "\n" +
                "1.Addition - +" + "\n" +
                "2.Subtraction - -" + "\n" +
                "3.Composition - *" + "\n" +
                "4.Division - /");
    }

    private static void showStartMessage() {
        System.out.println("For calculate, enter -calc");
        System.out.println("For help, enter -help");
        System.out.println("For exit, enter -exit");
    }
}
