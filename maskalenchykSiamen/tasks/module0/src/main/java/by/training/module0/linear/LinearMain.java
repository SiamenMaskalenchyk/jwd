package tasks.linear;

public class LinearMain {

    public static void main(String[] args) {
        task1(3.4, 5.6);
        task2(28);
        task3(4, 8);
        task4(2, 6, 8);
        task5(222, 26);
        task6(80, 5, 3);
        task7(80);
        task8(3, 6, 1.2);
        task9(7.5, 1.4, 5.1, 1.1);
        task10(33, 45);
        task11(5, 8);
        task12(5, 7, 3, 2);
        task13(4, 3, 2, 4, 7, 12);
        task14(25);
        task15();
        task16(3412);
        task17(5, 9);
        task18(25);
        task19(10);
        task20(15);
        task21(321.456);
        task22(3890);
        task23(54, 34);
        task24(10, 20, 45);
        task25(3, 1, -2);
        task26(5, 3, 45);
        task27(2);
        task28(4);
        task29(5, 3, 5);
        task30(5, 12, 4);
    }

    private static void task30(double r1, double r2, double r3) {
        double totalResistance = (r1 * r2 * r3) / (r1 * r2 + r2 * r3 + r1 * r3);
        System.out.println("Total resistance is " + totalResistance);
    }

    private static void task29(double a, double b, double c) {
        double firstAngle = Math.asin(a / c);
        double secondAngle = Math.asin(a / b);
        double thirdAngle = Math.asin(b / c);
        System.out.println("Angels of the triangle is " + firstAngle + "; " + secondAngle + "; " + thirdAngle + ";");
    }

    private static void task28(int radian) {
        double degree = 180 * radian / Math.PI;
        System.out.println(radian + " radians = " + degree + " degrees");
    }

    private static void task27(int a) {
        int firstOperation = a * a;
        int secondOperation = firstOperation * firstOperation;
        int thirdOperation = secondOperation * secondOperation;
        int foughtOperation = thirdOperation * firstOperation;
        System.out.println("a in 8 power is " + thirdOperation);
        System.out.println("a in 10 power is " + foughtOperation);
    }

    private static void task26(int a, int b, int angle) {
        double triangleSquare = a * b * Math.sin(angle);
        System.out.println("Square of a triangle is " + triangleSquare);
    }

    private static void task25(double a, double b, double c) {
        double discriminant = b * b - 4 * a * c;
        double x1 = (-b + Math.sqrt(discriminant)) / (2 * a);
        double x2 = (-b - Math.sqrt(discriminant)) / (2 * a);
        System.out.println("x1 = " + x1 + "; x2 = " + x2 + ";");
    }

    private static void task24(int a, int b, int angle) {
        double trapezeSquare = a * Math.sin(angle) * (b - Math.cos(angle));
        System.out.println("Square of the trapeze is " + trapezeSquare);
    }

    private static void task23(double r, double R) {
        double ringSquare = Math.PI * (R * R - r * r);
        System.out.println("Square of the ring is " + ringSquare);
    }

    private static void task22(int sec) {
        int minutes = 0;
        int hours = 0;
        if (sec >= 60) {
            minutes = sec / 60;
            sec = sec % 60;
        }
        if (minutes >= 60) {
            hours = minutes / 60;
            minutes = minutes % 60;
        }
        System.out.println(hours + " ч " + minutes + " мин " + sec + " сек");
    }

    private static void task21(double number) {
        double leftPart = (int) number;
        double rightPart = Math.round((number - leftPart) * 1000);
        number = rightPart + leftPart / 1000;
        System.out.println("Number after replacing lef and right parts is " + number);
    }

    private static void task20(double circleLength) {
        double radius = circleLength / (2 * Math.PI);
        double circleSquare = Math.PI * radius * radius;
        System.out.println("Square of the circle " + circleSquare);
    }

    private static void task19(double triangleSideLength) {
        double triangleHeight = triangleSideLength * Math.sqrt(3) / 2;
        double triangleSquare = triangleSideLength * triangleHeight;
        double circumscribedCircleRadius = triangleSideLength / Math.sqrt(3);
        double inscribedCircleRadius = triangleHeight / (2 * Math.sqrt(3));
        System.out.println("Height of a triangle is " + triangleHeight);
        System.out.println("Square of a triangle is " + triangleSquare);
        System.out.println("Radius of circumscribed circle is " + circumscribedCircleRadius);
        System.out.println("Radius of a inscribed circle is " + inscribedCircleRadius);
    }

    private static void task18(int lengthFaceOfTheCube) {
        double squareOneFace = Math.pow(lengthFaceOfTheCube, 2);
        double squareCube = 6 * squareOneFace;
        double volumeCube = Math.pow(squareOneFace, 3);
        System.out.println("Square of the one face is " + squareOneFace);
        System.out.println("Square of a cube is " + squareCube);
        System.out.println("Volume of a cube is " + volumeCube);
    }

    private static void task17(int numberOne, int numberTwo) {
        double averageCube = (Math.pow(numberOne, 3) + Math.pow(numberTwo, 3)) / 2;
        double geometricMeanModules = Math.sqrt(Math.abs(numberOne) + Math.abs(numberTwo));
        System.out.println("Average of number cubes is " + averageCube);
        System.out.println("Geometric mean of number modules is " + geometricMeanModules);
    }

    private static void task16(int number) {
        if (number < 1000 || number > 9999) {
            System.out.println("Incorrect number");
        }
        int firstNumber = number % 10;
        int secondNumber = (number % 100 - firstNumber) / 10;
        int thirdNumber = (number % 1000 - secondNumber - firstNumber) / 100;
        int fourthNumber = (number - thirdNumber * 100 - secondNumber * 10 - firstNumber) / 1000;
        int numbersProduct = firstNumber * secondNumber * thirdNumber * fourthNumber;
        System.out.println("Product numbers of number " + number + " is " + numbersProduct);
    }

    private static void task15() {
        double firstPowerPI = Math.PI;
        double secondPowerPI = Math.PI * Math.PI;
        double thirdPowerPI = secondPowerPI * Math.PI;
        double fourthPowerPI = thirdPowerPI * Math.PI;
        System.out.println("First power of PI is " + firstPowerPI);
        System.out.println("Second power of PI is " + secondPowerPI);
        System.out.println("Third power of PI is " + thirdPowerPI);
        System.out.println("Fourth power of PI is " + fourthPowerPI);
    }

    private static void task14(int radius) {
        double circleLength = 2 * Math.PI * radius;
        double circleSquare = Math.PI * radius * radius;
        System.out.println("Length of a circle is " + circleLength);
        System.out.println("Square of a circle is " + circleSquare);

    }

    private static void task13(int x1, int y1, int x2, int y2, int x3, int y3) {
        double triangleSquare = Math.abs((double) (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1)) / 2;
        System.out.println("Triangle square is " + triangleSquare);
    }

    private static void task12(int x1, int y1, int x2, int y2) {
        double distanceBetweenTwoPoints = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
        System.out.println("Distance between two points is" + distanceBetweenTwoPoints);
    }

    private static void task11(double cachetA, double cachetB) {
        double hypotenuse = Math.sqrt(cachetA * cachetA + cachetB * cachetB);
        double trianglePerimeter = cachetA + cachetB + hypotenuse;
        double triangleSquare = (cachetA + cachetB) / 2;
        System.out.println("Perimeter of a right triangle is " + trianglePerimeter);
        System.out.println("Square of a right triangle is " + triangleSquare);
    }

    private static void task10(double x, double y) {
        double res = ((Math.sin(x) + Math.cos(y)) / (Math.cos(x) - Math.sin(y))) * Math.tan(x * y);
        System.out.println("Result of task10 is " + res);
    }

    private static void task9(double a, double b, double c, double d) {
        double res = a * b / (c * d) - (a * b - c) / (c * d);
        System.out.println("Result of task9 is " + res);
    }

    private static void task8(double a, double b, double c) {
        double res = ((b + Math.sqrt(b * b + 4 * a * c)) / 2 * a) - a * a * a + Math.pow(b, -2);
        System.out.println("Result of task8 is " + res);
    }

    private static void task7(int rectangleLength) {
        int rectangleWidth = rectangleLength / 2;
        int rectangleSquare = rectangleLength * rectangleWidth;
        System.out.println("Square of rectangle is " + rectangleSquare);
    }

    private static void task6(int capacitySmallCanisters, int numberOfSmallCanister, int numberOfBigCanister) {
        int capacityOneSmallCanister = capacitySmallCanisters / numberOfSmallCanister;
        int capacityOneBigCanister = capacityOneSmallCanister + 12;
        int capacityBigCanisters = capacityOneBigCanister * numberOfBigCanister;
        System.out.println("In m big canisters is " + capacityBigCanisters + " litres of milk");
    }

    private static void task5(int num1, int num2) {
        System.out.println("Average of the two numbers is " + (num1 + num2) / 2);
    }

    private static void task4(int a, int b, int c) {
        int z = ((a - 3) * b / 2) + c;
        System.out.println("Result of the task4 is " + z);
    }

    private static void task3(int x, int y) {
        int z = 2 * x + (y - 2) * 5;
        System.out.println("Result of the task3 is " + z);
    }

    private static void task2(int a) {
        int c = 3 + a;
        System.out.println("Result of the task2 is " + c);
    }

    private static void task1(double num1, double num2) {
        System.out.println("Sum is " + (num1 + num2));
        System.out.println("Difference is " + (num1 - num2));
        System.out.println("Product is " + (num1 * num2));
        System.out.println("Division is " + (num1 / num2));
    }

}
