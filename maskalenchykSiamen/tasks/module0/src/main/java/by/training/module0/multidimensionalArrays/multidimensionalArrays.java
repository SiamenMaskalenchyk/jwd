package by.training.module0.multidimensionalArrays;

import java.util.Random;

public class multidimensionalArrays {

    public static void main(String[] args) {
        task1();
        task2();
        int[][] arrTask3456 = {{4, 6, 4, 2, 4}, {45, 34, 5, 3, 2, 4}, {423, 34, 6, 3, 2}, {623, 78, 5, 89, 789}};
        task3(arrTask3456);
        task4(arrTask3456);
        task5(arrTask3456);
        task6(arrTask3456);
        task7();
        task8(5, 9);
        task9();
        int[][] arrTask10 = new int[10][10];
        fillArray(arrTask10);
        task10(2, 5, arrTask10);
        task11(3, 6);
        task12(10);
        task13(8);
        task14(8);
        task15(8);
    }

    private static void task15(int n) {
        System.out.println("Task 15");
        int[][] array = new int[n][n];
        for (int i = 0, j = n, k = 0; i < n; i++, j--, k++) {
            array[i][k] = j;
        }
        printArray(array);
    }

    private static void task14(int n) {
        System.out.println("Task 14");
        int[][] array = new int[n][n];
        for (int i = n - 1, j = n, k = 0; i >= 0; i--, j--, k++) {
            array[i][k] = j;
        }
        printArray(array);
    }

    private static void task13(int n) {
        System.out.println("Task 13");
        int[][] array = new int[n][n];
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                int num = 1;
                for (int j = 0; j < array.length; j++) {
                    array[i][j] = num;
                    num += 1;
                }
            } else {
                int num = n;
                for (int j = 0; j < array.length; j++) {
                    array[i][j] = num;
                    num -= 1;
                }
            }
        }
        printArray(array);
    }

    private static void task12(int n) {
        System.out.println("Task 12");
        int[][] array = new int[n][n];
        for (int i = 1, j = 1; i < array.length; i++, j++) {
            array[i][j] = j;
        }
        printArray(array);
    }

    private static void task11(int m, int n) {
        System.out.println("Task 11");
        int[][] array = new int[m][n];
        fillArray(array);
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                for (int j = array[i].length - 1; j >= 0; j--) {
                    System.out.print(array[i][j] + " ");
                }
                System.out.println();
            }
            if (i % 2 != 0) {
                for (int j = 0; j < array[i].length; j++) {
                    System.out.print(array[i][j] + " ");
                }
                System.out.println();
            }
        }
    }

    private static void task10(int k, int p, int[][] array) {
        System.out.println("Task 10");
        System.out.println("String №" + k + ":");
        for (int i = 0; i < array[k].length; i++) {
            System.out.print(array[k][i] + " ");
        }
        System.out.println();
        System.out.println("Row №" + p + ":");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i][p] + " ");
        }
        System.out.println();
    }

    private static void task9() {
        System.out.println("Task 9");
        int[][] arrTask9 = new int[7][7];
        fillArray(arrTask9);
        int j = 0;
        for (int i = 0; i < arrTask9.length; i++) {
            System.out.print(arrTask9[i][j] + " ");
            j += 1;
        }
        System.out.println();
    }

    private static void task8(int n, int m) {
        System.out.println("Task 8");
        int[][] arr = new int[n][m];
        fillArray(arr);
        int countNumberSeven = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] == 7) {
                    countNumberSeven += 1;
                }
            }
        }
        System.out.println("Array have " + countNumberSeven + " sevens");
    }

    private static void task7() {
        System.out.println("Task 7");
        int[][] arrTask7 = new int[5][5];
        fillArray(arrTask7);
        int sumModulesNegativeNumbers = 0;
        for (int i = 0; i < arrTask7.length; i++) {
            for (int j = 0; j < arrTask7[i].length; j++) {
                if (arrTask7[i][j] < 0) {
                    sumModulesNegativeNumbers += Math.abs(arrTask7[i][j]);
                }
            }
        }
        System.out.println("Sum modules of negative numbers is " + sumModulesNegativeNumbers);
    }

    private static void task6(int[][] arr) {
        System.out.println("Task 6");
        for (int i = 1; i < arr.length; i += 2) {
            if (arr[i][0] > arr[i][arr[i].length - 1]) {
                for (int j = 0; j < arr[i].length; j++) {
                    System.out.print(arr[i][j] + " ");
                }
                System.out.println();
            }
        }
    }

    private static void task5(int[][] arr) {
        System.out.println("Task 5");
        for (int i = 2; i < arr.length; i += 2) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void task4(int[][] arr) {
        System.out.println("Task 4");
        for (int i = 0; i < arr[0].length; i++) {
            System.out.print(arr[0][i] + " ");
        }
        System.out.println();
        for (int i = 0; i < arr[arr.length - 1].length; i++) {
            System.out.print(arr[arr.length - 1][i] + " ");
        }
        System.out.println();
    }


    private static void task3(int[][] arr) {
        System.out.println("Task 3");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i][0] + " ");
        }
        System.out.println();
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i][arr[i].length - 1] + " ");
        }
        System.out.println();
    }

    private static void task2() {
        System.out.println("Task 2");
        int arr[][] = new int[2][3];
        Random random = new Random();
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = random.nextInt();
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void task1() {
        int arr[][] = new int[3][4];
        arr[0][0] = 1;
        arr[1][1] = 1;
        arr[2][2] = 1;
        System.out.println("Task 1");
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static int[][] fillArray(int[][] arr) {
        Random random = new Random();
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = random.nextInt();
            }
        }
        return arr;
    }

    private static void printArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }
}
