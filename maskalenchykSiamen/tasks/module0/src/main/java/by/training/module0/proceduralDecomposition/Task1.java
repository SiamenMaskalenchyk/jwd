package by.training.module0.proceduralDecomposition;

public class Task1 {

    public static void main(String[] args) {
        Point pointA = new Task1.Point("A", 3, 4);
        Point pointB = new Task1.Point("B", 5, 6);
        Point pointC = new Task1.Point("C", 4, 1);
        System.out.println();
        findTriangleSquare(pointA, pointB, pointC);
    }

    private static void findTriangleSquare(Point pointA, Point pointB, Point pointC) {
        double triangleSquare;
        double lengthSectionAB = findSectionLength(pointA, pointB);
        double lengthSectionBC = findSectionLength(pointA, pointB);
        double lengthSectionAC = findSectionLength(pointA, pointB);
        if (isTriangle(lengthSectionAB, lengthSectionBC, lengthSectionAC)) {
            triangleSquare = Math.abs((pointB.getX() - pointA.getX() * (pointC.getY() - pointA.getY())) -
                    (pointC.getX() - pointA.getX()) * (pointB.getY() - pointA.getY())) / 2;
            System.out.println("Square triangle which have points:" + pointA + " " + pointB + " " + pointC + " is " + triangleSquare);
        } else {
            System.out.println("This triangle doesn't exits");
        }
    }

    public static double findSectionLength(Point point1, Point point2) {
        //√ ((X2-X1)²+(Y2-Y1)²).
        return Math.sqrt(Math.pow((point2.getX() - point1.getX()), 2) + Math.pow((point2.getY() - point1.getY()), 2));
    }

    private static boolean isTriangle(double lengthAB, double lengthBC, double lengthAC) {
        //   a+b>c, a+c>b, b+c>a, (a>0, b>0, c>0),
        boolean firstCondition = lengthAB + lengthBC > lengthAC;
        boolean secondCondition = lengthAB + lengthAC > lengthBC;
        boolean thirdCondition = lengthAC + lengthBC > lengthAB;
        return lengthAB > 0 && lengthBC > 0 && lengthAC > 0 && firstCondition && secondCondition && thirdCondition;
    }

    public static class Point {
        private double x;
        private double y;
        String name;

        Point(String name, double x, double y) {
            this.name = name;
            this.x = x;
            this.y = y;
        }

        double getX() {
            return x;
        }

        double getY() {
            return y;
        }

        String getName() {
            return name;
        }

        @Override
        public String toString() {
            return name + " (" + x + "; " + y + ")";
        }
    }
}
