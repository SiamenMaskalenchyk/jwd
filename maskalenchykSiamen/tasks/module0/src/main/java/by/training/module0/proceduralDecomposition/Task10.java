package by.training.module0.proceduralDecomposition;

public class Task10 {

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        findPrintFactorialOddNumber(arr);
    }

    private static int findFactorial(int num) {
        if (num == 1 || num == 0) {
            return 1;
        }
        return num * findFactorial(num - 1);
    }

    private static void findPrintFactorialOddNumber(int[] arr) {
        for (int num : arr) {
            if (num % 2 != 0) {
                System.out.print(findFactorial(num) + "; ");
            }
        }
    }
}
