package by.training.module0.proceduralDecomposition;

import java.util.Random;

public class Task11 {

    public static void main(String[] args) {
        int[] arr = new int[20];
        arr = arrayFill(arr);
        System.out.println("Found sum of array: ");
        findSums(arr);
    }

    private static void findSums(int[] arr) {
        if (arr.length < 3) {
            System.out.println("Array length lower that 3");
            return;
        }
        int bound = arr.length - arr.length % 3;
        for (int i = 0; i < bound; i += 3) {
            int threeElementsSum = arr[i] + arr[i + 1] + arr[i + 2];
            System.out.print(threeElementsSum + "; ");
        }
    }

    private static int[] arrayFill(int[] arr) {
        Random random = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt();
        }
        return arr;
    }
}
