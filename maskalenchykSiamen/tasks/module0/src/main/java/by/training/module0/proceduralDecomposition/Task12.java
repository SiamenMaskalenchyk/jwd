package by.training.module0.proceduralDecomposition;

public class Task12 {

    public static void main(String[] args) {
        int x = 5;
        int y = 3;
        int z = 9;
        int t = 5;
        System.out.println("The square quadrangle " + findSquareQuadrangle(x, y, z, t));
    }

    private static double findSquareQuadrangle(int x, int y, int z, int t) {
        double diagonalLength = findDiagonalLength(x, y);
        return findSquareTriangle(x, y, diagonalLength) + findSquareTriangle(z, t, diagonalLength);
    }

    private static double findSquareTriangle(double a, double b, double c) {
        double p = findHalfPerimeter(a, b, c);
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    private static double findHalfPerimeter(double a, double b, double c) {
        return (a + b + c) / 2;
    }

    private static double findDiagonalLength(double x, double y) {
        return Math.sqrt(x * x + y * y);
    }
}
