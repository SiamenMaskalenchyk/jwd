package by.training.module0.proceduralDecomposition;

public class Task13 {

    public static void main(String[] args) {
        int num = 4657392;
        System.out.println("Numerals array: " + printArray(Task17.getNumeral(num)));
    }

    public static String printArray(int[] nums) {
        String output = "";
        for (int num : nums) {
            output += num + "; ";
        }
        return output;
    }
}
