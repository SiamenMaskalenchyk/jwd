package by.training.module0.proceduralDecomposition;

public class Task14 {

    public static void main(String[] args) {
        double firstNumber = 4.43351;
        double secondNumber = 34166466465.8;
        double numberWhichHaveMoreNumeral = findNumberWithMoreNumerals(firstNumber, secondNumber);
        System.out.println("Number " + numberWhichHaveMoreNumeral +" have more or equal number of numeral");
    }

    private static double findNumberWithMoreNumerals(double firstNumber, double secondNumber) {
        String firstString = String.valueOf(firstNumber);
        firstString = firstString.replace(".", "");
        String secondString = String.valueOf(secondNumber);
        secondString = secondString.replace(".", "");
        return firstString.length() >= secondString.length() ? firstNumber : secondNumber;
    }
}
