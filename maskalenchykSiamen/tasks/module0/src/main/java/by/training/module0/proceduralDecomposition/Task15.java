package by.training.module0.proceduralDecomposition;

public class Task15 {

    public static void main(String[] args) {
        int k = 5;
        int n = 200;
        int[] nums = {34, 52, 5, 54, 2, 56, 2, 56, 14, 6, 2, 678, 54, 97, 2, 5};
        int[] arr = createArray(k, n, nums);
        printArray(arr);
    }

    private static int[] createArray(int k, int n, int... nums) {
        int[] res = new int[findLengthNecessaryArray(k, n, nums)];
        int j = 0;
        for (int num : nums) {
            int sumNumerals = findNumeralsSum(num);
            if (sumNumerals == k && num <= n) {
                res[j] = num;
                j += 1;
            }
        }
        return res;
    }

    private static int findLengthNecessaryArray(int k, int n, int... nums) {
        int count = 0;
        for (int num : nums) {
            int sumNumerals = findNumeralsSum(num);
            if (sumNumerals == k && num <= n) {
                count += 1;
            }
        }
        return count;
    }

    private static int findNumeralsSum(int num) {
        int[] numerals = Task17.getNumeral(num);
        return sumArrayElements(numerals);
    }

    private static int sumArrayElements(int[] arr) {
        int sum = 0;
        for (int i : arr) {
            sum += i;
        }
        return sum;
    }

    private static void printArray(int[] arr) {
        for (int i : arr) {
            System.out.print(i + "; ");
        }
    }
}
