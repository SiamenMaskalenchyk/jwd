package by.training.module0.proceduralDecomposition;

public class Task16 {

    public static void main(String[] args) {
        int n = 20;
        findPrintTwins(n);
    }

    private static void findPrintTwins(int n) {
        if (n < 2) {
            System.out.println("Incorrect number");
            return;
        }
        int arrayLength = findArrayLength(n);
        int[] arr = new int[arrayLength];
        for (int i = 0, j = n; i < arr.length; i++, j++) {
            arr[i] = j;
        }
        System.out.println("Twins: ");
        int temp = arr[0];
        for (int i = 1; i < arr.length - 1; i++) {

            if (isTwins(temp, arr[i])) {
                System.out.println(temp + " - " + arr[i]);
                temp = arr[i - 1];
            }
        }
    }

    private static boolean isTwins(int n1, int n2) {
        return n2 - n1 == 2;
    }

    private static int findArrayLength(int n) {
        int length = 0;
        int currentValue = 2 * n;
        while (currentValue >= n) {
            length++;
            currentValue--;
        }
        return length;
    }
}
