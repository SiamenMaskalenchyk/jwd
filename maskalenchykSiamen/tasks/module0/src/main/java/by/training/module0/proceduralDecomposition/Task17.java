package by.training.module0.proceduralDecomposition;

public class Task17 {

//            1,2,3,4,5,6,7,8,9,153,370,371,407,1634,8208,
//            9474,54 748,92 727,93 084,548 834,
//            1 741 725,4 210 818,9 800 817,9 926 315,
//            24 678 050,24 678 051,88 593 477,146 511 208,472 335 975,534 494 836,912 985 153,4 679 307 774.

    public static void main(String[] args) {
        int k = 1000;
        System.out.println("Armstrong numbers");
        findPrintArmstrongNumbers(k);
    }

    private static void findPrintArmstrongNumbers(int k) {
        for (int i = 1; i <= k; i++) {
            int[] numerals = getNumeral(i);
            if (isArmstrongNumber(numerals, i)) {

                System.out.print(i + "; ");
            }
        }
    }

    static int[] getNumeral(int num) {
        String numString = String.valueOf(num);
        int[] res = new int[numString.length()];
        for (int i = 1; i <= numString.length(); i++) {
            String numeral = numString.substring(i - 1, i);
            res[i - 1] = Integer.parseInt(numeral);
        }
        return res;
    }

    private static boolean isArmstrongNumber(int[] numerals, int k) {
        int n = numerals.length;
        int sum = 0;
        for (int numeral : numerals) {
            sum += Math.pow(numeral, n);
        }
        return sum == k;
    }
}
