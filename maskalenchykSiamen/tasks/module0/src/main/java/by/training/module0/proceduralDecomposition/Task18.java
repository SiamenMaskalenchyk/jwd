package by.training.module0.proceduralDecomposition;

public class Task18 {

    public static void main(String[] args) {
        int num1 = 12345;
        int num2 = 13461;
        System.out.println(checkingSequence(num1));
        System.out.println(checkingSequence(num2));
    }

    private static boolean checkingSequence(int num) {
        int[] numerals = Task17.getNumeral(num);
        for (int i = 0; i < numerals.length - 2; i++) {
            if (numerals[0] > numerals[i + 1] || numerals[i] > numerals[i + 2]) {
                return false;
            }
        }
        return true;
    }
}
