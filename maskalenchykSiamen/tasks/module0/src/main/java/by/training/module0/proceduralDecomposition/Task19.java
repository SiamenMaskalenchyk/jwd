package by.training.module0.proceduralDecomposition;

public class Task19 {

    public static void main(String[] args) {
        int num = 35773593;
        printResultSumOddNumerals(sumOddNumerals(num));
    }

    private static int sumOddNumerals(int num) {
        int[] numerals = Task17.getNumeral(num);
        int sum = 0;
        for (int i = 0; i < numerals.length; i++) {
            if (numerals[i] % 2 != 0) {
                sum += numerals[i];
            } else {
                sum = 0;
                break;
            }
        }
        return sum;
    }

    private static void printResultSumOddNumerals(int sumOddNumerals) {
        if (sumOddNumerals != 0) {
            System.out.println("Odd numerals sum: " + sumOddNumerals);
            System.out.println("Number of even numerals in odd numeral sum: " + checkNumberEvenNumerals(sumOddNumerals));
        } else {
            System.out.println("Number contains the even numeral");
        }
    }

    private static int checkNumberEvenNumerals(int sumOddNumerals) {
        int[] numerals = Task17.getNumeral(sumOddNumerals);
        int countEvenNumerals = 0;
        for (int numeral : numerals) {
            if (numeral % 2 == 0) {
                countEvenNumerals += 1;
            }
        }
        return countEvenNumerals;
    }
}
