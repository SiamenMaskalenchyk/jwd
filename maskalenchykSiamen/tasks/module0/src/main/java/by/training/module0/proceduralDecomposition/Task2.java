package by.training.module0.proceduralDecomposition;

public class Task2 {

    public static void main(String[] args) {
        int a = 25;
        int b = 30;
        System.out.println("Greatest common factor numbers " + a + "; " + b + ": " + Task9.findGreatestCommonFactor(a, b));
        System.out.println("Least common multiple numbers " + a + "; " + b + ": " + findLeastCommonMultiple(a, b));
    }

    //НОК
    public static int findLeastCommonMultiple(int a, int b) {
        int greatestCommonFactor = Task9.findGreatestCommonFactor(a, b);
        return a * b / greatestCommonFactor;
    }
}
