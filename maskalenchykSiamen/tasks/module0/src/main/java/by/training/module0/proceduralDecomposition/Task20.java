package by.training.module0.proceduralDecomposition;

public class Task20 {

    public static void main(String[] args) {
        int num = 1234;
        System.out.println("The number acts:" +numberActs(num));
    }

    private static int numberActs(int num) {
        int count = 0;

     while(num > 0){
         int sumNumerals = sumNumerals(num);
         num -= sumNumerals;
         count += 1;
     }
        return count;
    }

    private static int sumNumerals(int num) {
        int[] numerals = Task17.getNumeral(num);
        int sum = 0;
        for (int numeral : numerals) {
            sum += numeral;
        }
        return sum;
    }
}
