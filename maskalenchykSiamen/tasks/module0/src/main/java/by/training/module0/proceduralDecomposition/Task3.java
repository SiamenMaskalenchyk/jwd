package by.training.module0.proceduralDecomposition;

public class Task3 {

    public static void main(String[] args) {
        int num1 = 16;
        int num2 = 36;
        int num3 = 128;
        int num4 = 12;
        System.out.println("Greatest common factor numbers " + num1 + "; " + num2 + "; " + num3 + "; " + num4 + "; " + ": " + findGreatestCommonFactor(num1, num2, num3, num4));
    }

    private static int findGreatestCommonFactor(int num1, int num2, int num3, int num4) {
        return Task9.findGreatestCommonFactor(Task9.findGreatestCommonFactor(num1, num2), Task9.findGreatestCommonFactor(num3, num4));
    }
}
