package by.training.module0.proceduralDecomposition;

public class Task4 {

    public static void main(String[] args) {
        int num1 = 25;
        int num2 = 120;
        int num3 = 130;
        System.out.println("Least common multiple numbers " + num1 + "; " + num2 + "; " + num3 + ": " + findLeastCommonMultiple(num1, num2, num3));
    }

    private static int findLeastCommonMultiple(int num1, int num2, int num3) {
        return Task2.findLeastCommonMultiple(Task2.findLeastCommonMultiple(num1, num2), num3);
    }

}
