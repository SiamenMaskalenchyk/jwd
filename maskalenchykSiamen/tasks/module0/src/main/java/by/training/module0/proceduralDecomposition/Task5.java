package by.training.module0.proceduralDecomposition;

public class Task5 {

    public static void main(String[] args) {
        double a = 1;
        double b = -2;
        double c = 22;
        System.out.println("Sum of max numbers from " + a + "; " + b + "; " + c + "; " + "is " + maxSum(a, b, c));
        System.out.println("Sum of min numbers from " + a + "; " + b + "; " + c + "; " + "is " + minSum(a, b, c));
    }

    private static double minSum(double a, double b, double c) {
        double maxAB = max(a, b);
        return min(a, b) + min(maxAB, c);
    }

    private static double maxSum(double a, double b, double c) {
        double minAB = min(a, b);
        return max(a, b) + max(minAB, c);
    }

    private static double max(double a, double b) {
        return a > b ? a : b;
    }

    private static double min(double a, double b) {
        return a < b ? a : b;
    }
}
