package by.training.module0.proceduralDecomposition;

public class Task6 {

    public static void main(String[] args) {
        int a = 5;
        System.out.println("Square regular hexagon is " + squareRegularHexagon(a));
    }

    private static double squareRegularHexagon(int a) {
        return findTriangleSquare(a) * 6;
    }

    private static double findTriangleSquare(int a) {
        return (Math.sqrt(a) * a * a / 4);
    }
}
