package by.training.module0.proceduralDecomposition;

public class Task7 {

    public static void main(String[] args) {
        Task1.Point[] points = {new Task1.Point("A", 5, 5), new Task1.Point("B", 1, 1), new Task1.Point("C", 2, 2),
                                new Task1.Point("D", 7, 7), new Task1.Point("E", 8, 9), new Task1.Point("F", 4, 7)};
        String[] res = findNamePointsWithMaxDistance(points);
        System.out.println("Max distance between points: " + res[0] + ", " + res[1]);
    }

    private static String[] findNamePointsWithMaxDistance(Task1.Point[] points) {
        String[] res = new String[2];
        double maxLength = Double.MIN_VALUE;
        for (int i = 0; i < points.length; i++) {
            for (int j = i; j < points.length; j++) {
                double currentLength = Task1.findSectionLength(points[i], points[j]);
                if (maxLength < currentLength) {
                    maxLength = currentLength;
                    res[0] = points[i].getName();
                    res[1] = points[j].getName();
                }
            }
        }
        return res;
    }
}
