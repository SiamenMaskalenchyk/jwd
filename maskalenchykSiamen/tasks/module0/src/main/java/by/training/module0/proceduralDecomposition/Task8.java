package by.training.module0.proceduralDecomposition;

public class Task8 {

    public static void main(String[] args) {
        int[] arr = {456, 7, 34, 7, 4, 7, 8, 3, 2, 894,999};
        System.out.println("Second max number in the array: " + findSecondMaxNumber(arr));
    }

    private static int findSecondMaxNumber(int[] arr) {
        int max = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                secondMax = max;
                max = arr[i];
            }
        }
        return secondMax;
    }
}
