package by.training.module0.proceduralDecomposition;

public class Task9 {

    public static void main(String[] args) {
        int[] nums = {331, 463, 733};
        printResults(isSimpleNums(nums), nums);
    }

    private static void printResults(boolean simpleNums, int[] nums) {
        if (simpleNums) {
            System.out.println("Numbers " + Task13.printArray(nums) + " are simple");
        } else {
            System.out.println("Numbers " + Task13.printArray(nums) + " aren't simple");
        }
    }

    private static boolean isSimpleNums(int[] nums) {
        boolean first = findGreatestCommonFactor(nums[0], nums[1]) == 1;
        boolean second = findGreatestCommonFactor(nums[1], nums[2]) == 1;
        boolean third = findGreatestCommonFactor(nums[0], nums[2]) == 1;
        return first && second && third;
    }

    //НОД
    static int findGreatestCommonFactor(int num1, int num2) {
        if (num2 == 0) {
            return Math.abs(num1);
        }
        return findGreatestCommonFactor(num2, num1 % num2);
    }
}
