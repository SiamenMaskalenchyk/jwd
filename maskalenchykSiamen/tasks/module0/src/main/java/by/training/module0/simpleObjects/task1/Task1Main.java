package by.training.module0.simpleObjects.task1;

public class Task1Main {

    public static void main(String[] args) {
        Test1 test1  = new Test1(5,3);
        Test1 test2  = new Test1(15,33);
        test1.printVariables();
        System.out.println(test1.findMaxVariable());
        System.out.println(test1.sumVariables());
        System.out.println("======================");
        test2.printVariables();
        System.out.println(test2.findMaxVariable());
        System.out.println(test2.sumVariables());
    }
}
