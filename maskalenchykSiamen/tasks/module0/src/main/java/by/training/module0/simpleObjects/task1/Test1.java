package by.training.module0.simpleObjects.task1;

public class Test1 {

    private int firstVariable;
    private int secondVariable;

    public Test1(int firstVariable, int secondVariable) {
        this.firstVariable = firstVariable;
        this.secondVariable = secondVariable;
    }

    public void setFirstVariable(int firstVariable) {
        this.firstVariable = firstVariable;
    }

    public void setSecondVariable(int secondVariable) {
        this.secondVariable = secondVariable;
    }

    public void printVariables() {
        System.out.println("First variable: " + this.firstVariable);
        System.out.println("Second variable: " + this.secondVariable);
    }

    public int sumVariables() {
        return this.firstVariable + this.secondVariable;
    }

    public int findMaxVariable() {
        return this.firstVariable > this.secondVariable ? firstVariable : secondVariable;
    }
}
