package by.training.module0.simpleObjects.task10;


import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.Objects;

public class Airline {

    private String destination;
    private int flightNumber;
    private TypePlane typePlane;
    private String dayOfWeek;
    private LocalDate departureTime;

    public Airline(String destination, int flightNumber, TypePlane typePlane, String departureTimeString) {
        this.destination = destination;
        this.flightNumber = flightNumber;
        this.typePlane = typePlane;
        this.departureTime = LocalDate.parse(departureTimeString, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        this.dayOfWeek = setDayOfWeek(this.departureTime);

    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public TypePlane getTypePlane() {
        return typePlane;
    }

    public void setTypePlane(TypePlane typePlane) {
        this.typePlane = typePlane;
    }

    public LocalDate getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDate departureTime) {
        this.departureTime = departureTime;
        this.dayOfWeek = setDayOfWeek(departureTime);
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    private String setDayOfWeek(LocalDate departureTime) {
        DayOfWeek dayOfWeek = departureTime.getDayOfWeek();
        Locale localeRu = new Locale("ru", "RU");
        return dayOfWeek.getDisplayName(TextStyle.FULL, localeRu);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Airline airline = (Airline) o;
        return flightNumber == airline.flightNumber &&
                Objects.equals(destination, airline.destination) &&
                typePlane == airline.typePlane &&
                Objects.equals(dayOfWeek, airline.dayOfWeek) &&
                Objects.equals(departureTime, airline.departureTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(destination, flightNumber, typePlane, dayOfWeek, departureTime);
    }

    @Override
    public String toString() {
        return "Airline: " + destination + " " + flightNumber + " " + typePlane.getType() + " type " + departureTime.toString() + " " + dayOfWeek;
    }
}
