package by.training.module0.simpleObjects.task10;

import java.util.Iterator;
import java.util.List;

public class Airlines {

    private List<Airline> airlines;

    public Airlines(List<Airline> airlines) {
        this.airlines = airlines;
    }

    public List<Airline> getAirlines() {
        return airlines;
    }

    public void addAirline(Airline airline){
        airlines.add(airline);
    }

    public void removeAirline(Airline airline){
        for(Iterator<Airline> iterator = airlines.iterator(); iterator.hasNext();){
            Airline tempAirline = iterator.next();
            if(tempAirline.equals(airline)){
                iterator.remove();
            }
        }
    }
}
