package by.training.module0.simpleObjects.task10;

import by.training.module0.simpleObjects.task10.service.DestinationSearcher;
import by.training.module0.simpleObjects.task10.service.Searching;
import by.training.module0.simpleObjects.task10.service.WeekDayAfterGivenTime;
import by.training.module0.simpleObjects.task10.service.WeekDaySearcher;

import java.util.LinkedList;
import java.util.List;

public class Task10Main {

    public static void main(String[] args) {
        List<Airline> airlines = init();
        Airlines airlinesContainer = new Airlines(airlines);
        Searching destinationSearcher = new DestinationSearcher(airlinesContainer);
        Searching weekDaySearcher = new WeekDaySearcher(airlinesContainer);
        Searching weekDayAfterGivenTime = new WeekDayAfterGivenTime(airlinesContainer);
        destinationSearcher.printAirlines("London");
        weekDaySearcher.printAirlines("воскресенье");
        weekDayAfterGivenTime.printAirlines("2020-10-07T10:12:30");
    }

    private static List<Airline> init() {
        List<Airline> airlines = new LinkedList<>();
        Airline airline1 = new Airline("Moscow", 1, TypePlane.MILITARY, "2020-11-06T10:16:30");
        Airline airline2 = new Airline("Berlin", 2, TypePlane.PRIVATE, "2020-10-07T10:17:30");
        Airline airline3 = new Airline("Sydney", 3, TypePlane.PRIVATE, "2020-11-06T10:15:30");
        Airline airline4 = new Airline("London", 4, TypePlane.PASSENGER, "2020-10-07T10:12:30");
        Airline airline5 = new Airline("London", 5, TypePlane.PASSENGER, "2020-03-08T10:11:30");
        airlines.add(airline1);
        airlines.add(airline2);
        airlines.add(airline3);
        airlines.add(airline4);
        airlines.add(airline5);
        return airlines;

    }
}