package by.training.module0.simpleObjects.task10;

public enum TypePlane {

    PASSENGER("passenger"),
    PRIVATE("private"),
    MILITARY("military");

    String type;

    TypePlane(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
