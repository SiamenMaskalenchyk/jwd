package by.training.module0.simpleObjects.task10.service;

import by.training.module0.simpleObjects.task10.Airline;
import by.training.module0.simpleObjects.task10.Airlines;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class WeekDayAfterGivenTime implements Searching {

    private Airlines airlines;

    public WeekDayAfterGivenTime(Airlines airlines) {
        this.airlines = airlines;
    }

    @Override
    public void printAirlines(String parameter) {
        boolean isNotFind = true;
        System.out.println("Airlines with departure day later than " + parameter);
        LocalDate localDate = LocalDate.parse(parameter, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        for (Airline airline : airlines.getAirlines()) {
            if (airline.getDepartureTime().compareTo(localDate) >= 0) {
                System.out.println(airline.toString());
                isNotFind = false;
            }
            if (isNotFind) {
                System.out.println("Didn't find");
            }
        }
    }
}
