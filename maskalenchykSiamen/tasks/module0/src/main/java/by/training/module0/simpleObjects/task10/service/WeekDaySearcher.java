package by.training.module0.simpleObjects.task10.service;

import by.training.module0.simpleObjects.task10.Airline;
import by.training.module0.simpleObjects.task10.Airlines;

public class WeekDaySearcher implements Searching {

    private Airlines airlines;

    public WeekDaySearcher(Airlines airlines) {
        this.airlines = airlines;
    }

    @Override
    public void printAirlines(String parameter) {
        boolean isNotFind = true;
        System.out.println("Airlines with departure day " + parameter);
        for (Airline airline : airlines.getAirlines()) {
            if (airline.getDayOfWeek().equals(parameter)) {
                System.out.println(airline.toString());
                isNotFind = false;
            }
        }
        if (isNotFind) {
            System.out.println("Didn't find");
        }
    }
}
