package by.training.module0.simpleObjects.task2;

public class Task2Main {

    public static void main(String[] args) {
        Test2 test = new Test2();
        test.setFirstVariable(2);
        test.setSecondVariable(23);
        Test2 test2 = new Test2(2,4);
        System.out.println(test2.getFirstVariable());
        System.out.println(test2.getSecondVariable());
    }
}
