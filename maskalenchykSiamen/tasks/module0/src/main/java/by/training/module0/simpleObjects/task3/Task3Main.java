package by.training.module0.simpleObjects.task3;

import by.training.module0.simpleObjects.task3.model.Student;
import by.training.module0.simpleObjects.task3.service.StatisticExcellentStudents;
import by.training.module0.simpleObjects.task3.service.StatisticService;

public class Task3Main {

    public static void main(String[] args) {
        StatisticService statisticService = new StatisticExcellentStudents();
        Student[] students = {
                new Student("Proxod A.I.", 235, 7, 9, 8, 9, 6),
                new Student("Niklov A.I.", 235, 7, 9, 8, 9, 6),
                new Student("Antonov A.I.", 235, 7, 9, 10, 9, 6),
                new Student("Sidorov A.I.", 34, 7, 9, 8, 9, 6),
                new Student("Proxorov A.I.", 235, 9, 9, 9, 9, 9),
                new Student("Levin A.I.", 25, 7, 9, 8, 9, 6),
                new Student("Bogomol A.I.", 235, 9, 9, 9, 9, 9),
                new Student("Lenat A.I.", 255, 7, 9, 8, 9, 6),
                new Student("Kletov A.I.", 235, 10, 9, 8, 9, 6),
                new Student("Leshov G.I.", 215, 7, 9, 10, 9, 6)};
        System.out.println("Excellent students:");
        statisticService.printStatistic(statisticService.findStatistic(students));
    }



}
