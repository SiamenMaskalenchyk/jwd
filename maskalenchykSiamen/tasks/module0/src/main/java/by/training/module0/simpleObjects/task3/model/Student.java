package by.training.module0.simpleObjects.task3.model;

public class Student {

    private String name;
    private int groupNumber;
    private int[] marks;

    public Student(String name, int groupNumber, int physicMark, int mathMark,int javaMark, int oopMark, int literatureMark) {
        this.name = name;
        this.groupNumber = groupNumber;
        marks = new int[]{physicMark, mathMark, javaMark, oopMark, literatureMark};
    }

    public String getName() {
        return name;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public int[] getMarks() {
        return marks;
    }

    public void setMarks(int[] marks) {
        this.marks = marks;
    }
}
