package by.training.module0.simpleObjects.task3.service;

import by.training.module0.simpleObjects.task3.model.Student;

import java.util.LinkedList;
import java.util.List;

public class StatisticExcellentStudents implements StatisticService {

    @Override
    public List<Student> findStatistic(Student[] students) {
        List<Student> studentList = new LinkedList<>();
        for (Student student : students) {
            if (checkMarks(student.getMarks())) {
                studentList.add(student);
            }
        }
        return studentList;
    }

    private boolean checkMarks(int[] marks) {
        for (int mark : marks) {
            if (mark < 9) {
                return false;
            }
        }
        return true;
    }
}
