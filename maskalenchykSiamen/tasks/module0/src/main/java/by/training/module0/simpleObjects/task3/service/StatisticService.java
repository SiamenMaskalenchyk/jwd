package by.training.module0.simpleObjects.task3.service;

import by.training.module0.simpleObjects.task3.model.Student;

import java.util.List;

public interface StatisticService {

    List<Student> findStatistic(Student[] students);

    default void printStatistic(List<Student> students) {
        for (Student student : students) {
            System.out.println(student.getName() + " " + student.getGroupNumber());
        }
    }
}
