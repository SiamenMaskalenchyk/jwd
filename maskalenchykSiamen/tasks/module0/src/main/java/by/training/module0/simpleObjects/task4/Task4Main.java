package by.training.module0.simpleObjects.task4;

import by.training.module0.simpleObjects.task4.model.Train;
import by.training.module0.simpleObjects.task4.service.SearchTrainByNumber;
import by.training.module0.simpleObjects.task4.service.SearchingTrain;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Task4Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Train[] trains = {
                new Train(55, "Future", new GregorianCalendar(2020, Calendar.JUNE, 15, 15, 15, 0)),
                new Train(155, "Minsk", new GregorianCalendar(2020, Calendar.SEPTEMBER, 15, 9, 15, 0)),
                new Train(525, "Brest", new GregorianCalendar(2020, Calendar.DECEMBER, 15, 11, 25, 0)),
                new Train(355, "Minsk", new GregorianCalendar(2020, Calendar.MARCH, 15, 12, 35, 0)),
                new Train(545, "Gomel", new GregorianCalendar(2020, Calendar.JUNE, 15, 16, 45, 0)),
                new Train(551, "Past", new GregorianCalendar(2020, Calendar.JULY, 15, 15, 15, 0))
        };
        SearchingTrain searchingTrain = new SearchTrainByNumber(trains);
        System.out.println("Start Array");
        printArray(trains);
        System.out.println("==========================");
        Arrays.sort(trains, Train.numberComparator);
        System.out.println("Sorted array by number");
        printArray(trains);
        System.out.println("==========================");
        Arrays.sort(trains, Train.destinationComparator);
        System.out.println("Sorted array by destination");
        printArray(trains);
        printFoundTrain(searchingTrain);
    }

    private static void printArray(Train[] trains) {
        for (Train train : trains) {
            System.out.println(train);
        }
    }

    private static void printFoundTrain(SearchingTrain searchingTrain) {
        int trainNumber;
        while (true) {
            System.out.println("Enter the train number");
            if (scanner.hasNextInt()) {
                trainNumber = scanner.nextInt();
                break;
            } else {
                scanner.nextLine();
            }
        }
        Train foundTrain = searchingTrain.search(String.valueOf(trainNumber));
        if (foundTrain == null) {
            System.out.println("Train didn't find");
        } else {
            System.out.println(foundTrain);
        }
    }

}
