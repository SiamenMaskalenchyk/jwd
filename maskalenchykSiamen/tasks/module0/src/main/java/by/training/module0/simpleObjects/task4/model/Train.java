package by.training.module0.simpleObjects.task4.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;

public class Train {

    private int number;
    private String destination;
    private Calendar departureTime;
    private static SimpleDateFormat formatter;

    static {
        formatter =new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    }

    public Train(int number, String destination, Calendar departureTime) {
        this.number = number;
        this.destination = destination;
        this.departureTime = departureTime;
    }

    public int getNumber() {
        return number;
    }

    public static Comparator<Train> numberComparator = new Comparator<Train>() {
        @Override
        public int compare(Train train1, Train train2) {
            return train1.number - train2.number;
        }
    };

    public static Comparator<Train> destinationComparator = new Comparator<Train>() {
        @Override
        public int compare(Train train1, Train train2) {
            if (train1.destination.equals(train2.destination)) {
                return train1.departureTime.compareTo(train2.departureTime);
            }
            return train1.destination.compareTo(train2.destination);
        }
    };

    @Override
    public String toString() {
        return "Train:" +
                "number = " + number +
                ", destination = '" + destination + '\'' +
                ", departureTime = " + formatter.format(departureTime.getTime());
    }
}
