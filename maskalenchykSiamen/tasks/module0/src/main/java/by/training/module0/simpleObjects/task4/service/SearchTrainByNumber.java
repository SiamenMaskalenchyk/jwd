package by.training.module0.simpleObjects.task4.service;

import by.training.module0.simpleObjects.task4.model.Train;

public class SearchTrainByNumber implements SearchingTrain {

    private Train[] trains;

    public SearchTrainByNumber(Train[] trains) {
        this.trains = trains;
    }

    @Override
    public Train search(String input) {
        int numberTrain = Integer.parseInt(input);
        for (Train train : trains) {
            if (train.getNumber()==numberTrain){
                return train;
            }
        }
        return null;
    }
}
