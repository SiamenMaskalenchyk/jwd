package by.training.module0.simpleObjects.task4.service;

import by.training.module0.simpleObjects.task4.model.Train;

public interface SearchingTrain {

    Train search(String input);
}
