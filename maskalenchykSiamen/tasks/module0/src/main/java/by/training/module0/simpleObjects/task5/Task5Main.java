package by.training.module0.simpleObjects.task5;

import by.training.module0.simpleObjects.task5.counter.Counter;
import by.training.module0.simpleObjects.task5.service.ChangingPosition;
import by.training.module0.simpleObjects.task5.service.MagnifierPosition;
import by.training.module0.simpleObjects.task5.service.ReducerPosition;

public class Task5Main {

    public static void main(String[] args) {
        ChangingPosition reducer = new ReducerPosition();
        ChangingPosition magnifier = new MagnifierPosition();
        Counter counter1 = new Counter();
        System.out.println("Counter #1 position " + counter1.getCurrentPosition());
        magnifier.changePosition(counter1);
        System.out.println("Counter #1 position " + counter1.getCurrentPosition());
        reducer.changePosition(counter1);
        System.out.println("Counter #1 position " + counter1.getCurrentPosition());
        reducer.changePosition(counter1);
        System.out.println("Counter #1 position " + counter1.getCurrentPosition());
        System.out.println("====================");
        Counter counter2 = new Counter(5, -444, 50);
        System.out.println("Counter #2 position " + counter2.getCurrentPosition());
        reducer.changePosition(counter2);
        System.out.println("Counter #2 position " + counter2.getCurrentPosition());
        magnifier.changePosition(counter2);
        System.out.println("Counter #2 position " + counter2.getCurrentPosition());
    }
}
