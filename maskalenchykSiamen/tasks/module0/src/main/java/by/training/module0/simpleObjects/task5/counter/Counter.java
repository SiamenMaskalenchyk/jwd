package by.training.module0.simpleObjects.task5.counter;

public class Counter {

    private int currentPosition;
    private int min;
    private int max;

    public Counter() {
        this(0, 0, 100);
    }

    public Counter(int currentPosition, int min, int max) {
        this.min = min;
        this.max = max;

    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

}
