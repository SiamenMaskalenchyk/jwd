package by.training.module0.simpleObjects.task5.service;

import by.training.module0.simpleObjects.task5.counter.Counter;

public interface ChangingPosition {

    void changePosition(Counter counter);
}
