package by.training.module0.simpleObjects.task5.service;

import by.training.module0.simpleObjects.task5.counter.Counter;

public class MagnifierPosition implements ChangingPosition {

    @Override
    public void changePosition(Counter counter) {
        int newCurrentPosition = counter.getCurrentPosition() + 1;
        if (newCurrentPosition > counter.getMax()) {
            newCurrentPosition -= 1;
        }
        counter.setCurrentPosition(newCurrentPosition);
    }
}
