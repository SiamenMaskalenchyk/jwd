package by.training.module0.simpleObjects.task5.service;

import by.training.module0.simpleObjects.task5.counter.Counter;

public class ReducerPosition implements ChangingPosition {

    @Override
    public void changePosition(Counter counter) {
            int newCurrentPosition = counter.getCurrentPosition() - 1;
            if (newCurrentPosition < counter.getMin()) {
                newCurrentPosition += 1;
            }
            counter.setCurrentPosition(newCurrentPosition);
    }
}
