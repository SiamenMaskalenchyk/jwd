package by.training.module0.simpleObjects.task6;

import by.training.module0.simpleObjects.task6.time.Time;

public class Task6Main {

    public static void main(String[] args) {
        Time time = new Time(3600);
        System.out.println(time);
        time = new Time (5,4,3);
        System.out.println(time);
        time.addHours(2);
        time.addMinutes(24);
        time.addSeconds(12);
        System.out.println(time);
    }
}
