package by.training.module0.simpleObjects.task6.time;

public class Time {

    private int hours;
    private int minutes;
    private int seconds;

    public Time(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        formatTime();
    }

    public Time(int minutes, int seconds) {
        this.minutes = minutes;
        this.seconds = seconds;
        formatTime();
    }

    public Time(int seconds) {
        this.seconds = seconds;
        formatTime();
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        if (hours < 0) {
            hours = 0;
        }
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        if (minutes < 0) {
            minutes = 0;
        }
        this.minutes = minutes;
        formatTime();
    }

    public int getSeconds() {
        return seconds;
    }

    public void addHours(int hours) {
        if (hours < 0) {
            hours = 0;
        }
        this.hours += hours;
    }

    public void addMinutes(int minutes) {
        if (minutes < 0) {
            minutes = 0;
        }
        this.minutes += minutes;
    }

    public void addSeconds(int seconds) {
        if (seconds < 0) {
            seconds = 0;
        }
        this.seconds += seconds;
    }

    public void setSeconds(int seconds) {
        if (seconds < 0) {
            seconds = 0;
        }
        this.seconds = seconds;
        formatTime();
    }

    private void formatTime() {
        if (seconds >= 60) {
            minutes += seconds / 60;
            seconds = seconds % 60;
        }
        if (minutes >= 60) {
            hours += minutes / 60;
            minutes = minutes % 60;
        }
    }

    @Override
    public String toString() {
        String out = "";
        if (hours != 0) {
            out += this.hours + "h ";
        }
        if (minutes != 0) {
            out += this.minutes + "m ";
        }
        if (seconds != 0) {
            out += this.seconds + "sec ";
        }
        return out;
    }
}
