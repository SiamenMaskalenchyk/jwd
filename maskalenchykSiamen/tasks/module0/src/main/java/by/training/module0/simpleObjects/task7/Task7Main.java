package by.training.module0.simpleObjects.task7;

import by.training.module0.simpleObjects.task7.triangle.Point;
import by.training.module0.simpleObjects.task7.triangle.Triangle;

public class Task7Main {

    public static void main(String[] args) {
        Triangle triangle = new Triangle(new Point(5, 5), new Point(10, 10), new Point(-5, -5));
        System.out.println("Square: " + triangle.getSquare());
        System.out.println("Perimeter:" + triangle.getPerimeter());
        System.out.println("Medians center: " + triangle.getMediansCenter());
    }
}
