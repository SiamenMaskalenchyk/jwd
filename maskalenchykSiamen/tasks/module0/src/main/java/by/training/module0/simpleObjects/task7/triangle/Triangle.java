package by.training.module0.simpleObjects.task7.triangle;

public class Triangle {

    private final Point pointA;
    private final Point pointB;
    private final Point pointC;

    public Triangle(Point pointA, Point pointB, Point pointC) {
        this.pointA = pointA;
        this.pointB = pointB;
        this.pointC = pointC;
    }

    public double getPerimeter() {
        return getFirstSide() + getSecondSide() + getThirdSide();
    }

    public double getSquare() {
        double halfPerimeter = getPerimeter() / 2;
        return Math.abs((pointB.getX() - pointA.getX()) * (pointC.getX() - pointA.getY()) - (pointC.getX() - pointA.getX()) * (pointB.getY() - pointC.getY())) / 2;
    }

    public Point getMediansCenter() {
        Point centerSide = new Point((pointB.getX() + pointC.getX()) / 2, (pointB.getY() + pointC.getY()) / 2);
        return new Point((centerSide.getX() + pointA.getX()) / 2, (centerSide.getY() + pointA.getY()) / 2);
    }

    public double getFirstSide() {
        return Math.sqrt((pointA.getX() - pointB.getX()) * (pointA.getX() - pointB.getX()) + (pointA.getY() - pointB.getY()) * (pointA.getY() - pointB.getY()));
    }

    public double getSecondSide() {
        return Math.sqrt((pointA.getX() - pointC.getX()) * (pointA.getX() - pointC.getX()) + (pointA.getY() - pointC.getY()) * (pointA.getY() - pointC.getY()));
    }

    public double getThirdSide() {
        return Math.sqrt((pointB.getX() - pointC.getX()) * (pointB.getX() - pointC.getX()) + (pointB.getY() - pointC.getY()) * (pointB.getY() - pointC.getY()));
    }
}
