package by.training.module0.simpleObjects.task8;

import by.training.module0.simpleObjects.task8.customer.Customer;

import java.util.LinkedList;
import java.util.List;

public class Customers {

    private List<Customer> customers;

    public Customers(List<Customer> customers) {
        this.customers = customers;
    }

    public Customers(Customer customer) {
        this.customers = new LinkedList<>();
        this.customers.add(customer);
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void addCustomer(Customer customer) {
        this.customers.add(customer);
    }

    public void removeCustomer(Customer customer) {
        customers.removeIf(tempCustomer -> tempCustomer.equals(customer));
    }
}
