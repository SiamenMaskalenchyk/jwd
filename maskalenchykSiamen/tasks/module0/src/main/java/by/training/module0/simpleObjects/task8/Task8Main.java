package by.training.module0.simpleObjects.task8;

import by.training.module0.simpleObjects.task8.customer.Customer;
import by.training.module0.simpleObjects.task8.service.AlphabeticStatisticsCustomer;
import by.training.module0.simpleObjects.task8.service.SearcherByNumberCreditCard;
import by.training.module0.simpleObjects.task8.service.Searching;
import by.training.module0.simpleObjects.task8.service.Statistics;

import java.util.List;

public class Task8Main {

    public static void main(String[] args) {
        Customer customer = new Customer(1, "Sam", "Petrov", "Olegovich", "Minsk", 4455, 1);
        Customers customers = new Customers(customer);
        Searching searcher = new SearcherByNumberCreditCard(customers);
        Statistics statistics = new AlphabeticStatisticsCustomer(customers);
        customers.addCustomer(new Customer(2, "Pavel", "Ivanov", "Olegovich", "Minsk", 4465, 2));
        customers.addCustomer(new Customer(3, "Michail", "Zaycev", "Olegovich", "Minsk", 4165, 3));
        customers.addCustomer(new Customer(4, "Pavel", "Ali", "Olegovich", "Minsk", 4225, 4));
        customers.removeCustomer(customer);
        printResult(statistics.getCustomers());
        printSearchResult(searcher.search("4332"));
        printSearchResult(searcher.search("4225"));
    }

    private static void printResult(List<Customer> customers) {
        for (Customer customer : customers) {
            System.out.println(customer.toString());
        }
    }

    private static void printSearchResult(Customer customer) {
        if (customer == null) {
            System.out.println("Customer don't find");
        } else {
            System.out.println(customer.toString());
        }
    }
}
