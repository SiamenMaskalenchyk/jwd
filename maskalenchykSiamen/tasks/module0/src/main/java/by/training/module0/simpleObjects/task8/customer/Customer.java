package by.training.module0.simpleObjects.task8.customer;

import java.util.Comparator;
import java.util.Objects;

public class Customer {

    private int id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String address;
    private int cardNumber;
    private int accountNumber;

    public Customer(int id, String fitstName, String lastName, String patronymic, String address, int cardNumber, int accountNumber) {
        this.id = id;
        this.firstName = fitstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.address = address;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return id == customer.id &&
                cardNumber == customer.cardNumber &&
                accountNumber == customer.accountNumber &&
                Objects.equals(firstName, customer.firstName) &&
                Objects.equals(lastName, customer.lastName) &&
                Objects.equals(patronymic, customer.patronymic) &&
                Objects.equals(address, customer.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, patronymic, address, cardNumber, accountNumber);
    }

    @Override
    public String toString() {
        return id + " " + lastName + " " + firstName + " " + patronymic + " " + address + " " + cardNumber + " " + accountNumber;
    }

    public static Comparator<Customer> alphabeticComparator = new Comparator<Customer>() {
        @Override
        public int compare(Customer customer1, Customer customer2) {
            return customer1.lastName.compareToIgnoreCase(customer2.lastName);
        }
    };
}
