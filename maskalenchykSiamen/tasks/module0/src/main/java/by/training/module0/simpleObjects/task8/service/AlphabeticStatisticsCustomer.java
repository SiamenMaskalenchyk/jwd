package by.training.module0.simpleObjects.task8.service;

import by.training.module0.simpleObjects.task8.customer.Customer;
import by.training.module0.simpleObjects.task8.Customers;

import java.util.List;

public class AlphabeticStatisticsCustomer implements Statistics {

    private Customers customers;

    public AlphabeticStatisticsCustomer(Customers customers) {
        this.customers = customers;
    }

    @Override
    public List<Customer> getCustomers() {
        List result = customers.getCustomers();
        result.sort(Customer.alphabeticComparator);
        return result;
    }
}
