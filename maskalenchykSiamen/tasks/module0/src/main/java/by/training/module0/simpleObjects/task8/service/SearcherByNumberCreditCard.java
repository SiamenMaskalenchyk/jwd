package by.training.module0.simpleObjects.task8.service;

import by.training.module0.simpleObjects.task8.customer.Customer;
import by.training.module0.simpleObjects.task8.Customers;

public class SearcherByNumberCreditCard implements Searching {

    private Customers customers;

    public SearcherByNumberCreditCard(Customers customers) {
        this.customers = customers;
    }

    @Override
    public Customer search(String searchCriteria) {
        int numberCreditCard = Integer.parseInt(searchCriteria);
        for (Customer customer : customers.getCustomers()) {
            if (customer.getCardNumber() == numberCreditCard) {
                return customer;
            }
        }
        return null;
    }

}
