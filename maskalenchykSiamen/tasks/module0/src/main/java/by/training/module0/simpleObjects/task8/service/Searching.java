package by.training.module0.simpleObjects.task8.service;

import by.training.module0.simpleObjects.task8.customer.Customer;

public interface Searching {

    Customer search(String searchCriteria);
}
