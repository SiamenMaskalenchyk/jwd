package by.training.module0.simpleObjects.task8.service;

import by.training.module0.simpleObjects.task8.customer.Customer;

import java.util.List;

public interface Statistics {

    List<Customer> getCustomers();
}
