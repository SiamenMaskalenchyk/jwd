package by.training.module0.simpleObjects.task9;

public class Book {

    private int id;
    private String name;
    private String authors;
    private String publishingHouse;
    private int theYearOfPublishing;
    private int pageNumbers;
    private int price;
    private TypeOfBinding typeOfBinding;

    public Book(int id, String name, String authors, String publishingHouse, int theYearOfPublishing, int pageNumbers, int price, TypeOfBinding typeOfBinding) {
        this.id = id;
        this.name = name;
        this.authors = authors;
        this.publishingHouse = publishingHouse;
        this.theYearOfPublishing = theYearOfPublishing;
        this.pageNumbers = pageNumbers;
        this.price = price;
        this.typeOfBinding = typeOfBinding;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public int getTheYearOfPublishing() {
        return theYearOfPublishing;
    }

    public void setTheYearOfPublishing(int theYearOfPublishing) {
        this.theYearOfPublishing = theYearOfPublishing;
    }

    public int getPageNumbers() {
        return pageNumbers;
    }

    public void setPageNumbers(int pageNumbers) {
        this.pageNumbers = pageNumbers;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public TypeOfBinding getTypeOfBinding() {
        return typeOfBinding;
    }

    public void setTypeOfBinding(TypeOfBinding typeOfBinding) {
        this.typeOfBinding = typeOfBinding;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + authors + " " + publishingHouse + " "
               + theYearOfPublishing + " " + pageNumbers + " " + price + " rub "
               + typeOfBinding.getTypeBinding();
    }
}
