package by.training.module0.simpleObjects.task9;

import java.util.Iterator;
import java.util.List;

public class Library {

    private List<Book> books;

    public Library(List<Book> books) {
        this.books = books;
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void removeBook(Book book) {
        for (Iterator<Book> iterator = books.iterator(); iterator.hasNext(); ) {
            Book tempBook = iterator.next();
            if (tempBook.equals(book)) {
                books.remove(book);
            }
        }
    }

    public List<Book> getBooks() {
        return books;
    }

    @Override
    public String toString() {
        String string ="";
        for (Book book : books) {
            string += book.toString() +"\n";
        }
        return string;
    }
}
