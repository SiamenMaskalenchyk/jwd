package by.training.module0.simpleObjects.task9;

import by.training.module0.simpleObjects.task9.service.PrinterBooksByAuthor;
import by.training.module0.simpleObjects.task9.service.PrinterBooksByPublishHome;
import by.training.module0.simpleObjects.task9.service.PrinterBooksByYear;
import by.training.module0.simpleObjects.task9.service.PrintingListBooks;

import java.util.LinkedList;
import java.util.List;

public class Task9Main {

    public static void main(String[] args) {
        List<Book> books = init();
        Library library = new Library(books);
        PrintingListBooks printerBooksByAuthor = new PrinterBooksByAuthor(library);
        PrintingListBooks printerByPublishHome = new PrinterBooksByPublishHome(library);
        PrintingListBooks printerByYear = new PrinterBooksByYear(library);
        printerBooksByAuthor.printBooks("Kart");
        printerByPublishHome.printBooks("RAST");
        printerByYear.printBooks("2004");
    }

    private static List<Book> init() {
        List<Book> books = new LinkedList<>();
        Book book1 = new Book(1, "Fables", "Kart", "AST", 2002, 20, 10, TypeOfBinding.SOFT);
        Book book2 = new Book(2, "Tutorial", "Kart", "BAST", 2001, 30, 12, TypeOfBinding.HARD);
        Book book3 = new Book(3, "Adventures", "Leshkov", "RAST", 2005, 50, 15, TypeOfBinding.SOFT);
        Book book4 = new Book(4, "Stories", "Gekker", "TAST", 2006, 10, 11, TypeOfBinding.HARD);
        Book book5 = new Book(5, "Poems", "Merton", "ZAST", 2009, 25, 9, TypeOfBinding.SOFT);
        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);
        books.add(book5);
        return books;
    }
}
