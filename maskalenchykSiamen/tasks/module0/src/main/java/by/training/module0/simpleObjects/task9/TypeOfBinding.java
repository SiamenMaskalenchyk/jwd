package by.training.module0.simpleObjects.task9;

public enum TypeOfBinding {

    SOFT ("soft"),
    HARD ("hard");

    private final String typeBinding;

    TypeOfBinding(String typeBinding){
        this.typeBinding = typeBinding;
    }

    public String getTypeBinding() {
        return typeBinding;
    }
}
