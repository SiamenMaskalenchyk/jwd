package by.training.module0.simpleObjects.task9.service;

import by.training.module0.simpleObjects.task9.Book;
import by.training.module0.simpleObjects.task9.Library;

public class PrinterBooksByAuthor implements PrintingListBooks {

    private Library library;

    public PrinterBooksByAuthor(Library library) {
        this.library = library;
    }

    @Override
    public void printBooks(String parameter) {
        System.out.println("Books by " + parameter);
        boolean isNotFound = true;
        for (Book book : library.getBooks()) {
            if (book.getAuthors().equals(parameter)) {
                System.out.println(book.toString());
                isNotFound = false;
            }
        }
        if (isNotFound) {
            System.out.println("Didn't find");
        }
    }
}
