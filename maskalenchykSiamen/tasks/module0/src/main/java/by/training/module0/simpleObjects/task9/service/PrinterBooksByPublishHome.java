package by.training.module0.simpleObjects.task9.service;

import by.training.module0.simpleObjects.task9.Book;
import by.training.module0.simpleObjects.task9.Library;

public class PrinterBooksByPublishHome implements PrintingListBooks {

    private Library library;

    public PrinterBooksByPublishHome(Library library) {
        this.library = library;
    }

    @Override
    public void printBooks(String parameter) {
        System.out.println("Books by publish home " + parameter);
        boolean isNotFound = true;
        for (Book book : library.getBooks()) {
            if (book.getPublishingHouse().equals(parameter)) {
                System.out.println(book.toString());
                isNotFound = false;
            }
        }
        if (isNotFound) {
            System.out.println("Didn't find");
        }
    }
}
