package by.training.module0.simpleObjects.task9.service;

import by.training.module0.simpleObjects.task9.Book;
import by.training.module0.simpleObjects.task9.Library;

public class PrinterBooksByYear implements PrintingListBooks {

    private Library library;

    public PrinterBooksByYear(Library library) {
        this.library = library;
    }

    @Override
    public void printBooks(String parameter) {
        boolean isNotFound = true;
        int boundYear = Integer.parseInt(parameter);
        System.out.println("List of books released after " + boundYear);
        for (Book book : library.getBooks()) {
            if (book.getTheYearOfPublishing() > boundYear) {
                System.out.println(book.toString());
                isNotFound = false;

            }
        }
        if (isNotFound) {
            System.out.println("Didn't find");
        }
    }
}
