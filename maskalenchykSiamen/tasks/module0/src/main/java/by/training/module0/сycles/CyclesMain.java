package tasks.сycles;

import java.util.Scanner;

public class CyclesMain {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        task1();
        task2();
        task3(3);
        task4();
        task5();
        task6();
        task7(1, 5, 1);
        task8(0, 25, 5, 2, 7);
        task9();
        task11();
        task12();
        task13();
        task14(20);
        task15();
        task16();
        task17(5.5, 5);
        task18(5);
        task19(5, 4, 3, 5, 6, 8, 42, 6);
        task20(36, 13, 346, 64823, 235, 236, 23);
        task21(2, 35, 2);
        task22(2, 35, 2);
        task23(2, 35, 2);
        task24(123456789);
        task25(120);
        task26('a', 'b', 'A', 'B');
    }

    private static void task26(char... chars) {
        for (int i = 0; i < chars.length; i++) {
            int ascii = (int) chars[i];
            System.out.println(chars[i] + " = " + ascii);
        }
    }

    private static void task25(long factorial) {
        long programFactorial = 1;
        int res = 1;
        while (programFactorial < factorial) {
            programFactorial *= res;
            res += 1;
        }
        res -= 1;
        System.out.println("Factorial number " + factorial + " is " + res + "!");
    }

    private static void task24(int number) {
        String stringNumber = String.valueOf(number);
        String reverseStringNumber = new StringBuffer(stringNumber).reverse().toString();
        int reverseNumber = Integer.parseInt(reverseStringNumber);
        int sumEvenNumeral = 0;
        for (int i = 0; i < stringNumber.length(); i++) {
            int numeral = Integer.parseInt(stringNumber.substring(i, i + 1));
            if (numeral % 2 == 0) {
                sumEvenNumeral += numeral;
            }
        }
        System.out.println("Reverse number of the " + number + " is " + reverseNumber);
        System.out.println("Sum of the even numerals number " + number + " is " + sumEvenNumeral);
    }

    private static void task23(double a, double b, double h) {
        System.out.println("Task 23");
        for (double i = a; i <= b; i += h) {
            double fx = 1 / Math.tan(i / 3) + Math.sin(i) / 2;
            System.out.println("|" + "x = " + i + "|" + "y = " + fx + "|");
        }
    }

    private static void task22(double a, double b, double h) {
        System.out.println("Task 22");
        for (double i = a; i <= b; i += h) {
            double fx = Math.pow(Math.sin(i), 2);
            System.out.println("|" + "x = " + i + "|" + "y = " + fx + "|");
        }
    }

    private static void task21(double a, double b, double h) {
        System.out.println("Task 21");
        for (double i = a; i <= b; i += h) {
            double fx = i - Math.sin(i);
            System.out.println("|" + "x = " + i + "|" + "y = " + fx + "|");
        }
    }

    private static void task20(double e, double... sequence) {
        System.out.println("Task 20");
        double sum = 0;
        for (int i = 0; i < sequence.length; i++) {
            double a = 1 / ((3 * sequence[i] - 2) * (3 * sequence[i] + 2));
            if (a >= e) {
                sum += sequence[i];
            }
        }
        System.out.println("Sum of the numbers >=" + e + " is " + sum);
    }

    private static void task19(double e, int... sequence) {
        System.out.println("Task 19");
        double sum = 0;
        for (int i = 0; i < sequence.length; i++) {
            double a = (1 / Math.pow(2, sequence[i])) + 1 / Math.pow(3, sequence[i]);
            if (a >= e) {
                sum += sequence[i];
            }
        }
        System.out.println("Sum of the numbers >=" + e + " is " + sum);
    }

    private static void task18(double e, double... sequence) {
        System.out.println("Task 18");
        double sum = 0;
        for (int i = 0; i < sequence.length; i++) {
            double a = Math.pow(-1, sequence[i]) / sequence[i];
            if (a >= e) {
                sum += sequence[i];
            }
        }
        System.out.println("Sum of the numbers >=" + e + " is " + sum);
    }

    private static void task17(double a, int n) {
        double resTask17 = a;
        for (int i = 1; i < n; i++) {
            resTask17 *= (a + i);
        }
        System.out.println("Result of the task17 is " + resTask17);
    }

    private static void task16() {
        int previousNum = 1;
        int res = 0;
        for (int i = 2; i <= 10; i++) {
            res += previousNum + i;
        }
        System.out.println("Result task16 is " + res);
    }

    private static void task15() {
        int res = 0;
        for (int i = 1; i <= 10; i++) {
            res += i;
        }
        for (int i = 9; i > 1; i--) {
            res += i;
        }
        System.out.println("Result of the task15 is " + res);
    }

    private static void task14(int number) {
        double res = 0d;
        for (int i = number; i >= 1; i--) {
            res += 1 / i;
        }
        System.out.println("Result of the task14 is " + res);
    }

    private static void task13() {
        double finish = 5;
        double step = 0.5;
        for (double start = -5; start <= finish; start += step) {
            System.out.println("(x = " + start + "; y = " + (5 - start * start / 2) + ")");
        }
    }

    private static void task12() {
        int res = 1;
        int prev = 1;
        for (int i = 2; i <= 10; i++) {
            res *= prev + 6;
            prev = i;
        }
        System.out.println("Product of the sequence " + res);
    }

    private static void task11() {
        long res = 1;
        for (int i = 1; i <= 200; i++) {
            res -= res * res * res - i * i * i;
        }
        System.out.println("Difference of cube first 200 numbers is " + res);
    }

    private static void task9() {
        int sum = 1;
        for (int i = 2; i <= 100; i++) {
            sum += i * i;
        }
        System.out.println("Sum of first 100 numbers is " + sum);
    }

    private static void task8(int a, int b, int h, int x, int c) {
        System.out.println("Task 8");
        while (a <= b) {
            int res;
            if (a == 15) {
                res = (x + c) * 2;
                System.out.println("(x = " + a + "; y = " + res + ")");
            } else {
                res = (x - c) * 2;
                System.out.println("( x = " + a + "; y = " + res + ")");
            }
            a += h;
        }
    }

    private static void task7(int a, int b, int h) {
        while (a <= b) {
            if (a > 2) {
                System.out.println("(x = " + a + "; y = " + a + ")");
            } else {
                System.out.println("( x = " + a + "; y = " + "-" + a + ")");
            }
            a += h;
        }
    }

    private static void task6() {
        System.out.println("Please enter a number");
        int num = scanner.nextInt();
        int sum;
        if (num <= 0) {
            sum = 0;
        } else {
            sum = 0;
            for (int i = 1; i <= num; i++) {
                sum += i;
            }
        }
        System.out.println("Result of the task 6: " + sum);
    }

    private static void task5() {
        int sum = 0;
        int number = 1;
        int bound = 100;
        while (number <= bound) {
            if (number % 2 != 0) {
                sum += number;
                number += 2;
            } else {
                number += 1;
            }
        }
        System.out.println("Result of the task 5: " + sum);
    }

    private static void task4() {
        int number = 2;
        int finish = 100;
        String resTask4 = "";
        while (number <= finish) {
            if (number % 2 == 0) {
                resTask4 += number + " ";
                number += 2;
            } else {
                number += 1;
            }
        }
        System.out.println("Result of the task 4: " + resTask4);
    }

    private static void task3(int column) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(column + " x " + i + " = " + column * i);
        }
    }

    private static void task2() {
        String resTask2 = "";
        for (int i = 5; i > 1; i--) {
            resTask2 += i + " ";
        }
        System.out.println("Result of the task 2: " + resTask2);
    }

    private static void task1() {
        String resTask1 = "";
        for (int i = 1; i < 5; i++) {
            resTask1 += i + " ";
        }
        System.out.println("Result of the task 1: " + resTask1);
    }

}
