package com.maskalenchyk.airline.application;

import com.maskalenchyk.airline.controller.OutputHandler;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Application {

    private static final Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) throws IOException {
        InetSocketAddress socket = new InetSocketAddress("localhost", 8001);
        HttpServer httpServer = HttpServer.create(socket, 0);
        HttpContext serverHomeContext1 = httpServer.createContext("/home-page", new OutputHandler());
        httpServer.start();
        logger.info("Server started on " + httpServer.getAddress().toString());
    }

}
