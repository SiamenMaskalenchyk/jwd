package com.maskalenchyk.airline.command;

import java.util.Map;

public interface Command {

    String execute(Map<String, String> params) throws CommandException;

    String getStringIdentifier();
}
