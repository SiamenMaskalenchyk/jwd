package com.maskalenchyk.airline.command;

import java.util.Map;

public interface CommandFactory {

    Command getCommand(Map<String, String> params);

}
