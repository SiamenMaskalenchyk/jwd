package com.maskalenchyk.airline.command;

public enum CommandType {
    DEFAULT,
    DISPLAY_AIRCRAFT,
    SORT_AIRCRAFT,
    GET_STATISTIC,
    UPLOAD_FILE,
    ERROR
}
