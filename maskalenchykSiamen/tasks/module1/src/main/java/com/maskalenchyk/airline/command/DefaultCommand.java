package com.maskalenchyk.airline.command;

import com.maskalenchyk.airline.util.ApplicationUtils;

import java.text.MessageFormat;
import java.util.Map;

public class DefaultCommand implements Command {

    @Override
    public String execute(Map<String, String> params) {
        String page = ApplicationUtils.htmlToString("home-page.html");
        return MessageFormat.format(page, "");
    }

    @Override
    public String getStringIdentifier() {
        return "DEFAULT";
    }
}
