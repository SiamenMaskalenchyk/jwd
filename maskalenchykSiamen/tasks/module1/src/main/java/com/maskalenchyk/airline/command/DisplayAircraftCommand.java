package com.maskalenchyk.airline.command;

import com.maskalenchyk.airline.model.Aircraft;
import com.maskalenchyk.airline.service.SearchService;
import com.maskalenchyk.airline.util.ApplicationUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

public class DisplayAircraftCommand implements Command {

    private final Logger LOGGER = LogManager.getLogger(DisplayAircraftCommand.class);
    private SearchService searchService;

    public DisplayAircraftCommand(SearchService searchService) {
        this.searchService = searchService;
    }

    @Override
    public String execute(Map<String, String> params) throws CommandException {
        List<Aircraft> aircraftList = getAircraft(params);
        String page = ApplicationUtils.htmlToString("home-page.html");
        if (aircraftList.isEmpty()) {
            return MessageFormat.format(page, "Aircraft didn't find.");
        } else {
            String response = getResponse(aircraftList);
            return MessageFormat.format(page, response);
        }
    }

    @Override
    public String getStringIdentifier() {
        return "DISPLAY_AIRCRAFT";
    }

    private String getResponse(List<Aircraft> aircraftList) {
        StringBuilder displayAllAircraftResponse = new StringBuilder();
        displayAllAircraftResponse.append("<ul>");
        for (Aircraft aircraft : aircraftList) {
            displayAllAircraftResponse.append("<li>");
            displayAllAircraftResponse.append(aircraft.toString());
            displayAllAircraftResponse.append("</li>");
        }
        displayAllAircraftResponse.append("</ul>");
        return displayAllAircraftResponse.toString();
    }

    private List<Aircraft> getAircraft(Map<String, String> params) throws CommandException {
        List<Aircraft> aircraftList;
        if (params.containsKey("findParameter")) {
            String findParameter = params.get("findParameter");
            switch (findParameter) {
                case "byAccessibility":
                    Integer accessibilityValue = parseValueParameter(params.get("value"));
                    aircraftList = findListByAccessibility(accessibilityValue);
                    break;
                case "byNumber":
                    Integer numberValue = parseValueParameter(params.get("value"));
                    aircraftList = searchService.findByAircraftNumber(numberValue);
                    break;
                default:
                    LOGGER.error(findParameter + " - unknown searching parameter");
                    throw new CommandException(findParameter + " - unknown searching parameter");
            }
        } else {
            aircraftList = searchService.findAll();
        }
        return aircraftList;
    }

    private Integer parseValueParameter(String value) throws CommandException {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            LOGGER.error("Uncorrected searching parameter value " + value);
            throw new CommandException("Uncorrected searching parameter value " + value);
        }
    }

    private List<Aircraft> findListByAccessibility(Integer value) {
        return value != 0 ? searchService.findByAccessibility(true) :
                searchService.findByAccessibility(false);
    }
}
