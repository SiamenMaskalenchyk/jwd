package com.maskalenchyk.airline.command;

import com.maskalenchyk.airline.util.ApplicationUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.text.MessageFormat;
import java.util.Map;

public class ErrorCommand implements Command {

    private final Logger LOGGER = LogManager.getLogger(ErrorCommand.class);

    @Override
    public String execute(Map<String, String> params) {
        String page = ApplicationUtils.htmlToString("error.html");
        params.putIfAbsent("error", "Unknown command");
        LOGGER.info("Error command with " + params.get("error"));
        return MessageFormat.format(page, params.get("error"));
    }

    @Override
    public String getStringIdentifier() {
        return "ERROR";
    }
}
