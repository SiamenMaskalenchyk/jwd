package com.maskalenchyk.airline.command;

public interface FileValidator {

    String validateFile(String path);
}
