package com.maskalenchyk.airline.command;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileValidatorImpl implements FileValidator {

    @Override
    public String validateFile(String path) {
        File file = new File(path);
        if (file.exists()) {
            if (isCVSFileExtension(file)) {
                return isReadable(path) ? "" : "File can't be read";
            } else {
                return "Invalid file extension";
            }
        } else {
            return "File doesn't exist";
        }
    }

    private boolean isReadable(String path) {
        Path path1 = Paths.get(path);
        return Files.isReadable(path1);
    }

    private boolean isCVSFileExtension(File file) {
        String fileExtension = getFileExtension(file);
        return "csv".equals(fileExtension);
    }

    private String getFileExtension(File file) {
        String fileName = file.getName();
        int pointLastIndex = fileName.lastIndexOf(".");
        if (pointLastIndex != -1 && pointLastIndex != 0) {
            return fileName.substring(pointLastIndex + 1);
        } else {
            return "";
        }
    }
}
