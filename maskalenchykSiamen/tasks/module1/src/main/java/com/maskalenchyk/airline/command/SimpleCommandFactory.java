package com.maskalenchyk.airline.command;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SimpleCommandFactory implements CommandFactory {

    private final Logger LOGGER = LogManager.getLogger(SimpleCommandFactory.class);
    private Map<String, Command> commands;

    public SimpleCommandFactory(Command... commands) {
        List<Command> commandsList = Arrays.asList(commands);
        initMapCommands(commandsList);
    }

    @Override
    public Command getCommand(Map<String, String> params) {
        if (params.containsKey("commandName")) {
            Command command = commands.getOrDefault(params.get("commandName"), commands.get("ERROR"));
            LOGGER.info("Get " + command.getStringIdentifier() + " command");
            return command;
        } else {
            LOGGER.info("Get DEFAULT command");
            return commands.get("DEFAULT");
        }
    }

    private void initMapCommands(List<Command> commandsList) {
        this.commands = new ConcurrentHashMap<>();
        for (Command command : commandsList) {
            commands.put(command.getStringIdentifier(), command);
        }
    }
}
