package com.maskalenchyk.airline.command;

import com.maskalenchyk.airline.service.SortService;
import com.maskalenchyk.airline.util.ApplicationUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SortAircraftCommand implements Command {

    private final Logger LOGGER = LogManager.getLogger(SortAircraftCommand.class);
    private SortService sortService;

    public SortAircraftCommand(SortService sortService) {
        this.sortService = sortService;
    }

    @Override
    public String execute(Map<String, String> params) {
        List<String> sortingParams = new ArrayList<>();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (entry.getKey().startsWith("parameter")) {
                sortingParams.add(entry.getValue());
            }
        }
        String sortResult = sortService.sortAircraft(sortingParams);
        LOGGER.info(sortResult);
        String page = ApplicationUtils.htmlToString("home-page.html");
        return MessageFormat.format(page, sortResult);
    }

    @Override
    public String getStringIdentifier() {
        return "SORT_AIRCRAFT";
    }
}
