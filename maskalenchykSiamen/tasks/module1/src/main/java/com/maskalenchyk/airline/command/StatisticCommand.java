package com.maskalenchyk.airline.command;

import com.maskalenchyk.airline.service.StatisticService;
import com.maskalenchyk.airline.util.ApplicationUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.text.MessageFormat;
import java.util.Map;

public class StatisticCommand implements Command {

    private final Logger LOGGER = LogManager.getLogger(StatisticCommand.class);
    private StatisticService statisticService;

    public StatisticCommand(StatisticService statisticService) {
        this.statisticService = statisticService;
    }

    @Override
    public String execute(Map<String, String> params) throws CommandException {
        String page = ApplicationUtils.htmlToString("home-page.html");
        String info = statisticService.getStatistic(params.get("parameter"));
        LOGGER.info(info);
        return MessageFormat.format(page, info);
    }

    @Override
    public String getStringIdentifier() {
        return "GET_STATISTIC";
    }
}
