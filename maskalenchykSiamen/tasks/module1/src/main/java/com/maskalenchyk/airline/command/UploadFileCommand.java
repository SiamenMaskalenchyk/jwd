package com.maskalenchyk.airline.command;

import com.maskalenchyk.airline.service.UploadDataFromFileService;
import com.maskalenchyk.airline.util.ApplicationUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Map;

public class UploadFileCommand implements Command {

    private final Logger LOGGER = LogManager.getLogger(UploadFileCommand.class);
    private UploadDataFromFileService uploadFileService;
    private FileValidator fileValidator;

    public UploadFileCommand(UploadDataFromFileService uploadFileService, FileValidator fileValidator) {
        this.uploadFileService = uploadFileService;
        this.fileValidator = fileValidator;
    }

    @Override
    public String execute(Map<String, String> params) throws CommandException {
        String result = getDataFromFile(params.get("path"));
        String page = ApplicationUtils.htmlToString("home-page.html");
        return MessageFormat.format(page, result);
    }

    private String getDataFromFile(String path) throws CommandException {
        String validationResult = fileValidator.validateFile(path);
        StringBuilder response = new StringBuilder();
        if (validationResult.isEmpty()) {
            File file = new File(path);
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
                String lineFromFile = bufferedReader.readLine();
                while (lineFromFile != null) {
                    response.append("<li>");
                    response.append(uploadFileService.uploadData(lineFromFile));
                    response.append("</li>");
                    lineFromFile = bufferedReader.readLine();
                }
                LOGGER.info("file: " + path + " read successful");
                return response.toString();
            } catch (IOException e) {
                LOGGER.error("Error during reading data from file", e);
                throw new CommandException("Error during reading data from file");
            }
        }
        LOGGER.info("file uncorrected - " + validationResult);
        return validationResult;
    }

    @Override
    public String getStringIdentifier() {
        return "UPLOAD_FILE";
    }
}
