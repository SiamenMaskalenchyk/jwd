package com.maskalenchyk.airline.controller;

import com.maskalenchyk.airline.command.*;
import com.maskalenchyk.airline.dal.AircraftContainer;
import com.maskalenchyk.airline.service.*;
import com.maskalenchyk.airline.util.ApplicationUtils;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Map;
import java.util.stream.Collectors;

public class OutputHandler implements HttpHandler {
    private Logger logger = LogManager.getLogger(OutputHandler.class);
    private AircraftContainer aircraftContainer = new AircraftContainer();
    private SortService sortService = new SortServiceImpl(aircraftContainer);
    private SearchService searchService = new SearchServiceImpl(aircraftContainer);
    private StatisticService statisticService = new StatisticServiceImpl(aircraftContainer);
    private UploadDataFromFileService uploadDataFromFileService = new UploadCSVFileService(aircraftContainer);
    private FileValidator fileValidator = new FileValidatorImpl();
    private Command sortCommand = new SortAircraftCommand(sortService);
    private Command defaultCommand = new DefaultCommand();
    private Command errorCommand = new ErrorCommand();
    private Command displayCommand = new DisplayAircraftCommand(searchService);
    private Command statisticCommand = new StatisticCommand(statisticService);
    private Command uploadFileCommand = new UploadFileCommand(uploadDataFromFileService, fileValidator);
    private CommandFactory simpleCommandFactory = new SimpleCommandFactory(
            sortCommand, defaultCommand, errorCommand, displayCommand, statisticCommand, uploadFileCommand);


    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String requestMethod = exchange.getRequestMethod();
        logger.info("Received incoming request: " + requestMethod);
        String query = exchange.getRequestURI().getQuery();
        logger.info("Queried: " + query);
        String inputData = URLDecoder.decode(new BufferedReader(new InputStreamReader(exchange.getRequestBody(), StandardCharsets.UTF_8))
                        .lines()
                        .collect(Collectors.joining()),
                StandardCharsets.UTF_8.name());
        logger.info("Received: " + inputData);
        Map<String, String> params = "GET".equals(requestMethod) ?
                ApplicationUtils.queryToMap(query) :
                ApplicationUtils.queryToMap(inputData);
        String view;
        try {
            Command command = simpleCommandFactory.getCommand(params);
            view = command.execute(params);
        } catch (CommandException e) {
            String page = ApplicationUtils.htmlToString("error.html");
            view = MessageFormat.format(page, e.getMessage());
        }
        OutputStream responseBody = exchange.getResponseBody();
        exchange.sendResponseHeaders(200, view.length());
        responseBody.write(view.getBytes());
        responseBody.flush();
        responseBody.close();
    }
}
