package com.maskalenchyk.airline.dal;

import com.maskalenchyk.airline.model.Aircraft;
import com.maskalenchyk.airline.model.AircraftType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AircraftContainer {

    private List<Aircraft> aircraftList;

    public AircraftContainer(List<Aircraft> aircraftList) {
        this.aircraftList = aircraftList;
    }

    public AircraftContainer() {
        this.aircraftList = new ArrayList<>();
        aircraftList.add(new Aircraft(1, "Ty-204", AircraftType.PASSENGER, 3000, 2000, new BigDecimal(4600), true));
        aircraftList.add(new Aircraft(2, "Sukhoi SuperJet-100", AircraftType.PASSENGER, 2800, 1900, new BigDecimal(7000), true));
        aircraftList.add(new Aircraft(3, "Airbus A310", AircraftType.PASSENGER, 3000, 2000, new BigDecimal(9000), true));
        aircraftList.add(new Aircraft(4, "Boeing-737", AircraftType.PASSENGER, 2100, 2500, new BigDecimal(6500), false));
        aircraftList.add(new Aircraft(5, "Hawker 4000", AircraftType.PASSENGER, 1821, 1200, new BigDecimal(4500), false));
        aircraftList.add(new Aircraft(6, "Su-80", AircraftType.TRANSPORT, 1700, 1200, new BigDecimal(3500), true));
        aircraftList.add(new Aircraft(7, "C-27J", AircraftType.TRANSPORT, 1600, 1100, new BigDecimal(9500), true));
        aircraftList.add(new Aircraft(8, "C-40", AircraftType.TRANSPORT, 1220, 1100, new BigDecimal(5500), false));
        aircraftList.add(new Aircraft(9, "Su-38", AircraftType.AGRICULTURAL, 780, 300, new BigDecimal(1500), true));
        aircraftList.add(new Aircraft(10, "An-2", AircraftType.AGRICULTURAL, 350, 150, new BigDecimal(1200), false));
    }

    public List<Aircraft> getAircraftList() {
        return new ArrayList<>(aircraftList);
    }

    public void setAircraftList(List<Aircraft> aircraftList) {
        this.aircraftList = aircraftList;
    }

    public void addAircraft(Aircraft aircraft) {
        aircraftList.add(aircraft);
    }

    public void sortAircraftList(List<Comparator<Aircraft>> aircraftComparators) {
        for (Comparator<Aircraft> comparator : aircraftComparators) {
            aircraftList.sort(comparator);
        }
    }

}