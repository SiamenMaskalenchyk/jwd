package com.maskalenchyk.airline.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Aircraft {

    private final Integer aircraftNumber;
    private final String model;
    private final AircraftType aircraftType;
    private final Integer capacity;
    private final Integer tonnage;
    private BigDecimal rentPrice;
    private Boolean isAvailable;

    public Aircraft(Integer aircraftNumber, String model, AircraftType aircraftType,
                    Integer capacity, Integer tonnage, BigDecimal rentPrice, Boolean isAvailable) {
        this.aircraftNumber = aircraftNumber;
        this.model = model;
        this.aircraftType = aircraftType;
        this.capacity = capacity;
        this.tonnage = tonnage;
        this.rentPrice = rentPrice;
        this.isAvailable = isAvailable;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public Integer getTonnage() {
        return tonnage;
    }

    public Integer getAircraftNumber() {
        return aircraftNumber;
    }

    public AircraftType getAircraftType() {
        return aircraftType;
    }

    public String getModel() {
        return model;
    }

    public BigDecimal getRentPrice() {
        return rentPrice;
    }

    public void setRentPrice(BigDecimal rentPrice) {
        this.rentPrice = rentPrice;
    }

    public Boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aircraft aircraft = (Aircraft) o;
        return isAvailable == aircraft.isAvailable &&
                Objects.equals(aircraftNumber, aircraft.aircraftNumber) &&
                aircraftType == aircraft.aircraftType &&
                Objects.equals(rentPrice, aircraft.rentPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(aircraftNumber, aircraftType, rentPrice, isAvailable);
    }

    @Override
    public String toString() {
        return "Aircraft{" +
                "aircraftNumber=" + aircraftNumber +
                ", model='" + model + '\'' +
                ", aircraftType=" + aircraftType +
                ", capacity=" + capacity +
                ", tonnage=" + tonnage +
                ", rentPrice=" + rentPrice +
                ", isAvailable=" + isAvailable +
                '}';
    }
}
