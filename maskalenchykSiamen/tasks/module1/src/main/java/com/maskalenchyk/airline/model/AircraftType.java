package com.maskalenchyk.airline.model;

public enum AircraftType {
    TRANSPORT("transport"),
    AGRICULTURAL("agricultural"),
    PASSENGER("passenger");

    private String aircraftType;

    AircraftType(String aircraftType) {
        this.aircraftType = aircraftType;
    }

    public String getAircraftType() {
        return aircraftType;
    }
}
