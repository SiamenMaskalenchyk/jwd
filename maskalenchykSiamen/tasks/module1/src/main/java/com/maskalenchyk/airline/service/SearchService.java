package com.maskalenchyk.airline.service;

import com.maskalenchyk.airline.model.Aircraft;

import java.util.List;

public interface SearchService {

    List<Aircraft> findByAircraftNumber(Integer aircraftNumber);

    List<Aircraft> findByAccessibility(Boolean isAccess);

    List<Aircraft> findAll();
}
