package com.maskalenchyk.airline.service;

import com.maskalenchyk.airline.dal.AircraftContainer;
import com.maskalenchyk.airline.model.Aircraft;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class SearchServiceImpl implements SearchService {
    private AircraftContainer aircraftContainer;

    public SearchServiceImpl(AircraftContainer aircraftContainer) {
        this.aircraftContainer = aircraftContainer;
    }

    @Override
    public List<Aircraft> findByAircraftNumber(Integer aircraftNumber) {
        for (Aircraft aircraft : aircraftContainer.getAircraftList()) {
            if (aircraft.getAircraftNumber().equals(aircraftNumber)) {
                return Collections.singletonList(aircraft);
            }
        }
        return Collections.emptyList();
    }

    @Override
    public List<Aircraft> findByAccessibility(Boolean isAccess) {
        List<Aircraft> result = new ArrayList<>();
        for (Aircraft aircraft : aircraftContainer.getAircraftList()) {
            if (aircraft.isAvailable().equals(isAccess)) {
                result.add(aircraft);
            }
        }
        return result;
    }

    @Override
    public List<Aircraft> findAll() {
        return aircraftContainer.getAircraftList();
    }
}
