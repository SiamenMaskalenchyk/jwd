package com.maskalenchyk.airline.service;

import java.util.List;

public interface SortService {

    String sortAircraft(List<String> sortingParams);
}
