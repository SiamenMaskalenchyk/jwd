package com.maskalenchyk.airline.service;

import com.maskalenchyk.airline.dal.AircraftContainer;
import com.maskalenchyk.airline.model.Aircraft;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SortServiceImpl implements SortService {

    private Comparator<Aircraft> byId = Comparator.comparing(Aircraft::getAircraftNumber);
    private Comparator<Aircraft> byTonnage = Comparator.comparing(Aircraft::getTonnage);
    private Comparator<Aircraft> byName = Comparator.comparing(Aircraft::getModel);
    private Comparator<Aircraft> byPrice = Comparator.comparing(Aircraft::getRentPrice);

    private AircraftContainer aircraftContainer;

    public SortServiceImpl(AircraftContainer aircraftContainer) {
        this.aircraftContainer = aircraftContainer;
    }

    @Override
    public String sortAircraft(List<String> sortingParams) {
        if (sortingParams.isEmpty()) {
            return "Sorting failed (sorting parameters didn't found)";
        }
        List<Comparator<Aircraft>> aircraftComparators = new ArrayList<>();
        StringBuilder outputString = new StringBuilder("Sorting success ");
        for (String sortingParam : sortingParams) {
            switch (sortingParam) {
                case "name":
                    aircraftComparators.add(byName);
                    outputString.append("by name ");
                    break;
                case "id":
                    aircraftComparators.add(byId);
                    outputString.append("by ID ");
                    break;
                case "price":
                    aircraftComparators.add(byPrice);
                    outputString.append("by price ");
                    break;
                case "tonnage":
                    aircraftComparators.add(byTonnage);
                    outputString.append("by tonnage ");
                    break;
                default:
                    return "Sorting failed (unknown sort parameter)";
            }
        }
        aircraftContainer.sortAircraftList(aircraftComparators);
        outputString.append(", for viewing sorted list, press View all");
        return outputString.toString();
    }
}
