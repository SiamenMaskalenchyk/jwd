package com.maskalenchyk.airline.service;

import com.maskalenchyk.airline.command.CommandException;

public interface StatisticService {

    String getStatistic(String param) throws CommandException;
}
