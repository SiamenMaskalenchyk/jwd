package com.maskalenchyk.airline.service;

import com.maskalenchyk.airline.command.CommandException;
import com.maskalenchyk.airline.dal.AircraftContainer;
import com.maskalenchyk.airline.model.Aircraft;

import java.util.List;

public class StatisticServiceImpl implements StatisticService {

    private AircraftContainer aircraftContainer;

    public StatisticServiceImpl(AircraftContainer aircraftContainer) {
        this.aircraftContainer = aircraftContainer;
    }

    @Override
    public String getStatistic(String param) throws CommandException {
        List<Aircraft> aircraftList = aircraftContainer.getAircraftList();
        switch (param) {
            case "tonnage":
                return getTonnageAllAircraft(aircraftList);
            case "capacity":
                return getCapacityAllAircraft(aircraftList);
            default:
                throw new CommandException("Invalid parameter for statistic command");
        }
    }

    private String getTonnageAllAircraft(List<Aircraft> aircraftList) {
        int tonnageAllAircraft = aircraftList.stream().mapToInt(Aircraft::getTonnage).sum();
        return "Tonnage all aircraft: " + tonnageAllAircraft;
    }

    private String getCapacityAllAircraft(List<Aircraft> aircraftList) {
        int capacityAllAircraft = aircraftList.stream().mapToInt(Aircraft::getCapacity).sum();
        return "Capacity all aircraft: " + capacityAllAircraft;
    }
}

