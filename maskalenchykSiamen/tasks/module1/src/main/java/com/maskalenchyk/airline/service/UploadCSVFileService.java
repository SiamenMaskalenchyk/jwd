package com.maskalenchyk.airline.service;

import com.maskalenchyk.airline.dal.AircraftContainer;
import com.maskalenchyk.airline.model.Aircraft;
import com.maskalenchyk.airline.model.AircraftType;

import java.math.BigDecimal;

public class UploadCSVFileService implements UploadDataFromFileService {

    private AircraftContainer aircraftContainer;

    public UploadCSVFileService(AircraftContainer aircraftContainer) {
        this.aircraftContainer = aircraftContainer;
    }

    @Override
    public String uploadData(String dataFromFile) {
        String[] dataArray = dataFromFile.split(",");
        try {
            Integer aircraftNumber = Integer.parseInt(dataArray[0]);
            String model = dataArray[1];
            AircraftType aircraftType = AircraftType.valueOf(dataArray[2].toUpperCase());
            Integer capacity = Integer.parseInt(dataArray[3]);
            Integer tonnage = Integer.parseInt(dataArray[4]);
            BigDecimal rentPrice = new BigDecimal(Integer.parseInt(dataArray[5]));
            Boolean isAvailable = "true".equals(dataArray[6].toLowerCase());
            Aircraft aircraft = new Aircraft(aircraftNumber, model, aircraftType, capacity, tonnage, rentPrice, isAvailable);
            aircraftContainer.addAircraft(aircraft);
        } catch (IllegalArgumentException e) {
            return "Failed - " + dataFromFile;
        }
        return "Success - " + dataFromFile;
    }
}