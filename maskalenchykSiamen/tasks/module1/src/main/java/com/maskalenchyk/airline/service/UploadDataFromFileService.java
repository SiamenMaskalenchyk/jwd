package com.maskalenchyk.airline.service;

public interface UploadDataFromFileService {

    String uploadData(String dataFromFile);
}
