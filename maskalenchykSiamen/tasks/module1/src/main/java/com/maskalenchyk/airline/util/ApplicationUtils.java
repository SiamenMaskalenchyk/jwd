package com.maskalenchyk.airline.util;

import com.maskalenchyk.airline.application.Application;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class ApplicationUtils {

    private static final Logger logger = LogManager.getLogger(ApplicationUtils.class);

    public static String htmlToString(String fileName) {
        String result = "";
        InputStream in = Application.class.getClassLoader().getResourceAsStream(fileName);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
            result = br.lines().parallel().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            logger.error("File " + fileName + " didn't find ", e);
        }
        return result;
    }

    public static Map<String, String> queryToMap(String query) {
        if (query == null) {
            return Collections.emptyMap();
        }
        String[] paramsWithKey = query.split("&");
        Map<String, String> params = new HashMap<>();
        for (String paramWithKey : paramsWithKey) {
            String[] temp = paramWithKey.split("=");
            params.put(temp[0], temp[1]);
        }
        return params;
    }
}