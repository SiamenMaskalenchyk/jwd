package com.maskalenchyk.airline.controller;

import com.maskalenchyk.airline.command.*;
import com.maskalenchyk.airline.dal.AircraftContainer;
import com.maskalenchyk.airline.service.*;
import com.maskalenchyk.airline.util.ApplicationUtils;
import org.junit.Assert;
import org.junit.Test;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

public class OutputHandlerTest {


    private AircraftContainer aircraftContainer = new AircraftContainer();
    private SortService sortService = new SortServiceImpl(aircraftContainer);
    private SearchService searchService = new SearchServiceImpl(aircraftContainer);
    private StatisticService statisticService = new StatisticServiceImpl(aircraftContainer);
    private UploadDataFromFileService uploadDataFromFileService = new UploadCSVFileService(aircraftContainer);
    private FileValidator fileValidator = new FileValidatorImpl();
    private Command sortCommand = new SortAircraftCommand(sortService);
    private Command defaultCommand = new DefaultCommand();
    private Command errorCommand = new ErrorCommand();
    private Command displayCommand = new DisplayAircraftCommand(searchService);
    private Command statisticCommand = new StatisticCommand(statisticService);
    private Command uploadFileCommand = new UploadFileCommand(uploadDataFromFileService, fileValidator);
    private CommandFactory simpleCommandFactory = new SimpleCommandFactory(
            sortCommand, defaultCommand, errorCommand, displayCommand, statisticCommand, uploadFileCommand);

    private String getView(Map<String, String> params) {
        String view;
        try {
            Command command = simpleCommandFactory.getCommand(params);
            view = command.execute(params);
        } catch (CommandException e) {
            String page = ApplicationUtils.htmlToString("error.html");
            view = MessageFormat.format(page, e.getMessage());
        }
        return view;
    }

    @Test
    public void checkDisplayAllCommand() {
        Map<String, String> params = new HashMap<>();
        params.put("commandName", "DISPLAY_AIRCRAFT");
        String view = getView(params);
        String displayAllAircraftString = ApplicationUtils.htmlToString("displayAllAircraft.html");
        Assert.assertEquals(displayAllAircraftString, view);
    }

    @Test
    public void checkDisplayAvailable() {
        Map<String, String> params = new HashMap<>();
        params.put("commandName", "DISPLAY_AIRCRAFT");
        params.put("findParameter", "byAccessibility");
        params.put("value", "1");
        String view = getView(params);
        String displayAllAircraftString = ApplicationUtils.htmlToString("displayAvailableAircraft.html");
        Assert.assertEquals(displayAllAircraftString, view);
    }

    @Test
    public void checkDisplayUnAvailable() {
        Map<String, String> params = new HashMap<>();
        params.put("commandName", "DISPLAY_AIRCRAFT");
        params.put("findParameter", "byAccessibility");
        params.put("value", "0");
        String view = getView(params);
        String displayAllAircraftString = ApplicationUtils.htmlToString("displayUnavailableAircraft.html");
        Assert.assertEquals(displayAllAircraftString, view);
    }

    @Test
    public void checkCorrectedIDDuringSearching() {
        Map<String, String> params = new HashMap<>();
        params.put("commandName", "DISPLAY_AIRCRAFT");
        params.put("findParameter", "byNumber");
        params.put("value", "1");
        String view = getView(params);
        String displayAllAircraftString = ApplicationUtils.htmlToString("dispayAircraftWithID1.html");
        Assert.assertEquals(displayAllAircraftString, view);
        params.put("findParameter", "byNumber");
        params.put("value", "5");
        view = getView(params);
        displayAllAircraftString = ApplicationUtils.htmlToString("dispayAircraftWithID5.html");
        Assert.assertEquals(displayAllAircraftString, view);
    }

    @Test
    public void checkUncorrectedNonexistentIDDuringSearching() {
        Map<String, String> params = new HashMap<>();
        params.put("commandName", "DISPLAY_AIRCRAFT");
        params.put("findParameter", "byNumber");
        params.put("value", "14");
        String view = getView(params);
        String displayAllAircraftString = ApplicationUtils.htmlToString("aircraftDidn'tFind.html");
        Assert.assertEquals(displayAllAircraftString, view);
    }

    @Test
    public void checkUncorrectedInputDuringSearching() {
        Map<String, String> params = new HashMap<>();
        params.put("commandName", "DISPLAY_AIRCRAFT");
        params.put("findParameter", "byNumber");
        params.put("value", "sdgs");
        String view = getView(params);
        String displayAllAircraftString = ApplicationUtils.htmlToString("uncorrectedInputDuringSearching.html");
        Assert.assertEquals(displayAllAircraftString, view);
    }

    @Test
    public void checkSortingByName() {
        Map<String, String> params = new HashMap<>();
        params.put("commandName", "SORT_AIRCRAFT");
        params.put("parameter4", "name");
        String view = getView(params);
        String displayAllAircraftString = ApplicationUtils.htmlToString("sortingByName.html");
        Assert.assertEquals(displayAllAircraftString, view);
        params.clear();
        params.put("commandName", "DISPLAY_AIRCRAFT");
        view = getView(params);
        displayAllAircraftString = ApplicationUtils.htmlToString("resultSortingByName.html");
        Assert.assertEquals(displayAllAircraftString, view);
    }

    @Test
    public void checkSortingByNameByTonnage() {
        Map<String, String> params = new HashMap<>();
        params.put("commandName", "SORT_AIRCRAFT");
        params.put("parameter3", "tonnage");
        params.put("parameter4", "name");
        String view = getView(params);
        String displayAllAircraftString = ApplicationUtils.htmlToString("sortingByNameByTonnage.html");
        Assert.assertEquals(displayAllAircraftString, view);
        params.clear();
        params.put("commandName", "DISPLAY_AIRCRAFT");
        view = getView(params);
        displayAllAircraftString = ApplicationUtils.htmlToString("sortingByNameByTonnageResult.html");
        Assert.assertEquals(displayAllAircraftString, view);
    }

    @Test
    public void checkSortingWithoutParameter() {
        Map<String, String> params = new HashMap<>();
        params.put("commandName", "SORT_AIRCRAFT");
        String view = getView(params);
        String displayAllAircraftString = ApplicationUtils.htmlToString("sortingWithoutParameters.html");
        Assert.assertEquals(displayAllAircraftString, view);
    }

    @Test
    public void checkWrongCommand() {
        Map<String, String> params = new HashMap<>();
        params.put("commandName", "wrong");
        String view = getView(params);
        String displayAllAircraftString = ApplicationUtils.htmlToString("wrongCommand.html");
        Assert.assertEquals(displayAllAircraftString, view);
    }

    @Test
    public void checkUncorrectedFilePath() {
        Map<String, String> params = new HashMap<>();
        params.put("commandName", "UPLOAD_FILE");
        params.put("path", "vc");
        String view = getView(params);
        String displayAllAircraftString = ApplicationUtils.htmlToString("uncorrectedFilePath.html");
        Assert.assertEquals(displayAllAircraftString, view);
    }

    @Test
    public void checkCorrectFilePathWithCorrectFile() {
        Map<String, String> params = new HashMap<>();
        params.put("commandName", "UPLOAD_FILE");
        params.put("path", "I:\\jwd\\maskalenchykSiamen\\tasks\\module1\\src\\main\\resources\\inputFiles\\first file (right).csv");
        String view = getView(params);
        String displayAllAircraftString = ApplicationUtils.htmlToString("successInputFile.html");
        Assert.assertEquals(displayAllAircraftString, view);
    }

    @Test
    public void checkCorrectFilePathWithUncorrectedFile() {
        Map<String, String> params = new HashMap<>();
        params.put("commandName", "UPLOAD_FILE");
        params.put("path", "I:\\jwd\\maskalenchykSiamen\\tasks\\module1\\src\\main\\resources\\inputFiles\\second file (wrong).csv");
        String view = getView(params);
        String displayAllAircraftString = ApplicationUtils.htmlToString("correctPathWithUncorrectedFile.html");
        Assert.assertEquals(displayAllAircraftString, view);
    }

    @Test
    public void checkCorrectFilePathWithUncorrectedExtension() {
        Map<String, String> params = new HashMap<>();
        params.put("commandName", "UPLOAD_FILE");
        params.put("path", "I:\\jwd\\maskalenchykSiamen\\tasks\\module1\\src\\main\\resources\\log4j.properties");
        String view = getView(params);
        String displayAllAircraftString = ApplicationUtils.htmlToString("correctPathWithUncorrectedExtension.html");
        Assert.assertEquals(displayAllAircraftString, view);
    }

}