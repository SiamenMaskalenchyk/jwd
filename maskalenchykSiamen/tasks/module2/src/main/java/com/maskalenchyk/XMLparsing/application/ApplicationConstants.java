package com.maskalenchyk.xmlparsing.application;

import java.time.format.DateTimeFormatter;

public class ApplicationConstants {
    private ApplicationConstants() {
    }

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ISO_LOCAL_DATE;
    public static final String XSD_SCHEMA_PATH = "tariffs.xsd";
}
