package com.maskalenchyk.xmlparsing.application;

import com.maskalenchyk.xmlparsing.controller.*;
import com.maskalenchyk.xmlparsing.dal.Tariffs;
import com.maskalenchyk.xmlparsing.document_builder.AppDocumentBuilder;
import com.maskalenchyk.xmlparsing.document_builder.XmlDocumentBuilder;
import com.maskalenchyk.xmlparsing.entity.Tariff;
import com.maskalenchyk.xmlparsing.parser.*;
import com.maskalenchyk.xmlparsing.service.TariffService;
import com.maskalenchyk.xmlparsing.service.TariffServiceImpl;

public class ApplicationContext {

    private static final ApplicationContext INSTANCE = new ApplicationContext();
    private CommandProvider commandProvider;

    public static ApplicationContext getInstance() {
        return INSTANCE;
    }

    public void initialize() {
        Tariffs tariffs = new Tariffs();
        TariffService tariffService = new TariffServiceImpl(tariffs);
        XmlFileParser domXmlFileParser = new DomXmlFileParser();
        XmlFileParser saxXmlFileParser = new SaxXmlFileParser();
        XmlFileParser staxXmlFileParser = new StaxXmlFileParser();
        XmlFileParserProvider xmlFileParserProvider = new XmlFileParserProviderImpl(domXmlFileParser, saxXmlFileParser, staxXmlFileParser);
        UploadFileValidator uploadFileValidator = new UploadXmlFileValidatorImpl();
        AppDocumentBuilder<Tariff> appDocumentBuilder = new XmlDocumentBuilder();
        AppCommand displayTariffCommand = new DisplayTariffsCommand(tariffService);
        AppCommand defaultCommand = new DefaultCommand();
        AppCommand uploadTariffsCommand = new UploadTariffsCommand(tariffService, uploadFileValidator, xmlFileParserProvider);
        AppCommand downloadCommand = new DownloadFromDataBaseCommand(tariffService,appDocumentBuilder);
        commandProvider = new CommandProviderImpl(displayTariffCommand, defaultCommand, uploadTariffsCommand,downloadCommand);
    }

    public CommandProvider getCommandProvider() {
        return this.commandProvider;
    }

    public void destroy() {
        this.commandProvider = null;
    }
}
