package com.maskalenchyk.xmlparsing.application;

import com.maskalenchyk.xmlparsing.controller.AppCommandName;
import com.maskalenchyk.xmlparsing.controller.ApplicationException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@MultipartConfig
@WebServlet("/home-page")
public class ApplicationServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(ApplicationServlet.class);
    private ApplicationContext applicationContext;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        processRequest(req, resp);
    }

    @Override
    public void init() {
        applicationContext = ApplicationContext.getInstance();
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            String commandName = getCommandName(req);
            applicationContext.getCommandProvider().getCommand(commandName).execute(req, resp);
        } catch (ApplicationException e) {
            LOGGER.error(e.getMessage() + e);
            req.getServletContext().setAttribute("message", e.getMessage());
            resp.sendRedirect("/error");
        } catch (Exception e) {
            LOGGER.error("Critical error " + e);
        }
    }

    private String getCommandName(HttpServletRequest req) {
        Object commandFromAttribute = req.getAttribute("command");
        String commandName = commandFromAttribute == null ? req.getParameter("command") : commandFromAttribute.toString();
        return commandName == null ? AppCommandName.DEFAULT.getName() : commandName;
    }
}
