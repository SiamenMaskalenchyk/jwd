package com.maskalenchyk.xmlparsing.application;

import java.util.Arrays;
import java.util.HashSet;

public class ApplicationUtils {

    private ApplicationUtils() {
    }

    private static final HashSet<String> trueSet = new HashSet<>(Arrays.asList("1", "true", "yes"));

    public static Boolean stringToBoolean(String string) {
        return trueSet.contains(string.toLowerCase());
    }

    public static Integer booleanToNumber(boolean value) {
        return value ? 1 : 0;
    }
}
