package com.maskalenchyk.xmlparsing.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class AbstractAppCommand implements AppCommand {

    private static final Logger LOGGER = LogManager.getLogger(AbstractAppCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ApplicationException {
        try {
            executeWrapped(request, response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    protected abstract void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception;

    protected void forward(HttpServletRequest request, HttpServletResponse response, String view) {

        try {
            request.setAttribute("view", view);
            request.getRequestDispatcher(request.getContextPath() + view).forward(request, response);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to forward view", e);
        }
    }

    protected void redirect(HttpServletResponse response, String url) {
        try {
            response.sendRedirect(url);
        } catch (IOException e) {
            throw new IllegalStateException("Redirect to " + url + " failed ", e);
        }
    }
}
