package com.maskalenchyk.xmlparsing.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AppCommand {

    void execute(HttpServletRequest request, HttpServletResponse response) throws ApplicationException;

    String getIdentifier();
}
