package com.maskalenchyk.xmlparsing.controller;

public enum AppCommandName {

    DISPLAY_TARIFFS("DISPLAY_TARIFFS"),
    UPLOAD_TARIFFS("UPLOAD_TARIFFS"),
    DOWNLOAD("DOWNLOAD"),
    DEFAULT("DEFAULT");

    private String name;

    AppCommandName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
