package com.maskalenchyk.xmlparsing.controller;

public interface CommandProvider {

    AppCommand getCommand(String param);
}
