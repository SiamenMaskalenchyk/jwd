package com.maskalenchyk.xmlparsing.controller;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CommandProviderImpl implements CommandProvider {

    private Map<String, AppCommand> commandMap;

    public CommandProviderImpl(AppCommand... appCommands) {
        initCommandMap(appCommands);
    }

    @Override
    public AppCommand getCommand(String param) {
        return commandMap.getOrDefault(param.toUpperCase(), commandMap.get(AppCommandName.DEFAULT.getName()));
    }

    private void initCommandMap(AppCommand[] servletCommands) {
        this.commandMap = new ConcurrentHashMap<>();
        for (AppCommand command : servletCommands) {
            commandMap.put(command.getIdentifier(), command);
        }
    }
}
