package com.maskalenchyk.xmlparsing.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DefaultCommand implements AppCommand {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ApplicationException {
        try {
            response.sendRedirect("/homepage");
        } catch (IOException e) {
            throw new ApplicationException("Error loading home-page " + e.getMessage(), e);
        }
    }

    @Override
    public String getIdentifier() {
        return AppCommandName.DEFAULT.getName();
    }
}
