package com.maskalenchyk.xmlparsing.controller;

import com.maskalenchyk.xmlparsing.service.TariffService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DisplayTariffsCommand extends AbstractAppCommand {

    private TariffService tariffService;

    public DisplayTariffsCommand(TariffService tariffService) {
        this.tariffService = tariffService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) {
        request.getServletContext().setAttribute("tariffs", tariffService.getAll());
        forward(request,response, "WEB-INF/jsp/tariffsList.jsp");
    }

    @Override
    public String getIdentifier() {
        return "DISPLAY_TARIFFS";
    }
}
