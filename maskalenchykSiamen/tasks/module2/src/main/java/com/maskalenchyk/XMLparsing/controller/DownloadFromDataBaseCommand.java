package com.maskalenchyk.xmlparsing.controller;

import com.maskalenchyk.xmlparsing.document_builder.AppDocumentBuilder;
import com.maskalenchyk.xmlparsing.entity.Tariff;
import com.maskalenchyk.xmlparsing.service.TariffService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class DownloadFromDataBaseCommand extends AbstractAppCommand {

    private TariffService tariffService;
    private AppDocumentBuilder<Tariff> appDocumentBuilder;

    public DownloadFromDataBaseCommand(TariffService tariffService, AppDocumentBuilder<Tariff> appDocumentBuilder) {
        this.tariffService = tariffService;
        this.appDocumentBuilder = appDocumentBuilder;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<Tariff> tariffList = tariffService.getAll();
        InputStream inputStreamFromDataBase = appDocumentBuilder.buildDocument(tariffList);
        String fileName = "tariffs-data.xml";
        int fileLength = inputStreamFromDataBase.available();
        String mimeType = request.getServletContext().getMimeType(fileName);
        response.setContentType(mimeType);
        response.setContentLength(fileLength);
        String headerKey = "content-disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", fileName);
        response.setHeader(headerKey, headerValue);
        OutputStream outputStream = response.getOutputStream();
        byte[] buffer = new byte[4096];
        int bytesRead;
        while ((bytesRead = inputStreamFromDataBase.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        inputStreamFromDataBase.close();
        outputStream.close();
    }

    @Override
    public String getIdentifier() {
        return AppCommandName.DOWNLOAD.getName();
    }
}
