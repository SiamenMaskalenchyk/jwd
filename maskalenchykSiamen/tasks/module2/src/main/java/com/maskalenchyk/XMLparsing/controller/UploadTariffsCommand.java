package com.maskalenchyk.xmlparsing.controller;

import com.maskalenchyk.xmlparsing.entity.Tariff;
import com.maskalenchyk.xmlparsing.parser.FileParser;
import com.maskalenchyk.xmlparsing.parser.UploadFileValidator;
import com.maskalenchyk.xmlparsing.parser.XmlFileParserProvider;
import com.maskalenchyk.xmlparsing.service.TariffService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.InputStream;
import java.util.List;

public class UploadTariffsCommand extends AbstractAppCommand {

    private TariffService tariffService;
    private UploadFileValidator fileValidator;
    private XmlFileParserProvider xmlFileParserProvider;

    public UploadTariffsCommand(TariffService tariffService, UploadFileValidator fileValidator,
                                XmlFileParserProvider xmlFileParserProvider) {
        this.tariffService = tariffService;
        this.fileValidator = fileValidator;
        this.xmlFileParserProvider = xmlFileParserProvider;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Part xmlFile = request.getPart("xmlFile");
        String fileValidateResult = fileValidator.validateFile(xmlFile);
        request.getServletContext().setAttribute("tariffs", null);
        if ("valid".equals(fileValidateResult)) {
            try (InputStream inputStream = xmlFile.getInputStream()) {
                FileParser<Tariff> parser = xmlFileParserProvider.getTariffBuilder(request.getParameter("parserType"));
                List<Tariff> parsedTariffs = parser.parse(inputStream);
                parsedTariffs.forEach(tariffService::save);
                request.getServletContext().setAttribute("tariffs", parsedTariffs);
            }
        } else {
            request.getServletContext().setAttribute("validateResult", fileValidateResult);
        }
        redirect(response, request.getContextPath() + "upload-result");
    }

    @Override
    public String getIdentifier() {
        return AppCommandName.UPLOAD_TARIFFS.getName();
    }
}
