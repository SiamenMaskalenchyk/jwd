package com.maskalenchyk.xmlparsing.dal;

import java.util.List;

public interface CrudEntityDao<ENTITY, KEY> {

    List<ENTITY> findAll();

    void save(ENTITY entity);

    void update(KEY key, ENTITY entity);

    ENTITY get(KEY key);

    void delete(KEY key);
}
