package com.maskalenchyk.xmlparsing.dal;

import com.maskalenchyk.xmlparsing.entity.Tariff;

public interface TariffDao extends CrudEntityDao<Tariff, String> {
}
