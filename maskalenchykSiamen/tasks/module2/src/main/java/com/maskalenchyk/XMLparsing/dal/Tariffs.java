package com.maskalenchyk.xmlparsing.dal;

import com.maskalenchyk.xmlparsing.entity.StandardTariff;
import com.maskalenchyk.xmlparsing.entity.Tariff;
import com.maskalenchyk.xmlparsing.entity.WorkingTariff;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "tariffStorage"
})
@XmlRootElement(name = "tariffsList", namespace = "http://www.tariffs.com/tariffs")
public class Tariffs implements TariffDao {

    @XmlElementRef(name = "tariffStorage", namespace = "http://www.tariffs.com/tariffs", type = JAXBElement.class)
    private final Map<String, Tariff> tariffStorage = new ConcurrentHashMap<>();

    @Override
    public void save(Tariff tariff) {
        this.tariffStorage.put(tariff.getId(), cloneEntity(tariff));
    }

    @Override
    public void update(String key, Tariff tariff) {
        if (this.tariffStorage.containsKey(key)) {
            this.tariffStorage.put(key, cloneEntity(tariff));
        } else {
            throw new EntityNotFoundException("Entity with key " + key + " not found");
        }
    }

    @Override
    public Tariff get(String key) {
        if (this.tariffStorage.containsKey(key)) {
            return this.tariffStorage.get(key);
        } else {
            throw new EntityNotFoundException("Entity with key " + key + " not found");
        }
    }

    @Override
    public void delete(String key) {
        this.tariffStorage.remove(key);
    }

    private Tariff cloneEntity(Tariff tariff) {
        if (tariff instanceof StandardTariff) {
            return new StandardTariff((StandardTariff) tariff);
        }
        if (tariff instanceof WorkingTariff) {
            return new WorkingTariff((WorkingTariff) tariff);
        }
        throw new WrongClassEntityException("Wrong class " + tariff.getClass());
    }

    @Override
    public List<Tariff> findAll() {
        return new ArrayList<>(tariffStorage.values());
    }
}
