package com.maskalenchyk.xmlparsing.dal;

public class WrongClassEntityException extends RuntimeException {

    public WrongClassEntityException(String message) {
        super(message);
    }

}
