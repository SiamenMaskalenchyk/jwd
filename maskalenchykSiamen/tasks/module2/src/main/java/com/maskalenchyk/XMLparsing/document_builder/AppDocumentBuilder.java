package com.maskalenchyk.xmlparsing.document_builder;

import java.io.InputStream;
import java.util.List;

public interface AppDocumentBuilder<T> {

    InputStream buildDocument(List<T> entityList) throws DocumentBuilderException;
}
