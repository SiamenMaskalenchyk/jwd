package com.maskalenchyk.xmlparsing.document_builder;

public class DocumentBuilderException extends Exception {

    public DocumentBuilderException(String message) {
        super(message);
    }

    public DocumentBuilderException(String message, Throwable cause) {
        super(message, cause);
    }

    public DocumentBuilderException(Throwable cause) {
        super(cause);
    }

}
