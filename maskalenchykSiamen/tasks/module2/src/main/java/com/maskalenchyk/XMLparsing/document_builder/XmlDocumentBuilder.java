package com.maskalenchyk.xmlparsing.document_builder;

import com.maskalenchyk.xmlparsing.application.ApplicationUtils;
import com.maskalenchyk.xmlparsing.entity.*;
import com.maskalenchyk.xmlparsing.parser.TariffEnum;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class XmlDocumentBuilder implements AppDocumentBuilder<Tariff> {

    @Override
    public InputStream buildDocument(List<Tariff> entityList) throws DocumentBuilderException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            String root = "tariffs";
            Element rootElement = document.createElement(root);
            rootElement.setAttribute("xmlns", "http://www.tariffs.com/tariffs");
            rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            rootElement.setAttribute("xsi:schemaLocation", "http://www.tariffs.com/tariffs tariffs.xsd");
            document.appendChild(rootElement);
            for (Tariff tariff : entityList) {
                String tariffElementTagName = getTariffName(tariff);
                Element tariffElement = document.createElement(tariffElementTagName);
                tariffElement.setAttribute(TariffEnum.ID.getValue(), tariff.getId());
                tariffElement.setAttribute(TariffEnum.DESCRIPTION.getValue(), tariff.getDescription());
                List<Element> tariffChilds = createTariffChildElements(document, tariff, tariffElementTagName);
                tariffChilds.forEach(tariffElement::appendChild);
                rootElement.appendChild(tariffElement);
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(byteArrayOutputStream);
            transformer.transform(source, result);
            return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        } catch (TransformerException e) {
            throw new DocumentBuilderException("Document build exception: " + e.getMessage(), e);
        } catch (ParserConfigurationException e) {
            throw new DocumentBuilderException("Build document_builder exception " + e.getMessage(), e);
        }
    }

    private List<Element> createTariffChildElements(Document document, Tariff tariff, String tariffElementTagName)
            throws DocumentBuilderException {
        List<Element> elements = new ArrayList<>();
        Element tariffName = document.createElement(TariffEnum.NAME.getValue());
        tariffName.appendChild(document.createTextNode(tariff.getName()));
        Element dateCreation = document.createElement(TariffEnum.DATECREATION.getValue());
        dateCreation.appendChild(document.createTextNode(tariff.getDateCreation().toString()));
        Element operatorName = document.createElement(TariffEnum.OPERATORNAME.getValue());
        operatorName.appendChild(document.createTextNode(tariff.getOperatorName().name()));
        Element payroll = document.createElement(TariffEnum.PAYROLL.getValue());
        payroll.appendChild(document.createTextNode(tariff.getPayroll().toString()));
        Element callPrices = createCallPricesElement(document, tariff);
        Element smsPrice = document.createElement(TariffEnum.SMSPRICE.getValue());
        smsPrice.appendChild(document.createTextNode(tariff.getSMSPrice().toString()));
        Element parameters = createParametersElement(document, tariff);
        elements.add(tariffName);
        elements.add(dateCreation);
        elements.add(dateCreation);
        elements.add(operatorName);
        elements.add(payroll);
        elements.add(callPrices);
        elements.add(smsPrice);
        elements.add(parameters);
        if (TariffEnum.WORKINGTARIFF.getValue().equals(tariffElementTagName)) {
            Element workingCallPrices = document.createElement(TariffEnum.WORKINGCALLPRICES.getValue());
            workingCallPrices.appendChild(document.createTextNode(((WorkingTariff) tariff).getWorkingCallPrices().toString()));
            elements.add(workingCallPrices);
        } else if (TariffEnum.STANDARDTARIFF.getValue().equals(tariffElementTagName)) {
            Element discountAmount = document.createElement(TariffEnum.WORKINGCALLPRICES.getValue());
            discountAmount.appendChild(document.createTextNode(((StandardTariff) tariff).getDiscountAmount().toString()));
            elements.add(discountAmount);
        }
        return elements;
    }

    private Element createParametersElement(Document document, Tariff tariff) throws DocumentBuilderException {
        String parametersTagName = getParametersName(tariff);
        Element parameters = document.createElement(parametersTagName);
        Element tariffication = document.createElement(TariffEnum.TARIFFICATION.getValue());
        tariffication.appendChild(document.createTextNode(tariff.getParameters().getTariffication().getValue()));
        Element connectionFee = document.createElement(TariffEnum.CONNECTIONFEE.getValue());
        connectionFee.appendChild(document.createTextNode(tariff.getParameters().getConnectionFee().toString()));
        parameters.appendChild(tariffication);
        parameters.appendChild(connectionFee);
        if (TariffEnum.WIDEPARAMETERS.getValue().equals(parametersTagName)) {
            Element isNumberHidden = document.createElement(TariffEnum.ISNUMBERHIDDEN.getValue());
            String valueNumberHidden = ApplicationUtils.booleanToNumber(
                    ((WideParameters) tariff.getParameters()).isNumberHidden()).toString();
            isNumberHidden.appendChild(document.createTextNode(valueNumberHidden));
            Element isAdvertisingAvailable = document.createElement(TariffEnum.ISADVERTISINGAVAILABLE.getValue());
            String valueAdvertisingAvailable = ApplicationUtils.booleanToNumber(
                    ((WideParameters) tariff.getParameters()).isAdvertisingAvailable()).toString();
            isAdvertisingAvailable.appendChild(document.createTextNode(valueAdvertisingAvailable));
            parameters.appendChild(isNumberHidden);
            parameters.appendChild(isAdvertisingAvailable);
            return parameters;
        } else if (TariffEnum.LOWPARAMETERS.getValue().equals(parametersTagName)) {
            Element isHaveFavoriteNumber = document.createElement(TariffEnum.ISHAVEFAVORITENUMBER.getValue());
            String valueHaveFavoriteNumber = ApplicationUtils.booleanToNumber(
                    ((LowParameters) tariff.getParameters()).isHaveFavoriteNumber()).toString();
            isHaveFavoriteNumber.appendChild(document.createTextNode(valueHaveFavoriteNumber));
            Element isRecallAvailable = document.createElement(TariffEnum.ISRECALLAVAILABLE.getValue());
            String valueIsRecallAvailable = ApplicationUtils.booleanToNumber(
                    ((LowParameters) tariff.getParameters()).isRecallAvailable()).toString();
            isRecallAvailable.appendChild(document.createTextNode(valueIsRecallAvailable));
            parameters.appendChild(isHaveFavoriteNumber);
            parameters.appendChild(isRecallAvailable);
            return parameters;
        }
        throw new DocumentBuilderException("Unknown tag name for parameters " + parametersTagName);
    }

    private String getParametersName(Tariff tariff) throws DocumentBuilderException {
        Parameters parameters = tariff.getParameters();
        if (parameters instanceof LowParameters) {
            return TariffEnum.LOWPARAMETERS.getValue();
        } else if (parameters instanceof WideParameters) {
            return TariffEnum.WIDEPARAMETERS.getValue();
        }
        throw new DocumentBuilderException("Unknown element type " + parameters.getClass());
    }

    private Element createCallPricesElement(Document document, Tariff tariff) {
        Element callPrices = document.createElement(TariffEnum.CALLPRICES.getValue());
        Element insideCallPrices = document.createElement(TariffEnum.INSIDECALLPRICES.getValue());
        insideCallPrices.appendChild(document.createTextNode(tariff.getCallPrices().getInsideCallPrices().toString()));
        Element outsideCallPrices = document.createElement(TariffEnum.OUTSIDECALLPRICES.getValue());
        outsideCallPrices.appendChild(document.createTextNode(tariff.getCallPrices().getOutsideCallPrices().toString()));
        Element landlinesCallPrices = document.createElement(TariffEnum.LANDLINESCALLPRICES.getValue());
        landlinesCallPrices.appendChild(document.createTextNode(tariff.getCallPrices().getLandlinesCallPrices().toString()));
        callPrices.appendChild(insideCallPrices);
        callPrices.appendChild(outsideCallPrices);
        callPrices.appendChild(landlinesCallPrices);
        return callPrices;
    }

    private String getTariffName(Tariff tariff) throws DocumentBuilderException {
        if (tariff instanceof StandardTariff) {
            return TariffEnum.STANDARDTARIFF.getValue();
        } else if (tariff instanceof WorkingTariff) {
            return TariffEnum.WORKINGTARIFF.getValue();
        }
        throw new DocumentBuilderException("Unknown element type " + tariff.getClass());
    }
}
