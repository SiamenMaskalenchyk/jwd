package com.maskalenchyk.xmlparsing.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.Objects;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallPrices", namespace = "http://www.tariffs.com/tariffs", propOrder = {
        "insideCallPrices",
        "outsideCallPrices",
        "landlinesCallPrices"
})

public class CallPrices {

    @XmlElement(namespace = "http://www.tariffs.com/tariffs", required = true)
    private BigDecimal insideCallPrices;
    @XmlElement(namespace = "http://www.tariffs.com/tariffs", required = true)
    private BigDecimal outsideCallPrices;
    @XmlElement(name = "landlines-call-prices", namespace = "http://www.tariffs.com/tariffs", required = true)
    private BigDecimal landlinesCallPrices;

    public BigDecimal getInsideCallPrices() {
        return insideCallPrices;
    }

    public void setInsideCallPrices(BigDecimal insideCallPrices) {
        this.insideCallPrices = insideCallPrices;
    }

    public BigDecimal getOutsideCallPrices() {
        return outsideCallPrices;
    }

    public void setOutsideCallPrices(BigDecimal outsideCallPrices) {
        this.outsideCallPrices = outsideCallPrices;
    }

    public BigDecimal getLandlinesCallPrices() {
        return landlinesCallPrices;
    }

    public void setLandlinesCallPrices(BigDecimal landlinesCallPrices) {
        this.landlinesCallPrices = landlinesCallPrices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CallPrices that = (CallPrices) o;
        return Objects.equals(insideCallPrices, that.insideCallPrices) &&
                Objects.equals(outsideCallPrices, that.outsideCallPrices) &&
                Objects.equals(landlinesCallPrices, that.landlinesCallPrices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(insideCallPrices, outsideCallPrices, landlinesCallPrices);
    }

    @Override
    public String toString() {
        return "CallPrices{" +
                "insideCallPrices=" + insideCallPrices +
                ", outsideCallPrices=" + outsideCallPrices +
                ", landlinesCallPrices=" + landlinesCallPrices +
                '}';
    }
}
