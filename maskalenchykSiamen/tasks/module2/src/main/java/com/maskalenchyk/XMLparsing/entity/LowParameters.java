package com.maskalenchyk.xmlparsing.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Objects;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LowParameters", namespace = "http://www.tariffs.com/tariffs", propOrder = {
        "isHaveFavoriteNumber",
        "isRecallAvailable"
})
public class LowParameters extends Parameters {

    @XmlElement(namespace = "http://www.tariffs.com/tariffs")
    private boolean isHaveFavoriteNumber;
    @XmlElement(namespace = "http://www.tariffs.com/tariffs")
    private boolean isRecallAvailable;

    public boolean isHaveFavoriteNumber() {
        return isHaveFavoriteNumber;
    }

    public void setHaveFavoriteNumber(boolean haveFavoriteNumber) {
        isHaveFavoriteNumber = haveFavoriteNumber;
    }

    public boolean isRecallAvailable() {
        return isRecallAvailable;
    }

    public void setRecallAvailable(boolean recallAvailable) {
        isRecallAvailable = recallAvailable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        LowParameters that = (LowParameters) o;
        return isHaveFavoriteNumber == that.isHaveFavoriteNumber &&
                isRecallAvailable == that.isRecallAvailable;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isHaveFavoriteNumber, isRecallAvailable);
    }

    @Override
    public String toString() {
        return "LowParameters{" +
                "isHaveFavoriteNumber=" + isHaveFavoriteNumber +
                ", isRecallAvailable=" + isRecallAvailable +
                '}';
    }
}
