
package com.maskalenchyk.xmlparsing.entity;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


@XmlType(name = "OperatorName", namespace = "http://www.tariffs.com/tariffs")
@XmlEnum
public enum OperatorName {

    MTC("MTC"),
    @XmlEnumValue("A1")
    A1("A1"),
    @XmlEnumValue("Life")
    LIFE("Life");
    private final String value;

    OperatorName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OperatorName fromValue(String v) {
        for (OperatorName c : OperatorName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
