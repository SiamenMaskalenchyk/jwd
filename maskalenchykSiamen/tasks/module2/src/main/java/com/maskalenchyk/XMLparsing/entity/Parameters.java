package com.maskalenchyk.xmlparsing.entity;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;
import java.util.Objects;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Parameters", namespace = "http://www.tariffs.com/tariffs", propOrder = {
        "tariffication",
        "connectionFee"
})
@XmlSeeAlso({
        WideParameters.class,
        LowParameters.class
})
public abstract class Parameters {

    @XmlElement(namespace = "http://www.tariffs.com/tariffs", required = true)
    private Tariffication tariffication;
    @XmlElement(namespace = "http://www.tariffs.com/tariffs", required = true)
    private BigDecimal connectionFee;

    public Tariffication getTariffication() {
        return tariffication;
    }

    public void setTariffication(Tariffication tariffication) {
        this.tariffication = tariffication;
    }

    public BigDecimal getConnectionFee() {
        return connectionFee;
    }

    public void setConnectionFee(BigDecimal connectionFee) {
        this.connectionFee = connectionFee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parameters that = (Parameters) o;
        return tariffication == that.tariffication &&
                Objects.equals(connectionFee, that.connectionFee);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tariffication, connectionFee);
    }

    @Override
    public String toString() {
        return "Parameters{" +
                "tariffication=" + tariffication +
                ", connectionFee=" + connectionFee +
                '}';
    }

}
