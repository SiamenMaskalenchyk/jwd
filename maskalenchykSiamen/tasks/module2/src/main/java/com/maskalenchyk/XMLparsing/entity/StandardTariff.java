package com.maskalenchyk.xmlparsing.entity;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StandardTariff", namespace = "http://www.tariffs.com/tariffs", propOrder = {
        "discountAmount"
})
public class StandardTariff extends Tariff {

    @XmlElement(namespace = "http://www.tariffs.com/tariffs", required = true)
    @XmlSchemaType(name = "positiveInteger")
    private BigDecimal discountAmount;

    public StandardTariff() {
    }

    public StandardTariff(String name, LocalDate dateCreation, OperatorName operatorName,
                          BigDecimal payroll, CallPrices callPrices, BigDecimal smsPrice,
                          Parameters parameters, String id, String description, BigDecimal discountAmount) {
        super(name, dateCreation, operatorName, payroll, callPrices, smsPrice, parameters, id, description);
        this.discountAmount = discountAmount;
    }

    public StandardTariff(StandardTariff standardTariff) {
        super(standardTariff);
        this.discountAmount = standardTariff.getDiscountAmount();
    }


    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    @Override
    public String toString() {
        return "StandardTariff{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                ", dateCreation=" + dateCreation +
                ", operatorName=" + operatorName +
                ", payroll=" + payroll +
                "," + System.lineSeparator() +
                " callPrices=" + callPrices +
                "," + System.lineSeparator() +
                " smsPrice=" + smsPrice +
                "," + System.lineSeparator() +
                " parameters=" + parameters +
                "," + System.lineSeparator() +
                "discountAmount=" + discountAmount +
                '}';
    }
}
