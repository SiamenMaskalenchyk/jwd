package com.maskalenchyk.xmlparsing.entity;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tariff", namespace = "http://www.tariffs.com/tariffs", propOrder = {
        "name",
        "dateCreation",
        "operatorName",
        "payroll",
        "callPrices",
        "smsPrice",
        "parameters"
})
@XmlSeeAlso({
        WorkingTariff.class,
        StandardTariff.class
})
public abstract class Tariff {

    @XmlElement(namespace = "http://www.tariffs.com/tariffs", required = true)
    protected String name;
    @XmlElement(namespace = "http://www.tariffs.com/tariffs", required = true)
    @XmlSchemaType(name = "date")
    protected LocalDate dateCreation;
    @XmlElement(namespace = "http://www.tariffs.com/tariffs", required = true)
    @XmlSchemaType(name = "string")
    protected OperatorName operatorName;
    @XmlElement(namespace = "http://www.tariffs.com/tariffs", required = true)
    @XmlSchemaType(name = "positiveInteger")
    protected BigDecimal payroll;
    @XmlElement(namespace = "http://www.tariffs.com/tariffs", required = true)
    protected CallPrices callPrices;
    @XmlElement(name = "SMSPrice", namespace = "http://www.tariffs.com/tariffs", required = true)
    protected BigDecimal smsPrice;
    @XmlElementRef(name = "parameters", namespace = "http://www.tariffs.com/tariffs", type = JAXBElement.class)
    protected Parameters parameters;
    @XmlAttribute(name = "id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    protected String id;
    @XmlAttribute(name = "description")
    protected String description;

    public Tariff() {
    }

    public Tariff(String name, LocalDate dateCreation, OperatorName operatorName,
                  BigDecimal payroll, CallPrices callPrices, BigDecimal smsPrice, Parameters parameters,
                  String id, String description) {
        this.name = name;
        this.dateCreation = dateCreation;
        this.operatorName = operatorName;
        this.payroll = payroll;
        this.callPrices = callPrices;
        this.smsPrice = smsPrice;
        this.parameters = parameters;
        this.id = id;
        this.description = description;
    }

    public Tariff(Tariff tariff) {
        this.name = tariff.getName();
        this.dateCreation = tariff.getDateCreation();
        this.operatorName = tariff.getOperatorName();
        this.payroll = tariff.getPayroll();
        this.callPrices = tariff.getCallPrices();
        this.smsPrice = tariff.getSMSPrice();
        this.parameters = tariff.getParameters();
        this.id = tariff.getId();
        this.description = tariff.getDescription();
    }

    public void setSmsPrice(BigDecimal smsPrice) {
        this.smsPrice = smsPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDate dateCreation) {
        this.dateCreation = dateCreation;
    }

    public OperatorName getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(OperatorName operatorName) {
        this.operatorName = operatorName;
    }

    public BigDecimal getPayroll() {
        return payroll;
    }

    public void setPayroll(BigDecimal payroll) {
        this.payroll = payroll;
    }

    public CallPrices getCallPrices() {
        return callPrices;
    }

    public void setCallPrices(CallPrices callPrices) {
        this.callPrices = callPrices;
    }

    public BigDecimal getSMSPrice() {
        return smsPrice;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        if (description == null) {
            return "information";
        } else {
            return description;
        }
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tariff tariff = (Tariff) o;
        return Objects.equals(name, tariff.name) &&
                Objects.equals(dateCreation, tariff.dateCreation) &&
                operatorName == tariff.operatorName &&
                Objects.equals(payroll, tariff.payroll) &&
                Objects.equals(callPrices, tariff.callPrices) &&
                Objects.equals(smsPrice, tariff.smsPrice) &&
                Objects.equals(parameters, tariff.parameters) &&
                Objects.equals(id, tariff.id) &&
                Objects.equals(description, tariff.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, dateCreation, operatorName, payroll, callPrices, smsPrice, parameters, id, description);
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "name='" + name + '\'' +
                ", dateCreation=" + dateCreation +
                ", operatorName=" + operatorName +
                ", payroll=" + payroll +
                "," + System.lineSeparator() +
                "callPrices=" + callPrices +
                ", smsPrice=" + smsPrice +
                "," + System.lineSeparator() +
                ", parameters=" + parameters +
                ", id='" + id + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
