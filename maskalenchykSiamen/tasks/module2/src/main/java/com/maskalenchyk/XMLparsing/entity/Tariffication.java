package com.maskalenchyk.xmlparsing.entity;

public enum Tariffication {

    SECONDS("seconds"),
    MINUTES("minutes");

String value;

    Tariffication(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
