package com.maskalenchyk.xmlparsing.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.Objects;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WideParameters", namespace = "http://www.tariffs.com/tariffs", propOrder = {
        "internetAmountMB",
        "isNumberHidden",
        "isAdvertisingAvailable"
})
public class WideParameters extends Parameters {

    @XmlElement(name = "internet-amount-MB", namespace = "http://www.tariffs.com/tariffs", required = true)
    private BigDecimal internetAmountMB;
    @XmlElement(namespace = "http://www.tariffs.com/tariffs")
    private boolean isNumberHidden;
    @XmlElement(namespace = "http://www.tariffs.com/tariffs")
    private boolean isAdvertisingAvailable;

    public BigDecimal getInternetAmountMB() {
        return internetAmountMB;
    }

    public void setInternetAmountMB(BigDecimal internetAmountMB) {
        this.internetAmountMB = internetAmountMB;
    }

    public boolean isNumberHidden() {
        return isNumberHidden;
    }

    public void setNumberHidden(boolean numberHidden) {
        isNumberHidden = numberHidden;
    }

    public boolean isAdvertisingAvailable() {
        return isAdvertisingAvailable;
    }

    public void setAdvertisingAvailable(boolean advertisingAvailable) {
        isAdvertisingAvailable = advertisingAvailable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        WideParameters that = (WideParameters) o;
        return isNumberHidden == that.isNumberHidden &&
                isAdvertisingAvailable == that.isAdvertisingAvailable &&
                Objects.equals(internetAmountMB, that.internetAmountMB);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), internetAmountMB, isNumberHidden, isAdvertisingAvailable);
    }

    @Override
    public String toString() {
        return "WideParameters{" +
                "internetAmountMB=" + internetAmountMB +
                ", isNumberHidden=" + isNumberHidden +
                ", isAdvertisingAvailable=" + isAdvertisingAvailable +
                '}';
    }

}
