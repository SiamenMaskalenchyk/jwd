package com.maskalenchyk.xmlparsing.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkingTariff", namespace = "http://www.tariffs.com/tariffs", propOrder = {
        "workingCallPrices"
})
public class WorkingTariff extends Tariff {

    @XmlElement(namespace = "http://www.tariffs.com/tariffs", required = true)
    private BigDecimal workingCallPrices;

    public WorkingTariff() {
    }

    public WorkingTariff(String name, LocalDate dateCreation, OperatorName operatorName,
                         BigDecimal payroll, CallPrices callPrices, BigDecimal smsPrice,
                         Parameters parameters, String id, String description, BigDecimal workingCallPrices) {
        super(name, dateCreation, operatorName, payroll, callPrices, smsPrice, parameters, id, description);
        this.workingCallPrices = workingCallPrices;
    }

    public WorkingTariff(WorkingTariff workingTariff) {
        super(workingTariff);
        this.workingCallPrices = workingTariff.getWorkingCallPrices();
    }

    public BigDecimal getWorkingCallPrices() {
        return workingCallPrices;
    }

    public void setWorkingCallPrices(BigDecimal workingCallPrices) {
        this.workingCallPrices = workingCallPrices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        WorkingTariff that = (WorkingTariff) o;
        return Objects.equals(workingCallPrices, that.workingCallPrices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), workingCallPrices);
    }

    @Override
    public String toString() {
        return "WorkingTariff{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                ", dateCreation=" + dateCreation +
                ", operatorName=" + operatorName +
                ", payroll=" + payroll +
                "," + System.lineSeparator() +
                " callPrices=" + callPrices +
                "," + System.lineSeparator() +
                " smsPrice=" + smsPrice +
                "," + System.lineSeparator() +
                " parameters=" + parameters +
                "," + System.lineSeparator() +
                " workingCallPrices=" + workingCallPrices +
                '}';
    }
}
