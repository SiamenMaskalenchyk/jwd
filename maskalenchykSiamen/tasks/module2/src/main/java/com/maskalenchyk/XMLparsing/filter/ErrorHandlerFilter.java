package com.maskalenchyk.xmlparsing.filter;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ErrorHandlerFilter implements Filter {

    private static final Logger LOGGER = LogManager.getLogger(ErrorHandlerFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        try {
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (Exception e) {
            String requestURL = request.getRequestURI();
            LOGGER.error("Request " + requestURL + " failed: " + e.getMessage(), e);
            request.getServletContext().setAttribute("message", "Request " + requestURL + " failed: " + e.getMessage());
            response.sendRedirect("/error");
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }
}
