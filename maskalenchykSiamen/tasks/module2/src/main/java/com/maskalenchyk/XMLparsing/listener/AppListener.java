package com.maskalenchyk.xmlparsing.listener;

import com.maskalenchyk.xmlparsing.application.ApplicationContext;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppListener implements ServletContextListener {

    private static final Logger LOGGER = LogManager.getLogger(AppListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ApplicationContext.getInstance().initialize();
        LOGGER.info("Context initialized");
        ServletContext servletContext = servletContextEvent.getServletContext();
        servletContext.setInitParameter("command", "default");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ApplicationContext.getInstance().destroy();
        LOGGER.info("Context destroyed");
    }
}
