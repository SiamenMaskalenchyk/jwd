package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.entity.CallPrices;

import java.math.BigDecimal;

public class CallPriceBuilder implements EntityBuilder<CallPrices> {

    private CallPrices currentCallPrices;

    public CallPriceBuilder(CallPrices currentCallPrices) {
        this.currentCallPrices = currentCallPrices;
    }

    @Override
    public CallPrices build() {
        return this.currentCallPrices;
    }

    public CallPriceBuilder setInsideCallPrices(BigDecimal value) {
        this.currentCallPrices.setInsideCallPrices(value);
        return this;
    }

    public CallPriceBuilder setOutsideCallPrices(BigDecimal value) {
        this.currentCallPrices.setOutsideCallPrices(value);
        return this;
    }

    public CallPriceBuilder setLandlinesCallPrices(BigDecimal value) {
        this.currentCallPrices.setLandlinesCallPrices(value);
        return this;
    }

}
