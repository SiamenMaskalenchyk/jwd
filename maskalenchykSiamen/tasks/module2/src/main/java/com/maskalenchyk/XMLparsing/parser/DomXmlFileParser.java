package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.application.ApplicationConstants;
import com.maskalenchyk.xmlparsing.application.ApplicationUtils;
import com.maskalenchyk.xmlparsing.entity.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DomXmlFileParser extends XmlFileParser {

    @Override
    public List<Tariff> buildTariffList(InputStream inputStream) throws ParseException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            List<Tariff> parsedTariffList = new ArrayList<>();
            Document document = documentBuilder.parse(inputStream);
            Element root = document.getDocumentElement();
            NodeList tariffNodeList = root.getChildNodes();
            for (int i = 0; i < tariffNodeList.getLength(); i++) {
                Node childNode = tariffNodeList.item(i);
                if (childNode instanceof Element) {
                    Element tariffElement = (Element) tariffNodeList.item(i);
                    Tariff tariff = buildTariff(tariffElement);
                    parsedTariffList.add(tariff);
                }
            }
            return parsedTariffList;
        } catch (SAXException | IOException | ParserConfigurationException e) {
            throw new ParseException("Parsing error in DOM parser " + e);
        }
    }

    @Override
    public String getIdentifier() {
        return "DOM";
    }

    private Tariff buildTariff(Element tariffElement) throws ParseException {
        if (tariffTagNames.contains(tariffElement.getTagName())) {
            String name = getElementTextContent(tariffElement, "name");
            String date = getElementTextContent(tariffElement, "dateCreation");
            LocalDate dateCreation = LocalDate.parse(date, ApplicationConstants.DATE_TIME_FORMATTER);
            OperatorName operatorName = OperatorName.valueOf(getElementTextContent(tariffElement, "operatorName").toUpperCase());
            BigDecimal payroll = new BigDecimal(Integer.parseInt(getElementTextContent(tariffElement, "payroll")));
            CallPrices callPrices = buildCallPrices(tariffElement);
            BigDecimal smsPrice = new BigDecimal(Integer.parseInt(getElementTextContent(tariffElement, "SMSPrice")));
            Parameters parameters = buildParameters(tariffElement);
            String id = tariffElement.getAttribute("id");
            String description = tariffElement.hasAttribute("description") ? tariffElement.getAttribute("description") : "information";
            if ("workingTariff".equals(tariffElement.getTagName())) {
                BigDecimal workingCallPrices = new BigDecimal(Integer.parseInt(getElementTextContent(tariffElement, "workingCallPrices")));
                return new WorkingTariffBuilder(new WorkingTariff())
                        .setName(name)
                        .setDateCreation(dateCreation)
                        .setOperatorName(operatorName)
                        .setPayroll(payroll)
                        .setCallPrices(callPrices)
                        .setSMSPrice(smsPrice)
                        .setParameters(parameters)
                        .setId(id)
                        .setDescription(description)
                        .setWorkingCallPrices(workingCallPrices).build();
            } else {
                BigDecimal discountAmount = new BigDecimal(Integer.parseInt(getElementTextContent(tariffElement, "discountAmount")));
                return new StandardTariffBuilder(new StandardTariff())
                        .setName(name)
                        .setDateCreation(dateCreation)
                        .setOperatorName(operatorName)
                        .setPayroll(payroll)
                        .setCallPrices(callPrices)
                        .setSMSPrice(smsPrice)
                        .setParameters(parameters)
                        .setId(id)
                        .setDescription(description)
                        .setDiscountAmount(discountAmount).build();
            }
        } else {
            throw new ParseException("Unsupported tag for tariff" + tariffElement.getTagName());
        }
    }

    private Parameters buildParameters(Element tariffElement) throws ParseException {
        Element exceptedWideParametersElement = (Element) tariffElement.getElementsByTagName("wideParameters").item(0);
        Element exceptedLowParametersElement = (Element) tariffElement.getElementsByTagName("lowParameters").item(0);
        Element currentParameters = Optional.ofNullable(exceptedWideParametersElement).orElse(exceptedLowParametersElement);
        if (parametersTags.contains(currentParameters.getTagName())) {
            BigDecimal connectionFee = new BigDecimal(Integer.parseInt(getElementTextContent(tariffElement, "connectionFee")));
            Tariffication tariffication = Tariffication.valueOf(getElementTextContent(tariffElement, "tariffication").toUpperCase());
            if ("wideParameters".equals(currentParameters.getTagName())) {
                BigDecimal internetAmount = new BigDecimal(Integer.parseInt(getElementTextContent(tariffElement, "internetAmountMB")));
                Boolean numberHidden = ApplicationUtils.stringToBoolean(getElementTextContent(tariffElement, "isNumberHidden"));
                Boolean advertisingAvailable = ApplicationUtils.stringToBoolean(getElementTextContent(tariffElement, "isAdvertisingAvailable"));
                return new WideParametersBuilder(new WideParameters())
                        .setConnectionFee(connectionFee)
                        .setTariffication(tariffication)
                        .setInternetAmount(internetAmount)
                        .setNumberHidden(numberHidden)
                        .setAdvertisingAvailable(advertisingAvailable)
                        .build();
            } else {
                Boolean haveFavoriteNumber = ApplicationUtils.stringToBoolean(getElementTextContent(tariffElement, "isHaveFavoriteNumber"));
                Boolean recallAvailable = ApplicationUtils.stringToBoolean(getElementTextContent(tariffElement, "isRecallAvailable"));
                return new LowParametersBuilder(new LowParameters())
                        .setConnectionFee(connectionFee)
                        .setTariffication(tariffication)
                        .setHaveFavoriteNumber(haveFavoriteNumber)
                        .setRecallAvailable(recallAvailable)
                        .build();
            }
        } else {
            throw new ParseException("Unsupported tag for parameters" + currentParameters.getTagName());
        }
    }

    private CallPrices buildCallPrices(Element tariffElement) {
        BigDecimal insideCallPrices = new BigDecimal(Integer.parseInt(getElementTextContent(tariffElement, "insideCallPrices")));
        BigDecimal outsideCalPrices = new BigDecimal(Integer.parseInt(getElementTextContent(tariffElement, "outsideCallPrices")));
        BigDecimal landLinesPrices = new BigDecimal(Integer.parseInt(getElementTextContent(tariffElement, "landlinesCallPrices")));
        return new CallPriceBuilder(new CallPrices())
                .setInsideCallPrices(insideCallPrices)
                .setOutsideCallPrices(outsideCalPrices)
                .setLandlinesCallPrices(landLinesPrices).build();
    }

    private String getElementTextContent(Element element, String elementName) {
        NodeList nodeList = element.getElementsByTagName(elementName);
        Node node = nodeList.item(0);
        return node.getTextContent();
    }
}
