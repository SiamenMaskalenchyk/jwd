package com.maskalenchyk.xmlparsing.parser;

public interface EntityBuilder<T> {

    T build();
}
