package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.controller.ApplicationException;

import java.io.InputStream;
import java.util.List;

public interface FileParser<T> {

    List<T> parse(InputStream inputStream) throws ParseException, ApplicationException;
}
