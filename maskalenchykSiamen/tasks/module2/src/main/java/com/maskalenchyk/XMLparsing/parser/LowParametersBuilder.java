package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.entity.LowParameters;
import com.maskalenchyk.xmlparsing.entity.Parameters;

public class LowParametersBuilder extends ParametersBuilder<LowParametersBuilder> {

    public LowParametersBuilder(Parameters currentParameters) {
        super(currentParameters);
    }

    @Override
    protected LowParametersBuilder self() {
        return this;
    }

    @Override
    public Parameters build() {
        return this.currentParameters;
    }

    public LowParametersBuilder setHaveFavoriteNumber(boolean value) {
        ((LowParameters) this.currentParameters).setHaveFavoriteNumber(value);
        return self();
    }

    public LowParametersBuilder setRecallAvailable(boolean value) {
        ((LowParameters) this.currentParameters).setRecallAvailable(value);
        return self();
    }

}
