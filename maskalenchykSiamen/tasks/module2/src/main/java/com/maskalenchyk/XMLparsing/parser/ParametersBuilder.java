package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.entity.Parameters;
import com.maskalenchyk.xmlparsing.entity.Tariffication;

import java.math.BigDecimal;

public abstract class ParametersBuilder<B> implements EntityBuilder<Parameters> {

    protected Parameters currentParameters;

    public ParametersBuilder(Parameters currentParameters) {
        this.currentParameters = currentParameters;
    }

    public B setTariffication(Tariffication value) {
        currentParameters.setTariffication(value);
        return self();
    }

    public B setConnectionFee(BigDecimal value) {
        currentParameters.setConnectionFee(value);
        return self();
    }

    public abstract Parameters build();

    protected abstract B self();
}
