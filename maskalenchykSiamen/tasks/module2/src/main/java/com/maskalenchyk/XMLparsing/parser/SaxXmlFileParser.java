package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.application.ApplicationConstants;
import com.maskalenchyk.xmlparsing.application.ApplicationUtils;
import com.maskalenchyk.xmlparsing.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class SaxXmlFileParser extends XmlFileParser {

    @Override
    public List<Tariff> buildTariffList(InputStream inputStream) throws ParseException {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        TariffHandler tariffHandler = new SaxXmlFileParser.TariffHandler();
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            saxParser.parse(inputStream, tariffHandler);
            return tariffHandler.getTariffList();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new ParseException("Error parse in SAX parser - " + e.getMessage());
        }
    }

    @Override
    public String getIdentifier() {
        return "SAX";
    }

    private class TariffHandler extends DefaultHandler {
        private List<Tariff> handlerParsedTariffList;
        private TariffBuilder currentTariffBuilder;
        private TariffEnum currentTariffEnum;
        private CallPriceBuilder currentCallPricesBuilder;
        private ParametersBuilder currentParametersBuilder;
        private EnumSet<TariffEnum> textFields;

        TariffHandler() {
            handlerParsedTariffList = new ArrayList<>();
            textFields = EnumSet.range(TariffEnum.NAME, TariffEnum.ISADVERTISINGAVAILABLE);
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {
            switch (qName) {
                case "callPrices":
                    currentCallPricesBuilder = new CallPriceBuilder(new CallPrices());
                    break;
                case "workingTariff":
                    currentTariffBuilder = new WorkingTariffBuilder(new WorkingTariff());
                    setAttributeValues(attributes);
                    break;
                case "standardTariff":
                    currentTariffBuilder = new StandardTariffBuilder(new StandardTariff());
                    setAttributeValues(attributes);
                    break;
                case "lowParameters":
                    currentParametersBuilder = new LowParametersBuilder(new LowParameters());
                    break;
                case "wideParameters":
                    currentParametersBuilder = new WideParametersBuilder(new WideParameters());
                    break;
            }
            TariffEnum tariffEnumTemp = TariffEnum.valueOf(qName.toUpperCase());
            if (textFields.contains(tariffEnumTemp)) {
                currentTariffEnum = tariffEnumTemp;
            }
        }

        private void setAttributeValues(Attributes attributes) {
            currentTariffBuilder.setId(attributes.getValue(attributes.getIndex("id")));
            if (attributes.getLength() == 2) {
                int index = attributes.getIndex("description");
                currentTariffBuilder.setDescription(attributes.getValue(index));
            } else {
                currentTariffBuilder.setDescription("information");
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) {
            if (tariffTagNames.contains(qName)) {
                handlerParsedTariffList.add(currentTariffBuilder.build());
            }
            if (parametersTags.contains(qName)) {
                currentTariffBuilder.setParameters(currentParametersBuilder.build());
            }
            if ("callPrices".equals(qName)) {
                currentTariffBuilder.setCallPrices(currentCallPricesBuilder.build());
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String currentString = new String(ch, start, length).trim();
            if (currentTariffEnum != null) {
                switch (currentTariffEnum) {
                    case NAME:
                        currentTariffBuilder.setName(currentString);
                        break;
                    case DATECREATION:
                        currentTariffBuilder.setDateCreation(LocalDate.parse(currentString, ApplicationConstants.DATE_TIME_FORMATTER));
                        break;
                    case OPERATORNAME:
                        currentTariffBuilder.setOperatorName(OperatorName.valueOf(currentString.toUpperCase()));
                        break;
                    case PAYROLL:
                        currentTariffBuilder.setPayroll(new BigDecimal(Integer.parseInt(currentString)));
                        break;
                    case SMSPRICE:
                        currentTariffBuilder.setSMSPrice(new BigDecimal(Integer.parseInt(currentString)));
                        break;
                    case INSIDECALLPRICES:
                        BigDecimal insideCallPrices = new BigDecimal(Integer.parseInt(currentString));
                        currentCallPricesBuilder.setInsideCallPrices(insideCallPrices);
                        break;
                    case OUTSIDECALLPRICES:
                        BigDecimal outsideCallPrices = new BigDecimal(Integer.parseInt(currentString));
                        currentCallPricesBuilder.setOutsideCallPrices(outsideCallPrices);
                        break;
                    case LANDLINESCALLPRICES:
                        BigDecimal landlinesCallPrices = new BigDecimal(Integer.parseInt(currentString));
                        currentCallPricesBuilder.setLandlinesCallPrices(landlinesCallPrices);
                        break;
                    case TARIFFICATION:
                        currentParametersBuilder.setTariffication(Tariffication.valueOf(currentString.toUpperCase()));
                        break;
                    case CONNECTIONFEE:
                        BigDecimal connectionFee = new BigDecimal(Integer.parseInt(currentString));
                        currentParametersBuilder.setConnectionFee(connectionFee);
                        break;
                    case ISHAVEFAVORITENUMBER:
                        ((LowParametersBuilder) currentParametersBuilder)
                                .setHaveFavoriteNumber(ApplicationUtils.stringToBoolean(currentString));
                        break;
                    case ISRECALLAVAILABLE:
                        ((LowParametersBuilder) currentParametersBuilder)
                                .setRecallAvailable(ApplicationUtils.stringToBoolean(currentString));
                        break;
                    case INTERNETAMOUNTMB:
                        BigDecimal internetAmount = new BigDecimal(Integer.parseInt(currentString));
                        ((WideParametersBuilder) currentParametersBuilder).setInternetAmount(internetAmount);
                        break;
                    case ISNUMBERHIDDEN:
                        ((WideParametersBuilder) currentParametersBuilder)
                                .setNumberHidden(ApplicationUtils.stringToBoolean(currentString));
                        break;
                    case ISADVERTISINGAVAILABLE:
                        ((WideParametersBuilder) currentParametersBuilder)
                                .setAdvertisingAvailable(ApplicationUtils.stringToBoolean(currentString));
                        break;
                    case WORKINGCALLPRICES:
                        BigDecimal workingCallPrices = new BigDecimal(Integer.parseInt(currentString));
                        ((WorkingTariffBuilder) currentTariffBuilder).setWorkingCallPrices(workingCallPrices);
                        break;
                    case DISCOUNTAMOUNT:
                        BigDecimal discountAmount = new BigDecimal(Integer.parseInt(currentString));
                        ((StandardTariffBuilder) currentTariffBuilder).setDiscountAmount(discountAmount);
                        break;
                    default:
                        throw new SAXException("Initialize variable error in SAX parser");
                }
                currentTariffEnum = null;
            }
        }

        List<Tariff> getTariffList() {
            return handlerParsedTariffList;
        }
    }
}
