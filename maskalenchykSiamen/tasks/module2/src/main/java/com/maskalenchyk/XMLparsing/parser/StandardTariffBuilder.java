package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.entity.StandardTariff;
import com.maskalenchyk.xmlparsing.entity.Tariff;

import java.math.BigDecimal;

public class StandardTariffBuilder extends TariffBuilder<StandardTariffBuilder> {

    public StandardTariffBuilder(Tariff currentTariff) {
        super(currentTariff);
    }

    @Override
    public Tariff build() {
        return this.currentTariff;
    }

    public StandardTariffBuilder setDiscountAmount(BigDecimal value) {
        ((StandardTariff) this.currentTariff).setDiscountAmount(value);
        return self();
    }

    @Override
    protected StandardTariffBuilder self() {
        return this;
    }
}
