package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.application.ApplicationConstants;
import com.maskalenchyk.xmlparsing.application.ApplicationUtils;
import com.maskalenchyk.xmlparsing.entity.*;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class StaxXmlFileParser extends XmlFileParser {

    @Override
    public List<Tariff> buildTariffList(InputStream inputStream) throws ParseException {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        try {
            XMLStreamReader readerXML = inputFactory.createXMLStreamReader(inputStream);
            List<Tariff> parsedTariffList = new ArrayList<>();
            String name;
            while (readerXML.hasNext()) {
                int type = readerXML.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = readerXML.getLocalName();
                    if (tariffTagNames.contains(name)) {
                        Tariff tariff = buildTariff(readerXML);
                        parsedTariffList.add(tariff);
                    }
                }
            }
            return parsedTariffList;
        } catch (XMLStreamException e) {
            throw new ParseException("Parse failed in StAX parser \n" + "Cause: " + e.getMessage());
        }
    }

    private Tariff buildTariff(XMLStreamReader readerXML) throws ParseException, XMLStreamException {
        TariffBuilder tariffBuilder = (readerXML.getLocalName().equals("workingTariff")) ?
                new WorkingTariffBuilder(new WorkingTariff()) : new StandardTariffBuilder(new StandardTariff());
        tariffBuilder.setId(readerXML.getAttributeValue(null, TariffEnum.ID.getValue()));
        String description = readerXML.getAttributeValue(null, TariffEnum.DESCRIPTION.getValue());
        tariffBuilder.setDescription(description == null ? "information" : description);
        String tagName;
        while (readerXML.hasNext()) {
            int type = readerXML.next();
            if (type == XMLStreamConstants.START_ELEMENT) {
                tagName = readerXML.getLocalName();
                setVariableTariff(tariffBuilder, tagName, readerXML);
            } else if (type == XMLStreamConstants.END_ELEMENT) {
                tagName = readerXML.getLocalName();
                if (tariffTagNames.contains(tagName)) {
                    return tariffBuilder.build();
                }
            }
        }
        throw new ParseException("Unknown element in tag Tariff");
    }

    private void setVariableTariff(TariffBuilder tariffBuilder, String tagName,
                                   XMLStreamReader readerXML) throws XMLStreamException, ParseException {
        switch (TariffEnum.valueOf(tagName.toUpperCase())) {
            case NAME:
                tariffBuilder.setName(getXMLText(readerXML));
                break;
            case DATECREATION:
                LocalDate dateCreation = LocalDate.parse(getXMLText(readerXML), ApplicationConstants.DATE_TIME_FORMATTER);
                tariffBuilder.setDateCreation(dateCreation);
                break;
            case OPERATORNAME:
                tariffBuilder.setOperatorName(OperatorName.valueOf(getXMLText(readerXML).toUpperCase()));
                break;
            case PAYROLL:
                BigDecimal payroll = new BigDecimal(Integer.parseInt(getXMLText(readerXML)));
                tariffBuilder.setPayroll(payroll);
                break;
            case CALLPRICES:
                tariffBuilder.setCallPrices(getXMLCallPrices(readerXML));
                break;
            case SMSPRICE:
                BigDecimal smsPrice = new BigDecimal(Integer.parseInt(getXMLText(readerXML)));
                tariffBuilder.setSMSPrice(smsPrice);
                break;
            case LOWPARAMETERS:
                tariffBuilder.setParameters(getParameters(new LowParametersBuilder(new LowParameters()), readerXML));
                break;
            case WIDEPARAMETERS:
                tariffBuilder.setParameters(getParameters(new WideParametersBuilder(new WideParameters()), readerXML));
                break;
            case WORKINGCALLPRICES:
                BigDecimal workingTariffPrices = new BigDecimal(Integer.parseInt(getXMLText(readerXML)));
                ((WorkingTariffBuilder) tariffBuilder).setWorkingCallPrices(workingTariffPrices);
                break;
            case DISCOUNTAMOUNT:
                BigDecimal discountAmount = new BigDecimal(Integer.parseInt(getXMLText(readerXML)));
                ((StandardTariffBuilder) tariffBuilder).setDiscountAmount(discountAmount);
                break;
        }
    }

    private Parameters getParameters(ParametersBuilder builder, XMLStreamReader readerXML)
            throws XMLStreamException, ParseException {
        int type;
        String tagName;
        while (readerXML.hasNext()) {
            type = readerXML.next();
            if (type == XMLStreamConstants.START_ELEMENT) {
                tagName = readerXML.getLocalName();
                setVariableParameters(builder, tagName, readerXML);
            } else if (type == XMLStreamConstants.END_ELEMENT) {
                tagName = readerXML.getLocalName();
                if (parametersTags.contains(tagName)) {
                    return builder.build();
                }
            }
        }
        throw new ParseException("Unknown element in tag Parameters");
    }

    private void setVariableParameters(ParametersBuilder parametersBuilder, String tagName, XMLStreamReader readerXML)
            throws XMLStreamException {
        switch (TariffEnum.valueOf(tagName.toUpperCase())) {
            case TARIFFICATION:
                parametersBuilder.setTariffication(Tariffication.valueOf(getXMLText(readerXML).toUpperCase()));
                break;
            case CONNECTIONFEE:
                BigDecimal connectionFee = new BigDecimal(Integer.parseInt(getXMLText(readerXML)));
                parametersBuilder.setConnectionFee(connectionFee);
                break;
            case ISHAVEFAVORITENUMBER:
                ((LowParametersBuilder) parametersBuilder)
                        .setHaveFavoriteNumber(ApplicationUtils.stringToBoolean(getXMLText(readerXML)));
                break;
            case ISRECALLAVAILABLE:
                ((LowParametersBuilder) parametersBuilder)
                        .setRecallAvailable(ApplicationUtils.stringToBoolean(getXMLText(readerXML)));
                break;
            case INTERNETAMOUNTMB:
                BigDecimal internetAmount = new BigDecimal(Integer.parseInt(getXMLText(readerXML)));
                ((WideParametersBuilder) parametersBuilder).setInternetAmount(internetAmount);
                break;
            case ISNUMBERHIDDEN:
                ((WideParametersBuilder) parametersBuilder)
                        .setNumberHidden(ApplicationUtils.stringToBoolean(getXMLText(readerXML)));
                break;
            case ISADVERTISINGAVAILABLE:
                ((WideParametersBuilder) parametersBuilder)
                        .setNumberHidden(ApplicationUtils.stringToBoolean(getXMLText(readerXML)));
                break;
        }
    }

    private CallPrices getXMLCallPrices(XMLStreamReader readerXML) throws XMLStreamException, ParseException {
        CallPriceBuilder callPricesBuilder = new CallPriceBuilder(new CallPrices());
        int type;
        String tagName;
        while (readerXML.hasNext()) {
            type = readerXML.next();
            if (type == XMLStreamConstants.START_ELEMENT) {
                tagName = readerXML.getLocalName();
                setVariableCallPrices(callPricesBuilder, tagName, readerXML);
            } else if (type == XMLStreamConstants.END_ELEMENT) {
                tagName = readerXML.getLocalName();
                if (TariffEnum.valueOf(tagName.toUpperCase()) == TariffEnum.CALLPRICES) {
                    return callPricesBuilder.build();
                }
            }
        }
        throw new ParseException("Unknown element in tag CallPrices");
    }

    private void setVariableCallPrices(CallPriceBuilder callPricesBuilder, String tagName, XMLStreamReader readerXML)
            throws XMLStreamException {
        switch (TariffEnum.valueOf(tagName.toUpperCase())) {
            case INSIDECALLPRICES:
                BigDecimal insideCallPrices = new BigDecimal(Integer.parseInt(getXMLText(readerXML)));
                callPricesBuilder.setInsideCallPrices(insideCallPrices);
                break;
            case OUTSIDECALLPRICES:
                BigDecimal outsideCallPrices = new BigDecimal(Integer.parseInt(getXMLText(readerXML)));
                callPricesBuilder.setOutsideCallPrices(outsideCallPrices);
                break;
            case LANDLINESCALLPRICES:
                BigDecimal landlinesCallPrices = new BigDecimal(Integer.parseInt(getXMLText(readerXML)));
                callPricesBuilder.setLandlinesCallPrices(landlinesCallPrices);
                break;
        }
    }

    private String getXMLText(XMLStreamReader readerXML) throws XMLStreamException {
        String text = "";
        if (readerXML.hasNext()) {
            readerXML.next();
            text = readerXML.getText();
        }
        return text;
    }

    @Override
    public String getIdentifier() {
        return "STAX";
    }

}
