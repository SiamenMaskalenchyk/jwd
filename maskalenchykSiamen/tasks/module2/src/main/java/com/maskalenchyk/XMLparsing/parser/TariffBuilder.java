package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.entity.CallPrices;
import com.maskalenchyk.xmlparsing.entity.OperatorName;
import com.maskalenchyk.xmlparsing.entity.Parameters;
import com.maskalenchyk.xmlparsing.entity.Tariff;

import java.math.BigDecimal;
import java.time.LocalDate;

public abstract class TariffBuilder<B> implements EntityBuilder<Tariff> {

    protected Tariff currentTariff;

    public TariffBuilder(Tariff currentTariff) {
        this.currentTariff = currentTariff;
    }

    public B setName(String value) {
        currentTariff.setName(value);
        return self();
    }

    public B setDateCreation(LocalDate value) {
        currentTariff.setDateCreation(value);
        return self();
    }

    public B setOperatorName(OperatorName value) {
        currentTariff.setOperatorName(value);
        return self();
    }

    public B setPayroll(BigDecimal value) {
        currentTariff.setPayroll(value);
        return self();
    }

    public B setCallPrices(CallPrices value) {
        currentTariff.setCallPrices(value);
        return self();
    }

    public B setSMSPrice(BigDecimal value) {
        currentTariff.setSmsPrice(value);
        return self();
    }

    public B setParameters(Parameters value) {
        currentTariff.setParameters(value);
        return self();
    }

    public B setId(String value) {
        currentTariff.setId(value);
        return self();
    }

    public B setDescription(String value) {
        currentTariff.setDescription(value);
        return self();

    }

    public abstract Tariff build();

    protected abstract B self();
}