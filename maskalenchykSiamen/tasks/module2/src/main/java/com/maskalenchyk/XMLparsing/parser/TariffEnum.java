package com.maskalenchyk.xmlparsing.parser;

public enum TariffEnum {
    TARIFFS("tariffs"),
    ID("id"),
    DESCRIPTION("description"),
    NAME("name"),
    DATECREATION("dateCreation"),
    OPERATORNAME("operatorName"),
    PAYROLL("payroll"),
    SMSPRICE("SMSPrice"),
    TARIFFICATION("tariffication"),
    CONNECTIONFEE("connectionFee"),
    ISHAVEFAVORITENUMBER("isHaveFavoriteNumber"),
    ISRECALLAVAILABLE("isRecallAvailable"),
    WORKINGCALLPRICES("workingCallPrices"),
    INSIDECALLPRICES("insideCallPrices"),
    OUTSIDECALLPRICES("outsideCallPrices"),
    LANDLINESCALLPRICES("landlinesCallPrices"),
    DISCOUNTAMOUNT("discountAmount"),
    INTERNETAMOUNTMB("internetAmountMB"),
    ISNUMBERHIDDEN("isNumberHidden"),
    ISADVERTISINGAVAILABLE("isAdvertisingAvailable"),
    STANDARDTARIFF("standardTariff"),
    WORKINGTARIFF("workingTariff"),
    CALLPRICES("callPrices"),
    WIDEPARAMETERS("wideParameters"),
    LOWPARAMETERS("lowParameters");

    private String value;

    TariffEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
