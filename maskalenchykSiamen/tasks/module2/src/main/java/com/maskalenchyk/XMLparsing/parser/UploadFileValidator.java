package com.maskalenchyk.xmlparsing.parser;

import javax.servlet.http.Part;

public interface UploadFileValidator {

    String validateFile(Part file);
}
