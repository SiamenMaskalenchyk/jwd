package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.application.ApplicationConstants;
import com.maskalenchyk.xmlparsing.controller.ApplicationException;
import org.xml.sax.SAXException;

import javax.servlet.http.Part;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class UploadXmlFileValidatorImpl implements UploadFileValidator {

    @Override
    public String validateFile(Part file) {
        if (file == null || file.getSize() <= 0L) {
            return "File doesn't exist or empty";
        }
        if (!file.getSubmittedFileName().endsWith(".xml")) {
            return "Invalid file extension";
        }
        return validateFileByXsd(file);
    }

    private String validateFileByXsd(Part file) {
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory schemaFactory = SchemaFactory.newInstance(language);
        try {
            URL resource = getClass().getClassLoader().getResource(ApplicationConstants.XSD_SCHEMA_PATH);
            File schemaFile;
            if (resource != null) {
                schemaFile = new File(resource.toURI());
            } else {
                throw new ApplicationException("Xml schema " + ApplicationConstants.XSD_SCHEMA_PATH + " didn't find ");
            }
            Schema schema = schemaFactory.newSchema(schemaFile);
            Validator xmlValidator = schema.newValidator();
            Source source = new StreamSource(file.getInputStream());
            xmlValidator.validate(source);
            return "valid";
        } catch (SAXException | IOException | URISyntaxException | ApplicationException e) {
            return "Uploaded file is not valid because " + e.getMessage();
        }
    }
}