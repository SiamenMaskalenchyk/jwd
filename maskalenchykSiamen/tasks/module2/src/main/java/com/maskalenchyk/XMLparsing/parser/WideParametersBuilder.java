package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.entity.Parameters;
import com.maskalenchyk.xmlparsing.entity.WideParameters;

import java.math.BigDecimal;

public class WideParametersBuilder extends ParametersBuilder<WideParametersBuilder> {


    public WideParametersBuilder(Parameters currentParameters) {
        super(currentParameters);
    }

    @Override
    protected WideParametersBuilder self() {
        return this;
    }

    @Override
    public Parameters build() {
        return this.currentParameters;
    }

    public WideParametersBuilder setInternetAmount(BigDecimal value) {
        ((WideParameters) currentParameters).setInternetAmountMB(value);
        return self();
    }

    public WideParametersBuilder setAdvertisingAvailable(boolean value) {
        ((WideParameters) currentParameters).setAdvertisingAvailable(value);
        return self();
    }

    public WideParametersBuilder setNumberHidden(boolean value) {
        ((WideParameters) currentParameters).setNumberHidden(value);
        return self();
    }
}
