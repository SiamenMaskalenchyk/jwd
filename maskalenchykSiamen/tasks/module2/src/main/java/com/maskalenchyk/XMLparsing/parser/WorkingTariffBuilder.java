package com.maskalenchyk.xmlparsing.parser;


import com.maskalenchyk.xmlparsing.entity.Tariff;
import com.maskalenchyk.xmlparsing.entity.WorkingTariff;

import java.math.BigDecimal;

public class WorkingTariffBuilder extends TariffBuilder<WorkingTariffBuilder> {

    public WorkingTariffBuilder(Tariff currentTariff) {
        super(currentTariff);
    }

    @Override
    public Tariff build() {
        return this.currentTariff;
    }

    public WorkingTariffBuilder setWorkingCallPrices(BigDecimal value) {
        ((WorkingTariff) this.currentTariff).setWorkingCallPrices(value);
        return self();
    }

    @Override
    protected WorkingTariffBuilder self() {
        return this;
    }
}
