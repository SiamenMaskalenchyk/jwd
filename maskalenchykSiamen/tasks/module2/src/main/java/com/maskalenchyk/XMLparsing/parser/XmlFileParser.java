package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.controller.ApplicationException;
import com.maskalenchyk.xmlparsing.entity.Tariff;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class XmlFileParser implements FileParser<Tariff> {

    private final Logger LOGGER = LogManager.getLogger(XmlFileParser.class);
    protected Set<String> tariffTagNames;
    protected Set<String> parametersTags;

    public XmlFileParser() {
        this.tariffTagNames = new HashSet<>(Arrays.asList("workingTariff", "standardTariff"));
        this.parametersTags = new HashSet<>(Arrays.asList("wideParameters", "lowParameters"));
    }

    @Override
    public List<Tariff> parse(InputStream inputStream) throws ApplicationException {
        try {
            return buildTariffList(inputStream);
        } catch (ParseException e) {
            LOGGER.error("Parse exception " + e.getMessage(), e);
            throw new ApplicationException("Parse error", e);
        }
    }

    protected abstract List<Tariff> buildTariffList(InputStream inputStream) throws ParseException;

    protected abstract String getIdentifier();
}
