package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.controller.ApplicationException;

public interface XmlFileParserProvider {

    XmlFileParser getTariffBuilder(String type) throws ApplicationException;

}
