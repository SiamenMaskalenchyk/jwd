package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.controller.ApplicationException;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class XmlFileParserProviderImpl implements XmlFileParserProvider {

    private Map<String, XmlFileParser> xmlFileParserMap;

    public XmlFileParserProviderImpl(XmlFileParser... xmlFileParsers) {
        init(xmlFileParsers);
    }

    @Override
    public XmlFileParser getTariffBuilder(String type) throws ApplicationException {
        return Optional.ofNullable(xmlFileParserMap.get(type.toUpperCase()))
                .orElseThrow(() -> new ApplicationException(("Unknown parser type " + type)));
    }

    private void init(XmlFileParser[] xmlFileParsers) {
        this.xmlFileParserMap = new ConcurrentHashMap<>();
        for (XmlFileParser xmlFileParser : xmlFileParsers) {
            this.xmlFileParserMap.put(xmlFileParser.getIdentifier(), xmlFileParser);
        }
    }
}
