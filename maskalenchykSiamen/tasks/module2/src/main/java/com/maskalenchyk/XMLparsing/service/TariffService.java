package com.maskalenchyk.xmlparsing.service;

import com.maskalenchyk.xmlparsing.entity.Tariff;

import java.util.List;

public interface TariffService {

    void save(Tariff tariff);

    List<Tariff> getAll();
}
