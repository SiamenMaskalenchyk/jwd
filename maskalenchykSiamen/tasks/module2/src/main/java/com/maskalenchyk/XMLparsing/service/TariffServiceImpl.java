package com.maskalenchyk.xmlparsing.service;

import com.maskalenchyk.xmlparsing.dal.TariffDao;
import com.maskalenchyk.xmlparsing.entity.Tariff;

import java.util.List;

public class TariffServiceImpl implements TariffService {

    private TariffDao tariffDao;

    public TariffServiceImpl(TariffDao tariffDao) {
        this.tariffDao = tariffDao;
    }

    @Override
    public void save(Tariff tariff) {
        this.tariffDao.save(tariff);
    }

    @Override
    public List<Tariff> getAll() {
        return this.tariffDao.findAll();
    }
}
