<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="com.maskalenchyk.xmlparsing.controller.AppCommandName" %>

<html>
<head>
    <title>Tariffs application</title>
</head>
<body>
<fmt:setLocale value="en"/>
<fmt:setBundle basename="com.tutorialspoint.Example" var="lang"/>
<div class="main">
    <br><br>
    Tariffs loader from xml files
    <br><br>
    <form action="home-page" method="post" enctype="multipart/form-data">
        <label> Choose parser</label>
        <br>
        <select name="parserType" required>
            <option value="DOM">DOM</option>
            <option value="SAX">SAX</option>
            <option value="StAX">StAX</option>
        </select>
        <br><br>
        <label>Please choose XML file for uploading</label><br>
        <input type="file" name="xmlFile" value="" required>
        <input type="hidden" name="command" value="${AppCommandName.UPLOAD_TARIFFS.name()}">
        <input type="submit" value="Send">
    </form>
    <br>
    <form action="home-page" method="get">
        <input type="hidden" name="command" value="${AppCommandName.DISPLAY_TARIFFS.name()}">
        <input type="submit" value="Show tariffs data base">
    </form>
    <br>
    <form action="home-page" method="get">
        <input type="hidden" name="command" value="${AppCommandName.DOWNLOAD}">
        <input type="submit" value="Download data base">
    </form>
</div>
</body>
</html>
