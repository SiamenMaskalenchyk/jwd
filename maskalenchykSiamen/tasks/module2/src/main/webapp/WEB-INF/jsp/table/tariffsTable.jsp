<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<table class="table-view">
    <thead>
    <th><abbr title="ID">ID</abbr></th>
    <th><abbr title="Date Creation">Date creation></abbr></th>
    <th><abbr title="Operator name"> Operator name </abbr></th>
    <th><abbr title="Payroll"> Payroll</abbr></th>
    <th><abbr title="SMS price"> SMS price</abbr></th>
    <th><abbr title="Inside call price"> Inside call price</abbr></th>
    <th><abbr title="Outside call price"> Outside call price</abbr></th>
    <th><abbr title="Tariffication"> Tariffication</abbr></th>
    <th><abbr title="Connection fee"> Connection fee</abbr></th>
    </thead>
    <tbody>
    <c:forEach items="${tariffs}" var="tariff">
        <tr>
            <td>${tariff.id}</td>
            <td>${tariff.dateCreation}</td>
            <td>${tariff.operatorName}</td>
            <td>${tariff.payroll}</td>
            <td>${tariff.SMSPrice}</td>
            <td>${tariff.getCallPrices().insideCallPrices}</td>
            <td>${tariff.getCallPrices().outsideCallPrices}</td>
            <td>${tariff.getParameters().tariffication}</td>
            <td>${tariff.getParameters().connectionFee}</td>
        </tr>
        ${tariff=null}
    </c:forEach>
    </tbody>
</table>
</body>
</html>
