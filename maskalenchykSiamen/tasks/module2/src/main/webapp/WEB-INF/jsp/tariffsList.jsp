<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="com.maskalenchyk.xmlparsing.controller.AppCommandName" %>
<html>
<c:if test="${not empty tariffs}">
    <div>
        Tariffs
    </div>
    <jsp:include page="/WEB-INF/jsp/table/tariffsTable.jsp"/>
</c:if>
<c:if test="${empty tariffs}">
    Tariff data base is empty
</c:if>
<br><br>
<form action="home-page" method="get">
    <input type="hidden" name="command" value="${AppCommandName.DEFAULT}" }>
    <input type="submit" value="Home page">
</form>
</body>
</html>
