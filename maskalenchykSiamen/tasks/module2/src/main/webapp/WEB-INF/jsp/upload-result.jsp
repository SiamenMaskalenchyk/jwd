<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="com.maskalenchyk.xmlparsing.controller.AppCommandName" %>
<html>
<head>
    <title>Upload result</title>
</head>
<body>
<br><br>
<c:if test="${validateResult != 'valid'}">
    <c:out value="${validateResult}"/>
</c:if>
<c:if test="${not empty tariffs}">
    <jsp:include page="/WEB-INF/jsp/table/tariffsTable.jsp"/>
</c:if>
<br><br>
<form action="home-page">
    <input type="hidden" name="command" value="${AppCommandName.DEFAULT.name()}">
    <input type="submit" value="Home">
</form>
</body>
</html>
