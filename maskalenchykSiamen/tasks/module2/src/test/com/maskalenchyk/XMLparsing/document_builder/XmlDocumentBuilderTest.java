package com.maskalenchyk.xmlparsing.document_builder;

import com.maskalenchyk.xmlparsing.application.ApplicationConstants;
import com.maskalenchyk.xmlparsing.entity.*;
import com.maskalenchyk.xmlparsing.parser.CallPriceBuilder;
import com.maskalenchyk.xmlparsing.parser.LowParametersBuilder;
import com.maskalenchyk.xmlparsing.parser.WorkingTariffBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class XmlDocumentBuilderTest {

    private List<Tariff> tariffs = new ArrayList<>();
    private AppDocumentBuilder documentBuilder = new XmlDocumentBuilder();

    @Before
    public void setUp() {
        CallPrices callPrices = new CallPriceBuilder(new CallPrices())
                .setInsideCallPrices(new BigDecimal(1))
                .setOutsideCallPrices(new BigDecimal(2))
                .setLandlinesCallPrices(new BigDecimal(3))
                .build();
        Parameters parameters = new LowParametersBuilder(new LowParameters())
                .setConnectionFee(new BigDecimal(5))
                .setTariffication(Tariffication.SECONDS)
                .setHaveFavoriteNumber(false)
                .setRecallAvailable(false)
                .build();
        Tariff tariff = new WorkingTariffBuilder(new WorkingTariff())
                .setName("first")
                .setDateCreation(LocalDate.parse("2002-09-24", ApplicationConstants.DATE_TIME_FORMATTER))
                .setOperatorName(OperatorName.A1)
                .setPayroll(new BigDecimal(1))
                .setCallPrices(callPrices)
                .setSMSPrice(new BigDecimal(1))
                .setParameters(parameters)
                .setId("t1")
                .setDescription("information")
                .setWorkingCallPrices(new BigDecimal(5))
                .build();
        tariffs.add(tariff);
    }

    @Test
    public void buildDocument() {
        String resultFromBuilder = null;
        String resultFromFile = null;
        try (InputStream inputStreamFromBuilder = documentBuilder.buildDocument(tariffs);) {

            resultFromBuilder = new BufferedReader(new InputStreamReader(inputStreamFromBuilder)).lines()
                    .parallel().collect(Collectors.joining("\n"));
        } catch (DocumentBuilderException | IOException e) {
            e.printStackTrace();
        }
        File xmlFile = new File("src/test/resourses/TariffsForTest.xml");
        try (InputStream inputStream = new FileInputStream(xmlFile)) {
            resultFromFile = new BufferedReader(new InputStreamReader(inputStream)).lines().parallel().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertNotNull(resultFromBuilder);
        Assert.assertNotNull(resultFromFile);
        Assert.assertEquals(resultFromBuilder, resultFromFile);

    }
}