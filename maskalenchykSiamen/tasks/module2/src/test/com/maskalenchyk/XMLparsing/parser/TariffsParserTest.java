package com.maskalenchyk.xmlparsing.parser;

import com.maskalenchyk.xmlparsing.application.ApplicationConstants;
import com.maskalenchyk.xmlparsing.controller.ApplicationException;
import com.maskalenchyk.xmlparsing.entity.*;
import com.maskalenchyk.xmlparsing.parser.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TariffsParserTest {

    private List<Tariff> tariffList;
    private List<Tariff> result;
    private XmlFileParser xmlFileParser;
    private File file;

    @Before
    public void setUp() {
        CallPrices callPrices = new CallPriceBuilder(new CallPrices())
                .setInsideCallPrices(new BigDecimal(1))
                .setOutsideCallPrices(new BigDecimal(2))
                .setLandlinesCallPrices(new BigDecimal(3))
                .build();
        Parameters parameters = new LowParametersBuilder(new LowParameters())
                .setConnectionFee(new BigDecimal(5))
                .setTariffication(Tariffication.SECONDS)
                .setHaveFavoriteNumber(false)
                .setRecallAvailable(false)
                .build();
        Tariff tariff = new WorkingTariffBuilder(new WorkingTariff())
                .setName("first")
                .setDateCreation(LocalDate.parse("2002-09-24", ApplicationConstants.DATE_TIME_FORMATTER))
                .setOperatorName(OperatorName.A1)
                .setPayroll(new BigDecimal(1))
                .setCallPrices(callPrices)
                .setSMSPrice(new BigDecimal(1))
                .setParameters(parameters)
                .setId("t1")
                .setDescription("information")
                .setWorkingCallPrices(new BigDecimal(5))
                .build();
        tariffList = new ArrayList<>();
        tariffList.add(tariff);
        file = new File("tariffsTest.xml");
    }

    @Test
    public void buildDOMTariffList() {

        result = new ArrayList<>();
        xmlFileParser = new DomXmlFileParser();

        try (InputStream inputStream = new FileInputStream(file)) {
            result = xmlFileParser.parse(inputStream);
        } catch (IOException | ApplicationException e) {
           e.printStackTrace();
        }
        Assert.assertEquals(result, tariffList);
    }

    @Test
    public void buildStAXTariffList() {
        result = new ArrayList<>();
        xmlFileParser = new StaxXmlFileParser();
        try (InputStream inputStream = new FileInputStream(file)) {
            result = xmlFileParser.parse(inputStream);
        } catch (IOException | ApplicationException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(result, tariffList);
    }

    @Test
    public void buildSAXTariffList() {
        result = new ArrayList<>();
        xmlFileParser = new SaxXmlFileParser();
        try (InputStream inputStream = new FileInputStream(file)) {
            result = xmlFileParser.parse(inputStream);
        } catch (IOException | ApplicationException e) {
           e.printStackTrace();
        }
        Assert.assertEquals(result, tariffList);
    }
}