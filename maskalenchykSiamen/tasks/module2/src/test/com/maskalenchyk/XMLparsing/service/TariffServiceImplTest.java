package com.maskalenchyk.xmlparsing.service;

import com.maskalenchyk.xmlparsing.application.ApplicationConstants;
import com.maskalenchyk.xmlparsing.dal.Tariffs;
import com.maskalenchyk.xmlparsing.entity.*;
import com.maskalenchyk.xmlparsing.parser.CallPriceBuilder;
import com.maskalenchyk.xmlparsing.parser.LowParametersBuilder;
import com.maskalenchyk.xmlparsing.parser.WorkingTariffBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TariffServiceImplTest {

    private TariffService tariffService;


    @Before
    public void setUp() {
        Tariffs tariffs = new Tariffs();
        tariffService = new TariffServiceImpl(tariffs);
    }

    @Test
    public void saveShouldEqual() {
        List<Tariff> tariffList = new ArrayList<>();
        tariffList.add(getTariff());
        tariffService.save(getTariff());
        Assert.assertEquals(tariffList, tariffService.getAll());
    }

    @Test
    public void saveShouldNotEqual() {
        Tariff tariff = getTariff();

        tariffService.save(getTariff());
        tariff.setSmsPrice(new BigDecimal(2335235));
        List<Tariff> tariffsList = new ArrayList<>();
        tariffsList.add(tariff);
        Assert.assertNotEquals(tariffsList, tariffService.getAll());
    }

    private Tariff getTariff() {
        CallPrices callPrices = new CallPriceBuilder(new CallPrices())
                .setInsideCallPrices(new BigDecimal(1))
                .setOutsideCallPrices(new BigDecimal(2))
                .setLandlinesCallPrices(new BigDecimal(3))
                .build();
        Parameters parameters = new LowParametersBuilder(new LowParameters())
                .setConnectionFee(new BigDecimal(5))
                .setTariffication(Tariffication.SECONDS)
                .setHaveFavoriteNumber(false)
                .setRecallAvailable(false)
                .build();
        return new WorkingTariffBuilder(new WorkingTariff())
                .setName("first")
                .setDateCreation(LocalDate.parse("2002-09-24", ApplicationConstants.DATE_TIME_FORMATTER))
                .setOperatorName(OperatorName.A1)
                .setPayroll(new BigDecimal(1))
                .setCallPrices(callPrices)
                .setSMSPrice(new BigDecimal(1))
                .setParameters(parameters)
                .setId("t1")
                .setDescription("information")
                .setWorkingCallPrices(new BigDecimal(5))
                .build();
    }
}